import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Import Containers
import {DefaultLayoutComponent} from './containers';

import {P404Component} from './views/error/404.component';
import {P500Component} from './views/error/500.component';
import {RegisterComponent} from './views/register/register.component';
import {LoginPageComponent} from '@app/views/login/login-page.component';
import {path} from "@amcharts/amcharts4/core";

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginPageComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'user',
        loadChildren: () => import('./views/user/user.module').then(m => m.UserModule)
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./views/dashboard/dashboard.module').then(m => m.DashboardModule)
      },
      {
        path: 'network',
        loadChildren: () => import('./views/network/network.module').then(m => m.NetworkModule)
      },
      {
        path: 'object',
        loadChildren: () => import('./views/object/object.module').then(m => m.ObjectModule)
      },
      {
        path: 'ip',
        loadChildren: () => import('./views/ip/ip.module').then(m => m.IpModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('./views/profile/profile.module').then(m => m.BwafProfileModule)
      },
      {
        path: 'about',
        loadChildren: () => import('./views/about/about.module').then(m => m.AboutModule)
      },
      {
        path: 'monitor',
        loadChildren: () => import('./views/monitor/monitor.module').then(m => m.MonitorModule)
      },
      {
        path: 'high-availability',
        loadChildren: () => import('./views/high-availability/high-availability.module').then(m => m.HighAvailabilityModule)
      },
      {
        path: 'web-vulnerability-scan',
        loadChildren: () => import('./views/web-vulnerability-scan/web-vulnerability-scan.module').then(m => m.WebVulnerabilityScanModule)
      },
      {
        path: 'ddos',
        loadChildren: () => import('./views/ddos/ddos.module').then(m => m.DdosModule)
      },
      {
        path: 'maintaince',
        loadChildren: () => import('./views/maintaince/maintaince.module').then(m => m.MaintainceModule)
      },
      {
        path: 'report',
        loadChildren: () => import('./views/report/report.module').then(m => m.BwafReportModule)
      },
      {
        path: 'diagnostic',
        loadChildren: () => import('./views/diagnostic/diagnostic.module').then(m => m.DiagnosticModule)
      },
      {
        path: 'waf',
        loadChildren: () => import('./views/waf/waf.module').then(m => m.WafModule)
      },
      {
        path: 'log-forward',
        loadChildren: () => import('./views/log-forward/log-forward.module').then(m => m.LogForwardModule)
      },
    ]
  },
  {path: '**', component: P404Component}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
