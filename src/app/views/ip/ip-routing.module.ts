import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {IpComponent} from './ip.component';
import {Paging} from "@app/shared/model/base-respone.model";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Ip Address'
    },
    children: [
      {
        path: '',
        component: IpComponent,
        data: {
          title: '',
          // pagingParams: new Paging()
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IpRoutingModule {}
