import {NgModule} from '@angular/core';
import {IpComponent} from '@app/views/ip/ip.component';
import {IpRoutingModule} from '@app/views/ip/ip-routing.module';
import {IpBlacklistComponent} from '@app/views/ip/ip-blacklist/ip-blacklist.component';
import {IpWhitelistComponent} from '@app/views/ip/ip-whitelist/ip-whitelist.component';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@app/shared';
import {ReactiveFormsModule} from '@angular/forms';
import {CollapseModule} from 'ngx-bootstrap';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";

@NgModule({
    imports: [IpRoutingModule, CommonModule, SharedModule, ReactiveFormsModule, CollapseModule, FontAwesomeModule],
  declarations: [
    IpComponent,
    IpBlacklistComponent,
    IpWhitelistComponent
  ]
})
export class IpModule { }
