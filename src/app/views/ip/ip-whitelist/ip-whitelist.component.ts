import {Component, OnInit} from '@angular/core';
import {IIp, Ip} from '@app/shared/model/ip.model';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {IPaging, Paging} from '@app/shared/model/base-respone.model';
import {ActivatedRoute} from '@angular/router';
import {getParameterPaging, OrderParam, PAGING_PER_PAGE} from '@app/shared/constants/common.model';
import {IpWhitelistService} from '@app/views/ip/ip-whitelist/ip-whitelist.service';

@Component({
  selector: 'app-ip-whitelist',
  templateUrl: 'ip-whitelist.component.html'
})
export class IpWhitelistComponent implements OnInit {
  isEditing = false;
  paging: IPaging = new Paging();
  whitelists: IIp[];
  blacklistFormGroup: FormGroup;
  selectedIp: IIp;
  pagingNumbers = PAGING_PER_PAGE;
  keywordSearch: string;
  order: OrderParam = new OrderParam(null, false);

  constructor(private fb: FormBuilder,
              private ipWhitelistService: IpWhitelistService,
              private modalService: NgbModal,
              private notificationService: ToastrCustomService,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.initTable();
  }

  initTable() {
    this.blacklistFormGroup = this.fb.group({
      data: this.fb.array([])
    });
    this.loadAll();
  }

  loadAll() {
    this.paging =  getParameterPaging(this.paging);
    this.isEditing = false;
    this.ipWhitelistService.query({
      offset: this.paging.offset,
      limit: this.paging.limit,
        orderType: this.order.getOrderType(),
        search: (this.keywordSearch && this.keywordSearch.trim().length > 0) ? this.keywordSearch.trim() : ''
      }
    ).subscribe(res => {
      this.blacklistFormGroup = this.fb.group({
        data: this.fb.array([])
      });

      this.whitelists = res.body.data;
      const control = <FormArray>this.blacklistFormGroup.get('data');
      for (const ipItem of this.whitelists) {
        const grp = this.fb.group({
          id: [ipItem.id],
          ip_address: [ipItem.ip_address, Validators.required],
          netmask: [ipItem.netmask, [Validators.required]],
          description: [ipItem.description, [Validators.required]],
          is_update: [false]
        });
        control.push(grp);
      }
      this.paging.total = res.body.paging.total;
    });
  }

  add() {
    this.isEditing = true;
    const control = <FormArray>this.blacklistFormGroup.get('data');
    control.push(this.initForm());
  }

  get getFormData(): FormArray {
    return <FormArray>this.blacklistFormGroup.get('data');
  }

  initForm() {
    return this.fb.group({
      ip_address: ['', Validators.required],
      netmask: ['', [Validators.required]],
      description:  ['', [Validators.maxLength(100)]],
      is_update: [true]
    });
  }

  onEdit(index: number) {
    this.isEditing = true;
    const control = <FormArray>this.blacklistFormGroup.get('data');
    this.selectedIp = control.at(index).value;
    control.at(index).get('is_update').setValue(true);
  }

  onCancelEdit(index: number) {
    const control = <FormArray>this.blacklistFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    // reload
    this.initTable();
    this.isEditing = false;
  }

  save(index: number) {
    this.isEditing = true;
    const control = <FormArray>this.blacklistFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    const value: Ip = control.at(index).value;
    if (value.id) {
      this.ipWhitelistService.update(value).subscribe(res => {
        this.loadAll();
      });
    } else {
      this.ipWhitelistService.create(value).subscribe(res => {
        this.loadAll();
      });
    }
  }

  remove(modal, index: number) {
    const control = <FormArray>this.blacklistFormGroup.get('data');
    this.selectedIp = control.at(index).value;
    this.modalService.open(modal, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static'
    }).result.then((result) => {
      if (result === 'delete') {
        if (this.selectedIp.id) {
          this.ipWhitelistService.delete(this.selectedIp.id).subscribe(res => {
            if ((this.paging.offset + this.getFormData.controls.length) - this.paging.offset === 1 ) {
              this.paging.page = this.paging.page - 1;
              this.loadAll();
            } else {
              this.loadAll();
            }
          });
        } else {
          // TODO: translate this
          this.notificationService.error('Thao tác không hợp kệ');
        }
      }
    }, (reason) => {
    });
  }

  onSearch() {
    this.paging.page = 1;
    if (this.keywordSearch) {
      this.keywordSearch = this.keywordSearch.trim();
    }
    this.loadAll();
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }
}
