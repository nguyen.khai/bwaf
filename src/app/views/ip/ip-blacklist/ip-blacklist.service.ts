import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from 'environments/environment';
import {createRequestOption} from 'app/shared';
import {IPaging} from '@app/shared/model/base-respone.model';
import {IIp} from '@app/shared/model/ip.model';

type EntityResponseType = HttpResponse<IIp>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;

interface PagingResponse {
  data: IIp[];
  paging: IPaging;
}

@Injectable({providedIn: 'root'})
export class IpBlacklistService {
  public resourceUrl = environment.apiUrl + '/blacklist';

  constructor(protected http: HttpClient) {
  }

  create(ip: IIp): Observable<EntityResponseType> {
    return this.http
      .post<IIp>(this.resourceUrl, ip, {observe: 'response'});
  }

  update(ip: IIp): Observable<EntityResponseType> {
    return this.http
      .put<IIp>(this.resourceUrl + '/' + ip.id, ip, {observe: 'response'});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IIp>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<PagingResponse>(this.resourceUrl, {params: options, observe: 'response'});
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
