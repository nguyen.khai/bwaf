import {Component, OnInit} from '@angular/core';
import {DdosAplicationService} from '@app/views/ddos/application/ddos-aplication.service';
import {
  IDdosApplication,
  IUrlApplication,
  IWebsiteApplication,
  RULE_TYPE
} from '@app/shared/model/ddos-application.model';
import {IWebsite} from '@app/shared/model/website.model';
import {IPaging, Paging} from '@app/shared/model/base-respone.model';
import {ISsl} from '@app/shared/model/ssl.model';
import {getParameterPaging, OrderParam, PAGING_PER_PAGE} from '@app/shared/constants/common.model';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {ActivatedRoute, Router} from '@angular/router';
import {IGroupWebsite} from '@app/shared/model/group-website.model';
import {ObjectGroupWebsiteService} from '@app/views/object/group-website/object-group-website.service';
import {IDdosUri} from '@app/shared/model/ddos-uri.model';
import {valueReferenceToExpression} from "@angular/compiler-cli/src/ngtsc/annotations/src/util";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-ddos-application',
  templateUrl: './ddos-application.component.html',
  styleUrls: ['./ddos-application.component.scss']
})
export class DdosApplicationComponent implements OnInit {

  ddosRules: IDdosApplication[] = [];
  PAGING_PER_PAGE = PAGING_PER_PAGE;
  rule_types = RULE_TYPE;
  keywordSearch: string;
  paging: IPaging;
  selectedddos: IDdosApplication;
  add: boolean = false;
  editing: boolean[] = [];
  editStatus = false;
  websiteTooltip = false;
  urlTooltip = false;
  groupWebsite: IGroupWebsite[];
  selectedGroupWebsite: IGroupWebsite = null;
  editGroupWebsite: IGroupWebsite = null;
  websites: IWebsite[];
  websiteSearch: string = "";
  uris: IDdosUri[];
  newUri: string = "";
  order: OrderParam = new OrderParam(null, false);
  public statusOptions = [
    {name: 'Log', value: 'log'},
    {name: 'Block', value: 'block'},
    {name: 'Challenge', value: 'challenge'},
  ];

  ddosForm = this.fb.group({
    id: [null],
    name: [''],
    count: [0],
    time: [0],
    block_time: [0],
    group_websites: [null],
    websites: this.fb.array([]),
    urls: this.fb.array([]),
    rule_type: [''],
    status: [''],
    active: [0]
  });


  constructor(
    private translate: TranslateService,
    private ddosAplicationService: DdosAplicationService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected groupWebsiteService: ObjectGroupWebsiteService
  ) {
    this.paging = new Paging();
  }

  ngOnInit() {
    this.groupWebsiteService.query().subscribe(res => {
      this.groupWebsite = res.body.data;
      this.loadAll();
    });
  }

  loadAll() {
    // const parameters = {
    //   offset: this.paging.offset - 1,
    //   limit: this.paging.limit
    // };
    this.paging = getParameterPaging(this.paging);
    const parameters = {
      limit: this.paging.limit,
      offset: this.paging.offset
    }
    if (this.keywordSearch) {
      parameters['search'] = this.keywordSearch;
    }
    if (this.selectedGroupWebsite && this.selectedGroupWebsite !== 'null') {
      parameters['group_websites'] = this.selectedGroupWebsite.id;
    }
    if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }
    this.ddosAplicationService.query(parameters).subscribe(res => {
      this.ddosRules = res.body.data;
      // this.paging.limit = res.body.paging.limit;
      // this.paging.offset = res.body.paging.offset;
      this.paging.total = res.body.paging.total;
      this.editing.length = 0;
      this.ddosRules.forEach(value => {
        value.isShowUrl = false;
        value.isShowWebsite = false;
      })
      this.ddosRules.forEach(() => {
        this.editing.push(false);
      });
    });
  }

  showWebsite(websites: Array<any>) {
    if (websites == null || this.websites.length <= 0) {
      return "";
    }
    return websites.map(value => {
      return value.website;
    }).join(", ");
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }


  onSearch() {
    this.paging.page = 1;
    if (this.keywordSearch) {
      this.keywordSearch = this.keywordSearch.trim();
    }
    this.loadAll();
  }

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }


  addDdosRule() {
    this.add = true;
    this.ddosForm.patchValue({
      'id': null,
      'name': '',
      'count': 0,
      'time': 0,
      'block_time': 0,
      'group_websites': this.selectedGroupWebsite,
      'rule_type': this.rule_types.type_1,
      'status': "block",
      'active': 0,
      // 'urls' : this.ddosRules[index].urls
    });

    const controlWebsite = <FormArray>this.ddosForm.get('websites');
    controlWebsite.clear();
    const controlUrl = <FormArray>this.ddosForm.get('urls');
    controlUrl.clear();
  }

  changeEditStatus(index) {
    // console.log(this.ddosRules[index].urls);
    this.editing[index] = !this.editing[index];
    this.ddosForm.patchValue({
      'id': this.ddosRules[index].id,
      'name': this.ddosRules[index].name,
      'count': this.ddosRules[index].count,
      'time': this.ddosRules[index].time,
      'block_time': this.ddosRules[index].block_time,
      'group_websites': this.ddosRules[index].group_websites,
      'status': this.ddosRules[index].status,
      'rule_type': this.ddosRules[index].rule_type,
      'active': this.ddosRules[index].active,
    });
    const controlWebsite = <FormArray>this.ddosForm.get('websites');
    controlWebsite.clear();
    this.ddosRules[index].websites.forEach(value => {
      controlWebsite.push(this.fb.control(value));
    });
    const controlUrl = <FormArray>this.ddosForm.get('urls');
    controlUrl.clear();
    this.ddosRules[index].urls.forEach(value => {
      controlUrl.push(this.fb.control(value));
    });
    this.editStatus = true;

    this.groupWebsiteService.find(this.ddosRules[index].group_websites.group_website_id).subscribe(res => {
      this.websites = res.body.websites;
    });
    // this.groupWebsiteService.find(this.ddosRules[index].group_website)
  }

  delete(modal, index) {
    this.selectedddos = this.ddosRules[index];
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      if (result === 'delete') {
        if (index !== undefined) {
          this.ddosAplicationService.delete(this.selectedddos.id).subscribe(res => {
            if ((this.paging.offset + this.ddosRules.length) - this.paging.offset === 1 ) {
              this.paging.page = this.paging.page - 1;
              this.loadAll();
            } else {
              this.loadAll();
            }
          });
        } else {
          this.notificationService.error('Thao tác không hợp kệ');
        }
      }
    }, (reason) => {
    });

  }

  distableShow(list) {
    list.forEach(value => {
      value.isShowUrl = false;
      value.isShowWebsite = false;
    });
    this.websiteTooltip = false;
    this.urlTooltip = false;
  }

  save() {
    this.add = false;
    this.ddosForm.patchValue({
      'active': this.ddosForm.value.active ? 1 : 0
    })
    const param = {...this.ddosForm.value};
    param['website_id'] = [];
    param['group_website_id'] = param['group_websites'].id;
    if (param.websites) {
      param.websites.forEach(value => {
        param['website_id'].push(value.website_id);
      });
    }
    if (param.urls.length >= 0) {
      param.urls = param.urls.map(value => {
        return value.uri;
      });
    }
    delete param.websites;
    delete param.group_websites;
    this.ddosAplicationService.create(param).subscribe(res => {
      this.reset();
    });

  }

  // isShowBtnAdd = false;
  changeGroupWebsite() {
    this.loadAll();
  }

  reset() {
    const controlWebsite = <FormArray>this.ddosForm.get('websites');
    controlWebsite.clear();
    const controlUrl = <FormArray>this.ddosForm.get('urls');
    controlUrl.clear();
    this.websites = this.selectedGroupWebsite ? this.selectedGroupWebsite.websites : null;
    this.websiteSearch = '';
    this.urlTooltip = false;
    this.websiteTooltip = false;
    this.ddosForm.patchValue({
      name: '',
      count: 0,
      time: 0,
      block_time: 0,
      group_website: 0,
      rule_type: '',
      status: '',
      active: 0
    });
    this.loadAll();
  }

  selectWebsite(event, website: IWebsite) {
    const control = <FormArray>this.ddosForm.get('websites');
    if (event.target.checked === false) {
      control.removeAt(control.value.findIndex(i => i.website_id === website.id));
    }
    if (event.target.checked === true) {
      control.push(this.fb.control({
        website_id: website.id,
        website: website.website_domain
      }));
    }
  }

  tickAllWebsite(websites) {
    const control = <FormArray>this.ddosForm.get('websites');
    control.clear();
    websites.forEach(website => {
      control.push(this.fb.control({
        website_id: website.id,
        website: website.website_domain
      }));
    });
  }

  unTickAllWebsite() {
    const control = <FormArray>this.ddosForm.get('websites');
    control.clear();
  }

  checkTick(website: IWebsite) {
    const control = <FormArray>this.ddosForm.get('websites');
    return !!control.value.find(i => i.website_id === website.id);
  }

  checkTickView(website, ddos: IDdosApplication) {
    return !!ddos.websites.find(i => i.website_id === website.id);
  }

  findGroupWebsite(index) {
    const groupWebsites = this.groupWebsiteService.find(this.ddosRules[index].group_websites.group_website_id).subscribe(res => {
      this.websites = res.body.websites;
    });
    return groupWebsites;
  }

  searchWebsite() {
    if (!this.editStatus) {
      this.websites = this.selectedGroupWebsite.websites.filter(value => value.website_domain.includes(this.websiteSearch.trim()));
    } else {
      this.websites = this.editGroupWebsite.websites.filter(value => value.website_domain.includes(this.websiteSearch.trim()));
    }
  }

  deleteUri(control, index) {
    // this.ddosAplicationService.deleteUri(uri.uri_id).subscribe(res => {
    //   this.loadUri();
    // });
    control.value.splice(index, 1);
  }

  loadUri() {
    this.ddosAplicationService.getUri().subscribe(res => {
      this.uris = res.body;
      const controlUrl = <FormArray>this.ddosForm.get('urls');
      controlUrl.clear();
      this.uris.forEach(uri => {
        controlUrl.push(this.fb.control(uri));
      });
    });
  }

  addUri(control) {
    // const param = {
    //   uri: this.newUri
    // };
    // this.ddosAplicationService.addUri(param).subscribe(res => {
    //   this.newUri = '';
    //   this.loadUri();
    // });
    const uriAdd = this.newUri.trim();
    const tmp = control.value.findIndex(value => {
      return uriAdd === value.uri;
    });
    if (tmp < 0) {
      control.value.push({
        uri_id: 0,
        uri: uriAdd
      });
      this.newUri = "";
    } else {
      const msg = this.translate.instant("monitor.ddos.duplicate_uri");
      alert(msg);
    }


  }

  editSave(index) {
    this.ddosForm.patchValue({
      'active': this.ddosForm.value.active ? 1 : 0
    })
    const param = {...this.ddosForm.value};
    param['website_id'] = [];
    if (param.websites) {
      param.websites.forEach(value => {
        param['website_id'].push(value.website_id);
      });
    }
    if (param.urls.length >= 0) {
      param.urls = param.urls.map(value => {
        return value.uri;
      });
    }
    param['group_website_id'] = param['group_websites'].group_website_id;
    delete param.websites;
    delete param.group_websites;
    this.ddosAplicationService.update(param).subscribe(res => {
      this.editing[index] = false;
      this.editStatus = false;
      this.reset();
    });
  }

  editCancel(index) {
    this.editing[index] = false;
    this.editStatus = false;
    this.reset();
  }

  changeActiveStatus() {
    this.ddosForm.patchValue({
      'active': !this.ddosForm.value.active
    });
  }

  validateAddUri(control) {
    if (control.value == null || this.newUri == null) {
      return false;
    }
    const tmp = control.value.findIndex(value => {
      return this.newUri.trim() === value.uri;
    });
    return tmp >= 0;
  }

}
