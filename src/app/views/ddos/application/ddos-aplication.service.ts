import {Injectable} from '@angular/core';
import {environment} from 'environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import * as moment from 'moment';
import {map} from 'rxjs/operators';
import {IRuleNetwork} from '../../../shared/model/rule-network.model';
import {IPaging} from '@app/shared/model/base-respone.model';
import {IDdosApplication} from '@app/shared/model/ddos-application.model';
import {createRequestOption} from '@app/shared';
import {IReportModel} from '@app/shared/model/report.model';
import {IDdosUri} from '@app/shared/model/ddos-uri.model';

type EntityResponseType = HttpResponse<IRuleNetwork>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;

interface PagingResponse {
  data: IDdosApplication[];
  paging: IPaging;
}

@Injectable({
  providedIn: 'root'
})
export class DdosAplicationService {
  public resourceUrl = environment.apiUrl + '/ddos-application';

  constructor(protected http: HttpClient) {
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<PagingResponse>(this.resourceUrl, {params: options, observe: 'response'});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IDdosApplication>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  update(ddos: IDdosApplication): Observable<HttpResponse<any>> {
    return this.http
      .put<any>(`${this.resourceUrl}/${ddos.id}`, ddos, {observe: 'response'});
  }

  create(deliveryInfo: IDdosApplication): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(deliveryInfo);
    return this.http
      .post<IDdosApplication>(this.resourceUrl, copy, { observe: 'response' });
  }

  getUri(): Observable<HttpResponse<IDdosUri[]>> {
    return this.http
      .get<IDdosUri[]>(this.resourceUrl + '/uri', {observe: 'response'});
  }

  deleteUri(uri_id): Observable<HttpResponse<any>> {
    return this.http
      .delete<any>(this.resourceUrl + `/uri/${uri_id}`, { observe: 'response' });
  }

  addUri(param: any): Observable<HttpResponse<IDdosUri>> {
    return this.http
      .post(this.resourceUrl + '/uri', param,  {observe: 'response'});
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(reportModel: IReportModel): IReportModel {
    const copy: IReportModel = Object.assign({}, reportModel, {});
    return copy;
  }


}
