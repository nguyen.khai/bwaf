import {Component, OnInit} from '@angular/core';
import {IRuleNetwork} from '../../../shared/model/rule-network.model';
import {DdosNetworkService} from './ddos-network.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrCustomService} from "@app/shared/toastr-custom-service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormArray, Validator, Validators, FormGroup} from '@angular/forms';
import {getStyle, hexToRgba} from '@coreui/coreui/dist/js/coreui-utilities';
import {CustomTooltips} from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import {OrderParam} from "@app/shared/constants/common.model";

@Component({
  selector: 'app-ddos-network',
  templateUrl: './ddos-network.component.html',
  styleUrls: ['./ddos-network.component.scss']
})
export class DdosNetworkComponent implements OnInit {

  ruleNetworks: IRuleNetwork[] = [];
  ruleNetworkFormGroup: FormGroup;
  selectedruleNetwork: IRuleNetwork;
  radioModel: string = 'On';
  order: OrderParam = new OrderParam(null, false);

  constructor(
    public ddosNetworkService: DdosNetworkService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {

  }

  ngOnInit() {
    this.initTable();
  }

  initTable() {
    this.ruleNetworkFormGroup = this.fb.group({
      data: this.fb.array([])
    });
    this.loadAll();
  }

  loadAll() {
    console.log('load ruleNetworks');
    const control = <FormArray>this.ruleNetworkFormGroup.get('data');

    let parameters = {};
    if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }
    this.ddosNetworkService.query(parameters).subscribe(res => {
      if (res.status === 200) {
        control.clear();
        this.ruleNetworks = res.body;
        for (const ruleNetwork of this.ruleNetworks) {
          const grp = this.fb.group({
            id: [ruleNetwork.id],
            name: [ruleNetwork.name, Validators.required],
            threshold: [ruleNetwork.threshold, [Validators.required]],
            duration: [ruleNetwork.duration, [Validators.required]],
            block_duration: [ruleNetwork.block_duration, [Validators.required]],
            state: [ruleNetwork.state, [Validators.required]],
            // created_at: [ruleNetwork.created_at],
            // updated_at: [ruleNetwork.updated_at],
            alert: [ruleNetwork.alert === 1],
            is_update: [false]
          });
          control.push(grp);
          // this.transition();
        }
      } else {
        console.warn("load rule network error");
      }
    });


  }


  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  save(modal, index: number) {
    const control = <FormArray>this.ruleNetworkFormGroup.get('data');
    this.selectedruleNetwork = control.at(index).value;
    this.modalService.open(modal, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static'
    }).result.then((result) => {
      if (result === 'save') {
        console.log("save");
        this.ddosNetworkService.update(this.selectedruleNetwork).subscribe(res => {
          this.loadAll();
        });
      }
    });
  }

  get getFormData(): FormArray {
    return <FormArray>this.ruleNetworkFormGroup.get('data');
  }

  onEdit(index: number) {
    const control = <FormArray>this.ruleNetworkFormGroup.get('data');
    control.at(index).get('is_update').setValue(true);
  }

  onCancelEdit(index: number) {
    const control = <FormArray>this.ruleNetworkFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    const id: number = control.at(index).get("id").value;
    if (id) {
      const ruleNetwork: IRuleNetwork = this.ruleNetworks.find((value) => {
        return value.id === id;
      });
      if (ruleNetwork) {
        control.at(index).setValue({
          id: ruleNetwork.id,
          name: ruleNetwork.name,
          threshold: ruleNetwork.threshold,
          duration: ruleNetwork.duration,
          block_duration: ruleNetwork.block_duration,
          state: ruleNetwork.state,
          // created_at: ruleNetwork.created_at,
          // updated_at: ruleNetwork.updated_at,
          alert: ruleNetwork.alert === 1,
          is_update: false
        });

      }
    }

  }

  // transition() {
  //   const parameters = {};
  //
  //   if (this.order.orderBy) {
  //     parameters['orderBy'] = this.order.orderBy;
  //     parameters['orderType'] = this.order.getOrderType();
  //   }
  //   this.router.navigate(['/ddos/network'], {
  //     queryParams: parameters
  //   });
  //   // this.loadAll();
  // }


}
