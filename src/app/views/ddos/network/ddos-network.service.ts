import {Injectable} from '@angular/core';
import {environment} from 'environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import * as moment from 'moment';
import {map} from 'rxjs/operators';
import {IRuleNetwork} from '../../../shared/model/rule-network.model';
import {IPaging} from '@app/shared/model/base-respone.model';
import {createRequestOption} from "@app/shared";

type EntityResponseType = HttpResponse<IRuleNetwork>;


interface MailServerResponse {
  data: IRuleNetwork[];
}

@Injectable({
  providedIn: 'root'
})
export class DdosNetworkService {
  public resourceUrl = environment.apiUrl + '/ddos-network';

  constructor(protected http: HttpClient) {
  }

  query(req?): Observable<HttpResponse<IRuleNetwork[]>> {
    const options = createRequestOption(req);
    return this.http
      .get<IRuleNetwork[]>(this.resourceUrl, {params: options, observe: 'response'});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRuleNetwork>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }

  update(ruleNetwork: IRuleNetwork): Observable<HttpResponse<any>> {
    return this.http
      .put<any>(`${this.resourceUrl}/${ruleNetwork.id}`, ruleNetwork, {observe: 'response'});
  }
}
