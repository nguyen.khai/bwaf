import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IPaging, Paging} from '@app/shared/model/base-respone.model';
import { DdosNetworkComponent } from './network/ddos-network.component';
import { DdosApplicationComponent } from './application/ddos-application.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'DDoS'
    },
    children: [
      {
        path: '',
        redirectTo: 'network'
      },
      {
        path: 'network',
        component: DdosNetworkComponent,
        data: {
          title: 'Network',
          pagingParams: {}
        }
      },
      {
        path: 'application',
        component: DdosApplicationComponent,
        data: {
          title: 'Application',
          pagingParams: new Paging()
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DdosRoutingModule {
}
