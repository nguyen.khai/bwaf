import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@app/shared';
import {ReactiveFormsModule} from '@angular/forms';
import {CollapseModule} from 'ngx-bootstrap';
import { DdosNetworkComponent } from './network/ddos-network.component';
import { DdosApplicationComponent } from './application/ddos-application.component';
import { DdosRoutingModule } from './ddos-routing.module';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';



@NgModule({
    imports: [DdosRoutingModule, CommonModule, SharedModule, ReactiveFormsModule, CollapseModule, FontAwesomeModule],
  declarations: [
    DdosNetworkComponent,
    DdosApplicationComponent
  ]
})
export class DdosModule {

}
