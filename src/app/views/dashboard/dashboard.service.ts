import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "environments/environment";
import { createRequestOption } from "app/shared";
import { IPaging } from "@app/shared/model/base-respone.model";
import {
  ILogFile,
  IScanStatus,
  IVulnerability,
  IWebScan,
} from "@app/shared/model/web-scan.model";
import { DashboardMapItem } from "@app/shared/model/map-item.model";
import { IInterface } from "@app/shared/model/interface.model";

interface WebConnection {
  datetime: string;
  request_site: number;
  site_connect: string;
}

interface IpConnection {
  datetime: string;
  request_ip: number;
  ip_connect: string;
}

interface WebConnectionPagingResponse {
  data: WebConnection[];
  paging: IPaging;
}

interface IpConnectionPagingResponse {
  data: IpConnection[];
  paging: IPaging;
}

@Injectable({ providedIn: "root" })
export class DashboardService {
  public resourceWafCountUrl = environment.apiUrl + "/monitor-waf-count";
  public resourceDdosCountUrl = environment.apiUrl + "/monitor-ddos-count";
  public resourceWebConnectionUrl = environment.apiUrl + "/website-connection";
  public resourceIpConnectionUrl = environment.apiUrl + "/ip-connection";

  constructor(protected http: HttpClient) {}

  getWafCount(): Observable<HttpResponse<DashboardMapItem[]>> {
    return this.http.get<DashboardMapItem[]>(`${this.resourceWafCountUrl}`, {
      observe: "response",
    });
  }

  getDdosCount(): Observable<HttpResponse<DashboardMapItem[]>> {
    return this.http.get<DashboardMapItem[]>(`${this.resourceDdosCountUrl}`, {
      observe: "response",
    });
  }

  getWebConnections(
    req?: any
  ): Observable<HttpResponse<WebConnectionPagingResponse>> {
    const options = createRequestOption(req);
    return this.http.get<WebConnectionPagingResponse>(
      `${this.resourceWebConnectionUrl}`,
      { params: options, observe: "response" }
    );
  }

  getIpConnections(
    req?: any
  ): Observable<HttpResponse<IpConnectionPagingResponse>> {
    const options = createRequestOption(req);
    return this.http.get<IpConnectionPagingResponse>(
      `${this.resourceIpConnectionUrl}`,
      { params: options, observe: "response" }
    );
  }

  protected convertDateFromClient(ssl: IWebScan): IWebScan {
    const copy: IWebScan = Object.assign({}, ssl, {});
    return copy;
  }
}
