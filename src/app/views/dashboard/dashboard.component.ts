import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { AboutService } from "@app/views/about/about.service";
import { IAbout } from "@app/shared/model/about.model";
import { ILicence } from "@app/shared/model/licence.model";
import { LicenceService } from "@app/views/about/licence.service";
import { MonitorResourceService } from "@app/views/monitor/resource/monitor-resource.service";
import { IMonitorResource } from "@app/shared/model/monitor-resource.model";
import * as moment from "moment";
import { BaseChartDirective, Label } from "ng2-charts";
import { ChartDataSets, ChartOptions, ChartType } from "chart.js";
import { DashboardService } from "@app/views/dashboard/dashboard.service";
import { IHighAvailability } from "@app/shared/model/high-availability.model";
import { HighAvailabilityService } from "@app/views/high-availability/high-availability.service";
import { MonitorSystemService } from "@app/views/monitor/system/monitor-system.service";
import { NetworkService } from "@app/views/network/network.service";
import { IInterface } from "@app/shared/model/interface.model";
import { REQUEST_INTERVAL } from "@app/shared/constants/common.model";
import { environment } from "@env/environment";
const SERVER_API_URL = environment.apiUrl;
import {
  IMonitorStatusCode,
  MonitorStatusCode,
} from "@app/shared/model/monitor-system.model";
import {
  PERIOD_MONITOR,
  PeriodMonitor,
} from "@app/shared/constants/monitor.model";
@Component({
  templateUrl: "dashboard.component.html",
})
export class DashboardComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild("resourceChart", { static: false })
  resourceChart: BaseChartDirective;
  @ViewChild("ddosWafChart", { static: false })
  ddosWafChart: BaseChartDirective;
  @ViewChild("trafficChart", { static: false })
  trafficChart: BaseChartDirective;
  @ViewChild("httpCodeChart", { static: false })
  httpCodeChart: BaseChartDirective;
  @ViewChild("websiteConnectionChart", { static: false })
  websiteConnectionChart: BaseChartDirective;
  @ViewChild("ipConnectionChart", { static: false })
  ipConnectionChart: BaseChartDirective;
  REQUEST_INTERVAL = REQUEST_INTERVAL;
  about: IAbout;
  licence: ILicence;
  @ViewChild("webConnectionHorizonBarChart", { static: false })
  webConnectionHorizonBarChart: BaseChartDirective;
  @ViewChild("ipConnectionHorizonBarChart", { static: false })
  ipConnectionHorizonBarChart: BaseChartDirective;
  periodWebConnectionChart = PERIOD_MONITOR;
  periodIpConnectionChart = PERIOD_MONITOR;
  periodFilterChart = this.fb.group({
    webConnectionChart: 5,
    ipConnectionChart: 5,
  });
  highAvailabilities: IHighAvailability;
  haMode: string;
  licence_API: string;
  selectedInterface: string;
  interfaces: IInterface[];
  monitorStatusCode: IMonitorStatusCode[];
  public code_1 = [];
  public code_2 = [];
  public code_3 = [];
  public code_4 = [];
  public code_5 = [];
  public datetimeData = [];
  size = "B";
  ddosAttack = 0;
  webappAttack = 0;
  isLoaded = false;
  public resourceChartOption: any = {
    responsive: true,
    elements: {
      line: {
        tension: 0.4,
        fill: false,
        spanGaps: true,
      },
      point: {
        radius: 0,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            suggestedMin: 0,
            suggestedMax: 100,
          },
        },
      ],
      xAxes: [
        {
          type: "time",
          time: {
            unit: "hour",
          },
        },
      ],
    },
  };
  public resourceChartPlugins = [];
  public resources: IMonitorResource[];
  public cpuData = [];
  public ramData = [];
  public diskData = [];
  public timeData = [];
  public resourceChartData: ChartDataSets[] = [
    { data: this.cpuData ? this.cpuData : [0], label: "CPU", spanGaps: true },
    { data: this.ramData ? this.ramData : [0], label: "RAM", spanGaps: true },
    {
      data: this.diskData ? this.diskData : [0],
      label: "Disk",
      spanGaps: true,
    },
  ];
  timer;

  public ddosWafChartOption: any = {
    responsive: true,
    elements: {
      line: {
        tension: 0.4,
        fill: false,
        spanGaps: true,
      },
      point: {
        radius: 0,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            suggestedMin: 0,
            suggestedMax: 1000,
          },
        },
      ],
      xAxes: [
        {
          type: "time",
          time: {
            unit: "hour",
          },
        },
      ],
    },
  };
  public ddosCount = [];
  public wafCount = [];
  public ddosWafChartData: ChartDataSets[] = [
    {
      data: this.ddosCount ? this.ddosCount : [0],
      label: "DDOS",
      spanGaps: true,
    },
    { data: this.wafCount ? this.wafCount : [0], label: "WAF", spanGaps: true },
  ];

  trafficUnit = "B";
  firstItem = 0;
  public trafficChartOption: any = {
    responsive: true,
    elements: {
      line: {
        tension: 0.4,
        fill: false,
      },
      point: {
        radius: 0,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            suggestedMin: 0,
            suggestedMax: 1000,
            callback: (value, index, values) => {
              return value + " " + this.size;
            },
          },
        },
      ],
      xAxes: [
        {
          type: "time",
          time: {
            unit: "minute",
          },
        },
      ],
    },
    options: {
      tooltips: {
        callbacks: {
          label: (tooltipItem, data) => {
            return tooltipItem.yLabel + " " + this.trafficUnit;
          },
        },
      },
      spanGaps: true,
    },
  };

  public trafficChartData: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: "Input", spanGaps: true },
    {
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      label: "Output",
      spanGaps: true,
    },
  ];

  public httpCodeChartOption: any = {
    responsive: true,
    elements: {
      line: {
        tension: 0.4,
        fill: false,
        spanGaps: true,
      },
      point: {
        radius: 0,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            suggestedMin: 0,
            suggestedMax: 10,
          },
        },
      ],
      xAxes: [
        {
          type: "time",
          time: {
            unit: "hour",
          },
        },
      ],
    },
  };

  public httpCodeChartData: ChartDataSets[] = [
    { data: this.code_1 ? this.code_1 : [0], label: "1xx", spanGaps: true },
    { data: this.code_2 ? this.code_2 : [0], label: "2xx", spanGaps: true },
    { data: this.code_3 ? this.code_3 : [0], label: "3xx", spanGaps: true },
    { data: this.code_4 ? this.code_4 : [0], label: "4xx", spanGaps: true },
    { data: this.code_5 ? this.code_5 : [0], label: "5xx", spanGaps: true },
  ];

  public barChartLabels: Label[] = [];
  public webConnectionChartOption: ChartOptions = {
    responsive: true,
    plugins: {
      legend: {
        position: "right",
      },
      title: {
        display: false,
        text: "",
      },
    },
  };
  public webConnectionChartData: ChartDataSets[] = [
    { data: [], label: "", spanGaps: true },
  ];

  public ipBarChartLabels: Label[] = [];
  public ipConnectionChartOption: ChartOptions = {
    responsive: true,
    plugins: {
      legend: {
        position: "right",
      },
      title: {
        display: false,
        text: "",
      },
    },
  };
  public ipConnectionChartData: ChartDataSets[] = [
    { data: [], label: "", spanGaps: true },
  ];

  constructor(
    private fb: FormBuilder,
    public aboutService: AboutService,
    public licenceService: LicenceService,
    protected resourceService: MonitorResourceService,
    public highAvailabilityService: HighAvailabilityService,
    protected monitorSystemService: MonitorSystemService,
    protected networkService: NetworkService,
    protected dashboardService: DashboardService
  ) {}

  public lineChartLabels: any;
  public lineChartLegend = true;
  public lineChartType = "line";
  public barChartType: ChartType = "horizontalBar";

  ngOnInit() {
    this.loadAll();
    this.timer = setInterval(() => {
      this.loadResource();
      this.loadDdosAndWaf();
      this.loadHttpCodeMonitor();
      this.getTrafficByInterface(this.selectedInterface);
      this.loadWebConnection();
      this.loadIpConnection();
    }, this.REQUEST_INTERVAL);
    this.licence_API = SERVER_API_URL;
  }

  ngOnDestroy(): void {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  ngAfterViewInit(): void {}

  loadAll() {
    this.loadAbout();
    this.loadLicence();
    this.loadResource();
    this.loadDdosAndWaf();
    this.loadHaInfo();
    this.loadNetworkTraffic();
    this.getTrafficByInterface(this.selectedInterface);
    this.loadHttpCodeMonitor();
    this.loadWebConnection();
    this.loadIpConnection();
  }

  loadAbout() {
    this.aboutService.find().subscribe((res) => {
      this.isLoaded = true;
      if (res.status === 200) {
        this.about = res.body;
      } else {
        console.log("can not load about");
      }
    });
  }

  loadLicence() {
    this.licenceService.find().subscribe((res) => {
      if (res.status === 200) {
        this.licence = res.body;
      } else {
        console.log("can not load licence");
      }
    });
  }

  loadHaInfo() {
    this.highAvailabilityService.getHighAvailabilityMode().subscribe((res) => {
      const modes = res.body;
      this.highAvailabilityService.query().subscribe((rs) => {
        this.highAvailabilities = rs.body;
        if (this.highAvailabilities) {
          this.haMode = modes.find(
            (m) => m.id === this.highAvailabilities.config.operation_mode_id
          ).mode_name;
          console.log(this.haMode);
        }
      });
    });
  }

  loadResource() {
    this.resourceService
      .get({
        orderBy: "id",
        orderType: "desc",
        dateType: "minute",
        dateValue: 30,
        avg_value: 100,
      })
      .subscribe((res) => {
        this.resources = res.body.data;
        this.prepareResourceData();
      });
  }

  prepareResourceData() {
    this.resetResourceChart();
    this.resources.forEach((data) => {
      this.cpuData.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: data.cpu,
      });
      this.diskData.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: data.disk,
      });
      this.ramData.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: data.ram,
      });
    });
    this.reloadChart();
  }

  resetResourceChart() {
    this.cpuData = [];
    this.diskData = [];
    this.timeData = [];
    this.ramData = [];
  }

  reloadChart() {
    if (this.resourceChart !== undefined) {
      this.resourceChart.datasets[0].data = this.cpuData;
      this.resourceChart.datasets[1].data = this.ramData;
      this.resourceChart.datasets[2].data = this.diskData;
      this.resourceChart.update();
    }
  }

  loadDdosAndWaf() {
    this.dashboardService.getWafCount().subscribe((res) => {
      this.dashboardService.getDdosCount().subscribe((res1) => {
        const d = new Date();
        const hour = d.getHours();
        const startCurrentDay = moment(d).startOf("day").toDate();
        const ddos = res1.body;
        const waf = res.body;
        let old = [];
        let cur = [];
        ddos.forEach((i) => {
          if (i.key > hour) {
            old.push({
              x: moment(startCurrentDay)
                .add(i.key - 24, "hours")
                .toDate(),
              y: i.value,
            });
          } else {
            cur.push({
              x: moment(startCurrentDay).add(i.key, "hours").toDate(),
              y: i.value,
            });
            if (i.key === hour) {
              this.ddosAttack = +i.value;
            }
          }
        });
        this.ddosCount = [];
        this.ddosCount.push(...old);
        this.ddosCount.push(...cur);
        old = [];
        cur = [];
        waf.forEach((i) => {
          if (i.key > hour) {
            old.push({
              x: moment(startCurrentDay)
                .add(i.key - 24, "hours")
                .toDate(),
              y: i.value,
            });
          } else {
            cur.push({
              x: moment(startCurrentDay).add(i.key, "hours").toDate(),
              y: i.value,
            });
            if (i.key === hour) {
              this.webappAttack = +i.value;
            }
          }
        });
        this.wafCount = [];
        this.wafCount.push(...old);
        this.wafCount.push(...cur);
        this.ddosWafChart.datasets[0].data = this.ddosCount;
        this.ddosWafChart.datasets[1].data = this.wafCount;
        this.ddosWafChart.update();
      });
    });
  }

  loadWebConnection() {
    const parameters = {
      orderBy: "id",
      orderType: "desc",
      dateType: "minute",
      avg_value: 100,
      dateValue: this.periodFilterChart.controls["webConnectionChart"].value,
    };
    this.dashboardService.getWebConnections(parameters).subscribe((res) => {
      const data = res.body.data;

      const labels = data.reduce((acc: string[], cur, idx) => {
        if (!idx || !acc.includes(cur.site_connect)) {
          acc.push(cur.site_connect);
        }

        return acc;
      }, []);

      const dateData = data.reduce((acc: string[], cur, idx) => {
        if (!idx || !acc.includes(cur.datetime.split(" ")[0])) {
          acc.push(cur.datetime.split(" ")[0]);
        }

        return acc;
      }, []);

      const formattedData = dateData.map((e) => {
        const d = { data: [], label: e, spanGaps: true };
        for (let i = 0; i < labels.length; i++) {
          const label = labels[i];
          const filteredDateData = data.filter(
            (ee) => ee.datetime.split(" ")[0] === e && ee.site_connect === label
          );
          const sum = filteredDateData.reduce((acc: number, cur) => {
            acc += cur.request_site;
            return acc;
          }, 0);
          d.data.push(sum);
        }
        return d;
      });

      if (formattedData.length) {
        this.webConnectionChartData = formattedData;
      }
      if (labels.length) {
        this.barChartLabels = labels;
      }
    });
  }

  loadIpConnection() {
    const parameters = {
      orderBy: "id",
      orderType: "desc",
      dateType: "minute",
      avg_value: 100,
      dateValue: this.periodFilterChart.controls["ipConnectionChart"].value,
    };
    this.dashboardService.getIpConnections(parameters).subscribe((res) => {
      const data = res.body.data;

      const labels = data.reduce((acc: string[], cur, idx) => {
        if (!idx || !acc.includes(cur.ip_connect)) {
          acc.push(cur.ip_connect);
        }

        return acc;
      }, []);

      const dateData = data.reduce((acc: string[], cur, idx) => {
        if (!idx || !acc.includes(cur.datetime.split(" ")[0])) {
          acc.push(cur.datetime.split(" ")[0]);
        }

        return acc;
      }, []);

      const formattedData = dateData.map((e) => {
        const d = { data: [], label: e, spanGaps: true };
        for (let i = 0; i < labels.length; i++) {
          const label = labels[i];
          const filteredDateData = data.filter(
            (ee) => ee.datetime.split(" ")[0] === e && ee.ip_connect === label
          );
          const sum = filteredDateData.reduce((acc: number, cur) => {
            acc += cur.request_ip;
            return acc;
          }, 0);
          d.data.push(sum);
        }
        return d;
      });

      if (formattedData.length) {
        this.ipConnectionChartData = formattedData;
      }

      if (labels.length) {
        this.ipBarChartLabels = labels;
      }
    });
  }

  loadNetworkTraffic() {
    this.networkService.query().subscribe((res) => {
      this.interfaces = res.body.data;
      this.getTrafficByInterface(this.interfaces[0].name);
    });
  }

  getTrafficByInterface(name: string) {
    this.selectedInterface = name;
    this.monitorSystemService
      .getMonitorTraffic({
        orderBy: "id",
        orderType: "desc",
        dateType: "minute",
        dateValue: 5,
        avg_value: 100,
        interface_id: name,
      })
      .subscribe((res) => {
        const monitorThroughtputs = res.body.data;
        this.prepareTrafficData(monitorThroughtputs);
      });
  }

  prepareTrafficData(monitorThroughputs) {
    const inputData = [];
    const outputData = [];
    this.firstItem = monitorThroughputs[0].input;
    monitorThroughputs.forEach((data) => {
      if (this.firstItem < data.input) {
        this.firstItem = data.input;
      }
    });

    if (this.firstItem < 1024) {
      this.size = "bps";
    } else if (this.firstItem < 1000000) {
      this.size = "Kbps";
    } else if (this.firstItem < 1000000000) {
      this.size = "Mbps";
    } else if (this.firstItem < 1000000000 * 1000) {
      this.size = "Gbps";
    } else if (this.firstItem < 1000000000 * 1000 * 1000) {
      this.size = "Tbps";
    } else {
      this.size = "Pbps";
    }

    monitorThroughputs.forEach((data) => {
      inputData.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: this.formatSizeUnits1(data.input, data.output),
      });
      outputData.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: this.formatSizeUnits2(data.input, data.output),
      });
    });
    this.trafficChart.datasets[0].data = inputData;
    this.trafficChart.datasets[1].data = outputData;
  }

  loadHttpCodeMonitor() {
    this.monitorSystemService
      .getMonitorStatusCode({
        orderBy: "id",
        orderType: "desc",
        dateType: "hour",
        dateValue: 24,
        avg_value: 100,
      })
      .subscribe((res) => {
        this.monitorStatusCode = res.body.data;
        this.prepareHttpCodeMonitorData(this.monitorStatusCode);
      });
  }

  prepareHttpCodeMonitorData(monitorStatusCode: MonitorStatusCode[]) {
    this.resetHttpStatusCode();
    this.monitorStatusCode.forEach((data) => {
      this.code_1.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: data.code_1,
      });
      this.code_2.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: data.code_2,
      });
      this.code_3.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: data.code_3,
      });
      this.code_4.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: data.code_4,
      });
      this.code_5.push({
        x: moment(data.datetime, "YYYY-MM-DD HH:mm:ss"),
        y: data.code_5,
      });
    });
    this.reloadChartStatusCode();
  }

  resetHttpStatusCode() {
    this.datetimeData.length = 0;
    this.code_5.length = 0;
    this.code_4.length = 0;
    this.code_3.length = 0;
    this.code_2.length = 0;
    this.code_1.length = 0;
  }

  reloadChartStatusCode() {
    if (this.httpCodeChart !== undefined) {
      this.httpCodeChartData[0].data = this.code_1;
      this.httpCodeChartData[1].data = this.code_2;
      this.httpCodeChartData[2].data = this.code_3;
      this.httpCodeChartData[3].data = this.code_4;
      this.httpCodeChartData[4].data = this.code_5;
      this.httpCodeChart.update();
    }
  }

  formatBytes(bytes) {
    if (bytes === 0) {
      return "0 byte";
    }
    const i = parseInt(String(Math.floor(Math.log(bytes) / Math.log(1000))));
    // @ts-ignore
    return Math.round(bytes / Math.pow(1000, i), 2);
  }

  formatSizeUnits1(bytes1, bytes2) {
    if (this.firstItem >= 1073741824) {
      bytes1 = (bytes1 / 1073741824).toFixed(2);
      bytes2 = (bytes2 / 1073741824).toFixed(2);
    } else if (this.firstItem >= 1048576) {
      bytes1 = (bytes1 / 1048576).toFixed(2);
      bytes2 = (bytes2 / 1048576).toFixed(2);
    } else if (this.firstItem >= 1) {
      bytes1 = (bytes1 / 1024).toFixed(2);
      bytes2 = (bytes2 / 1024).toFixed(2);
    } else {
      bytes1 = "0 bytes";
    }
    return bytes1;
  }

  formatSizeUnits2(bytes1, bytes2) {
    if (this.firstItem >= 1073741824) {
      bytes1 = (bytes1 / 1073741824).toFixed(2);
      bytes2 = (bytes2 / 1073741824).toFixed(2);
    } else if (this.firstItem >= 1048576) {
      bytes1 = (bytes1 / 1048576).toFixed(2);
      bytes2 = (bytes2 / 1048576).toFixed(2);
    } else if (this.firstItem >= 1) {
      bytes1 = (bytes1 / 1024).toFixed(2);
      bytes2 = (bytes2 / 1024).toFixed(2);
    } else {
      bytes1 = "0 bytes";
    }
    return bytes2;
  }
}
