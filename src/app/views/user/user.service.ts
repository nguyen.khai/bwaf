import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';
import {createRequestOption} from 'app/shared';
import {IPaging} from '@app/shared/model/base-respone.model';
import {IUser} from '@app/shared/model/user.model';
import {IWebsite} from "@app/shared/model/website.model";

type EntityResponseType = HttpResponse<IUser>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;
interface PagingResponse {
  data: IUser[];
  paging: IPaging;
}

@Injectable({ providedIn: 'root' })
export class UserService {
  public resourceUrl = environment.apiUrl + '/user';

  constructor(protected http: HttpClient) {}

  create(user: IUser): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(user);
    return this.http
      .post<IUser>(this.resourceUrl, copy, { observe: 'response' });
  }

  update(user: IUser): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(user);
    return this.http
      .put<IUser>(`${this.resourceUrl}/${user.id}`, copy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IUser>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<PagingResponse>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(user: IUser): IUser {
    const copy: IUser = Object.assign({}, user, {});
    return copy;
  }
}
