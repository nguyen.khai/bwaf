import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserComponent} from './user.component';
import {Paging} from '@app/shared/model/base-respone.model';

const routes: Routes = [
  {
    path: '',
    component: UserComponent,
    data: {
      title: 'User',
      pagingParams: new Paging()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
