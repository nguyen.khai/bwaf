import {NgModule} from '@angular/core';
import {UserRoutingModule} from './user-routing.module';
import {UserComponent} from './user.component';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '@app/shared';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";


@NgModule({
    imports: [UserRoutingModule, TranslateModule, ReactiveFormsModule, SharedModule, FontAwesomeModule],
  declarations: [
    UserComponent
  ]
})
export class UserModule { }

