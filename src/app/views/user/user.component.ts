import {Component, OnInit} from '@angular/core';
import {IPaging} from '@app/shared/model/base-respone.model';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IUser, User} from '@app/shared/model/user.model';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '@app/views/user/user.service';
import {getParameterPaging, OrderParam, PAGING_PER_PAGE} from '@app/shared/constants/common.model';
import {canEdit_STATUS} from '@app/shared/constants/base.constant';


@Component({
  selector: 'app-user',
  templateUrl: 'user.component.html'
})
export class UserComponent implements OnInit {
  users: IUser[];
  paging: IPaging;
  userFormGroup: FormGroup;
  selectedUser: IUser;
  txtSearch: string;
  isEdit: boolean;
  PAGING_PER_PAGE = PAGING_PER_PAGE;
  CANEDIT_STATUS = canEdit_STATUS;
  isCreate: boolean;
  order: OrderParam = new OrderParam(null, false);
  constructor(
    public userService: UserService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.paging = data.pagingParams;
    });
  }

  ngOnInit(): void {
    this.initTable();
  }

  initTable() {
    this.userFormGroup = this.fb.group({
      data: this.fb.array([])
    });
    this.loadAll();
  }

  initForm() {
    return this.fb.group({
      user_name: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]],
      password: ['', [Validators.required, Validators.minLength(8),
        Validators.required, Validators.maxLength(30),
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=)/)]],
      phone_number: ['', [Validators.maxLength(50)]],
      email: ['', [ Validators.minLength(8), Validators.maxLength(50)]],
      can_edit: [''] ? 1 : 0,
      is_update: [true],
      is_show_password: [false],
    });
  }

  onSearch() {
    this.paging.page = 1;
    if (this.txtSearch) {
      this.txtSearch = this.txtSearch.trim();
    }
    this.loadAll();
  }

  loadAll() {
    this.paging = getParameterPaging(this.paging);
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit
    };
    if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }
    if (this.txtSearch && this.txtSearch.trim().length > 0) {
      parameters['search'] = this.txtSearch;
    }
    this.userService.query(parameters).subscribe(res => {
      this.users = res.body.data;
      const control = <FormArray>this.userFormGroup.get('data');
      control.clear();
      for (const user of this.users) {
        const grp = this.fb.group({
          id: [user.id],
          user_name: [user.user_name, Validators.required],
          can_edit: [user.can_edit ? 1 : 0],
          phone_number: [user.phone_number],
          password: [user.password, Validators.required],
          email: [user.email],
          image_link: [user.image_link],
          name: [user.name],
          sex: [user.sex],
          date_of_birthday: [user.date_of_birthday],
          date_created: [user.date_created],
          is_update: [false],
          is_show_password: [false],
        });
        control.push(grp);
      }
      this.paging.limit =  res.body.paging.limit;
      this.paging.offset = res.body.paging.offset;
      this.paging.total = res.body.paging.total;
    });
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  // transition() {
  //   const parameters = {
  //     offset: this.paging.offset - 1,
  //     limit: this.paging.limit
  //   };
  //   if (this.order.orderBy) {
  //     parameters['orderBy'] = this.order.orderBy;
  //     parameters['orderType'] = this.order.getOrderType();
  //   }
  //   if (this.txtSearch && this.txtSearch.trim().length > 0) {
  //     parameters['search'] = this.txtSearch;
  //   }
  //   this.router.navigate(['/user'], {
  //     queryParams: parameters
  //   });
  // }

  get getFormData(): FormArray {
    return <FormArray>this.userFormGroup.get('data');
  }

  addUser() {
    this.isEdit = true;
    this.isCreate = true;
    const control = <FormArray>this.userFormGroup.get('data');
    if (control.length <= 0 || control.at(control.length - 1).get('id')) {
      control.push(this.initForm());
    }
  }

  save(index: number) {
    const control = <FormArray>this.userFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    const value: User = control.at(index).value;
    if (value.id) {
      this.userService.update(value).subscribe(res => {
          this.loadAll();
      });
      this.isEdit = false;
      this.isCreate = false;
    } else {
      this.userService.create(value).subscribe(res => {
          this.loadAll();
      });
      this.isEdit = false;
      this.isCreate = false;
    }
    // console.log()
  }

  onEdit(index: number) {
    const control = <FormArray>this.userFormGroup.get('data');
    control.at(index).get('is_update').setValue(true);
    this.selectedUser = control.at(index).value;
    this.isEdit = true;
    this.isCreate = false;
    console.log(control);
  }

  onCancelEdit(index: number) {
    const control = <FormArray>this.userFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    // reload
    this.initTable();
    this.isEdit = false;
    this.isCreate = false;
  }

  remove(modal, index: number) {
    const control = <FormArray>this.userFormGroup.get('data');
    this.selectedUser = control.at(index).value;
    this.modalService.open(modal, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static'
    }).result.then((result) => {
      if (result === 'delete') {
        if (this.selectedUser.id) {
          this.userService.delete(this.selectedUser.id).subscribe(res => {
            if ((this.paging.offset + this.getFormData.controls.length) - this.paging.offset === 1 ) {
              this.paging.page = this.paging.page - 1;
              this.loadAll();
            } else {
              this.loadAll();
            }
          });
        } else {
          this.notificationService.error('Thao tác không hợp kệ');
        }
      }
    }, (reason) => {
    });
    console.log(this.selectedUser);
  }

  openViewCertPopup(modal, user) {
    this.selectedUser = user;
    this.modalService.open(modal, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static'
    }).result.then((result) => {
    }, (reason) => {
    });
  }
}
