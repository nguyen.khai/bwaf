import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {ReportComponent} from '@app/views/report/report.component';
import {Paging} from '@app/shared/model/base-respone.model';
import {ObjectWebsiteEditComponent} from '@app/views/object/website/object-website.edit.component';
import {WebsiteResolve} from '@app/views/object/website/object-website.route';
import {ReportEditComponent} from '@app/views/report/report.edit.component';
import {Injectable} from '@angular/core';
import {IWebsite, Website} from '@app/shared/model/website.model';
import {ObjectWebsiteService} from '@app/views/object/website/object-website.service';
import {Observable, of} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {HttpResponse} from '@angular/common/http';
import {ReportService} from '@app/views/report/report.service';
import {IReportModel, ReportModel} from '@app/shared/model/report.model';

@Injectable({ providedIn: 'root' })
export class ReportResolve implements Resolve<ReportModel> {
  constructor(private service: ReportService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IReportModel> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ReportModel>) => response.ok),
        map((report: HttpResponse<ReportModel>) => report.body)
      );
    }
    return of(new ReportModel());
  }
}


export const reportRoute: Routes = [
  {
    path: '',
    component: ReportComponent,
    data: {
      title: 'Report',
      pagingParams: new Paging()
    }
  },
  {
    path: ':id',
    component: ReportEditComponent,
    resolve: {
      report: ReportResolve
    }
  }
];
