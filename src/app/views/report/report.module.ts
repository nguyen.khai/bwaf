import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {reportRoute} from '@app/views/report/report.route';
import {SharedModule} from '@app/shared';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CollapseModule} from 'ngx-bootstrap';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ReportComponent} from '@app/views/report/report.component';
import {ReportEditComponent} from '@app/views/report/report.edit.component';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {NgMultiSelectModule} from '@app/shared/ng-mutiselect';

const ENTITY_STATE = [...reportRoute];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ENTITY_STATE),
    FormsModule,
    CollapseModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    NgMultiSelectModule
  ],
  declarations: [
    ReportComponent,
    ReportEditComponent,
  ],
  entryComponents: [
    ReportComponent,
    ReportEditComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class BwafReportModule {
}

