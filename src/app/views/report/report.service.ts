import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IPaging} from '@app/shared/model/base-respone.model';
import {IReportModel} from '@app/shared/model/report.model';
import {createRequestOption} from '@app/shared';
import {IWebsite} from '@app/shared/model/website.model';
import {IPeriodModel} from '@app/shared/model/period.model';

interface PagingResponse {
  data: IReportModel[];
  paging: IPaging;
}
type EntityResponseType = HttpResponse<IReportModel>;


@Injectable({
  providedIn: 'root'
})
export class ReportService {
  public resourceUrl = environment.apiUrl + '/report';
  constructor(
    protected http: HttpClient
  ) {}

  get(req?: any): Observable<HttpResponse<PagingResponse>> {
    const options = createRequestOption(req);
    return this.http.get<any>(this.resourceUrl, {params: options, observe: 'response'});
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  create(deliveryInfo: IReportModel): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(deliveryInfo);
    return this.http
      .post<IReportModel>(this.resourceUrl, copy, { observe: 'response' });
  }
  protected convertDateFromClient(reportModel: IReportModel): IReportModel {
    const copy: IReportModel = Object.assign({}, reportModel, {});
    return copy;
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IReportModel>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  update(deliveryInfo: IReportModel, id): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(deliveryInfo);
    return this.http
      .put<IReportModel>(this.resourceUrl + `/${id}`, copy, { observe: 'response' });
  }

  getPeriod(): Observable<HttpResponse<IPeriodModel[]>> {
    return this.http.get<any>(environment.apiUrl + '/period', {observe: 'response'});

  }

}
