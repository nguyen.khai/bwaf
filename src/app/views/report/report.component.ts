import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ReportService} from '@app/views/report/report.service';
import {IReportModel} from '@app/shared/model/report.model';
import {IPaging} from '@app/shared/model/base-respone.model';
import {getParameterPaging, OrderParam, PAGING_PER_PAGE} from '@app/shared/constants/common.model';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {FormArray, FormBuilder, Validators} from '@angular/forms';
import {ObjectAccountService} from '@app/views/object/account/object-account.service';
import {ObjectWebsiteService} from '@app/views/object/website/object-website.service';
import {ObjectMailSenderService} from '@app/views/object/mail-sender/object-mail-sender.service';
import {IMailSender} from '@app/shared/model/mail-sender.model';
import {IAccount} from '@app/shared/model/account.model';
import {IPeriodModel} from '@app/shared/model/period.model';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./report.component.scss'],
})
export class ReportComponent implements OnInit {
  PAGING_PER_PAGE = PAGING_PER_PAGE;
  reports: IReportModel[] = [];
  paging: IPaging;
  add: boolean = false;
  keywordSearch: string;
  selectedReport: IReportModel;
  editing: boolean[] = [];
  editStatus = false;
  order: OrderParam = new OrderParam(null, false);
  selectedAccount = [];
  public PeriodOption = [
    {type: 'daily', value: '1', label: 'Daily'},
    {type: 'weekly', value: '7', label: 'Weekly'},
    {type: 'monthly', value: '30', label: 'Monthly'},
    {type: 'yearly', value: '365', label: 'Yearly'},
  ];

  reportFormTooltipView: boolean[] = [];
  reportTooltip = false;
  reportForm = this.fb.group({
    id: [null],
    name: ['', [Validators.required, Validators.maxLength(255)]],
    period: [this.PeriodOption[0].value],
    account_id: this.fb.array([]),
    mail_sender_id: [null],
    state: [0],
    report_form: ['']
  });
  mailsenders: IMailSender[];
  accounts: any;
  periods: IPeriodModel[];
  accountSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    noDataAvailablePlaceholderText: 'Not Found',
    allowSearchFilter: false,
    enableCheckAll: false,
    defaultOpen: false
  };

  constructor(
    private reportService: ReportService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    protected notificationService: ToastrCustomService,
    private fb: FormBuilder,
    public objectAccountService: ObjectAccountService,
    public objectMailSenderService: ObjectMailSenderService,
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.paging = data.pagingParams;
    });
  }

  ngOnInit(): void {
    this.loadAll();
    this.objectAccountService.query().subscribe(res => {
      this.accounts = res.body.data;
    });
    this.objectMailSenderService.query({}).subscribe(res => {
      this.mailsenders = res.body.data;
    });
    this.reportService.getPeriod().subscribe(res => {
      this.periods = res.body;
    });
  }

  addReport() {
    this.add = true;
  }

  onSearch() {
    this.paging.page = 1;
    if (this.keywordSearch) {
      this.keywordSearch = this.keywordSearch.trim();
    }
    this.loadAll();
  }

  loadAll() {
    this.paging = getParameterPaging(this.paging);
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit,
      search: this.keywordSearch
    };
    if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }
    this.reportService.get(parameters).subscribe(res => {
      this.reports = res.body.data;
      this.paging.limit = res.body.paging.limit;
      this.paging.offset = res.body.paging.offset;
      this.paging.total = res.body.paging.total;
      this.editing = new Array(this.reports.length);
      this.editing.fill(false);
      this.reportFormTooltipView = new Array(this.reports.length);
      this.reportFormTooltipView.fill(false);
      this.reports.forEach(() => {
        this.editing.push(false);
      });
      this.paging.limit = res.body.paging.limit;
      this.paging.offset = res.body.paging.offset;
      this.paging.total = res.body.paging.total;
    });
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }

  transition() {
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit,
      search: this.keywordSearch
    };
    if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }
    this.router.navigate(['/report'], {
      queryParams: parameters
    });
    this.loadAll();
  }

  delete(modal, index) {
    this.selectedReport = this.reports[index];
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      if (result === 'delete') {
        if (index !== undefined) {
          this.reportService.delete(this.selectedReport.id).subscribe(res => {
            if ((this.paging.offset + this.reports.length) - this.paging.offset === 1 ) {
              this.paging.page = this.paging.page - 1;
              this.loadAll();
            } else {
              this.loadAll();
            }
          });
        } else {
          this.notificationService.error('Thao tác không hợp kệ');
        }
      }
    }, (reason) => {
    });
  }

  save() {
    this.add = false;
    this.reportService.create(this.reportForm.value).subscribe(res => {
      this.reset();
    });
    this.loadAll();
  }

  openReportForm(content) {
    this.modalService.open(content, {
      windowClass: 'huge-modal',
      size: 'xl',
      scrollable: true
    }).result.then((result) => {
    }, (reason) => {
    });
  }

  updateAccount(event) {
    const control = <FormArray>this.reportForm.get('account_id');
    if (event.target.checked === false) {
      control.removeAt(control.value.findIndex(i => i === event.target.value));
    }
    if (event.target.checked === true) {
      control.push(this.fb.control(event.target.value));
    }
  }

  selectAccount(event) {
    const control = <FormArray>this.reportForm.get('account_id');
    control.push(this.fb.control(event.id));
  }

  unselectAccount(event) {
    const control = <FormArray>this.reportForm.get('account_id');
    control.removeAt(control.value.findIndex(i => i === event.id));
  }

  viewReportForm(content, selectReportForm) {
    this.selectedReport = selectReportForm;
    this.modalService.open(content, {
      windowClass: 'huge-modal',
      size: 'xl',
      scrollable: true
    }).result.then((result) => {
    }, (reason) => {
    });
  }

  editSave(index) {
    const paramerter = this.reportForm.value;
    paramerter.period = parseInt(paramerter.period);
    paramerter.mail_sender_id = parseInt(paramerter.mail_sender_id);
    this.reportService.update(this.reportForm.value, this.reports[index].id).subscribe(res => {
      this.editing[index] = false;
      this.editStatus = false;
      this.reset();
    });
  }

  reset() {
    const controlAccount = <FormArray>this.reportForm.get('account_id');
    controlAccount.clear();
    this.reportForm.reset({
      // id: null,
      name: '',
      period: this.PeriodOption[0].value,
      account_id: this.fb.array([]),
      mail_sender_id: null,
      state: 0,
      report_form: ''
    });
    this.selectedAccount = [];
    this.loadAll();
  }

  editCancel(index) {
    this.editing[index] = false;
    this.editStatus = false;
    this.reset();
    this.loadAll();
  }

  changeEditStatus(index) {
    this.editing[index] = !this.editing[index];
    for (let i = 0; i < this.accounts.length; i++) {
      for (let j = 0; j < this.reports[index].account.length; j++) {
        if (this.accounts[i].name === this.reports[index].account[j]) {
          this.accounts[i].checked = true;
          break;
        }
      }
    }
    this.reportForm.patchValue({
      'name': this.reports[index].name,
      'state': this.reports[index].state,
      'report_form': this.reports[index].report_form
    });
    const controlAccount = <FormArray>this.reportForm.get('account_id');
    controlAccount.clear();
    const mailSenderIndex = this.mailsenders.findIndex(i => i.email === this.reports[index].mail_sender);
    if (mailSenderIndex !== -1) {
      this.reportForm.patchValue({
        'mail_sender_id': this.mailsenders[mailSenderIndex].id,
      });
    }
    const periodIndex = this.periods.findIndex(i => i.period_name === this.reports[index].period);
    if (periodIndex !== -1) {
      this.reportForm.patchValue({
        'period': this.periods[periodIndex].id,
      });
    }
    this.reports[index].account.forEach(value => {
      const accountIndex = this.accounts.findIndex(i => i.name === value);
      controlAccount.push(this.fb.control(this.accounts[accountIndex].id));
      this.selectedAccount.push({
        id: this.accounts[accountIndex].id,
        name: this.accounts[accountIndex].name
      });
    });
    this.editStatus = true;
  }

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  disableValue(list, index) {
    for (let i = 0; i < list.length; i++) {
      if (index !== i) {
        list[i] = false;
      }
    }
  }
}
