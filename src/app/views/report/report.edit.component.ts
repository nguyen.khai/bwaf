import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IReportModel} from '@app/shared/model/report.model';
import {IMailSender} from '@app/shared/model/mail-sender.model';
import {IAccount} from '@app/shared/model/account.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {FormArray, FormBuilder} from '@angular/forms';
import {ObjectAccountService} from '@app/views/object/account/object-account.service';
import {ObjectMailSenderService} from '@app/views/object/mail-sender/object-mail-sender.service';
import {ReportService} from '@app/views/report/report.service';

@Component({
  selector: 'app-report-edit',
  templateUrl: './report.edit.component.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
    .huge-modal .modal-dialog {
      height: 100%;
    }
    .huge-modal .modal-content {
      height: 80%;
    }
  `]

})
export class ReportEditComponent implements OnInit {
  public PeriodOption = [
    {type: 'daily', value: 'daily', label: 'Daily'},
    {type: 'weekly', value: 'weekly', label: 'Weekly'},
    {type: 'monthly', value: 'monthly', label: 'Monthly'},
    {type: 'yearly', value: 'yearly', label: 'Yearly'},
  ];

  report: IReportModel;
  reportForm = this.fb.group({
    name: [''],
    period: [this.PeriodOption[0].value],
    account_id: this.fb.array([
    ]),
    mail_sender_id: [null],
    state: [0],
    report_form: ['']
  });
  mailsenders: IMailSender[];
  accounts: any;
  accountIDs: number[];
  constructor(
    private reportService: ReportService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    protected notificationService: ToastrCustomService,
    private fb: FormBuilder,
    public objectAccountService: ObjectAccountService,
    public objectMailSenderService: ObjectMailSenderService,

  ) {
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ report }) => {
      this.report = report;
    });
    this.objectAccountService.query().subscribe(res => {
      this.accounts = res.body.data;
      for (let i = 0; i < this.accounts.length; i++) {
        for (let j = 0; j < this.report.account.length; j++) {
          if (this.accounts[i].id === this.report.account[j]) {
            this.accounts[i].checked = true;
            break;
          }
        }
      }
    });
    this.objectMailSenderService.query({}).subscribe(res => {
      this.mailsenders = res.body.data;
    });
    this.reportForm.patchValue({
      'name': this.report.name,
      'period': this.report.period,
      'mail_sender_id': this.report.mail_sender,
      'state': this.report.state,
      'report_form': this.report.report_form
    });
    const controlAccount = <FormArray>this.reportForm.get('account_id');
    controlAccount.clear();
    this.report.account.forEach(value => {
      controlAccount.push(this.fb.control(value));
    });
  }

  updateAccount(event) {
    const control = <FormArray>this.reportForm.get('account_id');
    if (event.target.checked === false) {
      control.removeAt(control.value.findIndex(i => i === parseInt(event.target.value, 10)));
    }
    if (event.target.checked === true) {
      control.push(this.fb.control(event.target.value));
    }
    console.log('reportForm', this.reportForm.value);
  }

  openReportForm(content) {
    this.modalService.open(content, {
      windowClass: 'huge-modal',
      size: 'xl',
      scrollable: true
    }).result.then((result) => {
    }, (reason) => {
    });
  }

  save() {
    this.reportService.update(this.reportForm.value, this.report.id).subscribe(res => {
      this.previousState();
    });
  }

  previousState() {
    window.history.back();
  }


}
