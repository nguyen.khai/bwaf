import { Component, ElementRef, OnInit, Renderer } from "@angular/core";
import { JhiEventManager } from "ng-jhipster";
import { LoginService } from "@app/core/login/login.service";
import { StateStorageService } from "@app/core/auth/state-storage.service";
import { Router } from "@angular/router";
import { FormBuilder, Validators } from "@angular/forms";
import { LocalStorageService, SessionStorageService } from "ngx-webstorage";

@Component({
  templateUrl: "login.component.html",
})
export class LoginPageComponent implements OnInit {
  authenticationError: boolean;
  rememberMe: boolean;
  credentials: any;
  checkOTP = false;
  loginForm = this.fb.group({
    username: ["", Validators.required],
    password: ["", [Validators.required]],
  });

  constructor(
    private eventManager: JhiEventManager,
    private loginService: LoginService,
    private stateStorageService: StateStorageService,
    private elementRef: ElementRef,
    private renderer: Renderer,
    private router: Router,
    private localStorage: LocalStorageService,
    private sessionStorage: SessionStorageService,
    private fb: FormBuilder
  ) {
    this.credentials = {};
  }

  ngOnInit(): void {
    const token =
      this.localStorage.retrieve("authenticationToken") ||
      this.sessionStorage.retrieve("authenticationToken");
    if (token) {
      this.router.navigate(["/"]);
    }
  }

  cancel() {
    this.credentials = {
      username: null,
      password: null,
      rememberMe: true,
    };
    this.authenticationError = false;
  }

  login() {
    let DATA = {};
    DATA = {
      username: this.loginForm.get("username").value,
      password: this.loginForm.get("password").value,
      rememberMe: this.rememberMe,
      otp: this.checkOTP ? this.loginForm.get("OTP").value : "",
    };
    this.loginService
      .login(DATA)
      .then((data: { status: number; message: string }) => {
        console.log(data);
        if (data.status === 401) {
          this.loginForm = this.fb.group({
            ...this.loginForm.controls,
            OTP: ["", Validators.required],
          });
          this.checkOTP = true;
        } else {
          this.checkOTP = false;
          this.authenticationError = false;
          this.router.navigate([""]);
          this.eventManager.broadcast({
            name: "authenticationSuccess",
            content: "Sending Authentication Success",
          });

          // previousState  was set in the authExpiredInterceptor before being redirected to login modal.
          // since login is successful, go to stored previousState and clear previousState
          const redirect = this.stateStorageService.getUrl();
          if (redirect) {
            this.stateStorageService.storeUrl(null);
            this.router.navigate([redirect]);
          }
        }
      })
      .catch(() => {
        this.authenticationError = true;
      });
  }

  register() {
    this.router.navigate(["/register"]);
  }

  requestResetPassword() {
    this.router.navigate(["/reset", "request"]);
  }
}
