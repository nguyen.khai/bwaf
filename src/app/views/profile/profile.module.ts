import { SharedModule } from "@app/shared";
import { RouterModule } from "@angular/router";
import { profileRoute } from "@app/views/profile/profile.route";
import { ProfileComponent } from "@app/views/profile/profile.component";
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { AvatarModule } from "ngx-avatar";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CollapseModule } from "ngx-bootstrap";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { QRCodeModule } from "angularx-qrcode";

const ENTITY_STATES = [...profileRoute];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(ENTITY_STATES),
    AvatarModule,
    FormsModule,
    CollapseModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    QRCodeModule,
  ],
  declarations: [ProfileComponent],
  entryComponents: [ProfileComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class BwafProfileModule {
  constructor(private translate: TranslateService) {}
}
