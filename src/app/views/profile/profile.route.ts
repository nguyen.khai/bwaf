import {Routes} from '@angular/router';
import {ProfileComponent} from '@app/views/profile/profile.component';

export const profileRoute: Routes = [
  {
    path: '',
    component: ProfileComponent
  }
]
