import { Injectable } from "@angular/core";
import { environment } from "@env/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { IUser } from "@app/shared/model/user.model";

@Injectable({
  providedIn: "root",
})
export class ProfileService {
  public resourceUrl = environment.apiUrl + "/profile";
  public resourceUrlQR = environment.apiUrl + "/profile/2fa";
  constructor(protected http: HttpClient) {}

  get(): Observable<HttpResponse<IUser>> {
    return this.http.get<any>(this.resourceUrl, { observe: "response" });
  }

  update(profile: any): Observable<HttpResponse<any>> {
    return this.http.put<any>(this.resourceUrl, profile, {
      observe: "response",
    });
  }

  changePassword(password: {}): Observable<HttpResponse<any>> {
    return this.http.put<any>(this.resourceUrl + "/password", password, {
      observe: "response",
    });
  }
  getQR(): Observable<HttpResponse<any>> {
    return this.http.get<any>(this.resourceUrlQR, { observe: "response" });
  }
  updateTwoFactor(payload: any): Observable<HttpResponse<any>> {
    return this.http.post(this.resourceUrlQR, payload, {
      observe: "response",
    });
  }
  changeTwoFactor(payload: any): Observable<HttpResponse<any>> {
    return this.http.put(this.resourceUrlQR, payload, {
      observe: "response",
    });
  }
}
