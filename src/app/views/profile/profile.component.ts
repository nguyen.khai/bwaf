import { Component, OnDestroy, OnInit } from "@angular/core";
import { IUser, User } from "@app/shared/model/user.model";
import { ProfileService } from "@app/views/profile/profile.service";
import { FormBuilder, Validators } from "@angular/forms";
import * as moment from "moment";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent implements OnInit, OnDestroy {
  sexes = [
    { name: "Nam", value: "Nam" },
    { name: "Nữ", value: "Nữ" },
    { name: "Khác", value: "Khác" },
  ];
  qrdata = "";
  checkQR = true;
  user: IUser = new User();

  profileForm = this.fb.group({
    user_name: [
      "",
      [Validators.required, Validators.pattern(/^[a-zA-Z0-9_.]+$/)],
    ],
    name: [
      "",
      [Validators.required, Validators.maxLength(30), Validators.minLength(6)],
    ],
    phone_number: [
      "",
      [Validators.required, Validators.pattern(/^(0|\+84)\d{9}$/)],
    ],
    sex: [this.sexes[0].value],
    date_of_birthday: [null, Validators.required],
    email: ["", Validators.email],
    image_link: [""],
  });
  TowFactorAuthenticationForm = this.fb.group({
    checkTwoFactorAuthentication: true,
    security_code: ["", [Validators.required]],
    QR_password: [
      "",
      [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20),
        Validators.pattern(
          /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,20}$/
        ),
      ],
    ],
  });
  passwordForm = this.fb.group({
    current_password: ["", [Validators.required]],
    new_password: [
      "",
      [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20),
        Validators.pattern(
          /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,20}$/
        ),
      ],
    ],
    confirm_new_password: [
      "",
      [Validators.required, Validators.minLength(8), Validators.maxLength(20)],
    ],
  });

  constructor(
    private profileService: ProfileService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.loadAll();
    this.TowFactorAuthenticationForm.get("security_code").disable();
  }

  ngOnDestroy(): void {}

  loadAll() {
    this.profileService.get().subscribe((data) => {
      this.user = data.body;
      this.profileForm.patchValue({
        user_name: this.user.user_name,
        name: this.user.name,
        phone_number: this.user.phone_number,
        sex: this.user.sex,
        date_of_birthday: this.user.date_of_birthday,
        email: this.user.email,
        image_link: this.user.image_link,
      });
      this.cancelTwoFactor();
      if (this.user.date_of_birthday) {
        this.profileForm.patchValue({
          date_of_birthday: moment(this.user.date_of_birthday, "DD/MM/YYYY"),
        });
      }
    });
  }

  submitProfile() {
    this.profileForm.patchValue({
      date_of_birthday:
        this.profileForm.value.date_of_birthday.format("DD/MM/YYYY"),
    });
    this.profileService.update(this.profileForm.value).subscribe((data) => {
      this.loadAll();
    });
  }
  openQRCode(modal) {
    this.profileService.getQR().subscribe((data) => {
      this.modalService.open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "sm",
        centered: true,
      });
      this.qrdata = data.body;
      this.TowFactorAuthenticationForm.get("security_code").enable();
      this.checkQR = false;
    });
  }
  changeSwitch() {
    const payload = {
      two_fa_status: this.TowFactorAuthenticationForm.get(
        "checkTwoFactorAuthentication"
      ).value
        ? 1
        : 0,
    };
    this.profileService.changeTwoFactor(payload).subscribe((data) => {
      this.loadAll();
    });
  }
  changePassword() {
    const params = {
      old_password: this.passwordForm.value.current_password,
      password: this.passwordForm.value.new_password,
    };
    this.profileService.changePassword(params).subscribe((data) => {
      this.loadAll();
    });
  }
  cancelTwoFactor() {
    this.TowFactorAuthenticationForm.reset();
    this.TowFactorAuthenticationForm.get(
      "checkTwoFactorAuthentication"
    ).setValue(this.user.two_fa_status === 1);
  }
  changeTowFactorAuthentication() {
    const payload = {
      url: this.qrdata,
      password: this.TowFactorAuthenticationForm.get("QR_password").value,
      otp: this.TowFactorAuthenticationForm.get("security_code").value,
    };
    this.profileService.updateTwoFactor(payload).subscribe((data) => {
      this.TowFactorAuthenticationForm = this.fb.group({
        checkTwoFactorAuthentication: true,
        security_code: ["", [Validators.required]],
        QR_password: [
          "",
          [
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(20),
            Validators.pattern(
              /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,20}$/
            ),
          ],
        ],
      });
    });
  }
  cancelChangePassword() {
    this.passwordForm.reset();
  }
}
