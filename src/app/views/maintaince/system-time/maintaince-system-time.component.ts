import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as momenttz from 'moment-timezone';
import {ISystemTime} from '@app/shared/model/system-time.model';
import {MaintainceSystemTimeService} from '@app/views/maintaince/system-time/maintaince-system-time.service';
import {DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE} from 'ng-pick-datetime';
import {MomentDateTimeAdapter} from 'ng-pick-datetime-moment';
import {OWL_COMMON_DATETIME_FORMAT} from '@app/shared/constants/common.model';
import * as moment from 'moment';

@Component({
  selector: 'app-maintaince-system-time',
  templateUrl: './maintaince-system-time.component.html',
  styleUrls: ['./maintaince-system-time.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: OWL_COMMON_DATETIME_FORMAT}
  ]

})
export class MaintainceSystemTimeComponent implements OnInit {

  timezones: TimezoneData[];
  ntpServers = [
    'times.windows.com',
    'ntp.ubuntu.com'
  ];
  systemTime: ISystemTime;
  time: Date;
  currentOffsetHour;
  currentOffsetMin;
  timeZone: string[];

  suggestions: string[] = [];


  public static getTimezonesNames(timezoneNames: string[]): TimezoneData[] {
    const arr: TimezoneData[] = [];
    const names = timezoneNames;
    for (const name of names) {
      if ((name.indexOf('/') < 0 && name !== 'UTC') || name.startsWith('Etc/')) {
        continue;
      }
      const data = new TimezoneData();
      data.tzName = name;
      data.tzPresentationName = momenttz.tz(name).format('Z');
      arr.push(data);
    }
    arr.sort((a, b) => {
      if (a.tzPresentationName === b.tzPresentationName) {
        if (a.tzName === 'UTC') {
          return -1;
        }
        return a.tzName === b.tzName ? 0 : (a.tzName > b.tzName ? 1 : -1);
      }
      const afc = a.tzPresentationName.charAt(0);
      const bfc = b.tzPresentationName.charAt(0);
      if (afc === '-') {
        if (bfc === '+') {
          return -1;
        }
        return a.tzPresentationName > b.tzPresentationName ? -1 : 1;
      }
      if (bfc === '-') {
        return 1;
      }
      return a.tzPresentationName > b.tzPresentationName ? 1 : -1;
    });
    arr.forEach(a => a.tzPresentationName = `(GMT ${a.tzPresentationName}) ${a.tzName}`);
    return arr;
  }

  constructor(
    private systemTimeService: MaintainceSystemTimeService
  ) {
  }

  ngOnInit() {
    this.systemTimeService.getTimezone().subscribe(res => {
      this.timeZone = res.body;
      this.timezones = MaintainceSystemTimeComponent.getTimezonesNames(this.timeZone);
      this.systemTime = {
        server: 'ntp.ubuntu.com',
        timezone: this.timezones[0].tzName,
        timezoneStr: this.timezones[0].tzPresentationName
      };
      this.loadAll();
    });
  }

  loadAll() {
    this.systemTimeService.getCurrentSystemTime({
      // server: this.systemTime.server ? this.systemTime.server : 'ntp.ubuntu.com',
      // timezone: this.systemTime.timezone ? this.systemTime.timezone : ''
    }).subscribe(res => {
      this.systemTime.server = res.body.server ? res.body.server : 'ntp.ubuntu.com';
      this.systemTime.timezone = MaintainceSystemTimeComponent.getTimezonesNames([res.body.timezone])[0].tzName;
      this.systemTime.timezoneStr = MaintainceSystemTimeComponent.getTimezonesNames([res.body.timezone])[0].tzPresentationName;
      this.systemTime.datetime = res.body.datetime;
      this.time = new Date(this.systemTime.datetime);
      this.currentOffsetHour = this.getTimeZoneOffsetHour();
      this.currentOffsetMin = 0;
    });
  }

  changeSystemTime() {
    this.systemTimeService.changeSystemTimeSetting({
      server: this.systemTime.server ? this.systemTime.server : 'ntp.ubuntu.com',
      timezone: this.systemTime.timezone ? this.systemTime.timezone : ''
    }).subscribe(res => {
      this.systemTime.server = res.body.server ? res.body.server : 'ntp.ubuntu.com';
      this.systemTime.timezone = MaintainceSystemTimeComponent.getTimezonesNames([res.body.timezone])[0].tzName;
      this.systemTime.timezoneStr = MaintainceSystemTimeComponent.getTimezonesNames([res.body.timezone])[0].tzPresentationName;
      this.systemTime.datetime = res.body.datetime;
      this.time = new Date(this.systemTime.datetime);
      this.currentOffsetHour = this.getTimeZoneOffsetHour();
      this.currentOffsetMin = 0;
    });

  }

  suggest() {
    this.suggestions = this.ntpServers
      .filter(c => c.startsWith(this.systemTime.server))
      .slice(0, 5);
  }

  onTimeZoneChange() {
    const changeHour = parseInt(this.getTimeZoneOffsetHour(), 10) - this.currentOffsetHour;
    const changeMin = parseInt(this.getTimeZoneOffsetMin(), 10) - this.currentOffsetMin;
    this.currentOffsetHour = this.getTimeZoneOffsetHour();
    this.currentOffsetMin = this.getTimeZoneOffsetMin();
    this.time.setMinutes(this.time.getMinutes() + changeMin);
    this.time.setHours(this.time.getHours() + changeHour);
    this.time = new Date(this.time);
  }

  getTimeZoneOffsetHour() {
    return this.systemTime.timezoneStr.match(/[+-]\d+/)[0];
  }

  getTimeZoneOffsetMin() {
    const sign = this.systemTime.timezoneStr.match(/[+-]/)[0];
    return sign + this.systemTime.timezoneStr.match(/:\d+/)[0].replace(':', '');
  }

  save() {
    this.systemTime.datetime = moment(this.time).format('YYYY-MM-DD HH:mm:ss');
    this.systemTimeService.update({
      server: this.systemTime.server,
      timezone: this.systemTime.timezone,
      datetime: this.systemTime.datetime
    }).subscribe(res => {
    });
  }

  changeDateTime(event) {
    this.time = new Date(event.value._d);
  }

  getGMT() {
    this.changeSystemTime();
  }

  cancel() {
    this.loadAll();
  }
}

class TimezoneData {
  tzName: string;
  tzPresentationName: string;
}
