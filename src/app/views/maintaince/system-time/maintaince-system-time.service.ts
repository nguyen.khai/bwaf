import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IUser} from '@app/shared/model/user.model';
import {ISystemTime} from '@app/shared/model/system-time.model';


@Injectable({
  providedIn: 'root'
})
export class MaintainceSystemTimeService {
  public resourceUrl = environment.apiUrl;
  constructor(
    protected http: HttpClient
  ) {}
  getCurrentSystemTime(req?: any): Observable<HttpResponse<ISystemTime>> {
    return this.http.get<any>(this.resourceUrl + '/system-time',  {params: req, observe: 'response'});
  }

  changeSystemTimeSetting(req?: any): Observable<HttpResponse<ISystemTime>> {
    return this.http.get<any>(this.resourceUrl + '/system-time-ntp', { params: req, observe: 'response'});
  }

  update(params: ISystemTime): Observable<HttpResponse<any>> {
    return this.http.post<any>(this.resourceUrl + '/system-time', params, {observe: 'response'});
  }

  changePassword(password: {}): Observable<HttpResponse<any>> {
    return this.http.put<any>(this.resourceUrl + '/password', password, {observe: 'response'});
  }

  getTimezone(): Observable<any> {
    return this.http.get(this.resourceUrl + '/timezone', {observe: 'response'});
  }
}
