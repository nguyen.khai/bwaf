import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@app/shared';
import {ReactiveFormsModule} from '@angular/forms';
import {CollapseModule} from 'ngx-bootstrap';
import {ObjectAccountComponent} from '@app/views/object/account/object-account.component';
import { MaintainceRoutingModule } from './maintaince-routing.module';
import { MaintainceSystemTimeComponent } from './system-time/maintaince-system-time.component';
import {CanvasClockComponent} from '@app/views/maintaince/system-time/canvas-clock/canvas-clock.component';
import {BackupRestoreComponent} from '@app/views/maintaince/backup-restore/backup-restore.component';


@NgModule({
  imports: [MaintainceRoutingModule, CommonModule, SharedModule, ReactiveFormsModule, CollapseModule],
  declarations: [
    MaintainceSystemTimeComponent,
    CanvasClockComponent,
    BackupRestoreComponent
  ],
  exports: [
    CanvasClockComponent
  ]
})
export class MaintainceModule { }
