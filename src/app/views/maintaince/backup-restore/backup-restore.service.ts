import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable, scheduled } from "rxjs";
import { environment } from "@env/environment";
import { createRequestOption } from "@app/shared";
import { IBaseResponse, IPaging } from "@app/shared/model/base-respone.model";
import {
  IHighAvailability,
  IHighAvailabilityMode,
} from "@app/shared/model/high-availability.model";
import {
  IBackupLocal,
  IBackupSchedule,
  IFile,
  IRestoreFile,
} from "@app/shared/model/backup-restore.model";

type EntityResponseType = HttpResponse<IHighAvailability>;
type ArrayResponseType = HttpResponse<IHighAvailabilityMode[]>;
type EntityResponse = HttpResponse<IHighAvailability>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;

interface PagingResponse {
  data: IHighAvailability[];
  paging: IPaging;
}

@Injectable({ providedIn: "root" })
export class BackupRestoreService {
  public createNewSchedule = environment.apiUrl + "/backup-schedule";
  public resourceBackupLocalUrl = environment.apiUrl + "/backup-local";
  public resourceRestoreUrl = environment.apiUrl + "/restore";
  public resourceRestoreDefaultUrl = environment.apiUrl + "/restore-default";

  constructor(protected http: HttpClient) {}

  createNewSCD(
    backupLocal: IBackupLocal
  ): Observable<HttpResponse<IBackupLocal>> {
    return this.http.post<IBackupLocal>(this.createNewSchedule, backupLocal, {
      observe: "response",
    });
  }

  create(backupLocal: IBackupLocal): Observable<HttpResponse<IBackupLocal>> {
    return this.http.post<IBackupLocal>(
      this.resourceBackupLocalUrl,
      backupLocal,
      { observe: "response" }
    );
  }

  createRestoreDefault(userPassword): Observable<HttpResponse<any>> {
    return this.http.post<any>(this.resourceRestoreDefaultUrl, userPassword, {
      observe: "response",
    });
  }

  getRestoreFile(id: number): Observable<HttpResponse<IRestoreFile>> {
    return this.http.get<IRestoreFile>(`${this.resourceRestoreUrl}/${id}`, {
      observe: "response",
    });
  }

  createRestoreByFile(
    id: number,
    password: string
  ): Observable<HttpResponse<any>> {
    return this.http.post<any>(
      `${this.resourceRestoreUrl}/${id}`,
      {
        password: password,
      },
      { observe: "response" }
    );
  }

  getAllRestoreFiles(req?: any): Observable<HttpResponse<IFile[]>> {
    const options = createRequestOption(req);
    return this.http.get<IFile[]>(this.resourceRestoreUrl, {
      params: options,
      observe: "response",
    });
  }

  getAllBackup(
    req?: any
  ): Observable<HttpResponse<{ data: IBackupSchedule[] }>> {
    const options = createRequestOption(req);
    return this.http.get<{ data: IBackupSchedule[] }>(
      this.resourceBackupLocalUrl,
      { params: options, observe: "response" }
    );
  }

  download(id: number): Observable<any> {
    return this.http.get(`${this.resourceRestoreUrl}/${id}/download`, {
      observe: "response",
      responseType: "blob" as "json",
    });
  }

  uploadRestoreFile(file: File): Observable<HttpResponse<IBaseResponse>> {
    const data: FormData = new FormData();
    data.append("backup_file", file);
    return this.http.post<IBaseResponse>(this.resourceRestoreUrl, data, {
      observe: "response",
    });
  }
}
