import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import { ActivatedRoute, Router } from "@angular/router";
import { PERIOD_BACKUP, MODE } from "@app/shared/constants/common.model";
import { BackupRestoreService } from "@app/views/maintaince/backup-restore/backup-restore.service";
import {
  IBackupLocal,
  IBackupSchedule,
  IFile,
  IRestoreFile,
} from "@app/shared/model/backup-restore.model";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { IPeriodModel } from "@app/shared/model/period.model";
import { DatePipe } from "@angular/common";
import * as moment from "moment";

@Component({
  selector: "app-backup-restore",
  templateUrl: "backup-restore.component.html",
  styleUrls: ["backup-restore.component.scss"],
})
export class BackupRestoreComponent implements OnInit {
  dateFormat = new DatePipe("en-US");
  PERIOD_BACKUP = PERIOD_BACKUP;
  LIST_MODE = MODE;
  files: IFile[];
  showConfirmBackupLocal = false;
  showConfirmRestore = false;
  showConfirmRestorePopup = false;
  selectedRestoreFile: IRestoreFile;
  uploadRestoreFile: File;
  periods: IPeriodModel[];
  schedules: IBackupSchedule[] = [];

  backupLocalForm = this.fb.group({
    file_name: ["", Validators.required],
    schedule: [""],
    period: ["Daily"],
    day_start: [""],
    mode: ["compressed"],
  });

  restoreForm = this.fb.group({
    user_password: ["", Validators.required],
  });

  constructor(
    protected notificationService: ToastrCustomService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    protected backupRestoreService: BackupRestoreService,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.loadRestoreFiles();
    this.loadBackup();
  }

  loadRestoreFiles() {
    this.backupRestoreService.getAllRestoreFiles().subscribe((res) => {
      this.files = res.body;
    });
  }

  loadBackup() {
    this.backupRestoreService.getAllBackup().subscribe((res) => {
      this.schedules = res.body.data;
      console.log(this.schedules);
    });
  }

  saveBackupLocal() {
    this.showConfirmBackupLocal = false;
    const backupValue = this.backupLocalForm.value;
    if (!backupValue.schedule) {
      backupValue.period = backupValue.period.id;
    }
    if (backupValue.day_start) {
      backupValue.day_start = this.dateFormat.transform(
        new Date(backupValue.day_start),
        "yyyy-MM-dd"
      );
    } else {
      backupValue.day_start = null;
    }
    backupValue.schedule = backupValue.schedule ? 1 : 0;
    backupValue.period = parseInt(backupValue.period);
    this.backupRestoreService.createNewSCD(backupValue).subscribe((res) => {
      // this.backupLocalForm.get("user_password").setValue("");
    });
  }
  showPeriod(data) {
    if (data.period_id === 1) {
      return "Daily";
    }
    if (data.period_id === 2) {
      return "Weekly";
    }
    if (data.period_id === 3) {
      return "Monthly";
    }
    if (data.period_id === 4) {
      return "Yearly";
    }
  }

  onScheduleChange() {
    if (this.backupLocalForm.get("schedule").value) {
      this.backupLocalForm.get("period").setValidators([Validators.required]);
      this.backupLocalForm
        .get("day_start")
        .setValidators([Validators.required]);
    } else {
      this.backupLocalForm.get("period").setValue(null);
      this.backupLocalForm.get("day_start").setValue(null);
    }
  }

  createRestoreDefault() {
    this.showConfirmRestore = false;
    const userPassword = {
      user_password: this.restoreForm.get("user_password").value,
    };
    this.backupRestoreService
      .createRestoreDefault(userPassword)
      .subscribe((res) => {
        console.log(res.body);
        this.restoreForm.get("user_password").setValue("");
      });
  }

  onShowRestoreFile(modal, id: number) {
    this.backupRestoreService.getRestoreFile(id).subscribe((res) => {
      this.selectedRestoreFile = res.body;
    });

    this.modalService
      .open(modal, { ariaLabelledBy: "modal-basic-title", backdrop: "static" })
      .result.then((result) => {
        if (result === "restore") {
          this.showConfirmRestorePopup = true;
        }
      });
  }

  restoreFile() {
    this.showConfirmRestorePopup = false;
    this.backupRestoreService
      .createRestoreByFile(
        this.selectedRestoreFile.id,
        this.restoreForm.get("user_password").value
      )
      .subscribe((res) => {
        this.restoreForm.get("user_password").setValue("");
        this.modalService.dismissAll();
      });
  }

  onDownload(fileId) {
    this.backupRestoreService.download(fileId).subscribe((res: Response) => {
      let filename;
      const contentDisposition = res.headers.get("content-disposition");
      if (contentDisposition) {
        const parts: string[] = contentDisposition.split(";");
        if (parts && parts.length > 1) {
          const subParts = parts[1].split("=");
          if (subParts && subParts.length > 1) {
            filename = parts[1].split("=")[1];
          }
        }
      }

      const data = res.body;
      // @ts-ignore
      const blob = new Blob([data], { type: res.headers.get("Content-Type") });
      const downloadURL = window.URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = downloadURL;
      link.download = filename ? filename : "ddos_network_export_data.zip";
      link.click();
    });
  }

  onChangeUploadRestoreFile($event) {
    this.uploadRestoreFile = $event.target.files[0];
  }

  onUploadRestoreFile() {
    this.backupRestoreService
      .uploadRestoreFile(this.uploadRestoreFile)
      .subscribe((res) => {});
  }
}
