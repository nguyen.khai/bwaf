import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Paging} from '@app/shared/model/base-respone.model';
import { MaintainceSystemTimeComponent } from './system-time/maintaince-system-time.component';
import {BackupRestoreComponent} from '@app/views/maintaince/backup-restore/backup-restore.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Maintaince'
    },
    children: [
      {
        path: '',
        redirectTo: 'system-time'
      },
      {
        path: 'backup-restore',
        component: BackupRestoreComponent,
        data: {
          title: 'Backup & Restore',
          pagingParams: new Paging()
        }
      },
      {
        path: 'system-time',
        component: MaintainceSystemTimeComponent,
        data: {
          title: 'System Time',
          pagingParams: new Paging()
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaintainceRoutingModule {
}
