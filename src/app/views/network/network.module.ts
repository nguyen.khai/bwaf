import {NgModule} from '@angular/core';
import {NetworkRoutingModule} from './network-routing.module';
import {NetworkComponent} from './network.component';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '@app/shared';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgMultiSelectModule} from "@app/shared/ng-mutiselect";

@NgModule({
    imports: [NetworkRoutingModule, ReactiveFormsModule, SharedModule, NgMultiSelectDropDownModule.forRoot(), FontAwesomeModule, NgMultiSelectModule],
  declarations: [
    NetworkComponent
  ]
})
export class NetworkModule { }
