import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NetworkComponent} from './network.component';
import {Paging} from '@app/shared/model/base-respone.model';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Network Interface'
    },
    children: [
      {
        path: '',
        component: NetworkComponent,
        data: {
          title: '',
          pagingParams: new Paging()
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NetworkRoutingModule {}
