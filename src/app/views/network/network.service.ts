import { Injectable } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "environments/environment";
import { createRequestOption } from "app/shared";
import { IWebsite } from "@app/shared/model/website.model";
import { IPaging } from "@app/shared/model/base-respone.model";
import {
  IBondingVirtualNetwork,
  IBridgesVirtualNetwork,
  IInterface,
  IServiceAccess,
} from "@app/shared/model/interface.model";

type EntityResponseType = HttpResponse<IInterface>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;
type ServiceAccessArrayResponseType = HttpResponse<PagingServiceAccessResponse>;

interface PagingResponse {
  data: IInterface[];
  paging: IPaging;
}

interface PagingServiceAccessResponse {
  data: IServiceAccess[];
  paging: IPaging;
}

interface PagingBridgesVirtualNetworkResponse {
  data: IBridgesVirtualNetwork[];
  paging: IPaging;
}

interface PagingBondingVirtualNetworkResponse {
  data: IBondingVirtualNetwork[];
  paging: IPaging;
}

interface NetworkRoute {
  id?: number;
  name: string;
  type: number;
  destination: string;
  netmask: string;
  gateway: string;
  metric: number;
  interface: string;
  active: number;
}

@Injectable({ providedIn: "root" })
export class NetworkService {
  public resourceUrl = environment.apiUrl + "/interface";
  public virtualNetworkResourceUrl = `${environment.apiUrl}/virtual-interface`;
  public serviceAccessResourceUrl = environment.apiUrl + "/services";

  constructor(protected http: HttpClient) {}

  create(networkInterface: IInterface): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(networkInterface);
    // @ts-ignore
    return this.http.post<IWebsite>(this.resourceUrl, copy, {
      observe: "response",
    });
  }

  update(networkInterface: IInterface): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(networkInterface);
    return this.http.put<IInterface>(
      `${this.resourceUrl}/${networkInterface.name}`,
      copy,
      { observe: "response" }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IInterface>(`${this.resourceUrl}/${id}`, {
      observe: "response",
    });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<PagingResponse>(this.resourceUrl, {
      params: options,
      observe: "response",
    });
  }

  delete(name: string): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${name}`, {
      observe: "response",
    });
  }

  getServiceAccess(name: string): Observable<ServiceAccessArrayResponseType> {
    return this.http.get<PagingServiceAccessResponse>(
      `${this.serviceAccessResourceUrl}/${name}`,
      { observe: "response" }
    );
  }

  createServiceAccess(
    name: string,
    serviceAccess: IServiceAccess
  ): Observable<HttpResponse<IServiceAccess>> {
    const copy = this.convertDateFromClient(serviceAccess);
    return this.http.post<IServiceAccess>(
      `${this.serviceAccessResourceUrl}/${name}`,
      copy,
      { observe: "response" }
    );
  }

  updateServiceAccess(
    id: number,
    serviceAccess: IServiceAccess
  ): Observable<HttpResponse<IServiceAccess>> {
    const copy = this.convertDateFromClient(serviceAccess);
    return this.http.put<IServiceAccess>(
      `${this.serviceAccessResourceUrl}/${id}`,
      copy,
      { observe: "response" }
    );
  }

  deleteServiceAccess(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.serviceAccessResourceUrl}/${id}`, {
      observe: "response",
    });
  }

  getBridgesVirtualInterface(
    req?: any
  ): Observable<HttpResponse<PagingBridgesVirtualNetworkResponse>> {
    const options = createRequestOption(req);
    return this.http.get<PagingBridgesVirtualNetworkResponse>(
      `${this.virtualNetworkResourceUrl}/bridges`,
      { params: options, observe: "response" }
    );
  }

  getBondingVirtualInterface(
    req?: any
  ): Observable<HttpResponse<PagingBondingVirtualNetworkResponse>> {
    const options = createRequestOption(req);
    return this.http.get<PagingBondingVirtualNetworkResponse>(
      `${this.virtualNetworkResourceUrl}/bond`,
      { params: options, observe: "response" }
    );
  }

  addNewBonding(
    virtualInterface: IBondingVirtualNetwork
  ): Observable<HttpResponse<IBondingVirtualNetwork>> {
    const copy = this.convertDateFromClient(virtualInterface);
    return this.http.post<IBondingVirtualNetwork>(
      `${this.virtualNetworkResourceUrl}/bond`,
      copy,
      { observe: "response" }
    );
  }

  saveVirtualNetworkData(
    virtualInterface: IBridgesVirtualNetwork | IBridgesVirtualNetwork,
    type: string
  ): Observable<HttpResponse<IBridgesVirtualNetwork | IBridgesVirtualNetwork>> {
    const copy = this.convertDateFromClient(virtualInterface);
    return this.http.put<IBridgesVirtualNetwork | IBridgesVirtualNetwork>(
      `${this.virtualNetworkResourceUrl}/${
        type === "Bridges" ? "bridges" : "bond"
      }/${virtualInterface.name}`,
      copy,
      { observe: "response" }
    );
  }

  deleteVirtualNetworkData(
    name: string,
    type: string
  ): Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.virtualNetworkResourceUrl}/${
        type === "Bridges" ? "bridges" : "bond"
      }/${name}`,
      { observe: "response" }
    );
  }

  addNewBridges(
    virtualInterface: IBridgesVirtualNetwork
  ): Observable<HttpResponse<IBridgesVirtualNetwork>> {
    const copy = this.convertDateFromClient(virtualInterface);
    return this.http.post<IBridgesVirtualNetwork>(
      `${this.virtualNetworkResourceUrl}/bridges`,
      copy,
      { observe: "response" }
    );
  }

  protected convertDateFromClient(networkInterface: IInterface): IInterface {
    const copy: IInterface = Object.assign({}, networkInterface, {});
    return copy;
  }

  getRoute(): Observable<HttpResponse<any>> {
    return this.http.get<any>(`${environment.apiUrl}/routes`, {
      observe: "response",
    });
  }
  // NetworkRoute[]

  addRoute(route: NetworkRoute): Observable<HttpResponse<any>> {
    return this.http.post<any>(`${environment.apiUrl}/routes`, route, {
      observe: "response",
    });
  }

  updateRoute(route: NetworkRoute, id: number): Observable<HttpResponse<any>> {
    return this.http.put<any>(`${environment.apiUrl}/routes/${id}`, route, {
      observe: "response",
    });
  }

  deleteRoute(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${environment.apiUrl}/routes/${id}`, {
      observe: "response",
    });
  }
}
