import { Component, OnInit } from "@angular/core";
import { IPaging } from "@app/shared/model/base-respone.model";
import {
  AbstractControl,
  FormArray,
  FormBuilder,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  ADDRESSING_MODES,
  IBondingVirtualNetwork,
  IBridgesVirtualNetwork,
  IInterface,
  IP_TYPES,
  IServiceAccess,
  LACP_RATES,
  NETWORK_TYPES,
  PARAMETER_MODES,
  PROTOCOLS,
  VIRTUAL_NETWORK_TYPES,
} from "@app/shared/model/interface.model";
import { NetworkService } from "@app/views/network/network.service";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import {
  NETMASK_V4,
  OrderParam,
  PAGING_PER_PAGE,
} from "@app/shared/constants/common.model";

@Component({
  selector: "app-network",
  templateUrl: "network.component.html",
  styleUrls: ["network.component.scss"],
})
export class NetworkComponent implements OnInit {
  networkInterfaces: IInterface[];
  bridges: IBridgesVirtualNetwork[];
  bonding: IBondingVirtualNetwork[];
  serviceAccesses: IServiceAccess[];
  paging: IPaging;
  serviceAccessFormGroup: FormGroup;
  addIpFormGroup: FormGroup;
  addIpv6FormGroup: FormGroup;
  selectedNetworkInterface: IInterface;
  selectedServiceAccess: IServiceAccess;
  ADDRESSING_MODES = ADDRESSING_MODES;
  LACP_RATES = LACP_RATES;
  PROTOCOLS = PROTOCOLS;
  NETWORK_TYPES = NETWORK_TYPES;
  VIRTUAL_NETWORK_TYPES = VIRTUAL_NETWORK_TYPES;
  PARAMETER_MODES = PARAMETER_MODES;
  PAGING_PER_PAGE = PAGING_PER_PAGE;
  IP_TYPES = IP_TYPES;
  selectedProtocols: string[] = [];
  order: OrderParam = new OrderParam(null, false);
  bridgesOrder: OrderParam = new OrderParam(null, false);
  bondingOrder: OrderParam = new OrderParam(null, false);
  keywordSearch: string;
  isEditing = false;
  isShow: boolean;
  dropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: "value",
    textField: "label",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    itemsShowLimit: 4,
    allowSearchFilter: false,
  };

  interfacesDropdownSettings: IDropdownSettings = {
    singleSelection: false,
    idField: "name",
    textField: "name",
    selectAllText: "Select All",
    unSelectAllText: "Deselect All",
    // itemsShowLimit: 4,
    allowSearchFilter: false,
  };
  networkInterfaceFormGroup = this.fb.group({
    data: this.fb.array([]),
  });
  virtualBridgesFormGroup = this.fb.group({
    data: this.fb.array([]),
  });
  virtualBondingFormGroup = this.fb.group({
    data: this.fb.array([]),
  });

  addVirtualNetworkFormGroup = this.fb.group({
    name: ["", Validators.required],
    type: ["WAN", Validators.required],
    interfaces: [[], Validators.required],
    ipv4_addressing_mode: [""],
    ipv6_addressing_mode: [""],
    ipv4_address: ["", [this.validateIpv4Addresses()]],
    ipv6_address: ["", [this.validateIpv6Addresses()]],
    gateway_ipv4: [""],
    gateway_ipv6: [""],
    dns: [[]],
    preferred_dns: [""],
    alt_dns: [""],
    active: [true],
    status: [true],
    is_update: [false],
    is_show_update_service: [false],
    is_show_add_ipv4: [false],
    is_show_add_ipv6: [false],
    is_show_advanced: [false],
    parameters: [""],
    virtual_network_type: ["Bonding"],
    lacp_rate: [""],
    monitor_interval: ["", [Validators.min(0)]],
    primary_interface: [""],
  });

  routeFormGroup = this.fb.group({
    data: this.fb.array([]),
  });

  NETMASK_V4 = NETMASK_V4;
  NETMASK_V6 = [];
  NETMASK = [];

  constructor(
    public networkServiceService: NetworkService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.activatedRoute.data.subscribe((data) => {
      this.paging = data.pagingParams;
    });
  }

  ngOnInit(): void {
    this.initTable();
    this.initNetmaskV6();
    this.loadRoute();
  }

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  initTable() {
    this.loadAll();
  }

  initNetmaskV6() {
    for (let i = 0; i <= 128; i++) {
      this.NETMASK_V6.push({
        value: i,
        label: "/" + i,
      });
    }
  }

  initForm() {
    return this.fb.group({
      id: [null],
      name: [""],
      type: [""],
      ip_type: [""],
      addressing_mode: [""],
      ip_address: [""],
      netmask: [""],
      gateway: [""],
      dns: [""],
      service_access: [""],
      active: [""],
      is_update: [true],
      is_show_update_service: [true],
    });
  }

  initServiceAccessForm() {
    return this.fb.group({
      id: [null],
      port_from: ["", Validators.required],
      port_to: ["", Validators.required],
      protocol: [""],
      protocolData: [[], Validators.required],
      // active: ['', Validators.required],
      is_update: [true],
    });
  }

  loadAll() {
    this.isEditing = false;
    const parameters = {
      offset: 0,
      limit: 1000,
    };
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters["search"] = this.keywordSearch;
    }
    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }

    this.networkServiceService.query(parameters).subscribe((res) => {
      this.addIpFormGroup = this.fb.group({
        data: this.fb.array([]),
      });
      const AddIpControl = <FormArray>this.addIpFormGroup.get("data");
      const data = this.fb.group({
        ip: ["", Validators.required],
        netmask: [
          "",
          [Validators.required, Validators.max(32), Validators.min(0)],
        ],
      });

      AddIpControl.push(data);

      this.addIpv6FormGroup = this.fb.group({
        data: this.fb.array([]),
      });
      const AddIpv6Control = <FormArray>this.addIpv6FormGroup.get("data");
      const datav6 = this.fb.group({
        ip: ["", Validators.required],
        netmask: [
          "",
          [Validators.required, Validators.max(128), Validators.min(0)],
        ],
      });

      AddIpv6Control.push(datav6);
      this.networkInterfaceFormGroup = this.fb.group({
        data: this.fb.array([]),
      });
      this.networkInterfaces = res.body.data;
      const control = <FormArray>this.networkInterfaceFormGroup.get("data");
      for (const networkInterface of this.networkInterfaces) {
        // const addMode = ADDRESSING_MODES.filter(a => a.ip_type === networkInterface.ip_type && a.type === networkInterface.type);
        // let ip = '', netmask = '';
        // if (networkInterface.ip_address && networkInterface.ip_address.length > 0) {
        //   const tmp = networkInterface.ip_address[0].split('/');
        //   ip = tmp[0];
        //   netmask = tmp[1];
        // }
        // if (networkInterface.ip_type === 4) {
        //   this.NETMASK = NETMASK_V4;
        // } else {
        //   this.NETMASK = this.NETMASK_V6;
        // }
        const grp = this.fb.group({
          name: [networkInterface.name, Validators.required],
          type: [networkInterface.type],
          ipv4_addressing_mode: [networkInterface.ipv4_addressing_mode],
          ipv6_addressing_mode: [networkInterface.ipv6_addressing_mode],
          ipv4_address: [
            networkInterface.ipv4_address
              ? networkInterface.ipv4_address.join(", ")
              : "",
            [this.validateIpv4Addresses()],
          ],
          ipv6_address: [
            networkInterface.ipv6_address
              ? networkInterface.ipv6_address.join(", ")
              : "",
            [this.validateIpv6Addresses()],
          ],
          gateway_ipv4: [networkInterface.gateway_ipv4],
          gateway_ipv6: [networkInterface.gateway_ipv6],
          dns: [networkInterface.dns ? networkInterface.dns.join(", ") : ""],
          preferred_dns: [
            (networkInterface.dns && networkInterface.dns[0]) || "",
          ],
          alt_dns: [(networkInterface.dns && networkInterface.dns[1]) || ""],
          active: [!!networkInterface.active],
          status: [!!networkInterface.status],
          is_update: [false],
          is_show_update_service: [false],
          is_show_add_ipv4: [false],
          is_show_add_ipv6: [false],
          service_access: [""],
        });
        control.push(grp);
      }

      const length = control.length;
      for (let i = 0; i < length; i++) {
        this.networkServiceService
          .getServiceAccess(control.at(i).get("name").value)
          .subscribe((res1) => {
            const serviceAccess = res1.body.data;
            let serviceAccessView = "";
            if (serviceAccess && serviceAccess.length > 0) {
              serviceAccess.forEach((s) => {
                serviceAccessView = serviceAccessView + s.protocol + ", ";
              });
              serviceAccessView = serviceAccessView.substring(
                0,
                serviceAccessView.length - 2
              );
              control.at(i).get("service_access").setValue(serviceAccessView);
            }
          });
      }

      this.paging = res.body.paging;
    });
    this.networkServiceService
      .getBridgesVirtualInterface(parameters)
      .subscribe((res) => {
        this.bridges = res.body.data;
        this.virtualBridgesFormGroup = this.fb.group({
          data: this.fb.array([]),
        });
        const control = <FormArray>this.virtualBridgesFormGroup.get("data");
        control.setValue([]);
        for (const bridge of this.bridges) {
          const grp = this.fb.group({
            name: [bridge.name || "", Validators.required],
            type: [bridge.type || ""],
            interfaces: [bridge.interfaces || ""],
            ipv4_addressing_mode: [bridge.ipv4_addressing_mode || ""],
            ipv6_addressing_mode: [bridge.ipv6_addressing_mode || ""],
            ipv4_address: [
              bridge.ipv4_address ? bridge.ipv4_address.join(", ") : "",
              [this.validateIpv4Addresses()],
            ],
            ipv6_address: [
              bridge.ipv4_address ? bridge.ipv6_address.join(", ") : "",
              [this.validateIpv6Addresses()],
            ],
            gateway_ipv4: [bridge.gateway_ipv4 || ""],
            gateway_ipv6: [bridge.gateway_ipv6 || ""],
            dns: [bridge.dns ? bridge.dns.join(", ") : ""],
            preferred_dns: [(bridge.dns && bridge.dns[0]) || ""],
            alt_dns: [(bridge.dns && bridge.dns[1]) || ""],
            active: [!!bridge.active],
            status: [!!bridge.status],
            is_update: [false],
            is_show_update_service: [false],
            is_show_add_ipv4: [false],
            is_show_add_ipv6: [false],
            service_access: [""],
            advanced: [bridge.advanced],
          });
          control.push(grp);
        }

        const length = control.length;
        for (let i = 0; i < length; i++) {
          this.networkServiceService
            .getServiceAccess(control.at(i).get("name").value)
            .subscribe((res1) => {
              const serviceAccess = res1.body.data;
              let serviceAccessView = "";
              if (serviceAccess && serviceAccess.length > 0) {
                serviceAccess.forEach((s) => {
                  serviceAccessView = serviceAccessView + s.protocol + ", ";
                });
                serviceAccessView = serviceAccessView.substring(
                  0,
                  serviceAccessView.length - 2
                );
                control.at(i).get("service_access").setValue(serviceAccessView);
              }
            });
        }
      });

    this.networkServiceService
      .getBondingVirtualInterface(parameters)
      .subscribe((res) => {
        this.bonding = res.body.data;
        this.virtualBondingFormGroup = this.fb.group({
          data: this.fb.array([]),
        });
        const control = <FormArray>this.virtualBondingFormGroup.get("data");
        control.setValue([]);
        for (const bond of this.bonding) {
          const grp = this.fb.group({
            name: [bond.name || "", Validators.required],
            type: [bond.type || ""],
            interfaces: [bond.interfaces || ""],
            ipv4_addressing_mode: [bond.ipv4_addressing_mode || ""],
            ipv6_addressing_mode: [bond.ipv6_addressing_mode || ""],
            ipv4_address: [
              bond.ipv4_address ? bond.ipv4_address.join(", ") : "",
              [this.validateIpv4Addresses()],
            ],
            ipv6_address: [
              bond.ipv6_address ? bond.ipv6_address.join(", ") : "",
              [this.validateIpv6Addresses()],
            ],
            gateway_ipv4: [bond.gateway_ipv4 || ""],
            gateway_ipv6: [bond.gateway_ipv6 || ""],
            dns: [bond.dns ? bond.dns.join(", ") : ""],
            preferred_dns: [(bond.dns && bond.dns[0]) || ""],
            alt_dns: [(bond.dns && bond.dns[1]) || ""],
            active: [!!bond.active],
            status: [!!bond.status],
            is_update: [false],
            is_show_update_service: [false],
            is_show_add_ipv4: [false],
            is_show_add_ipv6: [false],
            parameters: [bond.parameters],
            service_access: [""],
            advanced: [bond.advanced],
          });
          control.push(grp);
        }

        const length = control.length;
        for (let i = 0; i < length; i++) {
          this.networkServiceService
            .getServiceAccess(control.at(i).get("name").value)
            .subscribe((res1) => {
              const serviceAccess = res1.body.data;
              let serviceAccessView = "";
              if (serviceAccess && serviceAccess.length > 0) {
                serviceAccess.forEach((s) => {
                  serviceAccessView = serviceAccessView + s.protocol + ", ";
                });
                serviceAccessView = serviceAccessView.substring(
                  0,
                  serviceAccessView.length - 2
                );
                control.at(i).get("service_access").setValue(serviceAccessView);
              }
            });
        }
      });
  }

  loadRoute() {
    this.networkServiceService.getRoute().subscribe((res) => {
      this.routeFormGroup = this.fb.group({
        data: this.fb.array([]),
      });
      // console.log("res.body: ", res.body);

      const routeControls = <FormArray>this.routeFormGroup.get("data");
      for (const networkRoute of res.body.data) {
        const grp = this.fb.group({
          id: [networkRoute.id],
          name: [networkRoute.name],
          type: [networkRoute.type],
          destination_v4: [
            networkRoute.type === 4 ? networkRoute.destination : "",
          ],
          destination_v6: [
            networkRoute.type === 6 ? networkRoute.destination : "",
          ],
          netmask_v4: [
            networkRoute.type === 4 ? networkRoute.netmask : "",
            [Validators.min(0), Validators.max(32)],
          ],
          netmask_v6: [
            networkRoute.type === 6 ? networkRoute.netmask : "",
            [Validators.min(0), Validators.max(128)],
          ],
          gateway_v4: [networkRoute.type === 4 ? networkRoute.gateway : ""],
          gateway_v6: [networkRoute.type === 6 ? networkRoute.gateway : ""],
          metric: [networkRoute.metric, [Validators.min(0)]],
          interface: [networkRoute.interface],
          active: [!!networkRoute.active],
          is_update: [false],
          is_add: [false],
        });
        routeControls.push(grp);
      }
      // console.log("routeControls: ", routeControls);
      this.routeFormGroup = this.fb.group({
        data: routeControls,
      });
      // console.log("this.routeFormGroup: ", this.routeFormGroup);
    });
  }

  startAddRoute() {
    const routeControls = <FormArray>this.routeFormGroup.get("data");

    const isAdding = routeControls.controls.find((e) => e.get("is_add").value);

    if (!isAdding) {
      const grp = this.fb.group({
        id: [""],
        name: [""],
        type: [4],
        destination_v4: [""],
        destination_v6: [""],
        netmask_v4: ["", [Validators.min(0), Validators.max(32)]],
        netmask_v6: ["", [Validators.min(0), Validators.max(128)]],
        gateway_v4: [""],
        gateway_v6: [""],
        metric: ["", [Validators.min(0)]],
        interface: [""],
        active: [true],
        is_update: [true],
        is_add: [true],
      });
      routeControls.push(grp);
    }
  }

  cancelAddRoute(index: number) {
    const routeControls = <FormArray>this.routeFormGroup.get("data");
    routeControls.controls.splice(index, 1);
  }

  addNewRoute(route: AbstractControl) {
    if (route.invalid) {
      route.get("name").markAsTouched();
      route.get("type").markAsTouched();
      route.get("destination_v4").markAsTouched();
      route.get("destination_v6").markAsTouched();
      route.get("netmask_v4").markAsTouched();
      route.get("netmask_v6").markAsTouched();
      route.get("gateway_v4").markAsTouched();
      route.get("gateway_v6").markAsTouched();
      route.get("metric").markAsTouched();
      route.get("interface").markAsTouched();
    } else {
      this.networkServiceService
        .addRoute({
          active: route.get("active").value ? 1 : 0,
          interface: route.get("interface").value || "",
          metric: Number(route.get("metric").value) || 0,
          destination:
            Number(route.get("type").value) === 4
              ? route.get("destination_v4").value
              : route.get("destination_v6").value,
          type: Number(route.get("type").value) || 4,
          gateway:
            Number(route.get("type").value) === 4
              ? route.get("gateway_v4").value
              : route.get("gateway_v6").value,
          name: route.get("name").value,
          netmask:
            Number(route.get("type").value) === 4
              ? route.get("netmask_v4").value
              : route.get("netmask_v6").value,
        })
        .subscribe((res) => {
          route.get("id").setValue(res.body.id);
          route.get("is_update").setValue(false);
          route.get("is_add").setValue(false);
        });
    }
  }

  startUpdateRoute(route: AbstractControl) {
    route.get("is_update").setValue(true);
  }

  updateRoute(route: AbstractControl) {
    if (route.invalid) {
      route.get("name").markAsTouched();
      route.get("type").markAsTouched();
      route.get("destination_v4").markAsTouched();
      route.get("destination_v6").markAsTouched();
      route.get("netmask_v4").markAsTouched();
      route.get("netmask_v6").markAsTouched();
      route.get("gateway_v4").markAsTouched();
      route.get("gateway_v6").markAsTouched();
      route.get("metric").markAsTouched();
      route.get("interface").markAsTouched();
    } else {
      console.log(route.get("id").value, " :ID");
      this.networkServiceService
        .updateRoute(
          {
            active: route.get("active").value ? 1 : 0,
            interface: route.get("interface").value || "",
            metric: Number(route.get("metric").value) || 0,
            destination:
              Number(route.get("type").value) === 4
                ? route.get("destination_v4").value
                : route.get("destination_v6").value,
            type: Number(route.get("type").value) || 4,
            gateway:
              Number(route.get("type").value) === 4
                ? route.get("gateway_v4").value
                : route.get("gateway_v6").value,
            name: route.get("name").value.trim(),
            netmask:
              Number(route.get("type").value) === 4
                ? route.get("netmask_v4").value
                : route.get("netmask_v6").value,
          },
          route.get("id").value
        )
        .subscribe((res) => {
          route.get("is_update").setValue(false);
          route.get("is_add").setValue(false);
        });
    }
  }

  cancelUpdateRoute(route: AbstractControl) {
    route.get("is_update").setValue(false);
    this.loadRoute();
  }

  confirmDeleteRoute(modal, route: AbstractControl) {
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
        centered: true,
      })
      .result.then((result) => {
        if (result === "delete") {
          this.networkServiceService
            .deleteRoute(route.get("id").value)
            .subscribe((res) => {
              this.loadRoute();
            });
        }
      });
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      // this.transition();
      this.loadAll();
    }
  }

  transition() {
    const parameters = {
      offset: this.paging.offset - 1,
      limit: this.paging.limit,
    };
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters["search"] = this.keywordSearch;
    }
    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }

    this.router.navigate(["/network"], {
      queryParams: parameters,
    });
  }

  onSearch() {
    this.paging.offset = 1;
    if (this.keywordSearch) {
      this.keywordSearch = this.keywordSearch.trim();
    }
    this.loadAll();
  }

  get getAddIpFormData() {
    return <FormArray>this.addIpFormGroup.get("data");
  }

  get getAddIpv6FormData() {
    return <FormArray>this.addIpv6FormGroup.get("data");
  }

  get getFormData(): FormArray {
    return <FormArray>this.networkInterfaceFormGroup.get("data");
  }

  get getRouteFormData(): FormArray {
    return <FormArray>this.routeFormGroup.get("data");
  }

  get getBridgesFormData(): FormArray {
    return <FormArray>this.virtualBridgesFormGroup.get("data");
  }

  get getBondingFormData(): FormArray {
    return <FormArray>this.virtualBondingFormGroup.get("data");
  }

  save(index: number) {
    const control = <FormArray>this.networkInterfaceFormGroup.get("data");
    control.at(index).get("is_update").setValue(false);
    control
      .at(index)
      .get("dns")
      .setValue(
        [
          control.at(index).get("preferred_dns").value || "",
          control.at(index).get("alt_dns").value || "",
        ].join(", ")
      );
    const value: IInterface = {
      type: control.at(index).get("type").value,
      name: control.at(index).get("name").value.trim(),
      active: control.at(index).get("active").value ? 1 : 0,
      status: control.at(index).get("status").value ? 1 : 0,
      dns: [
        control.at(index).get("preferred_dns").value || "",
        control.at(index).get("alt_dns").value || "",
      ],
      ipv6_addressing_mode: control.at(index).get("ipv6_addressing_mode").value,
      ipv4_addressing_mode: control.at(index).get("ipv4_addressing_mode").value,
      gateway_ipv4: control.at(index).get("gateway_ipv4").value,
      gateway_ipv6: control.at(index).get("gateway_ipv6").value,
      ipv6_address: control.at(index).get("ipv6_address").value.split(", "),
      ipv4_address: control.at(index).get("ipv4_address").value.split(", "),
    };
    // if (value.id) {
    this.networkServiceService.update(value).subscribe(
      (res) => {},
      (error) => {}
    );
    // }
    // else {
    //   this.networkServiceService.create(value).subscribe(res => {
    //   }, error => {
    //   });
    // }
    this.isEditing = false;
  }

  saveVirtualInterface(type: string) {
    const control = this.addVirtualNetworkFormGroup;
    const data: IBondingVirtualNetwork | IBridgesVirtualNetwork = {
      active: control.get("active").value ? 1 : 0,
      dns: [
        control.get("preferred_dns").value,
        control.get("alt_dns").value,
      ].reduce((acc, cur) => {
        if (cur) {
          acc.push(cur);
        }
        return acc;
      }, []),
      gateway_ipv4: control.get("gateway_ipv4").value,
      gateway_ipv6: control.get("gateway_ipv6").value,
      interfaces: control.get("interfaces").value.map((i) => i.name || i),
      ipv4_address: control
        .get("ipv4_address")
        .value.split(",")
        .reduce((acc, cur) => {
          if (cur) {
            acc.push(cur);
          }
          return acc;
        }, []),
      ipv4_addressing_mode: control.get("ipv4_addressing_mode").value,
      ipv6_address: control
        .get("ipv6_address")
        .value.split(",")
        .reduce((acc, cur) => {
          if (cur) {
            acc.push(cur);
          }
          return acc;
        }, []),
      ipv6_addressing_mode: control.get("ipv6_addressing_mode").value,
      advanced: {
        lacp_rate:
          control.get("parameters").value === "802.3ad"
            ? control.get("lacp_rate").value
            : "",
        monitor_interval: control.get("monitor_interval").value,
        primary_interface:
          control.get("parameters").value === "active-backup"
            ? control.get("primary_interface").value
            : "",
      },
      name: control.get("name").value.trim(),
      parameters: control.get("parameters").value,
      status: control.get("status").value ? 1 : 0,
      type: control.get("type").value,
    };
    this.networkServiceService
      .saveVirtualNetworkData(data, type)
      .subscribe((res) => {
        this.loadAll();
      });
    this.isEditing = false;
  }

  onEdit(index: number) {
    const control = <FormArray>this.networkInterfaceFormGroup.get("data");
    this.selectedNetworkInterface = control.at(index).value;
    this.onChangeIpVersion(index);
    control.at(index).get("is_update").setValue(true);
    this.isEditing = true;
  }

  onCancelEdit(index: number) {
    const control = <FormArray>this.networkInterfaceFormGroup.get("data");
    control.at(index).get("is_update").setValue(false);
    // reload
    this.initTable();
    this.isEditing = false;
  }

  remove(modal, index: number) {
    const control = <FormArray>this.networkInterfaceFormGroup.get("data");
    this.selectedNetworkInterface = control.at(index).value;
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
      })
      .result.then(
        (result) => {
          if (result === "delete") {
            if (this.selectedNetworkInterface.name) {
              this.networkServiceService
                .delete(this.selectedNetworkInterface.name)
                .subscribe((res) => {
                  control.removeAt(index);
                });
            } else {
              this.notificationService.error("Thao tác không hợp lệ");
            }
          }
        },
        (reason) => {}
      );
  }

  removeVirtualInterface(modal, name: string, type: string) {
    this.selectedNetworkInterface = { name };
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
      })
      .result.then(
        (result) => {
          if (result === "delete") {
            if (this.selectedNetworkInterface.name) {
              this.networkServiceService
                .deleteVirtualNetworkData(name, type)
                .subscribe((res) => {
                  console.log(res);
                  this.loadAll();
                });
            } else {
              this.notificationService.error("Thao tác không hợp lệ");
            }
          }
        },
        (reason) => {}
      );
  }

  async openEditModal(modal, index: number) {
    const control = <FormArray>this.networkInterfaceFormGroup.get("data");
    this.selectedNetworkInterface = control.at(index).value;
    control.at(index).get("is_update").setValue(true);
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
        centered: true,
      })
      .result.then(
        (result) => {
          control.at(index).get("is_update").setValue(false);
          if (result === "Save") {
            this.save(index);
          }
        },
        () => {
          control.at(index).get("is_update").setValue(false);
        }
      );
  }

  openEditVirtualInterfaceModal(modal, index: number, type: string) {
    const control =
      type === "Bridges"
        ? <FormArray>this.virtualBridgesFormGroup.get("data")
        : <FormArray>this.virtualBondingFormGroup.get("data");
    const data = control.at(index);

    const advanced = data.get("advanced").value || {};

    const DNS = (data.get("dns").value || "").split(", ");
    const preferred_dns = [DNS[0] || ""];
    const alt_dns = [DNS[1] || ""];

    this.addVirtualNetworkFormGroup = this.fb.group({
      name: [data.get("name").value, Validators.required],
      type: [data.get("type").value, Validators.required],
      interfaces: [data.get("interfaces").value, Validators.required],
      ipv4_addressing_mode: [data.get("ipv4_addressing_mode").value],
      ipv6_addressing_mode: [data.get("ipv6_addressing_mode").value],
      ipv4_address: [
        data.get("ipv4_address").value || "",
        [this.validateIpv4Addresses()],
      ],
      ipv6_address: [
        data.get("ipv6_address").value || "",
        [this.validateIpv6Addresses()],
      ],
      gateway_ipv4: [data.get("gateway_ipv4").value],
      gateway_ipv6: [data.get("gateway_ipv6").value],
      dns: [data.get("dns").value],
      preferred_dns,
      alt_dns,
      active: [!!data.get("active").value],
      status: [!!data.get("status").value],
      is_update: [false],
      is_show_update_service: [false],
      is_show_add_ipv4: [false],
      is_show_add_ipv6: [false],
      is_show_advanced: [false],
      lacp_rate: [advanced ? advanced.lacp_rate || "" : ""],
      monitor_interval: [advanced ? advanced.monitor_interval || "" : ""],
      parameters: [type === "Bridges" ? "" : data.get("parameters").value],
      virtual_network_type: [type],
      primary_interface: [advanced ? advanced.primary_interface || "" : ""],
    });
    this.selectedNetworkInterface = control.at(index).value;
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
        centered: true,
      })
      .result.then(
        (result) => {
          control.at(index).get("is_update").setValue(false);
          if (result === "Save") {
            this.saveVirtualInterface(type);
          }
        },
        () => {
          control.at(index).get("is_update").setValue(false);
        }
      );
  }

  openAddVirtualNetwork(modal) {
    this.addVirtualNetworkFormGroup = this.fb.group({
      name: ["", Validators.required],
      type: ["WAN", Validators.required],
      interfaces: [[], Validators.required],
      ipv4_addressing_mode: [""],
      ipv6_addressing_mode: [""],
      ipv4_address: ["", [this.validateIpv4Addresses()]],
      ipv6_address: ["", [this.validateIpv6Addresses()]],
      gateway_ipv4: [""],
      gateway_ipv6: [""],
      dns: [[]],
      preferred_dns: [""],
      alt_dns: [""],
      active: [true],
      status: [true],
      is_update: [false],
      is_show_update_service: [false],
      is_show_add_ipv4: [false],
      is_show_add_ipv6: [false],
      is_show_advanced: [false],
      lacp_rate: [""],
      monitor_interval: [""],
      parameters: ["802.3ad"],
      virtual_network_type: ["Bonding"],
      primary_interface: [""],
    });
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
        centered: true,
      })
      .result.then(
        (result) => {
          this.addVirtualNetworkFormGroup.get("is_update").setValue(false);
          if (result === "Save") {
            if (
              this.addVirtualNetworkFormGroup.get("virtual_network_type")
                .value === "Bonding"
            ) {
              this.addNewBonding();
            } else {
              this.addNewBridges();
            }
          }
        },
        () => {
          this.addVirtualNetworkFormGroup.get("is_update").setValue(false);
        }
      );
  }

  addNewBonding() {
    const control = this.addVirtualNetworkFormGroup;
    const data: IBondingVirtualNetwork = {
      active: control.get("active").value ? 1 : 0,
      dns: [
        control.get("preferred_dns").value,
        control.get("alt_dns").value,
      ].reduce((acc, cur) => {
        if (cur) {
          acc.push(cur);
        }
        return acc;
      }, []),
      gateway_ipv4: control.get("gateway_ipv4").value,
      gateway_ipv6: control.get("gateway_ipv6").value,
      interfaces: control.get("interfaces").value.map((i) => i.name),
      ipv4_address: control
        .get("ipv4_address")
        .value.split(",")
        .reduce((acc, cur) => {
          if (cur) {
            acc.push(cur);
          }
          return acc;
        }, []),
      ipv4_addressing_mode: control.get("ipv4_addressing_mode").value,
      ipv6_address: control
        .get("ipv6_address")
        .value.split(",")
        .reduce((acc, cur) => {
          if (cur) {
            acc.push(cur);
          }
          return acc;
        }, []),
      ipv6_addressing_mode: control.get("ipv6_addressing_mode").value,
      advanced: {
        lacp_rate:
          control.get("parameters").value === "802.3ad"
            ? control.get("lacp_rate").value
            : "",
        monitor_interval: control.get("monitor_interval").value,
        primary_interface:
          control.get("parameters").value === "active-backup"
            ? control.get("primary_interface").value
            : "",
      },
      name: control.get("name").value,
      parameters: control.get("parameters").value,
      status: control.get("status").value ? 1 : 0,
      type: control.get("type").value,
    };
    this.networkServiceService.addNewBonding(data).subscribe((res) => {
      this.loadAll();
    });
  }

  addNewBridges() {
    const control = this.addVirtualNetworkFormGroup;
    const data: IBridgesVirtualNetwork = {
      active: control.get("active").value ? 1 : 0,
      dns: [
        control.get("preferred_dns").value,
        control.get("alt_dns").value,
      ].reduce((acc, cur) => {
        if (cur) {
          acc.push(cur);
        }
        return acc;
      }, []),
      gateway_ipv4: control.get("gateway_ipv4").value,
      gateway_ipv6: control.get("gateway_ipv6").value,
      interfaces: control.get("interfaces").value.map((i) => i.name),
      ipv4_address: control
        .get("ipv4_address")
        .value.split(",")
        .reduce((acc, cur) => {
          if (cur) {
            acc.push(cur);
          }
          return acc;
        }, []),
      ipv4_addressing_mode: control.get("ipv4_addressing_mode").value,
      ipv6_address: control
        .get("ipv6_address")
        .value.split(",")
        .reduce((acc, cur) => {
          if (cur) {
            acc.push(cur);
          }
          return acc;
        }, []),
      ipv6_addressing_mode: control.get("ipv6_addressing_mode").value,
      advanced: {
        lacp_rate:
          control.get("parameters").value === "802.3ad"
            ? control.get("lacp_rate").value
            : "",
        monitor_interval: control.get("monitor_interval").value,
        primary_interface:
          control.get("parameters").value === "active-backup"
            ? control.get("primary_interface").value
            : "",
      },
      name: control.get("name").value,
      status: control.get("status").value ? 1 : 0,
      type: control.get("type").value,
    };
    this.networkServiceService.addNewBridges(data).subscribe((res) => {
      this.loadAll();
    });
  }

  validateNetworkForm(index, modal) {
    const control = <FormArray>this.networkInterfaceFormGroup.get("data");
    console.log(control.at(index));
    if (!control.at(index).invalid) {
      modal.close("Save");
    }
  }

  validateVirtualNetworkForm(modal) {
    const control = this.addVirtualNetworkFormGroup;
    // console.log(control);

    if (control.invalid) {
      control.get("name").markAsTouched();
      control.get("interfaces").markAsTouched();
    } else {
      modal.close("Save");
    }
  }

  get getServiceAccessFormData(): FormArray {
    return <FormArray>this.serviceAccessFormGroup.get("data");
  }

  onUpdateServiceAccess(index: number) {
    // this.disableValue(this.getFormData.controls);
    const bondingControl = <FormArray>this.virtualBondingFormGroup.get("data");
    for (let i = 0; i < bondingControl.controls.length; i++) {
      bondingControl.at(i).get("is_show_update_service").setValue(false);
    }

    const bridgesControl = <FormArray>this.virtualBridgesFormGroup.get("data");
    for (let i = 0; i < bridgesControl.controls.length; i++) {
      bridgesControl.at(i).get("is_show_update_service").setValue(false);
    }
    const control = <FormArray>this.networkInterfaceFormGroup.get("data");
    for (let i = 0; i < control.controls.length; i++) {
      if (index !== i) {
        control.at(i).get("is_show_update_service").setValue(false);
      }
    }
    control
      .at(index)
      .get("is_show_update_service")
      .setValue(!control.at(index).get("is_show_update_service").value);
    this.selectedNetworkInterface = control.at(index).value;
    this.initServiceAccessTable();
    this.isEditing = false;
  }

  onUpdateBondingServiceAccess(index: number) {
    // this.disableValue(this.getFormData.controls);
    const networkControl = <FormArray>(
      this.networkInterfaceFormGroup.get("data")
    );
    for (let i = 0; i < networkControl.controls.length; i++) {
      networkControl.at(i).get("is_show_update_service").setValue(false);
    }

    const bridgesControl = <FormArray>this.virtualBridgesFormGroup.get("data");
    for (let i = 0; i < bridgesControl.controls.length; i++) {
      bridgesControl.at(i).get("is_show_update_service").setValue(false);
    }
    const control = <FormArray>this.virtualBondingFormGroup.get("data");
    for (let i = 0; i < control.controls.length; i++) {
      if (index !== i) {
        control.at(i).get("is_show_update_service").setValue(false);
      }
    }
    control
      .at(index)
      .get("is_show_update_service")
      .setValue(!control.at(index).get("is_show_update_service").value);
    this.selectedNetworkInterface = control.at(index).value;
    this.initServiceAccessTable();
    this.isEditing = false;
  }

  onUpdateBridgesServiceAccess(index: number) {
    // this.disableValue(this.getFormData.controls);
    // networkInterfaceFormGroup
    // virtualBridgesFormGroup
    // virtualBondingFormGroup
    const networkControl = <FormArray>(
      this.networkInterfaceFormGroup.get("data")
    );
    for (let i = 0; i < networkControl.controls.length; i++) {
      networkControl.at(i).get("is_show_update_service").setValue(false);
    }

    const bondingControl = <FormArray>this.virtualBondingFormGroup.get("data");
    for (let i = 0; i < bondingControl.controls.length; i++) {
      bondingControl.at(i).get("is_show_update_service").setValue(false);
    }
    const control = <FormArray>this.virtualBridgesFormGroup.get("data");
    for (let i = 0; i < control.controls.length; i++) {
      if (index !== i) {
        control.at(i).get("is_show_update_service").setValue(false);
      }
    }
    control
      .at(index)
      .get("is_show_update_service")
      .setValue(!control.at(index).get("is_show_update_service").value);
    this.selectedNetworkInterface = control.at(index).value;
    this.initServiceAccessTable();
    this.isEditing = false;
  }

  initServiceAccessTable() {
    this.networkServiceService
      .getServiceAccess(this.selectedNetworkInterface.name)
      .subscribe((res) => {
        this.serviceAccessFormGroup = this.fb.group({
          data: this.fb.array([]),
        });
        this.serviceAccesses = res.body.data;
        const serviceAccessControl = <FormArray>(
          this.serviceAccessFormGroup.get("data")
        );
        for (const serviceAccess of this.serviceAccesses) {
          const protocols = serviceAccess.protocol
            ? this.PROTOCOLS.filter((e) =>
                serviceAccess.protocol.includes(e.value)
              )
            : [];
          const grp = this.fb.group({
            id: [serviceAccess.id],
            port_from: [serviceAccess.port_from, Validators.required],
            port_to: [serviceAccess.port_to, [Validators.required]],
            protocol: [serviceAccess.protocol],
            protocolData: [protocols, [Validators.required]],
            is_update: [false],
          });
          serviceAccessControl.push(grp);
        }
      });
  }

  saveServiceAccess(index: number) {
    const control = <FormArray>this.serviceAccessFormGroup.get("data");
    control.at(index).get("is_update").setValue(false);
    this.selectedProtocols = [];
    if (control.at(index).get("protocolData").value) {
      control
        .at(index)
        .get("protocolData")
        .value.forEach((s) => {
          this.selectedProtocols.push(s.value);
        });
    }
    control.at(index).get("protocol").setValue(this.selectedProtocols);

    const value: IServiceAccess = {
      id: control.at(index).get("id").value,
      port_from: control.at(index).get("port_from").value,
      port_to: control.at(index).get("port_to").value,
      protocol: this.selectedProtocols,
    };
    if (value.id) {
      this.networkServiceService
        .updateServiceAccess(this.selectedServiceAccess.id, value)
        .subscribe((res) => {
          this.initServiceAccessTable();
        });
    } else {
      this.networkServiceService
        .createServiceAccess(this.selectedNetworkInterface.name, value)
        .subscribe((res) => {
          this.initServiceAccessTable();
        });
    }
  }

  onEditServiceAccess(index: number) {
    const control = <FormArray>this.serviceAccessFormGroup.get("data");
    this.selectedServiceAccess = control.at(index).value;
    control.at(index).get("is_update").setValue(true);
    this.isEditing = false;
  }

  onCancelEditServiceAccess(index: number) {
    const control = <FormArray>this.serviceAccessFormGroup.get("data");
    control.at(index).get("is_update").setValue(false);
    // reload
    this.initServiceAccessTable();
  }

  removeServiceAccess(index: number) {
    const control = <FormArray>this.serviceAccessFormGroup.get("data");
    this.selectedServiceAccess = control.at(index).value;
    if (this.selectedServiceAccess.id) {
      this.networkServiceService
        .deleteServiceAccess(this.selectedServiceAccess.id)
        .subscribe((res) => {
          control.removeAt(index);
        });
    } else {
      this.notificationService.error("Thao tác không hợp lệ");
    }
  }

  addServiceAccess() {
    const control = <FormArray>this.serviceAccessFormGroup.get("data");
    control.push(this.initServiceAccessForm());
  }

  closeUpdateServiceAccess(index) {
    const control = <FormArray>this.networkInterfaceFormGroup.get("data");
    control.at(index).get("is_show_update_service").setValue(false);
  }

  closeUpdateBondingServiceAccess(index) {
    const control = <FormArray>this.virtualBondingFormGroup.get("data");
    control.at(index).get("is_show_update_service").setValue(false);
  }

  closeUpdateBridgesServiceAccess(index) {
    const control = <FormArray>this.virtualBridgesFormGroup.get("data");
    control.at(index).get("is_show_update_service").setValue(false);
  }

  checkPortValid(index, port) {
    const control = <FormArray>this.serviceAccessFormGroup.get("data");
    if (control.at(index).get(port).value < 0) {
      control.at(index).get(port).setValue(0);
    }
    if (control.at(index).get(port).value > 65535) {
      control.at(index).get(port).setValue(65535);
    }
  }

  validateIpv6Addresses(): ValidatorFn {
    const ipv6Reg =
      /(([0-9a-fA-F]{1,4}:){7,7}[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,7}:|([0-9a-fA-F]{1,4}:){1,6}:[0-9a-fA-F]{1,4}|([0-9a-fA-F]{1,4}:){1,5}(:[0-9a-fA-F]{1,4}){1,2}|([0-9a-fA-F]{1,4}:){1,4}(:[0-9a-fA-F]{1,4}){1,3}|([0-9a-fA-F]{1,4}:){1,3}(:[0-9a-fA-F]{1,4}){1,4}|([0-9a-fA-F]{1,4}:){1,2}(:[0-9a-fA-F]{1,4}){1,5}|[0-9a-fA-F]{1,4}:((:[0-9a-fA-F]{1,4}){1,6})|:((:[0-9a-fA-F]{1,4}){1,7}|:)|fe80:(:[0-9a-fA-F]{0,4}){0,4}%[0-9a-zA-Z]{1,}|::(ffff(:0{1,4}){0,1}:){0,1}((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])|([0-9a-fA-F]{1,4}:){1,4}:((25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9])\.){3,3}(25[0-5]|(2[0-4]|1{0,1}[0-9]){0,1}[0-9]))/;
    return (control: AbstractControl): ValidationErrors | null => {
      let invalidIp = false;
      let invalidNetmask = false;
      const ipList = control.value.split(",");
      for (let i = 0; i < ipList.length; i++) {
        const item = ipList[i];
        if (item) {
          const itemParts = item.trim().split("/");
          invalidIp = !ipv6Reg.test(itemParts[0]);
          invalidNetmask =
            !itemParts[1] || itemParts[1] > 128 || itemParts[1] < 0;
          if (invalidIp || invalidNetmask) {
            break;
          }
        }
      }

      return invalidIp || invalidNetmask
        ? { invalidIpv4: { value: control.value } }
        : null;
    };
  }

  validateIpv4Addresses(): ValidatorFn {
    const ipv4Reg = /(^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\.(?!$)|$)){4})/;
    return (control: AbstractControl): ValidationErrors | null => {
      let invalidIp = false;
      let invalidNetmask = false;
      const ipList = control.value.split(",");
      for (let i = 0; i < ipList.length; i++) {
        const item = ipList[i];
        if (item) {
          const itemParts = item.trim().split("/");
          console.log(itemParts);
          invalidIp = !ipv4Reg.test(itemParts[0]);
          invalidNetmask =
            !itemParts[1] || itemParts[1] > 32 || itemParts[1] < 0;
          if (invalidIp || invalidNetmask) {
            break;
          }
        }
      }

      return invalidIp || invalidNetmask
        ? { invalidIpv4: { value: control.value } }
        : null;
    };
  }

  onChangeIpVersion(index) {
    // const control = <FormArray>this.networkInterfaceFormGroup.get('data');
    // const addMode = ADDRESSING_MODES.filter(a => a.value === control.at(index).get('addressing_mode').value);
    // if (addMode && addMode.length > 0) {
    //   if (addMode[0].ip_type === 4) {
    //     this.NETMASK = NETMASK_V4;
    //   } else {
    //     this.NETMASK = this.NETMASK_V6;
    //   }
    //   if (addMode[0].type === 'dhcp') {
    //     this.isShow = true;
    //   } else {
    //     this.isShow = false;
    //   }
    // }
  }

  onNetworkTypeChange(index: number) {
    const control = <FormArray>this.networkInterfaceFormGroup.get("data");
    const networkType = NETWORK_TYPES.find(
      (nt) => nt.value === control.at(index).get("type").value
    );
  }

  addIpv6ToInterface(networkFormIdx: number) {
    const addIpControl = (<FormArray>this.addIpv6FormGroup.get("data")).at(0);
    const networkInterfaceControl = (<FormArray>(
      this.networkInterfaceFormGroup.get("data")
    )).at(networkFormIdx);
    let currentIpv6s = networkInterfaceControl
      .get("ipv6_address")
      .value.split(",")
      .map((item) => item.trim());
    currentIpv6s.push(
      `${addIpControl.get("ip").value}/${addIpControl.get("netmask").value}`
    );

    currentIpv6s = currentIpv6s.reduce((acc, cur, idx) => {
      if (cur) {
        acc.push(cur);
      }
      return acc;
    }, []);

    networkInterfaceControl
      .get("ipv6_address")
      .setValue(currentIpv6s.join(", "));
    networkInterfaceControl.get("is_show_add_ipv6").setValue(false);

    addIpControl.reset();
  }

  addIpv4ToInterface(networkFormIdx: number) {
    const addIpControl = (<FormArray>this.addIpFormGroup.get("data")).at(0);
    const networkInterfaceControl = (<FormArray>(
      this.networkInterfaceFormGroup.get("data")
    )).at(networkFormIdx);
    let currentIpv4s = networkInterfaceControl
      .get("ipv4_address")
      .value.split(",")
      .map((item) => item.trim());
    currentIpv4s.push(
      `${addIpControl.get("ip").value}/${addIpControl.get("netmask").value}`
    );
    currentIpv4s = currentIpv4s.reduce((acc, cur, idx) => {
      if (cur) {
        acc.push(cur);
      }
      return acc;
    }, []);

    networkInterfaceControl
      .get("ipv4_address")
      .setValue(currentIpv4s.join(", "));
    networkInterfaceControl.get("is_show_add_ipv4").setValue(false);

    addIpControl.reset();
  }

  addIpv4ToAddVirtualNetwork() {
    const addIpControl = (<FormArray>this.addIpFormGroup.get("data")).at(0);
    const networkInterfaceControl = this.addVirtualNetworkFormGroup;
    let currentIpv4s = networkInterfaceControl
      .get("ipv4_address")
      .value.split(",")
      .map((item) => item.trim());
    currentIpv4s.push(
      `${addIpControl.get("ip").value}/${addIpControl.get("netmask").value}`
    );
    currentIpv4s = currentIpv4s.reduce((acc, cur, idx) => {
      if (cur) {
        acc.push(cur);
      }
      return acc;
    }, []);

    networkInterfaceControl
      .get("ipv4_address")
      .setValue(currentIpv4s.join(", "));
    networkInterfaceControl.get("is_show_add_ipv4").setValue(false);

    addIpControl.reset();
  }

  addIpv6ToAddVirtualNetwork() {
    const addIpControl = (<FormArray>this.addIpFormGroup.get("data")).at(0);
    const networkInterfaceControl = this.addVirtualNetworkFormGroup;
    let currentIpv6s = networkInterfaceControl
      .get("ipv6_address")
      .value.split(",")
      .map((item) => item.trim());
    currentIpv6s.push(
      `${addIpControl.get("ip").value}/${addIpControl.get("netmask").value}`
    );

    currentIpv6s = currentIpv6s.reduce((acc, cur, idx) => {
      if (cur) {
        acc.push(cur);
      }
      return acc;
    }, []);

    networkInterfaceControl
      .get("ipv6_address")
      .setValue(currentIpv6s.join(", "));
    networkInterfaceControl.get("is_show_add_ipv6").setValue(false);

    addIpControl.reset();
  }

  disableValue(list) {
    list.forEach((value) => {
      value.control("is_show_update_service").setValue(false);
    });
    console.log(list);
    // this.serviceAccess = false;
  }
}
