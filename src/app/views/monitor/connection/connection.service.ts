import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { map } from "rxjs/operators";
import { IPaging } from "@app/shared/model/base-respone.model";
import {
  ILookMonitorDdos,
  IMonitorDdos,
} from "@app/shared/model/monitor-ddos.model";
import { createRequestOption } from "@app/shared";

@Injectable({
  providedIn: "root",
})
export class ConnectionService {
  public resourceUrl = environment.apiUrl + "/monitor-ddos";

  constructor(protected http: HttpClient) {}

  websiteConnection(req?: any): Observable<HttpResponse<{ data: any[] }>> {
    const options = createRequestOption(req);
    return this.http.get<{ data: any[] }>(
      `${environment.apiUrl}/website-connection`,
      { params: options, observe: "response" }
    );
  }

  ipConnection(req?: any): Observable<HttpResponse<{ data: any[] }>> {
    const options = createRequestOption(req);
    return this.http.get<{ data: any[] }>(
      `${environment.apiUrl}/ip-connection`,
      { params: options, observe: "response" }
    );
  }
  blacklistIp(req?: any): Observable<HttpResponse<any>> {
    return this.http.post<any>(`${environment.apiUrl}/blacklist`, req, {
      observe: "response",
    });
  }
}
