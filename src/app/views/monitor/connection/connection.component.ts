import { Component, OnInit, ViewChild } from "@angular/core";
import {
  PERIOD_MONITOR,
  PeriodMonitor,
} from "@app/shared/constants/monitor.model";
import { FormBuilder } from "@angular/forms";
import { ConnectionService } from "@app/views/monitor/connection/connection.service";
import { ChartDataSets, ChartOptions, ChartType } from "chart.js";
import { BaseChartDirective, Label } from "ng2-charts";
import { REQUEST_INTERVAL } from "@app/shared/constants/common.model";
@Component({
  selector: "app-connection",
  templateUrl: "./connection.component.html",
  styleUrls: ["./connection.component.scss"],
})
export class ConnectionComponent implements OnInit {
  @ViewChild("webConnectionHorizonBarChart", { static: false })
  webConnectionHorizonBarChart: BaseChartDirective;
  @ViewChild("ipConnectionHorizonBarChart", { static: false })
  ipConnectionHorizonBarChart: BaseChartDirective;
  periodWebConnectionChart = PERIOD_MONITOR;
  periodIpConnectionChart = PERIOD_MONITOR;
  REQUEST_INTERVAL = REQUEST_INTERVAL;
  periodFilterChart = this.fb.group({
    webConnectionChart: [this.periodWebConnectionChart[0]],
    ipConnectionChart: [this.periodIpConnectionChart[0]],
  });

  ipConnections = [];
  webConnectionChartData: ChartDataSets[] = [
    { data: [], label: "", spanGaps: true },
  ];
  webConnectionChartLabels: Label[] = [];

  ipConnectionChartData: ChartDataSets[] = [
    { data: [], label: "", spanGaps: true },
  ];
  ipConnectionChartLabels: Label[] = [];

  webConnectionChartOption: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    plugins: {
      legend: {
        position: "right",
      },
      title: {
        display: false,
        text: "",
      },
    },
  };

  lineChartLegend = true;
  barChartType: ChartType = "horizontalBar";
  resourceChartPlugins = [];

  constructor(
    private fb: FormBuilder,
    public connectionService: ConnectionService
  ) {}

  ngOnInit() {
    this.loadWebsiteConnectionChart();
    this.loadIpConnectionChart();
    setInterval(() => {
      this.loadWebsiteConnectionChart();
      this.loadIpConnectionChart();
    }, this.REQUEST_INTERVAL);
  }

  loadWebsiteConnectionChart() {
    const period: PeriodMonitor =
      this.periodFilterChart.controls["webConnectionChart"].value;
    console.log(period, "sdsd");
    const parameters = {
      orderBy: "id",
      orderType: "desc",
      dateType: period.type,
      avg_value: 100,
      dateValue: period.value,
    };
    this.connectionService.websiteConnection(parameters).subscribe((res) => {
      const data = res.body.data;

      const labels = data.reduce((acc: string[], cur, idx) => {
        if (!idx || !acc.includes(cur.site_connect)) {
          acc.push(cur.site_connect);
        }

        return acc;
      }, []);

      const dateData = data.reduce((acc: string[], cur, idx) => {
        if (!idx || !acc.includes(cur.datetime.split(" ")[0])) {
          acc.push(cur.datetime.split(" ")[0]);
        }

        return acc;
      }, []);

      const formattedData = dateData.map((e) => {
        const d = { data: [], label: e, spanGaps: true };
        for (let i = 0; i < labels.length; i++) {
          const label = labels[i];
          const filteredDateData = data.filter(
            (ee) => ee.datetime.split(" ")[0] === e && ee.site_connect === label
          );
          const sum = filteredDateData.reduce((acc: number, cur) => {
            acc += cur.request_site;
            return acc;
          }, 0);
          d.data.push(sum);
        }
        return d;
      });

      if (formattedData.length) {
        this.webConnectionChartData = formattedData;
      }
      if (labels.length) {
        this.webConnectionChartLabels = labels;
      }
    });
  }

  loadIpConnectionChart() {
    const period: PeriodMonitor =
      this.periodFilterChart.controls["ipConnectionChart"].value;
    const parameters = {
      orderBy: "id",
      orderType: "desc",
      dateType: period.type,
      avg_value: 100,
      dateValue: period.value,
    };
    this.connectionService.ipConnection(parameters).subscribe((res) => {
      const data = res.body.data;

      this.ipConnections = data;

      const labels = data.reduce((acc: string[], cur, idx) => {
        if (!idx || !acc.includes(cur.ip_connect)) {
          acc.push(cur.ip_connect);
        }

        return acc;
      }, []);

      const dateData = data.reduce((acc: string[], cur, idx) => {
        if (!idx || !acc.includes(cur.datetime.split(" ")[0])) {
          acc.push(cur.datetime.split(" ")[0]);
        }

        return acc;
      }, []);

      const formattedData = dateData.map((e) => {
        const d = { data: [], label: e, spanGaps: true };
        for (let i = 0; i < labels.length; i++) {
          const label = labels[i];
          const filteredDateData = data.filter(
            (ee) => ee.datetime.split(" ")[0] === e && ee.ip_connect === label
          );
          const sum = filteredDateData.reduce((acc: number, cur) => {
            acc += cur.request_ip;
            return acc;
          }, 0);
          d.data.push(sum);
        }
        return d;
      });

      if (formattedData.length) {
        this.ipConnectionChartData = formattedData;
      }

      if (labels.length) {
        this.ipConnectionChartLabels = labels;
      }
    });
  }

  blacklistIp(ip) {
    this.connectionService
      .blacklistIp({
        ip_address: ip,
        netmask: "255.255.255.255",
        description: "Blacklist from monitor connection",
      })
      .subscribe((req) => {
        this.loadIpConnectionChart();
      });
  }
}
