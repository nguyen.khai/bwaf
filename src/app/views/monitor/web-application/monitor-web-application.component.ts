import { Component, ElementRef, OnDestroy, OnInit } from "@angular/core";
import { MonitorDdosService } from "@app/views/monitor/ddos/monitor-ddos.service";
import { ActivatedRoute, Router, RouterModule } from "@angular/router";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, Validators } from "@angular/forms";
import { MonitorWafService } from "@app/views/monitor/web-application/monitor-web-application.service";
import { IPaging, Paging } from "@app/shared/model/base-respone.model";
import {
  getParameterPaging,
  OrderParam,
  PAGING_PER_PAGE,
  REQUEST_INTERVAL,
} from "@app/shared/constants/common.model";
import { IMonitorWaf } from "@app/shared/model/monitor-waf";
import { Location } from "@angular/common";
import {
  COUNTRIES,
  COUNTRY_CENTER_GEO_POINTS,
  PERIOD_MONITOR,
  PeriodMonitor,
} from "@app/shared/constants/monitor.model";
import { IField } from "@app/shared/ng-mutifilter/ng-mutifilter.model";

@Component({
  selector: "app-monitor-web-application",
  templateUrl: "./monitor-web-application.component.html",
  styleUrls: ["./monitor-web-application.component.scss"],
})
export class MonitorWebApplicationComponent implements OnInit, OnDestroy {
  periodFilterForms: PeriodMonitor[] = PERIOD_MONITOR;
  dataFilterForm = this.fb.group({
    filter: ["", [Validators.required]],
    period: [this.periodFilterForms[0]],
  });

  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  order: OrderParam = new OrderParam("datetime", false);
  // periodWorldMapCharts: PeriodMonitor[] = PERIOD_MONITOR;
  // periodWorldPieCharts: PeriodMonitor[] = PERIOD_MONITOR;
  // periodIpCharts: PeriodMonitor[] = PERIOD_MONITOR;
  // periodAttackWebsiteMapCharts: PeriodMonitor[] = PERIOD_MONITOR;
  // periodAttackActionMapCharts: PeriodMonitor[] = PERIOD_MONITOR;

  periodCharts: PeriodMonitor[] = PERIOD_MONITOR;
  periodFilterChart = this.fb.group({
    worldMapChart: [this.periodCharts[0]],
    worldPieChart: [this.periodCharts[0]],
    ipChart: [this.periodCharts[0]],
    attackWebsiteChart: [this.periodCharts[0]],
    attackActionChart: [this.periodCharts[0]],
  });

  timer;
  monitorWafs: IMonitorWaf[];
  paging: IPaging;
  limits = PAGING_PER_PAGE;
  attackerCountryChart;
  attackerIpChart;
  attackerWebsiteChart;
  attackerActionChart;
  // world-map
  worldMapChart: Array<any>;

  urlDownload: string;

  fields: Array<IField> = [
    { label: "IP Addr", value: "attacker_ip" },
    { label: "Blocked time", value: "datetime" },
    { label: "County", value: "attacker_country" },
    { label: "Rule", value: "group_rule" },
    { label: "Website domain", value: "website_domain" },
  ];

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  changeLimit() {
    this.paging.page = 1;
    this.loadAll();
  }

  constructor(
    public monitorWafService: MonitorWafService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.paging = new Paging();
    this.monitorWafs = [];
  }

  loadWorldMapChart() {
    console.log("loadWorldMapChart");
    const parameters = {
      attacker_country: 10,
    };
    const period = this.periodFilterChart.controls["worldMapChart"].value;
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorWafService.chart(parameters).subscribe((res) => {
      if (res.status === 200) {
        this.worldMapChart = res.body;

        // this.worldMapChart = [];
        // res.body.forEach(value => {
        //   if (COUNTRY_CENTER_GEO_POINTS[value.key]) {
        //     this.worldMapChart.push(
        //       {
        //         latitude: COUNTRY_CENTER_GEO_POINTS[value.key].latlng[0],
        //         longitude: COUNTRY_CENTER_GEO_POINTS[value.key].latlng[1],
        //         width: 32,
        //         height: 32,
        //         label: COUNTRY_CENTER_GEO_POINTS[value.key].name,
        //         value: value.value,
        //         imageURL: ""
        //       }
        //     );
        //   }
        // });
        // this.worldMapChart = res.body.map(value => {
        //   return {
        //     latitude: COUNTRY_CENTER_GEO_POINTS[value.key].latlng[0],
        //     longitude: COUNTRY_CENTER_GEO_POINTS[value.key].latlng[1],
        //     width: 32,
        //     height: 32,
        //     label: value.key,
        //     value: value.value,
        //     imageURL: ""
        //   };
        // });
      } else {
        console.warn("can not load world map");
      }
    });
  }

  ngOnInit() {
    this.loadAll();
    this.loadWorldMapChart();
    this.loadAttackerCountryChart();
    this.loadAttackerIpChart();
    this.loadAttackerWebsiteChart();
    this.loadAttackerActionChart();
    this.interval();
  }

  onFilter($event) {
    this.paging.page = 1;
    this.dataFilterForm.controls["filter"].setValue($event);
    this.loadAll();
  }
  showCountry(code: string): string {
    return COUNTRIES.hasOwnProperty(code) ? COUNTRIES[code].country : code;
  }

  interval() {
    this.timer = setInterval(() => {
      console.log("interval");
      if (this.dataFilterForm.controls["period"].value.type !== "between") {
        this.loadAll();
      }
      if (
        this.periodFilterChart.controls["worldMapChart"].value.type !==
        "between"
      ) {
        this.loadWorldMapChart();
      }
      if (
        this.periodFilterChart.controls["worldPieChart"].value.type !==
        "between"
      ) {
        this.loadAttackerCountryChart();
      }
      if (this.periodFilterChart.controls["ipChart"].value.type !== "between") {
        this.loadAttackerIpChart();
      }
      if (
        this.periodFilterChart.controls["attackWebsiteChart"].value.type !==
        "between"
      ) {
        this.loadAttackerWebsiteChart();
      }
      if (
        this.periodFilterChart.controls["attackActionChart"].value.type !==
        "between"
      ) {
        this.loadAttackerActionChart();
      }
    }, REQUEST_INTERVAL);
  }

  refreshSearchForm() {
    this.dataFilterForm.controls["period"].setValue(this.periodFilterForms[0]);
    this.dataFilterForm.controls["filter"].setValue("");
    this.order.orderBy = "datetime";
    this.order.orderType = false;
    this.loadAll();
  }

  changePeriodWorldCharts($event) {}

  loadAttackerActionChart() {
    console.log("load attacker acction chart");
    const period: PeriodMonitor =
      this.periodFilterChart.controls["attackActionChart"].value;
    const parameters = {
      group_rule: 10,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorWafService.chart(parameters).subscribe((res) => {
      if (res.status === 200) {
        if (res.body.length <= 0) {
          res.body.push({
            key: "No data",
            value: 100,
          });
        }
        const data = [];
        const lables = [];
        const colors = [];
        res.body.forEach((value) => {
          data.push(value.value);
          lables.push(value.key);
          colors.push(this.dynamicColors());
        });
        this.attackerActionChart = this.converDataChartPie(
          data,
          lables,
          colors
        );
      } else {
        console.warn("cannot load attacker action chart");
      }
    });
  }

  loadAttackerWebsiteChart() {
    console.log("load attacker website chart");
    const period: PeriodMonitor =
      this.periodFilterChart.controls["attackWebsiteChart"].value;
    const parameters = {
      website_domain: 10,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorWafService.chart(parameters).subscribe((res) => {
      if (res.status === 200) {
        if (res.body.length <= 0) {
          res.body.push({
            key: "No data",
            value: 100,
          });
        }

        const data = [];
        const lables = [];
        const colors = [];
        res.body.forEach((value) => {
          data.push(value.value);
          lables.push(value.key);
          colors.push(this.dynamicColors());
        });

        this.attackerWebsiteChart = this.converDataChartPie(
          data,
          lables,
          colors
        );
      } else {
        console.warn("cannot load attacker website chart");
      }
    });
  }

  loadAttackerIpChart() {
    console.log("load attacker ip chart");
    const period: PeriodMonitor =
      this.periodFilterChart.controls["ipChart"].value;
    const parameters = {
      attacker_ip: 10,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorWafService.chart(parameters).subscribe((res) => {
      if (res.status === 200) {
        const data = [];
        const labels = [];
        res.body.forEach((value) => {
          data.push(value.value);
          labels.push(value.key);
        });

        this.attackerIpChart = {
          data: [
            ...[
              {
                data: data,
                label: "Ip Attack",
              },
            ],
          ],
          labels: labels,
        };
      } else {
        console.warn("cannot load attack ip chart");
      }
    });
  }

  loadAttackerCountryChart() {
    console.log("load attacker coutry chart");
    const period: PeriodMonitor =
      this.periodFilterChart.controls["worldPieChart"].value;
    const parameters = {
      attacker_country: 10,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorWafService.chart(parameters).subscribe((res) => {
      if (res.status === 200) {
        const data = [];
        const lables = [];
        const colors = [];
        if (res.body.length <= 0) {
          res.body.push({
            key: "No data",
            value: 100,
          });
        }

        res.body.forEach((value) => {
          data.push(value.value);
          const lable = COUNTRY_CENTER_GEO_POINTS[value.key]
            ? COUNTRY_CENTER_GEO_POINTS[value.key].name
            : value.key;
          lables.push(lable);
          colors.push(this.dynamicColors());
        });
        this.attackerCountryChart = this.converDataChartPie(
          data,
          lables,
          colors
        );
      } else {
        console.warn("can not load attack country chart");
      }
    });
  }

  converDataChartPie(data: number[], lables: string[], colors: string[]) {
    return {
      data: data,
      lables: lables,
      colors: [
        ...[
          {
            backgroundColor: colors,
          },
        ],
      ],
    };
  }

  loadAll() {
    this.paging = getParameterPaging(this.paging);
    console.log("load all");
    const period: PeriodMonitor = this.dataFilterForm.controls["period"].value;
    const parameters = {
      limit: this.paging.limit,
      offset: this.paging.offset,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }
    console.log(parameters);
    const filter = !this.dataFilterForm.controls["filter"].invalid
      ? this.dataFilterForm.controls["filter"].value
      : null;
    this.monitorWafService.query(parameters, filter).subscribe((res) => {
      if (res.status === 200) {
        this.monitorWafs = res.body.data.map((value) => {
          value["is_show"] = false;
          // value.request_header = JSON.parse(value.request_header);
          return value;
        });
        this.paging.limit = res.body.paging.limit;
        this.paging.offset = res.body.paging.offset;
        this.paging.total = res.body.paging.total;
        this.urlDownload = this.monitorWafService.downloadUrl(
          parameters,
          filter
        );
      } else {
        console.warn("can not load monitor ddos");
      }
    });
  }

  dynamicColors() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    return "rgba(" + r + "," + g + "," + b + ",0.8)";
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
    // this.loadAll();
  }

  //
  // transition() {
  //   const parameters = {
  //     offset: this.paging.offset - 1,
  //     limit: this.paging.limit
  //   }
  //   if (this.order.orderBy) {
  //     parameters['orderBy'] = this.order.orderBy;
  //     parameters['orderType'] = this.order.getOrderType();
  //   }
  //   if (this.dataFilterForm.controls['search'].value) {
  //     parameters['search'] = this.dataFilterForm.controls['search'].value;
  //   }
  //
  //   this.router.navigate(['/monitor/web-application'], {
  //     queryParams: parameters
  //   });
  //   // this.loadAll();
  // }

  changePeriodFilterForm() {
    this.paging.offset = 1;
    this.loadAll();
  }

  ngOnDestroy(): void {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  onDownload() {
    this.monitorWafService
      .download(this.urlDownload)
      .subscribe((res: Response) => {
        // 'Access-Control-Expose-Headers': 'Content-Disposition'
        let filename;
        const contentDisposition = res.headers.get("content-disposition");
        if (contentDisposition) {
          const parts: string[] = contentDisposition.split(";");
          if (parts && parts.length > 1) {
            const subParts = parts[1].split("=");
            if (subParts && subParts.length > 1) {
              filename = parts[1].split("=")[1];
            }
          }
        }
        const data = res.body;
        // @ts-ignore
        const blob = new Blob([data], {
          type: res.headers.get("Content-Type"),
        });
        const downloadURL = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = downloadURL;
        link.download = filename ? filename : "export_data.csv";
        link.click();
      });
  }
}
