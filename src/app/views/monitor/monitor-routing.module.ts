import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { Paging } from "@app/shared/model/base-respone.model";
import { MonitorSystemComponent } from "@app/views/monitor/system/monitor-system.component";
import { MonitorSettingComponent } from "@app/views/monitor/setting/monitor-setting.component";
import { MonitorDdosComponent } from "@app/views/monitor/ddos/monitor-ddos.component";
import { MonitorWebApplicationComponent } from "@app/views/monitor/web-application/monitor-web-application.component";
import { MonitorResourceComponent } from "@app/views/monitor/resource/monitor-resource.component";
import { ConnectionComponent } from "@app/views/monitor/connection/connection.component";
import { fileScanned } from "@app/views/monitor/file-scanned/file-scanned.component";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Monitor",
    },
    children: [
      {
        path: "",
        redirectTo: "system",
      },
      {
        path: "system",
        component: MonitorSystemComponent,
        data: {
          title: "System",
          pagingParams: new Paging(),
        },
      },
      {
        path: "setting",
        component: MonitorSettingComponent,
        data: {
          title: "Setting",
          pagingParams: new Paging(),
        },
      },
      {
        path: "ddos",
        component: MonitorDdosComponent,
        data: {
          title: "DDoS",
          pagingParams: new Paging(),
        },
      },
      {
        path: "web-application",
        component: MonitorWebApplicationComponent,
        data: {
          title: "Web Application",
          pagingParams: new Paging(),
        },
      },
      {
        path: "resource",
        component: MonitorResourceComponent,
        data: {
          title: "Resource",
        },
      },
      {
        path: "connection",
        component: ConnectionComponent,
        data: {
          title: "Connection",
        },
      },
      {
        path: "file-scanned",
        component: fileScanned,
        data: {
          title: "File Scanned",
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MonitorRoutingModule {}
