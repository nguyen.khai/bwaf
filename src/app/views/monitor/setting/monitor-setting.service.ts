import {HttpClient, HttpResponse} from '@angular/common/http';
import {IPaging} from '@app/shared/model/base-respone.model';
import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import {Observable} from "rxjs";
import {createRequestOption} from "@app/shared";
import {IGroupWebsite} from "@app/shared/model/group-website.model";
import {ISystem} from "@app/shared/model/setting.model";


type EntityResponseType = HttpResponse<ISystem>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;
interface PagingResponse {
  data: ISystem[];
  paging: IPaging;
}

@Injectable({ providedIn: 'root' })
export class MonitorSettingService {
  public resourceUrl = environment.apiUrl + '/monitor-system';

  constructor(protected http: HttpClient) {
  }
  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    console.log(options);
    console.log(req);
    return this.http
      .get<PagingResponse>(this.resourceUrl, { params: options, observe: 'response' });
  }
  search(name: string): Observable<EntityArrayResponseType> {
    const options = createRequestOption(name);
    return this.http
      .get<PagingResponse>(this.resourceUrl, { params: options, observe: 'response' });
  }
}
