import { Component, OnInit } from "@angular/core";
import { IPaging } from "@app/shared/model/base-respone.model";
import { FormArray, FormBuilder, FormGroup } from "@angular/forms";
import {
  getParameterPaging,
  OrderParam,
  PAGING_PER_PAGE,
} from "@app/shared/constants/common.model";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ISystem } from "@app/shared/model/setting.model";
import { MonitorSettingService } from "@app/views/monitor/setting/monitor-setting.service";

@Component({
  selector: "app-monitor-setting",
  templateUrl: "./monitor-setting.component.html",
  styleUrls: ["./monitor-setting.component.scss"],
})
export class MonitorSettingComponent implements OnInit {
  systems: ISystem[];
  paging: IPaging;
  systemFormGroup: FormGroup;
  keywordSearch: string;
  PAGING_PER_PAGE = PAGING_PER_PAGE;
  searchForm = this.fb.group({
    search: [""],
  });
  order: OrderParam = new OrderParam(null, false);

  constructor(
    public monitorSettingService: MonitorSettingService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.activatedRoute.data.subscribe((data) => {
      this.paging = data.pagingParams;
    });
  }

  ngOnInit(): void {
    this.initTable();
  }

  onSearch() {
    this.paging.page = 1;
    if (this.keywordSearch) {
      this.keywordSearch = this.keywordSearch.trim();
    }
    this.loadAll();
  }

  loadAll() {
    this.paging = getParameterPaging(this.paging);
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit,
    };
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters["search"] = this.keywordSearch;
    }
    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }
    this.monitorSettingService
      .query({
        ...parameters,
      })
      .subscribe((res) => {
        this.systemFormGroup = this.fb.group({
          data: this.fb.array([]),
        });
        this.systems = res.body.data;
        console.log(this.systems);
        const control = <FormArray>this.systemFormGroup.get("data");
        for (const system of this.systems) {
          const grp = this.fb.group({
            id: [system.id],
            ip_address: [system.ip_address],
            datetime: [system.datetime],
            user_name: [system.user_name],
            action: [system.action],
            description: [system.description],
          });
          control.push(grp);
        }
        this.paging.total = res.body.paging.total;
      });
  }
  get getFormData(): FormArray {
    return <FormArray>this.systemFormGroup.get("data");
  }

  initTable() {
    this.systemFormGroup = this.fb.group({
      data: this.fb.array([]),
    });
    this.loadAll();
  }
  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }
  /*transition() {
    const parameters = {
      offset: this.paging.offset - 1,
      limit: this.paging.limit
    };
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters['search'] = this.keywordSearch;
    }
    /!*if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }*!/
    this.router.navigate(['/monitor/setting'], {
      queryParams: parameters
    });
  }*/
  changeOder(order) {
    this.order = order;
    this.loadAll();
  }
}
