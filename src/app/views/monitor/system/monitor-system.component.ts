import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BaseChartDirective} from 'ng2-charts';
import {ChartDataSets} from 'chart.js';
import {Router} from '@angular/router';
import * as moment from 'moment';
import {MonitorSystemService} from '@app/views/monitor/system/monitor-system.service';
import {
  MonitorStatusCode,
  IMonitorConnection,
  IMonitorThroughput,
  IMonitorStatusCode
} from '@app/shared/model/monitor-system.model';
import {NetworkService} from "@app/views/network/network.service";
import {IInterface} from "@app/shared/model/interface.model";
import {PERIOD_MONITOR, PeriodMonitor} from "@app/shared/constants/monitor.model";
import {REQUEST_INTERVAL} from "@app/shared/constants/common.model";

@Component({
  selector: 'app-monitor-system',
  templateUrl: './monitor-system.component.html',
  styleUrls: ['./monitor-system.component.scss']
})
export class MonitorSystemComponent implements OnInit, OnDestroy {
  @ViewChild('baseChart', {static: false}) chart: BaseChartDirective;
  @ViewChild('httpCodeChart', {static: false}) httpCodeChart: BaseChartDirective;
  @ViewChild('connectionChart', {static: false}) connectionChart: BaseChartDirective;
  REQUEST_INTERVAL = REQUEST_INTERVAL;
  public selectTimeOption = [
    {type: 'minute', value: 5, label: '5 Minutes'},
    {type: 'minute', value: 30, label: '30 Minutes'},
    {type: 'hour', value: 1, label: '1 Hour'},
    {type: 'day', value: 1, label: '1 Day'},
    {type: 'day', value: 7, label: '7 Day'},
    {type: 'month', value: 1, label: '1 Month'},
  ];
  public selectTimeOptionConnection = [
    {type: 'minute', value: 5, label: '5 Minutes'},
    {type: 'minute', value: 30, label: '30 Minutes'},
    {type: 'hour', value: 1, label: '1 Hour'},
    {type: 'day', value: 1, label: '1 Day'},
    {type: 'day', value: 7, label: '7 Day'},
    {type: 'month', value: 1, label: '1 Month'},
  ];
  public selectTimeOptionStatusCode = [
    {type: 'minute', value: 5, label: '5 Minutes'},
    {type: 'minute', value: 30, label: '30 Minutes'},
    {type: 'hour', value: 1, label: '1 Hour'},
    {type: 'day', value: 1, label: '1 Day'},
    {type: 'day', value: 7, label: '7 Day'},
    {type: 'month', value: 1, label: '1 Month'},
  ];
  public selectTime = this.selectTimeOption[0];
  public selectTimeConnection = this.selectTimeOptionConnection[0];
  public selectTimeStatusCode = this.selectTimeOptionStatusCode[0];
  public dateRange = null;
  public params2 = {
    orderBy: 'id',
    orderType: 'desc',
    dateType: 'minute',
    dateValue: 5,
    dateFrom: null,
    dateTo: null,
    avg_value: 100,
    interface_name: ''
  };
  public params = {
    orderBy: 'id',
    orderType: 'desc',
    dateType: 'minute',
    dateValue: 5,
    dateFrom: null,
    dateTo: null,
    avg_value: 100
  };
  public params1 = {
    orderBy: 'id',
    orderType: 'desc',
    dateType: 'minute',
    dateValue: 5,
    dateFrom: null,
    dateTo: null,
    avg_value: 100
  };
  timer;
  networkInterfaces: IInterface[];
  selectedInterface: string;
  public count = [];
  public inputData = [];
  public outputData = [];
  public datetimeData = [];
  monitorConnection: IMonitorConnection[];
  public accepts = [];
  public active = [];
  public datetime = [];
  public handled = [];
  public reading = [];
  public requests = [];
  public waiting = [];
  public writing = [];
  monitorStatusCode: IMonitorStatusCode[];
  public code_1 = [];
  public code_2 = [];
  public code_3 = [];
  public code_4 = [];
  public code_5 = [];
  public size = '';
  lineChartLabels: any;
  public lineChartType = 'line';
  public lineChartLegend = true;
  public lineChartPlugins = [];

  public selected: number = 0;

  public lineChartData: ChartDataSets[] = [
    { data: [0], label: 'Input', spanGaps: true},
    {data: [0], label: 'Output', spanGaps: true},
  ];

  public httpCodeChartData: ChartDataSets[] = [
    {data: this.code_1 ? this.code_1 : [0], label: '1xx', spanGaps: true},
    {data: this.code_2 ? this.code_2 : [0], label: '2xx', spanGaps: true},
    {data: this.code_3 ? this.code_3 : [0], label: '3xx', spanGaps: true},
    {data: this.code_4 ? this.code_4 : [0], label: '4xx', spanGaps: true},
    {data: this.code_5 ? this.code_5 : [0], label: '5xx', spanGaps: true},
  ];

  public lineChartDataConnection: ChartDataSets[] = [
    // {data: this.accepts ? this.accepts : [0], label: 'Accepts', spanGaps: true},
    {data: this.active ? this.active : [0], label: 'Active', spanGaps: true},
    // {data: this.handled ? this.handled : [0], label: 'Handled', spanGaps: true},
    {data: this.reading ? this.reading : [0], label: 'Reading', spanGaps: true},
    // {data: this.requests ? this.requests : [0], label: 'Requests', spanGaps: true},
    {data: this.waiting ? this.waiting : [0], label: 'Waiting', spanGaps: true},
    {data: this.writing ? this.writing : [0], label: 'Writing', spanGaps: true},
  ];
  // size = 'B';
  firstItem = 0;
  public lineChartOptions: any = {
    responsive: true,
    labels: {
      fontSize: 10,
      usePointStyle: true,
    },
    tooltips: {
      backgroundColor: 'rgba(255,255,255,0.9)',
      bodyFontColor: '#999',
      borderColor: '#999',
      borderWidth: 1,
      caretPadding: 15,
      colorBody: '#666',
      displayColors: false,
      enabled: true,
      intersect: true,
      mode: 'x',
      titleFontColor: '#999',
      titleMarginBottom: 10,
      xPadding: 15,
      yPadding: 15,
    },
    elements: {
      point: {
        radius: 2
      },
      line: {
        tension: 0.4,
        fill: false,
        spanGaps: true
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10,
          callback: (value, index, values) => {
            return value + ' ' + this.size;
          }
        }
      }],
      xAxes: [{
        type: 'time',
        time: {
          unit: 'minute'
        }
      }]
    }
  };

  public httpCodeChartOption: any = {
    responsive: true,
    elements: {
      point: {
        radius: 0
      },
      line: {
        tension: 0.4,
        fill: false,
        spanGaps: true
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          suggestedMin: 0,
          suggestedMax: 10
        }
      }],
      xAxes: [{
        type: 'time',
        time: {
          unit: 'minute'
        }
      }]
    }
  };

  public connectionChartOptions: any = {
    responsive: true,
    elements: {
      line: {
        tension: 0.4,
        fill: false,
        spanGaps: true
      },
      point: {
        radius: 0
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          suggestedMin: 0,
          suggestedMax: 100
        }
      }],
      xAxes: [{
        type: 'time',
        time: {
          unit: 'minute'
        }
      }]
    }
  };

  constructor(
    protected monitorSystemService: MonitorSystemService,
    protected router: Router,
    public networkServiceService: NetworkService,
  ) {

  }

  ngOnInit() {
    this.loadAll();
    this.timer = setInterval(() => {
      // this.getMonitorStatusCode();
      // this.getMonitorConnection();
      this.getTrafficByInterface(this.selectedInterface);
    }, this.REQUEST_INTERVAL);
  }

  ngOnDestroy(): void {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  loadAll() {
    this.transition();
    this.getMonitorStatusCode();
    this.getMonitorConnection();
    this.getMonitorTraffic();
  }

  getMonitorStatusCode() {
    this.monitorSystemService.getMonitorStatusCode(this.params).subscribe(res => {
      this.monitorStatusCode = res.body.data;
      this.prepareHttpCodeMonitorData(this.monitorStatusCode);
      // this.prepareHttpCodeMonitorData(res.body.data);
    });
  }

  getMonitorConnection() {
    this.monitorSystemService.getMonitorConnection(this.params1).subscribe(res => {
      this.monitorConnection = res.body.data;
      this.prepareMonitorConnection(this.monitorConnection);
    });
  }
  onChange (points, evt) {
    console.log(points, evt);
  }

  getMonitorTraffic() {
    this.networkServiceService.query().subscribe(res => {
      this.networkInterfaces = res.body.data;
      this.getTrafficByInterface(this.networkInterfaces[0].name);
    });
  }

  getTrafficByInterface(name: string) {
    this.selectedInterface = name;
    if (name !== undefined) {
      this.params2.interface_name = name;
    }
    this.monitorSystemService.getMonitorTraffic(this.params2).subscribe(res => {
      const monitorThroughtputs = res.body.data;
      this.prepareData(monitorThroughtputs);
    });
  }

  reset() {
    this.datetimeData.length = 0;
    this.inputData.length = 0;
    this.outputData.length = 0;
  }

  resetConnection() {
    this.datetimeData.length = 0;
    this.accepts.length = 0;
    this.active.length = 0;
    this.datetime.length = 0;
    this.handled.length = 0;
    this.reading.length = 0;
    this.requests.length = 0;
    this.waiting.length = 0;
    this.writing.length = 0;
  }

  resetHttpStatusCode() {
    this.datetimeData.length = 0;
    this.code_5.length = 0;
    this.code_4.length = 0;
    this.code_3.length = 0;
    this.code_2.length = 0;
    this.code_1.length = 0;
  }

  trackByTypeId(item) {
    return item.id;
  }

  prepareData(monitorThroughputs) {
    this.reset();
    this.firstItem = monitorThroughputs[0].input;
    monitorThroughputs.forEach(data => {
      if (this.firstItem < data.input) {
        this.firstItem = data.input;
      }
    });
    if (this.firstItem < 1024) {
      this.size = 'bps';
    } else if (this.firstItem < 1000000) {
      this.size = 'Kbps';
    } else if (this.firstItem < 1000000000) {
      this.size = 'Mbps';
    } else if (this.firstItem < 1000000000 * 1000) {
      this.size = 'Gbps';
    } else if (this.firstItem < 1000000000 * 1000 * 1000) {
      this.size = 'Tbps';
    } else {
      this.size = 'Pbps';
    }
    monitorThroughputs.forEach(data => {
      this.inputData.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        // y: this.fomatBytes(data.input)
        y: this.formatSizeUnits1(data.input, data.output)
      });
      this.outputData.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        // y: this.fomatBytes(data.output)
        y: this.formatSizeUnits2(data.input, data.output)
      });
    });
    this.reloadChart();

  }

  prepareMonitorConnection(monitorConnection: IMonitorConnection[]) {
    this.resetConnection();
    this.monitorConnection.forEach(data => {
      // this.accepts.push({
      //   x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
      //   y: data.accepts
      // });
      this.active.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.active
      });
      // this.handled.push({
      //   x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
      //   y: data.handled
      // });
      this.reading.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.reading
      });
      // this.requests.push({
      //   x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
      //   y: data.requests
      // });
      this.waiting.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.waiting
      });
      this.writing.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.writing
      });
    });
    this.reloadChartConnection();
  }

  prepareHttpCodeMonitorData(monitorStatusCode: MonitorStatusCode[]) {
    this.resetHttpStatusCode();
    this.monitorStatusCode.forEach(data => {
      this.code_1.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.code_1
      });
      this.code_2.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.code_2
      });
      this.code_3.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.code_3
      });
      this.code_4.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.code_4
      });
      this.code_5.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.code_5
      });
    });
    this.reloadChartStatusCode();
  }

  transition() {
    const params = {
      interface_id: null
    };
    this.router.navigate(['/monitor/system'], {
      queryParams: params
    });
  }

  reloadChart() {
    if (this.chart !== undefined) {
      this.lineChartData[0].data = this.inputData;
      this.lineChartData[1].data = this.outputData;
      this.chart.datasets = this.lineChartData;
      this.chart.update();
    }
  }

  reloadChartConnection() {
    if (this.chart !== undefined) {
      // this.lineChartDataConnection[0].data = this.accepts;
      this.lineChartDataConnection[1].data = this.active;
      // this.lineChartDataConnection[2].data = this.handled;
      this.lineChartDataConnection[3].data = this.reading;
      // this.lineChartDataConnection[4].data = this.requests;
      this.lineChartDataConnection[5].data = this.waiting;
      this.lineChartDataConnection[6].data = this.writing;
      this.chart.datasets = this.lineChartDataConnection;
      this.chart.update();
    }
  }

  reloadChartStatusCode() {
    if (this.httpCodeChart !== undefined) {
      this.httpCodeChartData[0].data = this.code_1;
      this.httpCodeChartData[1].data = this.code_2;
      this.httpCodeChartData[2].data = this.code_3;
      this.httpCodeChartData[3].data = this.code_4;
      this.httpCodeChartData[4].data = this.code_5;
      this.httpCodeChart.update();
    }
  }

  refresh() {
    this.resetParams();
    this.getMonitorTraffic();
  }

  refreshStatusCode() {
    this.resetParamsStatusCode();
    this.getMonitorStatusCode();
  }

  refreshConnection() {
    this.resetParamsConnection();
    this.getMonitorConnection();
  }

  resetParams() {
    this.selectTime = this.selectTimeOption[0];
    this.dateRange = null;
    this.params = {
      orderBy: 'id',
      orderType: 'desc',
      dateType: null,
      dateValue: null,
      dateFrom: null,
      dateTo: null,
      avg_value: null,
    };
  }

  resetParamsConnection() {
    this.selectTimeConnection = this.selectTimeOptionConnection[0];
    this.dateRange = null;
    this.params1 = {
      orderBy: 'id',
      orderType: 'desc',
      dateType: null,
      dateValue: null,
      dateFrom: null,
      dateTo: null,
      avg_value: null
    };
  }

  resetParamsStatusCode() {
    this.selectTimeStatusCode = this.selectTimeOptionStatusCode[0];
    this.dateRange = null;
    this.params = {
      orderBy: 'id',
      orderType: 'desc',
      dateType: null,
      dateValue: null,
      dateFrom: null,
      dateTo: null,
      avg_value: null
    };
  }

  selectTimeInterval(selectedTime) {
    this.selectTime = selectedTime;
    this.params2.dateFrom = null;
    this.params2.dateTo = null;
    this.params2.dateValue = null;
    this.params2.dateType = this.selectTime.type;
    if (this.selectTime.type === 'between') {
      this.params2.dateFrom = this.selectTime.value[0].format('YYYY-MM-DD HH:mm:ss');
      this.params2.dateTo = this.selectTime.value[1].format('YYYY-MM-DD HH:mm:ss');
    } else {
      this.params2.dateValue = this.selectTime.value;
    }
    this.getMonitorTraffic();
  }

  selectTimeIntervalStatusCode(selectedTime) {
    this.selectTimeStatusCode = selectedTime;
    this.params.dateFrom = null;
    this.params.dateTo = null;
    this.params.dateValue = null;
    this.params.dateType = this.selectTimeStatusCode.type;
    if (this.selectTimeConnection.type === 'between') {
      this.params.dateFrom = this.selectTimeStatusCode.value[0].format('YYYY-MM-DD HH:mm:ss');
      this.params.dateTo = this.selectTimeStatusCode.value[1].format('YYYY-MM-DD HH:mm:ss');
    } else {
      this.params.dateValue = this.selectTimeStatusCode.value;
    }
    this.getMonitorStatusCode();
  }

  selectTimeIntervalConnection(selectedTime) {
    this.selectTimeConnection = selectedTime;
    this.params1.dateFrom = null;
    this.params1.dateTo = null;
    this.params1.dateValue = null;
    this.params1.dateType = this.selectTimeConnection.type;
    if (this.selectTimeStatusCode.type === 'between') {
      this.params1.dateFrom = this.selectTimeConnection.value[0].format('YYYY-MM-DD HH:mm:ss');
      this.params1.dateTo = this.selectTimeConnection.value[1].format('YYYY-MM-DD HH:mm:ss');
    } else {
      this.params1.dateValue = this.selectTimeConnection.value;
    }
    this.getMonitorConnection();
  }

  fomatBytes(bytes) {
    if (bytes < 1000) {
      return '0 byte';
    } else {
      const i = parseInt(String(Math.floor(Math.log(bytes) / Math.log(1000))));
      // @ts-ignore
      return Math.round(bytes / Math.pow(1000, i), 2);
    }
  }

  formatSizeUnits1(bytes1, bytes2) {
    if (this.firstItem >= 1073741824) {
      bytes1 = (bytes1 / 1073741824).toFixed(2);
      bytes2 = (bytes2 / 1073741824).toFixed(2);
    } else if (this.firstItem >= 1048576) {
      bytes1 = (bytes1 / 1048576).toFixed(2);
      bytes2 = (bytes2 / 1048576).toFixed(2);
    } else if (this.firstItem >= 1) {
      bytes1 = (bytes1 / 1024).toFixed(2);
      bytes2 = (bytes2 / 1024).toFixed(2);
    } else {
      bytes1 = "0 bytes";
    }
    return bytes1;
  }

  formatSizeUnits2(bytes1, bytes2) {
    if (this.firstItem >= 1073741824) {
      bytes1 = (bytes1 / 1073741824).toFixed(2);
      bytes2 = (bytes2 / 1073741824).toFixed(2);
    } else if (this.firstItem >= 1048576) {
      bytes1 = (bytes1 / 1048576).toFixed(2);
      bytes2 = (bytes2 / 1048576).toFixed(2);
    } else if (this.firstItem >= 1) {
      bytes1 = (bytes1 / 1024).toFixed(2);
      bytes2 = (bytes2 / 1024).toFixed(2);
    } else {
      bytes1 = "0 bytes";
    }
    return bytes2;
  }
  public chartHovered(e: any): void {
    console.log("hover", e);
  }
  public chartClicked(e: any): void {
    console.log("click", e);
  }
}
