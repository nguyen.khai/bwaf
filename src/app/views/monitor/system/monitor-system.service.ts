import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IPaging} from '@app/shared/model/base-respone.model';
import {createRequestOption} from '@app/shared';
import {IMonitorConnection, IMonitorStatusCode, IMonitorThroughput} from '@app/shared/model/monitor-system.model';
import {initHour} from 'ngx-bootstrap/chronos/units/hour';

type EntityArrayResponseType = HttpResponse<PagingResponse>;

interface PagingResponse {
  data: IMonitorThroughput[];
  paging: IPaging;
}

type EntityArrayResponseTypeStatusCode = HttpResponse<PagingResponse2>;

interface PagingResponse2 {
  data: IMonitorStatusCode[];
  paging: IPaging;
}

type EntityArrayResponseTypeConnection = HttpResponse<PagingResponse1>;

interface PagingResponse1 {
  data: IMonitorConnection[];
  paging: IPaging;
}


@Injectable({
  providedIn: 'root'
})
export class MonitorSystemService {
  public resourceUrl = environment.apiUrl + '/monitor-traffic';
  public resourceUrlConnection = environment.apiUrl + '/monitor-connection';
  public resourceUrlStatusCode = environment.apiUrl + '/monitor-http-status';

  constructor(
    protected http: HttpClient
  ) {
  }

  getMonitorTraffic(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<any>(this.resourceUrl, {params: options, observe: 'response'});
    // const options = {
    //   interface_id: interface_id
    // };
    // return this.http
    //   .get<any>(`${this.resourceUrl}`, {params: options, observe: 'response'});
  }

  getMonitorConnection(req?: any): Observable<EntityArrayResponseTypeConnection> {
    const options = createRequestOption(req);
    return this.http.get<any>(this.resourceUrlConnection, {params: options, observe: 'response'});
  }


  getMonitorStatusCode(req?: any): Observable<EntityArrayResponseTypeStatusCode> {
    const options = createRequestOption(req);
    return this.http.get<any>(this.resourceUrlStatusCode, {params: options, observe: 'response'});
  }
}
