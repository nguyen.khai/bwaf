import { Component, ElementRef, OnDestroy, OnInit } from "@angular/core";
import { MonitorDdosService } from "@app/views/monitor/ddos/monitor-ddos.service";
import { ActivatedRoute, Router, RouterModule } from "@angular/router";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, Validators } from "@angular/forms";
import { MonitorWafService } from "@app/views/monitor/file-scanned/file-scanned.service";
import { IPaging, Paging } from "@app/shared/model/base-respone.model";
import {
  getParameterPaging,
  OrderParam,
  PAGING_PER_PAGE,
  REQUEST_INTERVAL,
} from "@app/shared/constants/common.model";
import { IMonitorWaf } from "@app/shared/model/monitor-waf";
import { Location } from "@angular/common";
import {
  COUNTRIES,
  COUNTRY_CENTER_GEO_POINTS,
  PERIOD_MONITOR,
  PeriodMonitor,
} from "@app/shared/constants/monitor.model";
import { IField } from "@app/shared/ng-mutifilter/ng-mutifilter.model";
@Component({
  selector: "app-file-scanned",
  templateUrl: "./file-scanned.component.html",
  styleUrls: ["./file-scanned.component.scss"],
})
export class fileScanned implements OnInit, OnDestroy {
  periodFilterForms: PeriodMonitor[] = PERIOD_MONITOR;
  dataFilterForm = this.fb.group({
    filter: ["", [Validators.required]],
    period: [this.periodFilterForms[0]],
  });

  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  order: OrderParam = new OrderParam("datetime", false);

  periodCharts: PeriodMonitor[] = PERIOD_MONITOR;
  periodFilterChart = this.fb.group({
    worldMapChart: [this.periodCharts[0]],
    worldPieChart: [this.periodCharts[0]],
    ipChart: [this.periodCharts[0]],
    attackWebsiteChart: [this.periodCharts[0]],
    attackActionChart: [this.periodCharts[0]],
  });

  timer;
  monitorWafs: IMonitorWaf[];
  paging: IPaging;
  limits = PAGING_PER_PAGE;
  attackerCountryChart;
  attackerIpChart;
  attackerWebsiteChart;
  attackerActionChart;
  // world-map
  worldMapChart: Array<any>;
  keywordSearch: string;
  urlDownload: string;
  parameters: Object;
  filter: string;
  fields: Array<IField> = [
    { label: "IP Addr", value: "attacker_ip" },
    { label: "Blocked time", value: "datetime" },
    { label: "County", value: "attacker_country" },
    { label: "Rule", value: "group_rule" },
    { label: "Website domain", value: "website_domain" },
  ];

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  changeLimit() {
    this.paging.page = 1;
    this.loadAll();
  }

  constructor(
    public monitorWafService: MonitorWafService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.paging = new Paging();
    this.monitorWafs = [];
  }

  ngOnInit() {
    this.loadAll();
  }

  onFilter($event) {
    this.paging.page = 1;
    this.dataFilterForm.controls["filter"].setValue($event);
    this.loadAll();
  }

  onSearch() {
    this.paging.offset = 1;
    if (this.keywordSearch) {
      this.keywordSearch = this.keywordSearch.trim();
    }
    this.loadAll();
  }

  refreshSearchForm() {
    this.dataFilterForm.controls["period"].setValue(this.periodFilterForms[0]);
    this.dataFilterForm.controls["filter"].setValue("");
    this.order.orderBy = "datetime";
    this.order.orderType = false;
    this.loadAll();
  }

  loadAll() {
    this.paging = getParameterPaging(this.paging);
    console.log("load all");
    const period: PeriodMonitor = this.dataFilterForm.controls["period"].value;
    const parameters = {
      limit: this.paging.limit,
      offset: this.paging.offset,
    };
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters["search"] = this.keywordSearch;
    }
    const filter = !this.dataFilterForm.controls["filter"].invalid
      ? this.dataFilterForm.controls["filter"].value
      : null;
    this.monitorWafService.query(parameters, filter).subscribe((res) => {
      if (res.status === 200) {
        this.monitorWafs = res.body.data.map((value) => {
          value["is_show"] = false;
          return value;
        });

        this.paging.limit = res.body.paging.limit;
        this.paging.offset = res.body.paging.offset;
        this.paging.total = res.body.paging.total;
        this.parameters = parameters;
        this.filter = filter;
      } else {
        console.warn("can not load monitor ddos");
      }
    });
  }

  dynamicColors() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    return "rgba(" + r + "," + g + "," + b + ",0.8)";
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }

  changePeriodFilterForm() {
    this.paging.offset = 1;
    this.loadAll();
  }

  ngOnDestroy(): void {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  onDownload(filename) {
    this.urlDownload = this.monitorWafService.downloadUrl(
      this.parameters,
      this.filter,
      filename
    );
    this.monitorWafService
      .download(this.urlDownload)
      .subscribe((res: Response) => {
        const data = res.body;
        // @ts-ignore
        const blob = new Blob([data], {
          type: res.headers.get("Content-Type"),
        });
        const downloadURL = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        const that = this;
        var reader = new FileReader();
        reader.onload = function () {
          // if (
          //   typeof JSON.parse(reader.result as string) == "object" &&
          //   JSON.parse(reader.result as string).status
          // ) {
          //   that.notificationService.error(
          //     JSON.parse(reader.result as string).message
          //   );
          // } else {
          //   link.href = downloadURL;
          //   link.download = filename ? filename : "export_data.csv";
          //   link.click();
          // }
          try {
            if (
              typeof JSON.parse(reader.result as string) == "object" &&
              JSON.parse(reader.result as string).status
            ) {
              that.notificationService.error(
                JSON.parse(reader.result as string).message
              );
            }
          } catch (e) {
            link.href = downloadURL;
            link.download = filename ? filename : "export_data.csv";
            link.click();
          }
        };
        reader.readAsText(blob);
      });
  }
}
