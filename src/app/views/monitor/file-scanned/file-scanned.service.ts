import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { map } from "rxjs/operators";
import { IPaging } from "@app/shared/model/base-respone.model";
import { createRequestOption } from "@app/shared";
import { IMonitorWaf } from "@app/shared/model/monitor-waf";

type EntityResponseType = HttpResponse<IMonitorWaf>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;

interface PagingResponse {
  data: IMonitorWaf[];
  paging: IPaging;
}

@Injectable({
  providedIn: "root",
})
export class MonitorWafService {
  public resourceUrl = environment.apiUrl + "/file_scanned";

  constructor(protected http: HttpClient) {}

  query(req, filter?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    let url = this.resourceUrl;
    if (filter) {
      url += `?${filter}`;
    }
    return this.http.get<PagingResponse>(url, {
      params: options,
      observe: "response",
    });
  }

  downloadUrl(req, filter?: any, filename?: any): string {
    const options = createRequestOption(req);
    let paramUrl = "";
    if (filter) {
      paramUrl += filter;
    }
    options.keys().forEach((key) => {
      if (paramUrl !== "") {
        paramUrl += "&";
      }
      paramUrl += key + "=" + encodeURIComponent(options.get(key));
    });
    return `${this.resourceUrl}/${filename}/download`;
  }

  chart(req?: any): Observable<HttpResponse<Array<any>>> {
    const options = createRequestOption(req);
    return this.http
      .get<Array<any>>(`${environment.apiUrl}/monitor-waf-chart`, {
        params: options,
        observe: "response",
      })
      .pipe(
        map((res: HttpResponse<Array<any>>) => {
          return res;
        })
      );
  }

  download(url: string): Observable<any> {
    return this.http.get(url, {
      observe: "response",
      responseType: "blob" as "json",
    });
  }
}
