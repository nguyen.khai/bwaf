import {Injectable} from '@angular/core';
import {environment} from '@env/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IMonitorResource} from '@app/shared/model/monitor-resource.model';
import {IPaging} from '@app/shared/model/base-respone.model';
import {createRequestOption} from "@app/shared";

type EntityArrayResponseType = HttpResponse<PagingResponse>;
interface PagingResponse {
  data: IMonitorResource[];
  paging: IPaging;
}


@Injectable({
  providedIn: 'root'
})
export class MonitorResourceService {
  public resourceUrl = environment.apiUrl + '/monitor-resource';
  constructor(
    protected http: HttpClient
  ) {}

  get(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<any>(this.resourceUrl, {params: options, observe: 'response'});
  }

}
