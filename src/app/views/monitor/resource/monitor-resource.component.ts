import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {BaseChartDirective, Color, Label} from 'ng2-charts';
import {ChartDataSets, ChartElementsOptions, ChartOptions, ChartType} from 'chart.js';
import {IMonitorResource} from '@app/shared/model/monitor-resource.model';
import {MonitorResourceService} from '@app/views/monitor/resource/monitor-resource.service';
import {DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE} from "ng-pick-datetime";
import {MomentDateTimeAdapter} from "ng-pick-datetime-moment";
import {OWL_COMMON_DATE_FORMAT, OWL_COMMON_DATETIME_FORMAT, REQUEST_INTERVAL} from "@app/shared/constants/common.model";
import {Router} from "@angular/router";
import * as moment from 'moment';
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-monitor-resource',
  templateUrl: './monitor-resource.component.html',
  styleUrls: ['./monitor-resource.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: OWL_COMMON_DATETIME_FORMAT}
  ]
})
export class MonitorResourceComponent implements OnInit, OnDestroy {
  @ViewChild('baseChart', {static: false}) chart: BaseChartDirective;
  public resources: IMonitorResource[];
  public cpuData = [];
  public ramData = [];
  public diskData = [];
  public timeData = [];
  public params = {
    orderBy: 'id',
    orderType: 'desc',
    dateType: 'minute',
    dateValue: 5,
    dateFrom: null,
    dateTo: null,
    avg_value: 100
  };
  timer;
  lineChartLabels: any;
  public selectTimeOption = [
    {type: 'minute', value: 5, label: '5 Minutes'},
    {type: 'minute', value: 30, label: '30 Minutes'},
    {type: 'hour', value: 1, label: '1 Hour'},
    {type: 'day', value: 1, label: '1 Day'},
    {type: 'day', value: 7, label: '7 Days'},
    {type: 'month', value: 1, label: '1 Month'},
  ];
  public selectTime = this.selectTimeOption[0];
  periodFilterChart = this.fb.group({
    selectTime: this.selectTimeOption[0],
  });
  public lineChartData: ChartDataSets[] = [
    {data: this.cpuData ? this.cpuData : [0], label: 'CPU', spanGaps: true},
    {data: this.ramData ? this.ramData : [0], label: 'RAM', spanGaps: true},
    {data: this.diskData ? this.diskData : [0], label: 'Disk', spanGaps: true}
  ];
  public lineChartOptions: any = {
    responsive: true,
    elements: {
      point: {
        radius: 0
      },
      line: {
        tension: 0.4,
        fill: false,
        spanGaps: true

      }
    },
    scales: {
      yAxes: [{
        ticks: {
          suggestedMin: 0,
          suggestedMax: 100
        }
      }],
      xAxes: [{
        type: 'time',
        time: {
          unit: 'hour',
          // stepSize: '12'
        }
      }]
    }
  };

  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  constructor(
    protected resourceService: MonitorResourceService,
    protected router: Router,
    private fb: FormBuilder
  ) {
  }

  ngOnInit() {
    this.loadAll();
    this.interval();
  }

  ngOnDestroy(): void {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  resetParams() {
    this.selectTime = this.selectTimeOption[0];
    this.params = {
      orderBy: 'id',
      orderType: 'desc',
      dateType: null,
      dateValue: null,
      dateFrom: null,
      dateTo: null,
      avg_value: null
    };
  }

  refresh() {
    this.resetParams();
    this.loadAll();
  }

  loadAll() {
    this.resourceService.get(
      this.params
    ).subscribe(res => {
      this.resources = res.body.data;
      this.prepareData(this.resources);
    });
  }

  reset() {
    this.cpuData.length = 0;
    this.diskData.length = 0;
    this.timeData.length = 0;
    this.ramData.length = 0;
  }

  prepareData(resources: IMonitorResource[]) {
    this.reset();
    this.resources.forEach(data => {
      this.cpuData.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.cpu
      });
      this.diskData.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.disk
      });
      this.ramData.push({
        x: moment(data.datetime, 'YYYY-MM-DD HH:mm:ss'),
        y: data.ram
      });
    });
    this.reloadChart();
  }

  selectTimeInterval(selectedTime) {
    this.selectTime = selectedTime;
    console.log(this.selectTime);
    this.params.dateFrom = null;
    this.params.dateTo = null;
    this.params.dateValue = null;
    this.params.dateType = this.selectTime.type;
    if (this.selectTime.type === 'between') {
      this.params.dateFrom = this.selectTime.value[0].format('YYYY-MM-DD HH:mm:ss');
      this.params.dateTo = this.selectTime.value[1].format('YYYY-MM-DD HH:mm:ss');
    } else {
      this.params.dateValue = this.selectTime.value;
    }
    this.loadAll();
  }

  // transition() {
  //   const params = {
  //     orderBy: 'id',
  //     orderType: 'desc',
  //     dateType: null,
  //     dateValue: null,
  //     dateFrom: null,
  //     dateTo: null
  //   };
  //   if (this.dateRange) {
  //     params.dateType = 'between';
  //     params.dateFrom = this.dateRange[0].format('YYYY-MM-DD HH:mm:ss');
  //     params.dateTo = this.dateRange[1].format('YYYY-MM-DD HH:mm:ss');
  //   } else if (this.selectTime) {
  //     params.dateType = this.selectTime.type;
  //     params.dateValue = this.selectTime.value;
  //   } else {
  //   }
  //   this.router.navigate(['/monitor/resource'], {
  //     queryParams: params
  //   });
  // }

  reloadChart() {
    if (this.chart !== undefined) {
      this.lineChartData[0].data = this.cpuData;
      this.lineChartData[1].data = this.ramData;
      this.lineChartData[2].data = this.diskData;
      this.chart.datasets = this.lineChartData;
      this.chart.update();
    }
  }

  interval() {
    this.timer = setInterval(() => {
        if (this.periodFilterChart.controls['selectTime'].value.type !== 'between') {
          this.loadAll();
        }
      }, REQUEST_INTERVAL
    );
  }
}
