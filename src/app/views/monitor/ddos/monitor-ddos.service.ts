import {Injectable} from '@angular/core';
import {environment} from 'environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import * as moment from 'moment';
import {map} from 'rxjs/operators';
import {IPaging} from '@app/shared/model/base-respone.model';
import {ILookMonitorDdos, IMonitorDdos} from "@app/shared/model/monitor-ddos.model";
import {createRequestOption} from "@app/shared";


type EntityResponseType = HttpResponse<IMonitorDdos>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;

interface PagingResponse {
  data: IMonitorDdos[];
  paging: IPaging;
}

interface WorldMapResponse {
  country?: string;
  percent?: number;
}

interface WorldChartPieResponse {
  website?: string;
  percent?: number;
}

const APPLICATION = 'application';
const NETWORK = 'network';

@Injectable({
  providedIn: 'root'
})
export class MonitorDdosService {

  public resourceUrl = environment.apiUrl + '/monitor-ddos';

  constructor(protected http: HttpClient) {
  }

  private query(type: string, req: any, filter?: any): Observable<EntityArrayResponseType> {
    let url = `${this.resourceUrl}/${type}`;
    if (filter) {
      url += `?${filter}`;
    }
    const options = createRequestOption(req);
    return this.http
      .get<PagingResponse>(url, {params: options, observe: 'response'});
  }

  private find(type: string, id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMonitorDdos>(`${this.resourceUrl}/${type}/${id}`, {observe: 'response'});
  }

  private downloadUrl(type: string, req, filter?: any): string {
    const options = createRequestOption(req);
    let paramUrl = "";
    if (filter) {
      paramUrl += filter;
    }
    options.keys().forEach(key => {
      if (paramUrl != "") {
        paramUrl += "&";
      }
      paramUrl += key + "=" + encodeURIComponent(options.get(key));
    })
    return `${this.resourceUrl}/${type}/download?${paramUrl}`;

  }

  downloadNetworkUrl(req: any, filter?: any): string {
    return this.downloadUrl(NETWORK, req, filter);
  }

  downloadApplicationUrl(req: any, filter?: any): string {
    return this.downloadUrl(APPLICATION, req, filter);
  }

  queryApplication(req: any, filter?: any): Observable<EntityArrayResponseType> {
    return this.query(APPLICATION, req, filter);
  }

  findApplication(id: number): Observable<EntityResponseType> {
    return this.find(APPLICATION, id);
  }

  queryNetwork(req: any, filter?: any): Observable<EntityArrayResponseType> {
    return this.query(NETWORK, req, filter);
  }

  findNetwork(id: number): Observable<EntityResponseType> {
    return this.find(NETWORK, id);
  }
  //
  // worldChartPie(req?: any): Observable<HttpResponse<WorldChartPieResponse[]>> {
  //   const options = createRequestOption(req);
  //   return this.http
  //     .get<WorldChartPieResponse[]>(`${environment.apiUrl}/monitor-ips`, {params: options, observe: 'response'});
  // }

  worldMap(req?: any): Observable<HttpResponse<any[]>> {
    const options = createRequestOption(req);
    return this.http
      .get<any[]>(`${environment.apiUrl}/monitor-ddos-chart`, {params: options, observe: 'response'});
  }

  getTop10Websites(req?: any): Observable<HttpResponse<any[]>> {
    const options = createRequestOption(req);
    return this.http
      .get<any[]>(`${environment.apiUrl}/monitor-ips`, {params: options, observe: 'response'});
  }

  download(url: string): Observable<any> {
    return this.http
      .get(url, { observe: 'response', responseType: 'blob' as 'json'});
  }

  lockIpNet(lookMonitorDdos: ILookMonitorDdos ): Observable<EntityResponseType> {
    return this.http
      .put<any>(`${this.resourceUrl}/network`, lookMonitorDdos, {observe: 'response'});
  }

  lockIpApp(lookMonitorDdos: ILookMonitorDdos ): Observable<EntityResponseType> {
    return this.http
      .put<any>(`${this.resourceUrl}/application`, lookMonitorDdos, {observe: 'response'});
  }
}
