import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import {
  ILookMonitorDdos,
  IMonitorDdos,
  LookMonitorDdos,
} from "@app/shared/model/monitor-ddos.model";
import { IPaging, Paging } from "@app/shared/model/base-respone.model";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MonitorDdosService } from "@app/views/monitor/ddos/monitor-ddos.service";
import { PAGING_LIMITS } from "@app/shared/constants/base.constant";
import {
  COUNTRIES,
  PERIOD_MONITOR,
  PeriodMonitor,
} from "@app/shared/constants/monitor.model";
import {
  getParameterPaging,
  OrderParam,
  REQUEST_INTERVAL,
} from "@app/shared/constants/common.model";
import { IField } from "@app/shared/ng-mutifilter/ng-mutifilter.model";
import { ChartDataSets, ChartOptions, ChartType } from "chart.js";
import { BaseChartDirective, Label } from "ng2-charts";

@Component({
  selector: "app-monitor-ddos",
  templateUrl: "./monitor-ddos.component.html",
  styleUrls: [
    "./monitor-ddos.component.scss",
    // '../node_modules/flag-icon-css/css/flag-icon.min.css'
  ],
})
export class MonitorDdosComponent implements OnInit, OnDestroy {
  @ViewChild("ipHorizonColumnChart", { static: false })
  ipHorizonColumnChart: BaseChartDirective;
  @ViewChild("websiteTop10HorizonBarChart", { static: false })
  websiteTop10HorizonBarChart: BaseChartDirective;
  periodFilterForms: PeriodMonitor[] = PERIOD_MONITOR;
  monitorDdossNetwork: IMonitorDdos[];
  pagingNetwork: IPaging;
  accountFormGroup: FormGroup;
  dataFilterNetworkForm = this.fb.group({
    filter: ["", [Validators.required]],
    period: [this.periodFilterForms[0]],
  });
  test = this.periodFilterForms[0];
  orderApplication: OrderParam = new OrderParam("datetime", false);
  orderNetwork: OrderParam = new OrderParam("datetime", false);
  urlDownloadNetwork: string;
  urlDownloadApplication: string;
  monitorDdossApplication: IMonitorDdos[];
  pagingApplication: IPaging;
  dataFilterApplicationForm = this.fb.group({
    filter: ["", [Validators.required]],
    period: [this.periodFilterForms[0]],
  });
  timer;
  dateRange = null;
  limits = PAGING_LIMITS;
  fieldsNetwork: Array<IField> = [
    { label: "IP Addr", value: "attacker_ip" },
    { label: "Blocked time", value: "datetime" },
    { label: "County", value: "attacker_country" },
    { label: "Rule", value: "group_rule" },
    { label: "Unlock", value: "unlock" },
  ];
  fieldsApplication: Array<IField> = [
    { label: "IP Addr", value: "attacker_ip" },
    { label: "Blocked time", value: "datetime" },
    { label: "County", value: "attacker_country" },
    { label: "Rule", value: "group_rule" },
    { label: "Website", value: "website_domain" },
    { label: "Unlock", value: "unlock" },
  ];

  periodWorldMapCharts: PeriodMonitor[] = PERIOD_MONITOR;
  periodWorldPieCharts: PeriodMonitor[] = PERIOD_MONITOR;
  periodWebsitePieChart: PeriodMonitor[] = PERIOD_MONITOR;
  periodIpHorizonColumnChart = PERIOD_MONITOR;
  periodWebsiteHorizonColumnChart = PERIOD_MONITOR;
  periodFilterChart = this.fb.group({
    worldMapChart: [this.periodWorldMapCharts[0]],
    worldPieChart: [this.periodWorldPieCharts[0]],
    websitePieChart: [this.periodWebsitePieChart[0]],
    ipHorizonColumnChart: [this.periodIpHorizonColumnChart[0]],
    websiteHorizonColumnChart: [this.periodWebsiteHorizonColumnChart[0]],
  });

  ipHorizonColumnChartData: ChartDataSets[] = [
    { data: [], label: "Data", spanGaps: true },
  ];

  ipHorizonColumnChartLabels: Label[] = [];

  websiteTop10HorizonBarChartData: ChartDataSets[] = [
    { data: [], label: "Data", spanGaps: true },
  ];

  websiteTop10HorizonBarChartLabels: Label[] = [];

  webConnectionChartOption: ChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    plugins: {
      legend: {
        position: "right",
      },
      title: {
        display: false,
        text: "",
      },
    },
  };

  lineChartLegend = true;
  barChartType: ChartType = "horizontalBar";
  resourceChartPlugins = [];

  worldPieChartDataSet = {
    data: [],
    lables: [],
    colors: [],
  };

  attackWebsitePieTop10DataSet = {
    data: [],
    lables: [],
    colors: [],
  };

  dataWorldMap: Array<any>;

  constructor(
    public monitorDdosService: MonitorDdosService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.pagingApplication = new Paging();
    this.pagingNetwork = new Paging();
    this.monitorDdossNetwork = [];
    this.monitorDdossApplication = [];
  }

  ngOnInit() {
    this.loadWorldPieChart();
    this.loadWorldMapTop();
    this.loadApplication();
    this.loadNetwork();
    this.loadWebsitePieChart();
    this.loadIPHorizonColumnChart();
    this.loadWebsiteTop10ColumnChart();
    this.interval();
  }

  interval() {
    this.accountFormGroup = this.fb.group({
      data: this.fb.array([]),
    });
    this.timer = setInterval(() => {
      if (
        this.dataFilterApplicationForm.controls["period"].value.type !==
        "between"
      ) {
        this.loadApplication();
      }
      if (
        this.dataFilterNetworkForm.controls["period"].value.type !== "between"
      ) {
        this.loadNetwork();
      }
      if (
        this.periodFilterChart.controls["worldPieChart"].value.type !==
        "between"
      ) {
        this.loadWorldPieChart();
      }
      if (
        this.periodFilterChart.controls["worldMapChart"].value.type !==
        "between"
      ) {
        this.loadWorldMapTop();
      }
      if (
        this.periodFilterChart.controls["ipHorizonColumnChart"].value.type !==
        "between"
      ) {
        this.loadWebsitePieChart();
      }
      if (
        this.periodFilterChart.controls["websitePieChart"].value.type !==
        "between"
      ) {
        this.loadIPHorizonColumnChart();
      }
      if (
        this.periodFilterChart.controls["websiteHorizonColumnChart"].value
          .type !== "between"
      ) {
        this.loadWebsiteTop10ColumnChart();
      }
    }, REQUEST_INTERVAL);
  }

  changePeriodWorldMap($event) {
    this.periodFilterChart.controls["worldMapChart"].setValue($event);
    this.loadWorldMapTop();
  }

  loadData() {
    this.loadWorldPieChart();
    this.loadWorldMapTop();
    this.loadApplication();
    this.loadNetwork();
  }

  loadWorldMapTop() {
    console.log("load world map top");
    const period: PeriodMonitor =
      this.periodFilterChart.controls["worldMapChart"].value;
    const parameters = {
      orderBy: "id",
      orderType: "desc",
      avg_value: 100,
      attacker_country: 10,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorDdosService.worldMap(parameters).subscribe((res) => {
      if (res.status === 200) {
        this.dataWorldMap = res.body;
      } else {
        console.warn("can not load world map top");
      }
    });
  }

  loadWorldPieChart() {
    console.log("load world pie chart");
    const period: PeriodMonitor =
      this.periodFilterChart.controls["websitePieChart"].value;
    const parameters = {
      orderBy: "id",
      orderType: "desc",
      avg_value: 100,
      websites: 10,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorDdosService.worldMap(parameters).subscribe((res) => {
      if (res.status === 200) {
        if (res.body.length <= 0) {
          res.body.push({
            key: "No data",
            value: 100,
          });
        }
        this.worldPieChartDataSet = this.setDataWorldChartPie(res.body);
      }
    });
  }

  loadWebsitePieChart() {
    console.log("load world pie chart");
    const period: PeriodMonitor =
      this.periodFilterChart.controls["websitePieChart"].value;
    const parameters = {
      orderBy: "id",
      orderType: "desc",
      avg_value: 100,
      websites: 10,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorDdosService.worldMap(parameters).subscribe((res) => {
      if (res.status === 200) {
        if (res.body.length <= 0) {
          res.body.push({
            key: "No data",
            value: 100,
          });
        }
        this.attackWebsitePieTop10DataSet = this.setDataWorldChartPie(res.body);
      }
    });
  }

  loadIPHorizonColumnChart() {
    console.log("load world pie chart");
    const period: PeriodMonitor =
      this.periodFilterChart.controls["ipHorizonColumnChart"].value;
    const parameters = {
      ip_address: 10,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorDdosService.worldMap(parameters).subscribe((res) => {
      if (res.status === 200) {
        if (res.body.length <= 0) {
          res.body.push({
            key: "No data",
            value: 0,
          });
        }

        this.ipHorizonColumnChartData[0].data = res.body.map(
          (data) => data.value
        );
        this.ipHorizonColumnChartLabels = res.body.map((data) => data.key);
      }
    });
  }

  loadWebsiteTop10ColumnChart() {
    const period: PeriodMonitor =
      this.periodFilterChart.controls["websiteHorizonColumnChart"].value;
    const parameters = {
      orderBy: "id",
      orderType: "desc",
      avg_value: 100,
      websites: 10,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    this.monitorDdosService.worldMap(parameters).subscribe((res) => {
      if (res.status === 200) {
        if (res.body.length <= 0) {
          res.body.push({
            key: "No data",
            value: 0,
          });
        }

        this.websiteTop10HorizonBarChartData[0].data = res.body.map(
          (data) => data.value
        );
        this.websiteTop10HorizonBarChartLabels = res.body.map(
          (data) => data.key
        );
      }
    });
  }

  loadNetwork() {
    console.log("load network");
    const period: PeriodMonitor =
      this.dataFilterNetworkForm.controls["period"].value;
    this.pagingNetwork = getParameterPaging(this.pagingNetwork);
    const parameters = {
      limit: this.pagingNetwork.limit,
      offset: this.pagingNetwork.offset,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }

    if (this.orderNetwork.orderBy) {
      parameters["orderBy"] = this.orderNetwork.orderBy;
      parameters["orderType"] = this.orderNetwork.getOrderType();
    }
    const filter = !this.dataFilterNetworkForm.controls["filter"].invalid
      ? this.dataFilterNetworkForm.controls["filter"].value
      : null;
    this.monitorDdosService
      .queryNetwork(parameters, filter)
      .subscribe((res) => {
        if (res.status === 200) {
          this.monitorDdossNetwork = res.body.data;
          this.monitorDdossNetwork.forEach((value) => {
            value["is_show"] = false;
          });
          this.urlDownloadNetwork = this.monitorDdosService.downloadNetworkUrl(
            parameters,
            filter
          );
          this.pagingNetwork.limit = res.body.paging.limit;
          this.pagingNetwork.offset = res.body.paging.offset;
          this.pagingNetwork.total = res.body.paging.total;
        } else {
          console.warn("can not load monitor ddos");
        }
      });
  }

  showCountry(code: string): string {
    return COUNTRIES.hasOwnProperty(code) ? COUNTRIES[code].country : code;
  }

  loadApplication() {
    const period: PeriodMonitor =
      this.dataFilterApplicationForm.controls["period"].value;
    this.pagingApplication = getParameterPaging(this.pagingApplication);
    const parameters = {
      limit: this.pagingApplication.limit,
      offset: this.pagingApplication.offset,
    };
    parameters["dateType"] = period.type;
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }

    if (this.orderApplication.orderBy) {
      parameters["orderBy"] = this.orderApplication.orderBy;
      parameters["orderType"] = this.orderApplication.getOrderType();
    }
    const filter = !this.dataFilterApplicationForm.controls["filter"].invalid
      ? this.dataFilterApplicationForm.controls["filter"].value
      : null;
    this.monitorDdosService
      .queryApplication(parameters, filter)
      .subscribe((res) => {
        if (res.status === 200) {
          this.monitorDdossApplication = res.body.data;
          this.monitorDdossApplication.forEach((value) => {
            value["is_show"] = false;
          });
          this.urlDownloadApplication =
            this.monitorDdosService.downloadApplicationUrl(parameters, filter);
          this.pagingApplication.limit = res.body.paging.limit;
          this.pagingApplication.offset = res.body.paging.offset;
          this.pagingApplication.total = res.body.paging.total;
        } else {
          console.warn("can not load monitor ddos");
        }
      });
  }

  loadPage(page: number, paging: IPaging, type: boolean) {
    if (page !== paging.previousPage) {
      paging.previousPage = page;
      if (type) {
        this.loadApplication();
      } else {
        this.loadNetwork();
      }
    }
  }

  showDetailNetwork(monitorDdos) {
    monitorDdos.is_show = true;
    if (!monitorDdos.detail) {
      this.monitorDdosService.findNetwork(monitorDdos.id).subscribe((res) => {
        if (res.status === 200) {
          monitorDdos.detail = res.body.detail;
        }
      });
    }
  }

  showDetailApplication(monitorDdos) {
    monitorDdos.is_show = true;
    if (!monitorDdos.detail) {
      monitorDdos.detail = "No Detail";
      //   this.monitorDdosService
      //     .findApplication(monitorDdos.id)
      //     .subscribe((res) => {
      //       if (res.status === 200) {
      //         monitorDdos.detail = res.body.detail;
      //       }
      //     });
    }
  }

  refreshSearchFormNetwork() {
    this.dataFilterNetworkForm.controls["filter"].setValue("");
    this.dataFilterNetworkForm.controls["period"].setValue(
      this.periodFilterForms[0]
    );
    this.orderNetwork.orderBy = "datetime";
    this.orderNetwork.orderType = false;
    this.loadNetwork();
  }

  refreshSearchFormApplication() {
    this.dataFilterApplicationForm.controls["filter"].setValue("");
    this.dataFilterApplicationForm.controls["period"].setValue(
      this.periodFilterForms[0]
    );
    this.orderApplication.orderBy = "datetime";
    this.orderApplication.orderType = false;
    this.loadApplication();
  }

  searchNetwork() {
    this.pagingNetwork.offset = 1;
    this.loadNetwork();
  }

  searchApplication() {
    this.pagingApplication.offset = 1;
    this.loadApplication();
  }

  dynamicColors() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    return "rgba(" + r + "," + g + "," + b + ",0.8)";
  }

  setDataWorldChartPie(res) {
    let rs = {
      data: [],
      lables: [],
      colors: [],
    };
    const backgroundColors = [];
    res.forEach((value) => {
      rs.data.push(value.value);
      rs.lables.push(value.key);
      backgroundColors.push(this.dynamicColors());
    });

    rs.colors.push({
      backgroundColor: backgroundColors,
    });
    return rs;
  }

  changePeriodFilterFormNetwork() {
    this.pagingNetwork.page = 1;
    this.loadNetwork();
  }

  changePeriodFilterFormApplication() {
    this.pagingApplication.page = 1;
    this.loadApplication();
  }

  changeOderApplication(order) {
    this.orderApplication = order;
    this.loadApplication();
  }

  changeOderNetwork(order) {
    this.orderNetwork = order;
    this.loadNetwork();
  }

  changeLimitNetwork() {
    this.pagingNetwork.page = 1;
    this.loadNetwork();
  }

  changeLimitApplication() {
    this.pagingApplication.page = 1;
    this.loadApplication();
  }

  ngOnDestroy(): void {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  onFilterNetwork($event) {
    this.dataFilterNetworkForm.controls["filter"].setValue($event);
    this.loadNetwork();
  }

  onFilterApplication($event) {
    this.dataFilterApplicationForm.controls["filter"].setValue($event);
    this.loadApplication();
  }

  onDownload(url: string) {
    this.monitorDdosService.download(url).subscribe((res: Response) => {
      // 'Access-Control-Expose-Headers': 'Content-Disposition'
      let filename;
      const contentDisposition = res.headers.get("content-disposition");
      if (contentDisposition) {
        const parts: string[] = contentDisposition.split(";");
        if (parts && parts.length > 1) {
          const subParts = parts[1].split("=");
          if (subParts && subParts.length > 1) {
            filename = parts[1].split("=")[1];
          }
        }
      }
      const data = res.body;
      // @ts-ignore
      const blob = new Blob([data], { type: res.headers.get("Content-Type") });
      const downloadURL = window.URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = downloadURL;
      link.download = filename ? filename : "export_data.csv";
      link.click();
    });
  }

  lockIpNet(blogTime: string, ip: string) {
    const value = new LookMonitorDdos();
    if (blogTime != null && ip != null) {
      value.block_time = blogTime;
      value.ip_addr = ip;
    }
    if (value) {
      this.monitorDdosService.lockIpNet(value).subscribe((res) => {});
    }
  }

  lockIpApp(blogTime: string, ip: string) {
    const value = new LookMonitorDdos();
    if (blogTime != null && ip != null) {
      value.block_time = blogTime;
      value.ip_addr = ip;
    }
    if (value) {
      this.monitorDdosService.lockIpApp(value).subscribe((res) => {});
    }
  }
}
