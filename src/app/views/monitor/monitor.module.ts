import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule, SortByDirective, SortDirective } from "@app/shared";
import { ReactiveFormsModule } from "@angular/forms";
import { CollapseModule } from "ngx-bootstrap";
import { MonitorRoutingModule } from "@app/views/monitor/monitor-routing.module";
import { MonitorSystemComponent } from "@app/views/monitor/system/monitor-system.component";
import { MonitorSettingComponent } from "@app/views/monitor/setting/monitor-setting.component";
import { MonitorDdosComponent } from "@app/views/monitor/ddos/monitor-ddos.component";
import { MonitorWebApplicationComponent } from "@app/views/monitor/web-application/monitor-web-application.component";
import { MonitorResourceComponent } from "@app/views/monitor/resource/monitor-resource.component";
import { ChartsModule } from "ng2-charts";
import { RouterModule } from "@angular/router";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgMutifilterComponent } from "@app/shared/ng-mutifilter/ng-mutifilter.component";
import { ConnectionComponent } from "./connection/connection.component";
import { fileScanned } from "@app/views/monitor/file-scanned/file-scanned.component";

@NgModule({
  imports: [
    MonitorRoutingModule,
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    CollapseModule,
    ChartsModule,
    FontAwesomeModule,
  ],
  declarations: [
    MonitorSystemComponent,
    MonitorSettingComponent,
    MonitorDdosComponent,
    MonitorWebApplicationComponent,
    MonitorResourceComponent,
    ConnectionComponent,
    fileScanned,
  ],
  exports: [SortByDirective, SortDirective],
})
export class MonitorModule {}
