import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AboutComponent} from '@app/views/about/about.component';
import {UserManualComponent} from '@app/views/about/manual/user-manual.component';


const routes: Routes = [
  {
    path: '',
    component: AboutComponent,
    data: {
      title: 'About'
    }
  },
  {
    path: 'user-manual',
    component: UserManualComponent,
    data: {
      title: 'User Manual'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AboutRoutingModule {}
