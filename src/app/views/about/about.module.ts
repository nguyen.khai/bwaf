import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@app/shared';
import {ReactiveFormsModule} from '@angular/forms';
import {CollapseModule} from 'ngx-bootstrap';
import {AboutRoutingModule} from '@app/views/about/about-routing.module';
import {AboutComponent} from '@app/views/about/about.component';
import {UserManualComponent} from '@app/views/about/manual/user-manual.component';

@NgModule({
  imports: [AboutRoutingModule, CommonModule, SharedModule, ReactiveFormsModule, CollapseModule],
  declarations: [AboutComponent, UserManualComponent]
})
export class AboutModule {
}
