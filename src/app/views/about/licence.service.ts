import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';
import {ILicence} from '@app/shared/model/licence.model';
import * as moment from 'moment';
import {map} from 'rxjs/operators';

type EntityResponseType = HttpResponse<ILicence>;

@Injectable({
  providedIn: 'root'
})
export class LicenceService {
  public resourceUrl = environment.apiUrl + '/licence';

  constructor(protected http: HttpClient) {
  }

  find(): Observable<EntityResponseType> {
    return this.http
      .get<ILicence>(this.resourceUrl, {observe: 'response'})
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  setSystemLicence(licence: string): Observable<HttpResponse<any>> {
    const body = {
      licence: licence
    }
    return this.http
      .post<any>(this.resourceUrl, body, {observe: 'response'});
  }


  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.licence_expired = res.body.licence_expired != null ? moment(res.body.licence_expired) : null;
    }
    return res;
  }
}
