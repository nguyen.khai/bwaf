import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';
import {IAbout} from '@app/shared/model/about.model';


type EntityResponseType = HttpResponse<IAbout>;



@Injectable({
  providedIn: 'root'
})
export class AboutService {
  public resourceUrl = environment.apiUrl + '/about';

  constructor(protected http: HttpClient) {
  }

  find(): Observable<EntityResponseType> {
    return this.http
      .get<IAbout>(this.resourceUrl, {observe: 'response'});
  }

  changeSystemname(system_name: string): Observable<HttpResponse<any>> {
    const body = {
      system_name: system_name
    };
    return this.http
      .post<any>(this.resourceUrl, body, {observe: 'response'});
  }
}
