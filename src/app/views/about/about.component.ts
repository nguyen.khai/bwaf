import {Component, OnInit} from '@angular/core';
import {IAbout} from '@app/shared/model/about.model';
import {ILicence} from '@app/shared/model/licence.model';
import {AboutService} from '@app/views/about/about.service';
import {LicenceService} from '@app/views/about/licence.service';
import {FirmwareService} from "@app/views/about/firmware.service";
import {IFirmware} from "@app/shared/model/firmware.model";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  about: IAbout;
  licence: ILicence;
  checkForVersion: boolean = false;
  licenceSerial: string;
  firmwares: IFirmware[];
  firmwareNew: any;
  loadingDowload: number = 0;
  isShowloading: boolean = false;
  distableInstallUpdate: boolean = true;

  constructor(
    public firmwareService: FirmwareService,
    public aboutService: AboutService,
    public lincenceService: LicenceService,
    private modalService: NgbModal,
  ) {
  }

  ngOnInit() {
    this.loadAll();
  }

  loadAll() {
    this.loadAbout();
    this.loadLicence();
  }

  loadAbout() {
    this.aboutService.find().subscribe(res => {
      if (res.status === 200) {
        this.about = res.body;
      } else {
        console.warn("can not load about");
      }
    });
  }


  loadLicence() {
    console.log("loadLicence");
    this.lincenceService.find().subscribe(res => {
      if (res.status === 200) {
        this.licence = res.body;

      } else {
        console.warn("can not load licence");
      }
    });
  }

  showCheckForVersion() {
    console.log('load firmwareNew');
    if (this.licence.licence_expired.valueOf() < Date.now()) {
      this.checkForVersion = true;
      this.firmwareService.find().subscribe(res => {
        if (res.status === 200) {
          this.firmwares = res.body;
          this.firmwareNew = this.firmwares.reduce((prev, current) => {
            return prev.release_date.valueOf() > current.release_date.valueOf() ? prev : current;
          });

          if (this.firmwareNew) {
            this.firmwareNew.is_newversion = this.firmwareNew.version !== this.about.firmware_version;
            this.firmwareNew.is_download = false;
          }
        } else {
          console.warn("can not load firmware");
        }
      });
    }

  }

  hiddenUpdate() {
    this.checkForVersion = false;
    this.firmwareNew = null;
    this.isShowloading = false;
    this.loadingDowload = 0;
  }

  updateLicence() {
    console.log('updateLicence');
    this.lincenceService.setSystemLicence(this.licenceSerial).subscribe(res => {
      if (res.status === 200) {
      }
    });
  }

  showManual() {

  }

  downloadNewVersion() {
    this.isShowloading = true;
    setInterval(() => {
      if (this.loadingDowload < 90 && this.isShowloading) {
        this.loadingDowload++;
      }
    }, 300);
    this.firmwareService.downloadNewVersion().subscribe(res => {
      if (res.status === 200) {
        this.firmwareNew.is_download = true;
        this.loadingDowload = 100;
      }
    });
  }

  installFirmware(modal) {
    this.distableInstallUpdate = false;
    this.modalService.open(modal, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static'
    }).result.then((result) => {
      if (result === "ok") {
        this.firmwareService.updateFrimware().subscribe(res => {
          if (res.status === 200) {
            this.loadAbout();
            this.hiddenUpdate();
          }
          this.distableInstallUpdate = true;
        });
      }
    });

  }


}
