import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';
import * as moment from 'moment';
import {map} from 'rxjs/operators';
import {IFirmware} from "@app/shared/model/firmware.model";


type EntityArrayResponseType = HttpResponse<IFirmware[]>;


@Injectable({
  providedIn: 'root'
})
export class FirmwareService {
  public resourceUrl = environment.apiUrl;

  constructor(protected http: HttpClient) {
  }

  find(): Observable<EntityArrayResponseType> {
    const url = this.resourceUrl + "/update-firmware-check";
    return this.http
      .get<IFirmware[]>(url, {observe: 'response'})
      .pipe(map((res: EntityArrayResponseType) => this.convertDateFromServerArray(res)));
  }

  downloadNewVersion(): Observable<HttpResponse<any>> {
    const url = this.resourceUrl + "/update-firmware-download";
    return this.http
      .get<any>(url, {observe: 'response'});
  }


  updateFrimware(): Observable<HttpResponse<any>> {
    const url = this.resourceUrl + "/update-firmware";
    return this.http
      .post<any>(url, {}, {observe: 'response'});
  }


  protected convertDateFromServerArray(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((value) => {
        value.release_date = value.release_date != null ? moment(value.release_date) : null;
      });
    }
    return res;
  }
}
