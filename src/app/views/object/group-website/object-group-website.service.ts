import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';
import {createRequestOption} from 'app/shared';
import {IPaging} from '@app/shared/model/base-respone.model';
import {IGroupWebsite} from '@app/shared/model/group-website.model';

type EntityResponseType = HttpResponse<IGroupWebsite>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;
interface PagingResponse {
  data: IGroupWebsite[];
  paging: IPaging;
}

@Injectable({ providedIn: 'root' })
export class ObjectGroupWebsiteService {
  public resourceUrl = environment.apiUrl + '/group-website';

  constructor(protected http: HttpClient) {}

  create(groupWebsite: IGroupWebsite): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(groupWebsite);
    return this.http
      .post<IGroupWebsite>(this.resourceUrl, copy, { observe: 'response' });
  }

  update(groupWebsite: IGroupWebsite): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(groupWebsite);
    return this.http
      .put<IGroupWebsite>(`${this.resourceUrl}/${groupWebsite.id}`, copy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IGroupWebsite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<PagingResponse>(this.resourceUrl, { params: options, observe: 'response' });
  }
  search(name: string): Observable<EntityArrayResponseType> {
    const options = createRequestOption(name);
    return this.http
      .get<PagingResponse>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
  updateGroupWebsite(idGroupWebsite, website: any): Observable<EntityResponseType> {
    return this.http
      .post<any>(`${this.resourceUrl}/${idGroupWebsite}`, website );
  }
  protected convertDateFromClient(groupWebsite: IGroupWebsite): IGroupWebsite {
    const copy: IGroupWebsite = Object.assign({}, groupWebsite, {});
    return copy;
  }
}
