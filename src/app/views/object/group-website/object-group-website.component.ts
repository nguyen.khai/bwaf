import {Component, OnInit} from '@angular/core';
import {IPaging} from '@app/shared/model/base-respone.model';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {GroupWebsite, IGroupWebsite} from '@app/shared/model/group-website.model';
import {ObjectGroupWebsiteService} from '@app/views/object/group-website/object-group-website.service';
import {ObjectWebsiteService} from '@app/views/object/website/object-website.service';
import {IWebsite} from '@app/shared/model/website.model';
import {getParameterPaging, OrderParam, PAGING_PER_PAGE} from '@app/shared/constants/common.model';

@Component({
  selector: 'app-object-group-website',
  templateUrl: 'object-group-website.component.html',
  styleUrls: ['object-group-website.component.scss']
})
export class ObjectGroupWebsiteComponent implements OnInit {
  groupWebsites: IGroupWebsite[];
  websites: IWebsite[];
  keywordSearch: string;
  paging: IPaging;
  PAGING_PER_PAGE = PAGING_PER_PAGE;
  groupWebsiteFormGroup: FormGroup;
  websiteFormGroup: FormGroup;
  selectedWebsite: IWebsite;
  selectedGroupWebsite: GroupWebsite;
  isEdit: boolean;
  isCreate: boolean;
  isAction: boolean;
  order: OrderParam = new OrderParam(null, false);

  constructor(
    public objectGroupWebsiteService: ObjectGroupWebsiteService,
    public objectWebsiteService: ObjectWebsiteService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.paging = data.pagingParams;
    });
  }

  ngOnInit(): void {
    this.initTable();
  }

  initTable() {
    this.groupWebsiteFormGroup = this.fb.group({
      data: this.fb.array([])
    });
    this.websiteFormGroup = this.fb.group({
      data: this.fb.array([])
    });
    this.loadAll();
  }

  initForm() {
    return this.fb.group({
      name: ['', [Validators.required, Validators.maxLength(100)]],
      description: ['', [Validators.required, Validators.maxLength(100)]],
      websites: [''],
      is_update: [true],
      is_show: [false]
    });
  }

  loadAll() {
    const control = <FormArray>this.groupWebsiteFormGroup.get('data');
    this.paging = getParameterPaging(this.paging);
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit
    };
    if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters['search'] = this.keywordSearch;
    }
    this.isAction = true;

    this.objectGroupWebsiteService.query(
      parameters
    ).subscribe(res => {
      control.clear();
      this.groupWebsites = res.body.data;
      for (const groupWebsites of this.groupWebsites) {
        const grp = this.fb.group({
          id: [groupWebsites.id],
          name: [groupWebsites.name, [Validators.required, Validators.maxLength(100)]],
          description: [groupWebsites.description, [Validators.required, Validators.maxLength(100)]],
          websites: [groupWebsites.websites],
          is_update: [false],
          is_show: [false]
        });
        control.push(grp);
      }
      this.paging.total = res.body.paging.total;
    });

  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }

  /*transition() {
    const parameters = {
      offset: this.paging.offset - 1,
      limit: this.paging.limit
    };
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters['search'] = this.keywordSearch;
    }
    this.router.navigate(['/object/group-website'], {
      queryParams: parameters
    });
    this.loadAll();
  }*/

  get getFormWebsiteData(): FormArray {
    return <FormArray>this.websiteFormGroup.get('data');
  }

  get getFormData(): FormArray {
    return <FormArray>this.groupWebsiteFormGroup.get('data');
  }

  addGroupWebsite() {
    const control = <FormArray>this.groupWebsiteFormGroup.get('data');
    this.isEdit = true;
    this.isCreate = true;
    this.isAction = false;
    control.push(this.initForm());
  }

  save(index: number) {
    this.isAction = false;
    const control = <FormArray>this.groupWebsiteFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    const value: GroupWebsite = control.at(index).value;
    if (value.id) {
      this.objectGroupWebsiteService.update(value).subscribe(res => {
        this.loadAll();
      });
      this.isEdit = false;
      this.isCreate = false;
    } else {
      this.objectGroupWebsiteService.create(value).subscribe(res => {
        this.loadAll();
      });
      this.isCreate = false;
      this.isEdit = false;
    }
  }

  onEdit(index: number) {
    const control = <FormArray>this.groupWebsiteFormGroup.get('data');
    control.at(index).get('is_update').setValue(true);
    this.selectedGroupWebsite = control.at(index).value;
    this.isEdit = true;
    this.isAction = false;
    this.isCreate = false;
  }

  onCancelEdit(index: number) {
    this.isAction = false;
    const control = <FormArray>this.groupWebsiteFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    this.selectedGroupWebsite = control.at(index).value;
    this.isEdit = false;
    this.isCreate = false;
    this.initTable();
  }

  remove(modal, index: number) {
    this.isAction = false;
    const control = <FormArray>this.groupWebsiteFormGroup.get('data');
    this.selectedGroupWebsite = control.at(index).value;
    this.modalService.open(modal, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static'
    }).result.then((result) => {
      if (result === 'delete') {
        if (this.selectedGroupWebsite.id) {
          this.objectGroupWebsiteService.delete(this.selectedGroupWebsite.id).subscribe(res => {
            if (res.status === 200) {
              if ((this.paging.offset + this.getFormData.controls.length) - this.paging.offset === 1) {
                this.paging.page = this.paging.page - 1;
                this.loadAll();
              } else {
                this.loadAll();
              }
            }
          });
        } else {
          this.notificationService.error('Invalid');
        }
      }
    }, (reason) => {
    });
  }

  updateWebsiteDomain(index: number, action: string) {
    const controlWebsite = <FormArray>this.websiteFormGroup.get('data');
    this.selectedWebsite = controlWebsite.at(index).value;
    const valueBody = {
      action: action,
      website_id: this.selectedWebsite.id
    };
    const idGroupWebsite = this.selectedGroupWebsite.id;
    this.objectGroupWebsiteService.updateGroupWebsite(idGroupWebsite, valueBody).subscribe(res => {
      switch (action) {
        case 'add':
          controlWebsite.at(index).get('is_available').setValue(false);
          break;
        case 'remove':
          controlWebsite.at(index).get('is_available').setValue(true);
          break;
      }
    });
  }

  openViewWebsitePopup(index: number) {
    this.isAction = false;
    const control = <FormArray>this.groupWebsiteFormGroup.get('data');
    this.selectedGroupWebsite = control.at(index).value;
    control.at(index).get('is_show').setValue(!control.at(index).get('is_show').value);
    console.log(this.selectedGroupWebsite);
    this.websiteFormGroup = this.fb.group({
      data: this.fb.array([])
    });
    const controlWebsite = <FormArray>this.websiteFormGroup.get('data');
    if (this.selectedGroupWebsite.websites) {
      this.selectedGroupWebsite.websites.forEach(website => {
        const grpWebsite = this.fb.group({
          id: [website.id],
          website_domain: [website.website_domain],
          is_available: [false],
          is_show: [false]
        });
        controlWebsite.push(grpWebsite);
      });
    }
    this.loadWebsite('');
  }

  loadWebsite(keywordSearchGroupWebsite) {
    const controlWebsite = <FormArray>this.websiteFormGroup.get('data');
    const parameters = {
      offset: 0,
      limit: 1000,
      available: 1
    };
    if (keywordSearchGroupWebsite && keywordSearchGroupWebsite.trim().length > 0) {
      parameters['search'] = keywordSearchGroupWebsite;
      controlWebsite.clear();
    }
    //
    this.objectWebsiteService.query(parameters).subscribe(res => {
      this.websites = res.body.data;
      for (const websites of this.websites) {
        const grpWebsite = this.fb.group({
          id: [websites.id],
          website_domain: [websites.website_domain],
          is_available: [true],
          is_show: [false]
        });
        controlWebsite.push(grpWebsite);
        console.log(controlWebsite.value);
      }
    });
  }

  onSearch() {
    this.paging.offset = 1;
    if (this.keywordSearch) {
      this.keywordSearch = this.keywordSearch.trim();
    }
    this.loadAll();
  }

  onSearchGroupWebsite(keywordSearchGroupWebsite: string) {
    this.paging.offset = 1;
    if (keywordSearchGroupWebsite) {
      keywordSearchGroupWebsite = keywordSearchGroupWebsite.trim();
    }
    this.loadWebsite(keywordSearchGroupWebsite);
  }

  showDetail(groupWebsites, index) {
    if (groupWebsites.is_show === undefined || groupWebsites.is_show === false) {
      groupWebsites.is_show = false;
    }
    const control = <FormArray>this.groupWebsiteFormGroup.get('data');
    // this.groupWebsites = control.at(index).value;
    if (groupWebsites.is_show === false) {
      groupWebsites.is_show = true;
      control.at(index).get('is_show').setValue(true);
      return;
    } else if (groupWebsites.is_show === true) {
      groupWebsites.is_show = false;
      control.at(index).get('is_show').setValue(false);
      return;
    }
  }

  hideDetail(groupWebsites, index) {
    const control = <FormArray>this.groupWebsiteFormGroup.get('data');
    // this.groupWebsites = control.at(index).value;

  }

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }
}
