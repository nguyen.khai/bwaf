import {Component, OnInit} from '@angular/core';
import {IPaging} from '@app/shared/model/base-respone.model';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Account, IAccount} from '@app/shared/model/account.model';
import {ObjectAccountService} from '@app/views/object/account/object-account.service';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {getParameterPaging, OrderParam, PAGING_PER_PAGE} from '../../../shared/constants/common.model';

@Component({
  selector: 'app-object-account',
  templateUrl: 'object-account.component.html'
})
export class ObjectAccountComponent implements OnInit {
  accounts: IAccount[];
  selectedAccount: Account;
  paging: IPaging;
  add: boolean = false;
  closeResult: string;
  accountFormGroup: FormGroup;
  txtSearch: string;
  isEdit: boolean;
  isCreate: boolean;
  PAGING_PER_PAGE = PAGING_PER_PAGE;
  order: OrderParam = new OrderParam(null, false);
  constructor(
    protected notificationService: ToastrCustomService,
    public objectAccountService: ObjectAccountService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.paging = data.pagingParams;
    });
  }

  ngOnInit(): void {
    this.initTable();
  }

  loadAll() {
    this.paging =  getParameterPaging(this.paging);
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit
    };
    if (this.txtSearch && this.txtSearch.trim().length > 0) {
      parameters['search'] = this.txtSearch;
    }
    if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }

    this.objectAccountService.query(parameters).subscribe(res => {
      this.accounts = res.body.data;
      const control = <FormArray>this.accountFormGroup.get('data');
      control.clear();
      for (const account of this.accounts) {
        const grp = this.fb.group({
          id: [account.id],
          name: [account.name, [Validators.required, Validators.minLength(8), Validators.maxLength(20)]],
          email: [account.email],
          phone: [account.phone],
          is_update: [false]
        });
        control.push(grp);
      }
      this.paging.total = res.body.paging.total;
    });
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }
  changeOder(order) {
    this.order = order;
    this.loadAll();
  }
  // transition() {
  //   const parameters = {
  //     offset: this.paging.offset - 1,
  //     limit: this.paging.limit
  //   };
  //   if (this.order.orderBy) {
  //     parameters['orderBy'] = this.order.orderBy;
  //     parameters['orderType'] = this.order.getOrderType();
  //   }
  //   if (this.txtSearch && this.txtSearch.trim().length > 0) {
  //     parameters['search'] = this.txtSearch;
  //   }
  //   this.router.navigate(['/object/account'], {
  //     queryParams: parameters
  //   });
  //   // this.loadAll();
  // }

  initTable() {
    this.accountFormGroup = this.fb.group({
      data: this.fb.array([])
    });
    this.loadAll();
  }


  addAccount() {
    this.isCreate = true;
    const control = <FormArray>this.accountFormGroup.get('data');
    if (control.length <= 0 || control.at(control.length - 1).get('id')) {
      control.push(this.initForm());
    }
  }

  initForm() {
    return this.fb.group({
      name: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]],
      email: ['', [ Validators.minLength(8), Validators.maxLength(50)]],
      phone: ['', [ Validators.maxLength(11)]],
      is_update: [true]
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  previousState() {
    window.history.back();
  }

  get getFormData(): FormArray {
    return <FormArray>this.accountFormGroup.get('data');
  }

  save(index: number) {
    this.isCreate = false;
    const control = <FormArray>this.accountFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    const value: Account = control.at(index).value;
    if (value.id) {
      this.objectAccountService.update(value).subscribe(res => {
        this.loadAll();
      });
      this.isEdit = false;
      this.isCreate = false;
    } else {
      this.objectAccountService.create(value).subscribe(res => {
        if ((this.paging.offset + this.getFormData.controls.length) - this.paging.offset === 1 ) {
          this.paging.page = this.paging.page - 1;
          this.loadAll();
        } else {
          this.loadAll();
        }
      });
    }
  }

  onEdit(index: number) {
    const control = <FormArray>this.accountFormGroup.get('data');
    control.at(index).get('is_update').setValue(true);
    // this.selectedAccount = control.at(index).value;
    this.isEdit = true;
    this.isCreate = false;
  }

  onCancelEdit(index: number) {
    const control = <FormArray>this.accountFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    if (control.at(index).get('id')) {
      const id: number = control.at(index).get('id').value;
      const accountBackup: IAccount = this.accounts.find((value) => {
        return value.id === id;
      });
      if (accountBackup) {
        control.at(index).setValue({
          id: accountBackup.id,
          email: accountBackup.email,
          phone: accountBackup.phone,
          name: accountBackup.name,
          is_update: false,
        });
      }
    } else {
      control.removeAt(index);
    }
    // this.selectedAccount = control.at(index).value;
    this.isEdit = false;
    this.isCreate = false;
  }

  remove(modal, index: number) {
    const control = <FormArray>this.accountFormGroup.get('data');
    this.selectedAccount = control.at(index).value;
    this.modalService.open(modal, {
      ariaLabelledBy: 'modal-basic-title',
      size: 'lg',
      backdrop: 'static'
    }).result.then((result) => {
      if (result === 'delete') {
        if (this.selectedAccount.id) {
          this.objectAccountService.delete(this.selectedAccount.id).subscribe(res => {
            this.loadAll();
          });
        } else {
          this.notificationService.error('Thao tác không hợp kệ');
        }
      }
    }, (reason) => {
    });
  }

  onSearch() {
    this.paging.page = 1;
    if (this.txtSearch) {
      this.txtSearch = this.txtSearch.trim();
    }
    this.loadAll();
  }
}
