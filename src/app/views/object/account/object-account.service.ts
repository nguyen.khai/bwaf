import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';
import {createRequestOption} from 'app/shared';
import {IPaging} from '@app/shared/model/base-respone.model';
import {IAccount} from '@app/shared/model/account.model';
import {ISsl} from "@app/shared/model/ssl.model";

type EntityResponseType = HttpResponse<IAccount>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;
interface PagingResponse {
  data: IAccount[];
  paging: IPaging;
}

@Injectable({ providedIn: 'root' })
export class ObjectAccountService {
  public resourceUrl = environment.apiUrl + '/account';

  constructor(protected http: HttpClient) {}

  create(account: IAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(account);
    return this.http
      .post<IAccount>(this.resourceUrl, copy, { observe: 'response' });
  }

  update(account: IAccount): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(account);
    return this.http
      .put<IAccount>(`${this.resourceUrl}/${account.id}`, copy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<PagingResponse>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(website: IAccount): IAccount {
    const copy: IAccount = Object.assign({}, website, {});
    return copy;
  }
}
