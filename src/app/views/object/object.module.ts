import { NgModule } from "@angular/core";
import { ObjectRoutingModule } from "./object-routing.module";
import { ObjectGroupWebsiteComponent } from "./group-website/object-group-website.component";
import { ObjectSslComponent } from "./ssl/object-ssl.component";
import { ObjectMailSenderComponent } from "./mail-sender/object-mail-sender.component";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@app/shared";
import { ReactiveFormsModule } from "@angular/forms";
import { CollapseModule, TooltipModule } from "ngx-bootstrap";
import { ObjectAccountComponent } from "@app/views/object/account/object-account.component";
import { ObjectWebsiteModule } from "@app/views/object/website/object-website.module";
import { RuleManagementComponent } from "@app/views/object/rule-management/rule-management.component";

import {
  FaIconComponent,
  FontAwesomeModule,
} from "@fortawesome/angular-fontawesome";
@NgModule({
  imports: [
    ObjectRoutingModule,
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    CollapseModule,
    ObjectWebsiteModule,
    TooltipModule,
    FontAwesomeModule,
  ],
  declarations: [
    ObjectGroupWebsiteComponent,
    ObjectSslComponent,
    ObjectMailSenderComponent,
    ObjectAccountComponent,
    RuleManagementComponent,
  ],
  exports: [],
})
export class ObjectModule {}
