import { Component, OnInit } from "@angular/core";
import { IPaging, Paging } from "@app/shared/model/base-respone.model";
import { ActivatedRoute, Router } from "@angular/router";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ObjectMailSenderService } from "@app/views/object/mail-sender/object-mail-sender.service";
import {
  IMailSender,
  IMailServer,
  MailSender,
} from "../../../shared/model/mail-sender.model";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import { PAGING_LIMITS } from "@app/shared/constants/base.constant";
import {
  getParameterPaging,
  OrderParam,
} from "@app/shared/constants/common.model";

@Component({
  selector: "app-object-mail-sender",
  templateUrl: "object-mail-sender.component.html",
})
export class ObjectMailSenderComponent implements OnInit {
  mailSenders: IMailSender[];
  paging: IPaging;
  mailSenderFormGroup: FormGroup;
  selectedMailSender: MailSender;
  mailServers: IMailServer[];
  mapMailServer = {};
  txtSearch: string;
  limits = PAGING_LIMITS;
  isAcction: boolean = true;
  order: OrderParam = new OrderParam(null, false);

  constructor(
    public objectMailSenderService: ObjectMailSenderService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.paging = new Paging();
    this.mailSenders = [];
  }

  ngOnInit(): void {
    this.loadMailServer();
    this.initTable();
  }

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  initTable() {
    this.mailSenderFormGroup = this.fb.group({
      data: this.fb.array([]),
    });
    this.loadAll();
  }

  initForm() {
    // console.log(this.mailSenders.length, "sdsd");
    const mailSender = this.mailSenders.length > 0 ? this.mailSenders[0].id : 0;
    return this.fb.group({
      email: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(20),
          Validators.email,
        ],
      ],
      password: [
        "",
        [
          Validators.required,
          Validators.minLength(8),
          Validators.required,
          Validators.maxLength(30),
          Validators.pattern(
            /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=)/
          ),
        ],
      ],
      mail_server_id: [mailSender, [Validators.required]],
      is_update: [true],
      is_show_password: [false],
    });
  }

  search() {
    this.paging.page = 1;
    this.loadAll();
  }

  loadAll() {
    console.log("loadAll");
    const control = <FormArray>this.mailSenderFormGroup.get("data");
    this.paging = getParameterPaging(this.paging);
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit,
    };
    if (this.txtSearch && this.txtSearch.trim().length > 0) {
      parameters["search"] = this.txtSearch;
    }
    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }
    this.objectMailSenderService.query(parameters).subscribe((res) => {
      this.isAcction = true;
      if (res.status === 200) {
        control.clear();
        this.mailSenders = res.body.data;
        console.log(this.mailSenders);
        for (const mailSender of this.mailSenders) {
          const grp = this.fb.group({
            id: [mailSender.id],
            email: [
              mailSender.email,
              [
                Validators.required,
                Validators.minLength(8),
                Validators.maxLength(20),
                Validators.email,
              ],
            ],
            password: [
              mailSender.password,
              [
                Validators.required,
                Validators.minLength(8),
                Validators.required,
                Validators.maxLength(30),
                Validators.pattern(
                  /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=)/
                ),
              ],
            ],
            mail_server_id: [mailSender.mail_server_id, [Validators.required]],
            is_update: [false],
            is_show_password: [false],
          });
          control.push(grp);
        }
        // this.paging = res.body.paging;
        // this.paging.limit = res.body.paging.limit;
        // this.paging.offset = res.body.paging.offset;
        this.paging.total = res.body.paging.total;
      } else {
        console.warn("can not load mail sender");
      }
    });
  }

  loadMailServer() {
    console.log("load mail server");
    this.objectMailSenderService.findAllMailServer({}).subscribe((res) => {
      if (res.status === 200) {
        if (res.body) {
          this.mailServers = res.body;
          this.mailServers.forEach((value, index) => {
            this.mapMailServer[value.id] = value;
          });
        }
      } else {
        console.warn("can not load mail server");
      }
    });
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }

  // transition() {
  //   const parameters = {
  //     offset: this.paging.offset - 1,
  //     limit: this.paging.limit
  //   }
  //   if (this.order.orderBy) {
  //     parameters['orderBy'] = this.order.orderBy;
  //     parameters['orderType'] = this.order.getOrderType();
  //   }
  //   if (this.txtSearch && this.txtSearch.trim().length > 0) {
  //     parameters['search'] = this.txtSearch;
  //   }
  //   this.router.navigate(['/object/mail-sender'], {
  //     queryParams: parameters
  //   });
  //   // this.loadAll();
  // }

  get getFormData(): FormArray {
    return <FormArray>this.mailSenderFormGroup.get("data");
  }

  addMailSender() {
    this.isAcction = false;
    const control = <FormArray>this.mailSenderFormGroup.get("data");
    if (control.length <= 0 || control.at(control.length - 1).get("id")) {
      control.push(this.initForm());
    }
  }

  save(modal, index: number) {
    const control = <FormArray>this.mailSenderFormGroup.get("data");
    this.selectedMailSender = control.at(index).value;
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
      })
      .result.then((result) => {
        if (result === "save") {
          this.isAcction = true;
          console.log("save");
          console.log(this.selectedMailSender);
          if (this.selectedMailSender.id) {
            this.objectMailSenderService
              .update(this.selectedMailSender)
              .subscribe((res) => {
                if (res.status === 200) {
                  this.loadAll();
                  // control.at(index).get('is_update').setValue(false);
                  // const id = control.at(index).get("id").value;
                  // const indexMailSender = this.mailSenders.findIndex(value => {
                  //   return value.id === id;
                  // });
                  // this.mailSenders[indexMailSender].password = control.at(index).get("password").value;
                  // this.mailSenders[indexMailSender].email = control.at(index).get("email").value;
                  // this.mailSenders[indexMailSender].mail_server_id = control.at(index).get("mail_server_id").value;
                }
              });
          } else {
            this.objectMailSenderService
              .create(this.selectedMailSender)
              .subscribe((res) => {
                if (res.status === 200) {
                  this.loadAll();
                }
              });
          }
        }
      });
  }

  onEdit(index: number) {
    this.isAcction = false;
    const control = <FormArray>this.mailSenderFormGroup.get("data");
    control.at(index).get("is_update").setValue(true);
  }

  onCancelEdit(index: number) {
    this.isAcction = true;
    const control = <FormArray>this.mailSenderFormGroup.get("data");
    control.at(index).get("is_update").setValue(false);
    if (control.at(index).get("id")) {
      const id: number = control.at(index).get("id").value;
      const mailSenderBackup: IMailSender = this.mailSenders.find((value) => {
        return value.id === id;
      });
      if (mailSenderBackup) {
        control.at(index).setValue({
          id: mailSenderBackup.id,
          email: mailSenderBackup.email,
          password: mailSenderBackup.password,
          mail_server_id: mailSenderBackup.mail_server_id,
          is_update: false,
          is_show_password: false,
        });
      }
    } else {
      control.removeAt(index);
    }
  }

  remove(modal, index: number) {
    const control = <FormArray>this.mailSenderFormGroup.get("data");
    this.selectedMailSender = control.at(index).value;
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
      })
      .result.then(
        (result) => {
          if (result === "delete") {
            console.log("delete");
            console.log(this.selectedMailSender);
            if (this.selectedMailSender.id) {
              this.objectMailSenderService
                .delete(this.selectedMailSender.id)
                .subscribe((res) => {
                  // control.removeAt(index);
                  if (res.status === 200) {
                    if (
                      this.paging.offset +
                        this.getFormData.controls.length -
                        this.paging.offset ===
                      1
                    ) {
                      this.paging.page = this.paging.page - 1;
                      this.loadAll();
                    } else {
                      this.loadAll();
                    }
                  }
                });
            } else {
              this.notificationService.error("Thao tác không hợp kệ");
            }
          }
        },
        (reason) => {}
      );
  }

  openViewCertPopup(modal, mailSender) {
    this.selectedMailSender = mailSender;
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
      })
      .result.then(
        (result) => {},
        (reason) => {}
      );
  }

  changeLimit() {
    this.paging.page = 1;
    this.loadAll();
  }
}
