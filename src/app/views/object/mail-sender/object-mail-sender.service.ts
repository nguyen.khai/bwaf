import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from 'environments/environment';
import {createRequestOption} from 'app/shared';
import {IMailSender, IMailServer} from '@app/shared/model/mail-sender.model';
import {IPaging} from '@app/shared/model/base-respone.model';


type EntityResponseType = HttpResponse<IMailSender>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;

interface PagingResponse {
  data: IMailSender[];
  paging: IPaging;
}

interface MailServerResponse {
  data: IMailServer[];
}

@Injectable({
  providedIn: 'root'
})
export class ObjectMailSenderService {
  public resourceUrl = environment.apiUrl + '/mail-sender';

  constructor(protected http: HttpClient) {
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<PagingResponse>(this.resourceUrl, {params: options, observe: 'response'});
  }

  findAllMailServer(req?: any): Observable<HttpResponse<IMailServer[]>> {
    const options = createRequestOption(req);
    return this.http
      .get<IMailServer[]>(this.resourceUrl + '/mail-server', {params: options, observe: 'response'});
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMailSender>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }


  create(mailSender: IMailSender): Observable<HttpResponse<any>> {
    return this.http
      .post<any>(this.resourceUrl, mailSender, {observe: 'response'});
  }

  update(mailSender: IMailSender): Observable<HttpResponse<any>> {
    return this.http
      .put<any>(`${this.resourceUrl}/${mailSender.id}`, mailSender, {observe: 'response'});
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, {observe: 'response'});
  }
}
