import {Component, OnInit} from '@angular/core';
import {ObjectSSLService} from '@app/views/object/ssl/object-ssl.service';
import {ISsl, Ssl} from '@app/shared/model/ssl.model';
import {IPaging} from '@app/shared/model/base-respone.model';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {getParameterPaging, OrderParam, PAGING_PER_PAGE} from '@app/shared/constants/common.model';
import * as JSZip from 'jszip';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-object-ssl',
  templateUrl: 'object-ssl.component.html',
  styleUrls: ['object-ssl.component.scss']
})
export class ObjectSslComponent implements OnInit {
  ssls: ISsl[] = [];
  paging: IPaging;
  sslFormGroup: FormGroup;
  selectedSSL: ISsl;
  keywordSearch: string;
  PAGING_PER_PAGE = PAGING_PER_PAGE;
  isEditing = false;
  order: OrderParam = new OrderParam(null, false);

  constructor(
    public objectSSLService: ObjectSSLService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.activatedRoute.data.subscribe(data => {
      this.paging = data.pagingParams;
    });
  }

  ngOnInit(): void {
    this.initTable();
  }

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  initTable() {
    this.sslFormGroup = this.fb.group({
      data: this.fb.array([])
    });
    this.loadAll();
  }

  initForm() {
    return this.fb.group({
      ssl_name: ['', Validators.required],
      ssl_description: ['', [Validators.required]],
      ssl_key: ['', [Validators.required]],
      ssl_cert: ['', Validators.required],
      ssl_key_file: ['', Validators.required],
      ssl_cert_file: ['', Validators.required],
      is_update: [true],
      is_show_cert: [false]
    });
  }

  loadAll() {
    this.isEditing = false;
    this.paging = getParameterPaging(this.paging);
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit
    };
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters['search'] = this.keywordSearch;
    }
    if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }

    this.objectSSLService.query(parameters).subscribe(res => {
      this.sslFormGroup = this.fb.group({
        data: this.fb.array([])
      });
      this.ssls = res.body.data;
      const control = <FormArray>this.sslFormGroup.get('data');
      for (const ssl of this.ssls) {
        const grp = this.fb.group({
          id: [ssl.id],
          ssl_name: [ssl.ssl_name, Validators.required],
          ssl_description: [ssl.ssl_description, [Validators.required]],
          ssl_key: [ssl.ssl_key],
          ssl_cert: [ssl.ssl_cert],
          ssl_key_file: ['', Validators.required],
          ssl_cert_file: ['', Validators.required],
          is_update: [false],
          is_show_cert: [false]
        });
        control.push(grp);
      }
      // this.paging.limit = res.body.paging.limit;
      // this.paging.offset = res.body.paging.offset + 1;
      this.paging.total = res.body.paging.total;
    });
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      // this.transition();
      this.loadAll();
    }
  }

  transition() {
    const parameters = {
      offset: this.paging.offset - 1,
      limit: this.paging.limit
    };
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters['search'] = this.keywordSearch;
    }
    if (this.order.orderBy) {
      parameters['orderBy'] = this.order.orderBy;
      parameters['orderType'] = this.order.getOrderType();
    }

    this.router.navigate(['/object/ssl'], {
      queryParams: parameters
    });
  }

  onSearch() {
    this.paging.page = 1;
    if (this.keywordSearch) {
      this.keywordSearch = this.keywordSearch.trim();
    }
    this.loadAll();
  }

  get getFormData(): FormArray {
    return <FormArray>this.sslFormGroup.get('data');
  }

  addSsl() {
    const control = <FormArray>this.sslFormGroup.get('data');
    control.push(this.initForm());
    this.isEditing = true;
  }

  save(index: number) {
    const control = <FormArray>this.sslFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    const value: Ssl = control.at(index).value;
    if (value.id) {
      this.objectSSLService.update(value).subscribe(res => {
        this.loadAll();
      }, () => {
        this.isEditing = false;
      });
    } else {
      this.objectSSLService.create(value).subscribe(res => {
        this.loadAll();
      }, () => {
        this.isEditing = false;
      });
    }
    this.isEditing = true;
  }

  onEdit(index: number) {
    const control = <FormArray>this.sslFormGroup.get('data');
    control.at(index).get('is_update').setValue(true);
    this.isEditing = true;
  }

  onCancelEdit(index: number) {
    const control = <FormArray>this.sslFormGroup.get('data');
    control.at(index).get('is_update').setValue(false);
    // reload
    this.initTable();
    this.isEditing = false;
  }

  remove(modal, index: number) {
    const control = <FormArray>this.sslFormGroup.get('data');
    this.selectedSSL = control.at(index).value;
    this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static'}).result.then((result) => {
      if (result === 'delete') {
        if (this.selectedSSL.id) {
          this.objectSSLService.delete(this.selectedSSL.id).subscribe(res => {
            control.removeAt(index);
          });
        } else {
          this.notificationService.error('Thao tác không hợp kệ');
        }
        this.isEditing = false;
      }
    }, (reason) => {
    });
  }

  openViewCert(index: number, ssl) {
    this.selectedSSL = ssl;
    const control = <FormArray>this.sslFormGroup.get('data');
    control.at(index).get('is_show_cert').setValue(!control.at(index).get('is_show_cert').value);
  }

  readFile(index: number, $event, cert) {
    const control = <FormArray>this.sslFormGroup.get('data');
    const reader = new FileReader();
    reader.onload = function (e) {
      const text = reader.result;
      control.at(index).get(cert).setValue(text);
    };
    reader.readAsText($event.target.files[0]);
  }

  downloadSSLCert() {
    const zip = new JSZip();
    zip.file('ssl.key', this.selectedSSL.ssl_key);
    zip.file('ssl.cert', this.selectedSSL.ssl_cert);

    zip.generateAsync({type: 'blob'}).then(function (blob) {
      FileSaver.saveAs(blob, 'ssl_key_cert.zip');
    }, function (err) {
      console.log('err: ' + err);
    });
  }

  disableValue(list, index) {
    for (let i = 0; i < list.length; i++) {
      if (index !== i) {
        list[i] = false;
      }
    }
  }
}
