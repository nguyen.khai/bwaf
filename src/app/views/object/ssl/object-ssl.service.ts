import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';
import {createRequestOption} from 'app/shared';
import {IPaging} from '@app/shared/model/base-respone.model';
import {ISsl} from '@app/shared/model/ssl.model';

type EntityResponseType = HttpResponse<ISsl>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;
interface PagingResponse {
  data: ISsl[];
  paging: IPaging;
}

@Injectable({ providedIn: 'root' })
export class ObjectSSLService {
  public resourceUrl = environment.apiUrl + '/ssl';

  constructor(protected http: HttpClient) {}

  create(ssl: ISsl): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ssl);
    return this.http
      .post<ISsl>(this.resourceUrl, copy, { observe: 'response' });
  }

  update(ssl: ISsl): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ssl);
    return this.http
      .put<ISsl>(`${this.resourceUrl}/${ssl.id}`, copy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ISsl>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<PagingResponse>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(ssl: ISsl): ISsl {
    const copy: ISsl = Object.assign({}, ssl, {});
    return copy;
  }
}
