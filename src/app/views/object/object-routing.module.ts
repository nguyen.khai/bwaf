import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ObjectWebsiteComponent } from "./website/object-website.component";
import { ObjectGroupWebsiteComponent } from "./group-website/object-group-website.component";
import { ObjectSslComponent } from "./ssl/object-ssl.component";
import { ObjectMailSenderComponent } from "./mail-sender/object-mail-sender.component";
import { Paging } from "@app/shared/model/base-respone.model";
import { ObjectAccountComponent } from "@app/views/object/account/object-account.component";
import { ObjectWebsiteEditComponent } from "@app/views/object/website/object-website.edit.component";
import { RuleManagementComponent } from "@app/views/object/rule-management/rule-management.component";

const routes: Routes = [
  {
    path: "",
    data: {
      title: "Object",
    },
    children: [
      {
        path: "",
        redirectTo: "ssl",
      },
      {
        path: "website",
        loadChildren: () =>
          import("./website/object-website.module").then(
            (m) => m.ObjectWebsiteModule
          ),
      },
      {
        path: "group-website",
        component: ObjectGroupWebsiteComponent,
        data: {
          title: "Group Website",
          pagingParams: new Paging(),
        },
      },
      {
        path: "ssl",
        component: ObjectSslComponent,
        data: {
          title: "SSL",
          pagingParams: new Paging(),
        },
      },
      {
        path: "mail-sender",
        component: ObjectMailSenderComponent,
        data: {
          title: "Mail Sender",
        },
      },
      {
        path: "account",
        component: ObjectAccountComponent,
        data: {
          title: "Account",
          pagingParams: new Paging(),
        },
      },
      {
        path: "rule-management",
        component: RuleManagementComponent,
        data: {
          title: "Rule Management",
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ObjectRoutingModule {}
