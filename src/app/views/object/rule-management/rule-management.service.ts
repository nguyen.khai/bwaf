import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { map } from "rxjs/operators";
import { createRequestOption } from "@app/shared";
import { listRule } from "@app/shared/model/rule-management";
import { Item } from "../../../shared/model/item.model";
type EntityArrayResponseType = HttpResponse<listRule[]>;

@Injectable({
  providedIn: "root",
})
// MonitorWafService
export class ObjectRuleService {
  public resourceUrl = environment.apiUrl + "/rule_available";
  public resourceUrlOther = environment.apiUrl + "/waf_rule";
  constructor(protected http: HttpClient) {}

  query(req, filter?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    let url = this.resourceUrl;
    if (filter) {
      url += `?${filter}`;
    }
    return this.http.get<any[]>(url, {
      params: options,
      observe: "response",
    });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrlOther}/${id}`, {
      observe: "response",
    });
  }

  deleteChidren(idChidren: number, id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.resourceUrlOther}/${id}/rule/${idChidren}`,
      {
        observe: "response",
      }
    );
  }

  createOther(value: any): Observable<any> {
    // const copy = createRequestOption(value);
    return this.http.post<any>(this.resourceUrlOther, value, {
      observe: "response",
    });
  }
  createOtherChidren(value: any, id: any): Observable<any> {
    // const copy = createRequestOption(value);
    return this.http.post<any>(`${this.resourceUrlOther}/${id}/rule`, value, {
      observe: "response",
    });
  }

  uploadFile(file: File, id: any): Observable<HttpResponse<any>> {
    const data: FormData = new FormData();
    data.append("backup_file", file);
    return this.http.post<any>(`this.resourceUrlOther/${id}/rule`, data, {
      observe: "response",
    });
  }

  updateOther(value: any): Observable<any> {
    // const copy = createRequestOption(value);
    return this.http.put<any>(
      `${this.resourceUrlOther}/${value.id}/rule`,
      value,
      {
        observe: "response",
      }
    );
  }

  updateOtherChidren(value: any, id: any): Observable<any> {
    // const copy = createRequestOption(value);
    return this.http.put<any>(
      `${this.resourceUrlOther}/${id}/rule/${value.id}`,
      value,
      {
        observe: "response",
      }
    );
  }

  queryOther(req, filter?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    let url = this.resourceUrlOther;
    return this.http.get<any[]>(url, {
      params: options,
      observe: "response",
    });
  }

  detailAvaiable(
    name: any,
    req: any,
    filter: any
  ): Observable<HttpResponse<Array<any>>> {
    const options = createRequestOption(req);
    let url = this.resourceUrl + "/" + name;
    if (filter) {
      url += `?${filter}`;
    }
    return this.http.get<Array<any>>(url, {
      params: options,
      observe: "response",
    });
  }

  detailOther(
    id: any,
    req: any,
    filter: any
  ): Observable<HttpResponse<Array<any>>> {
    const options = createRequestOption(req);
    let url = this.resourceUrlOther + "/" + id + "/rule";
    if (filter) {
      url += `?${filter}`;
    }
    return this.http.get<Array<any>>(url, {
      params: options,
      observe: "response",
    });
  }

  downloadUrl(req, filter?: any, filename?: any): string {
    const options = createRequestOption(req);
    let paramUrl = "";
    if (filter) {
      paramUrl += filter;
    }
    options.keys().forEach((key) => {
      if (paramUrl !== "") {
        paramUrl += "&";
      }
      paramUrl += key + "=" + encodeURIComponent(options.get(key));
    });
    return `${environment.apiUrl}/${
      filename ? filename.toLowerCase() : filename
    }/download`;
  }

  downloadOther(item: any): Observable<any> {
    const url = this.resourceUrlOther + "/" + item.value.id;
    return this.http.get(url, {
      observe: "response",
      responseType: "blob" as "json",
    });
  }

  detailCRS(itemDetailAvaiable: any, item: any) {
    const url =
      this.resourceUrl + "/" + itemDetailAvaiable.id + "/crs_rule/" + item.id;
    return this.http.get<Array<any>>(url, {
      params: {},
      observe: "response",
    });
  }

  SaveCRS(itemDetailAvaiable: any, item: any, body: any) {
    const options = createRequestOption(body);
    const url =
      this.resourceUrl + "/" + itemDetailAvaiable.id + "/crs_rule/" + item.id;
    return this.http.put<Array<any>>(url, options, {
      observe: "response",
    });
  }

  chart(req?: any): Observable<HttpResponse<Array<any>>> {
    const options = createRequestOption(req);
    return this.http
      .get<Array<any>>(`${environment.apiUrl}/monitor-waf-chart`, {
        params: options,
        observe: "response",
      })
      .pipe(
        map((res: HttpResponse<Array<any>>) => {
          return res;
        })
      );
  }

  download(url: string): Observable<any> {
    return this.http.get(url, {
      observe: "response",
      responseType: "blob" as "json",
    });
  }
}
