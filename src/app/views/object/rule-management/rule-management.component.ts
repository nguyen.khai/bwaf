import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import { FormArray, FormBuilder, Validators } from "@angular/forms";
import { ObjectRuleService } from "@app/views/object/rule-management/rule-management.service";
import { IPaging, Paging } from "@app/shared/model/base-respone.model";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import {
  getParameterPaging,
  OrderParam,
  PAGING_PER_PAGE,
  REQUEST_INTERVAL,
} from "@app/shared/constants/common.model";
import { listRule } from "@app/shared/model/rule-management";
import {
  PERIOD_MONITOR,
  PeriodMonitor,
} from "@app/shared/constants/monitor.model";
import { IField } from "@app/shared/ng-mutifilter/ng-mutifilter.model";
@Component({
  selector: "app-rule-management",
  templateUrl: "./rule-management.component.html",
  styleUrls: ["./rule-management.component.scss"],
})
export class RuleManagementComponent implements OnInit {
  periodFilterForms: PeriodMonitor[] = PERIOD_MONITOR;
  dataFilterForm = this.fb.group({
    filter: ["", [Validators.required]],
    period: [this.periodFilterForms[0]],
  });

  fieldsApplication: Array<IField> = [
    { label: "ID Rule", value: "id_rule" },
    { label: "Description", value: "description" },
    { label: "Message", value: "msg" },
    { label: "Tag", value: "tag" },
  ];
  otherForm = this.fb.group({
    data: this.fb.array([]),
  });
  childDtailOther = this.fb.group({
    data: this.fb.array([]),
  });
  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true,
  };
  order: OrderParam = new OrderParam("datetime", false);

  periodCharts: PeriodMonitor[] = PERIOD_MONITOR;
  periodFilterChart = this.fb.group({
    worldMapChart: [this.periodCharts[0]],
    worldPieChart: [this.periodCharts[0]],
    ipChart: [this.periodCharts[0]],
    attackWebsiteChart: [this.periodCharts[0]],
    attackActionChart: [this.periodCharts[0]],
  });
  DataCRS: Array<any> = [];
  objectRule: listRule[];
  objectRuleOther: listRule[];
  paging: IPaging;
  pagingAva: IPaging;
  pagingOther: IPaging;
  pagingOtherChidren: IPaging;
  limits = PAGING_PER_PAGE;
  // world-map
  worldMapChart: Array<any>;
  keywordSearch: string;
  urlDownload: string;
  itemDetailAvaiable: any;
  itemDetailChidren: any;
  itemDetailOther: any;
  rules: any;
  parameters: Object;
  parametersOther: Object;
  filter: string;
  dataSearchAva: any;
  searchChidren: any;
  itemOther: any;
  itemOtherChidren: any;
  add: boolean = false;
  editStatus: boolean = false;
  isEditing = false;
  demo: any;

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  addOther() {
    const control = <FormArray>this.otherForm.get("data");
    control.push(
      this.fb.group({
        id: [null],
        name: ["", Validators.required],
        description: ["", [Validators.required]],
        is_update: [true],
        is_show: [false],
      })
    );
    this.isEditing = true;
  }
  addOtherChidren(i: any) {
    const listData = <FormArray>this.otherForm.get("data");
    let control = <FormArray>listData.at(i).get("childDtailOther").get("data");
    control.push(
      this.fb.group({
        id_rule: ["", Validators.required],
        description: ["", [Validators.required]],
        rules: ["", [Validators.required]],
        is_update: [true],
        is_show: [false],
      })
    );
    this.isEditing = true;
  }

  changeLimit() {
    this.paging.page = 1;
    this.loadAll();
  }
  changeLimitOthers() {
    this.pagingOther.page = 1;
    this.loadAllOther();
  }

  changeLimitOthersChidren(item: any, filter: any, index: any) {
    this.pagingOtherChidren.page = 1;
    let value: any = document.getElementById("pagingOtherChidren");
    this.pagingOtherChidren.limit = value.value;
    this.openDetailOther(item, null, index);
  }

  changeLimitAva() {
    this.pagingAva.page = 1;
    this.openDetailAvaiable(this.itemDetailAvaiable, null);
  }

  constructor(
    public ObjectRuleService: ObjectRuleService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
    this.paging = new Paging();
    this.pagingAva = new Paging();
    this.pagingOther = new Paging();
    this.pagingOtherChidren = new Paging();
    this.objectRule = [];
  }

  ngOnInit() {
    this.loadAll();
    this.loadAllOther();
  }

  onFilter($event) {
    this.paging.page = 1;
    this.dataFilterForm.controls["filter"].setValue($event);
    this.loadAll();
  }

  onSearch() {
    this.pagingAva.offset = 1;
    if (this.dataSearchAva) {
      this.dataSearchAva = this.dataSearchAva.trim();
    }
    this.openDetailAvaiable(this.itemDetailAvaiable, null);
  }
  onSearchChidren(i: any) {
    let value: any = document.getElementById("searchChidren");
    if (value.value) {
      this.searchChidren = value.value.trim();
    }
    this.openDetailOther(this.itemDetailChidren, null, i);
  }

  refreshSearchForm() {
    this.dataFilterForm.controls["period"].setValue(this.periodFilterForms[0]);
    this.dataFilterForm.controls["filter"].setValue("");
    this.order.orderBy = "datetime";
    this.order.orderType = false;
    this.loadAll();
  }

  loadAll() {
    this.paging = getParameterPaging(this.paging);
    const period: PeriodMonitor = this.dataFilterForm.controls["period"].value;
    const parameters = {
      limit: this.paging.limit,
      offset: this.paging.offset,
    };
    if (period.type === "between") {
      parameters["dateFrom"] = period.value[0].format("YYYY-MM-DD HH:mm:ss");
      parameters["dateTo"] = period.value[1].format("YYYY-MM-DD HH:mm:ss");
    } else {
      parameters["dateValue"] = period.value;
    }
    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters["search"] = this.keywordSearch;
    }
    const filter = !this.dataFilterForm.controls["filter"].invalid
      ? this.dataFilterForm.controls["filter"].value
      : null;
    this.ObjectRuleService.query(parameters, filter).subscribe((res) => {
      if (res.status === 200) {
        this.objectRule = res.body.map((value) => {
          value["is_show"] = false;
          return value;
        });
        this.parameters = parameters;
        this.filter = filter;
      } else {
        console.warn("can not load monitor ddos");
      }
    });
  }

  loadAllOther() {
    this.pagingOther = getParameterPaging(this.pagingOther);
    const period: PeriodMonitor = this.dataFilterForm.controls["period"].value;
    const parameters = {
      limit: this.pagingOther.limit,
      offset: this.pagingOther.offset,
    };
    this.otherForm = this.fb.group({
      data: this.fb.array([]),
    });
    const control = <FormArray>this.otherForm.get("data");

    this.ObjectRuleService.queryOther(parameters).subscribe((res: any) => {
      if (res.status === 200) {
        this.objectRuleOther = res.body.data.map((value) => {
          const grp = this.fb.group({
            id: [value.id],
            name: [value.name, Validators.required],
            description: [value.description, [Validators.required]],
            is_update: [false],
            is_show: [false],
            childDtailOther: this.fb.group({
              data: this.fb.array([]),
            }),
          });
          control.push(grp);
          this.pagingOther.limit = res.body.paging.limit;
          this.pagingOther.offset = res.body.paging.offset;
          this.pagingOther.total = res.body.paging.total;
          return value;
        });
        this.parametersOther = parameters;
      } else {
        console.warn("can not load monitor ddos");
      }
    });
  }

  get getFormData(): FormArray {
    return <FormArray>this.otherForm.get("data");
  }

  onEdit(index: number) {
    const control = <FormArray>this.otherForm.get("data");
    control.at(index).get("is_update").setValue(true);
  }

  onCancelEdit(index: number) {
    const control = <FormArray>this.otherForm.get("data");
    control.at(index).get("is_update").setValue(false);
    // reload
    this.loadAllOther();
    // this.isEditing = false;
  }

  onCancelChidren(index: number, item: any) {
    this.demo.at(index).get("is_update").setValue(false);
    // reload
    this.openDetailOther(item, null, index);
    // this.isEditing = false;
  }

  onEditChidren(index: number) {
    this.demo.controls.map((e) => {
      e.get("is_update").setValue(false);
      e.get("is_show").setValue(false);
    });
    this.demo.at(index).get("is_update").setValue(true);
  }

  loadPage(page: number, item: any, i: any) {
    if (page !== this.pagingAva.previousPage) {
      this.pagingAva.previousPage = page;
      this.openDetailAvaiable(item, null);
    }
  }

  loadPageChidren(page: number, item: any, i: any) {
    if (page !== this.pagingOtherChidren.previousPage) {
      this.pagingOtherChidren.previousPage = page;
      this.openDetailOther(item, null, i);
    }
  }

  loadPageOther(page: number, item: any) {
    if (page !== this.pagingOther.previousPage) {
      this.pagingOther.previousPage = page;
      this.loadAllOther();
    }
  }

  delete(modal, index: number, item: any) {
    this.itemOther = item.value;
    this.modalService
      .open(modal, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {
          if (result === "delete") {
            if (index !== undefined) {
              this.ObjectRuleService.delete(item.value.id).subscribe((res) => {
                this.loadAllOther();
              });
            } else {
              this.notificationService.error("Thao tác không hợp kệ");
            }
          }
        },
        (reason) => {}
      );
  }

  onUploadOther(value) {
    document.getElementById("fileOther").click();
  }
  onChangeUploadFile($event, item: any) {
    this.ObjectRuleService.uploadFile(
      $event.target.files[0],
      item.value.id
    ).subscribe((res) => {});
  }

  deleteChidren(modal, index: number, itemChidren: any, item: any, idx) {
    console.log(index, idx);
    this.itemOtherChidren = itemChidren.value;
    console.log(itemChidren, "sdsd");
    this.modalService
      .open(modal, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {
          if (result === "delete") {
            if (index !== undefined) {
              this.ObjectRuleService.deleteChidren(
                itemChidren.value.id,
                item.value.id
              ).subscribe((res) => {
                this.openDetailOther(item, null, index);
              });
            } else {
              this.notificationService.error("Thao tác không hợp kệ");
            }
          }
        },
        (reason) => {}
      );
  }

  onFilterApplication($event) {
    this.openDetailAvaiable(this.itemDetailAvaiable, $event);
  }
  onFilterApplicationChidren($event, i) {
    this.openDetailOther(this.itemDetailChidren, $event, i);
  }

  openDetailAvaiable(item: any, filter: any) {
    this.itemDetailAvaiable = item;
    this.pagingAva = getParameterPaging(this.pagingAva);
    const parameters: any = {
      limit: this.pagingAva.limit,
      offset: this.pagingAva.offset,
    };
    if (this.dataSearchAva) {
      parameters.id_rule = this.dataSearchAva;
    }
    item.ruleAvailableTooltip = true;
    const nameUrl =
      item.id + "/" + item.rule_available_name.toLowerCase() + "_rule";
    this.ObjectRuleService.detailAvaiable(
      nameUrl,
      parameters,
      filter
    ).subscribe((res: any) => {
      this.DataCRS = res.body.data;
      this.DataCRS.map((e) => {
        e.tag_dialog = false;
        e.edit_detail_available = false;
      });
      this.pagingAva.limit = res.body.paging.limit;
      this.pagingAva.offset = res.body.paging.offset;
      this.pagingAva.total = res.body.paging.total;
    });
  }

  openDetailOther(item: any, filter: any, i: any) {
    this.itemDetailChidren = item;
    this.pagingOtherChidren = getParameterPaging(this.pagingOtherChidren);
    const parameters: any = {
      limit: this.pagingOtherChidren.limit,
      offset: this.pagingOtherChidren.offset,
    };
    if (this.searchChidren) {
      parameters.search = this.searchChidren;
    }

    const listData = <FormArray>this.otherForm.get("data");
    let control = <FormArray>listData.at(i).get("childDtailOther").get("data");
    // control.controls = [];
    this.ObjectRuleService.detailOther(
      item.get("id").value,
      parameters,
      filter
    ).subscribe((res: any) => {
      const data = [];
      res.body.data.map((value) => {
        const grp = this.fb.group({
          id: [value.id],
          id_rule: [value.id_rule, Validators.required],
          description: [value.description, [Validators.required]],
          rules: [value.rules, [Validators.required]],
          is_update: [false],
          is_show: [false],
        });
        data.push(grp);
      });

      control.controls = data;
      item.get("is_show").setValue(true);
      this.pagingOtherChidren.limit = res.body.paging.limit;
      this.pagingOtherChidren.offset = res.body.paging.offset;
      this.pagingOtherChidren.total = res.body.paging.total;
      this.demo = control;
    });
  }

  get getFormChildOther(): FormArray {
    return <FormArray>this.childDtailOther.get("data");
  }

  closeDeditAvailable(item) {
    item.tag_dialog = !item.tag_dialog;
    this.DataCRS.map((e) => {
      e.edit_detail_available = false;
    });
  }

  onEditCRS(item: any) {
    item.tag_dialog = true;
    this.DataCRS.map((e) => {
      e.edit_detail_available = true;
    });
    item.edit_detail_available = false;
    this.ObjectRuleService.detailCRS(this.itemDetailAvaiable, item).subscribe(
      (res: any) => {
        item.rule_detail = res.body.rule_detail;
      }
    );
  }
  changeRule(item) {
    item.get("is_show").setValue(true);
    this.rules = item.get("rules").value;
  }

  save(index: number) {
    const control = <FormArray>this.otherForm.get("data");
    control.at(index).get("is_update").setValue(false);
    const value: any = control.at(index).value;
    if (value.id) {
      this.ObjectRuleService.updateOther(value).subscribe(
        (res: any) => {
          this.loadAllOther();
        },
        () => {
          this.isEditing = false;
        }
      );
    } else {
      this.ObjectRuleService.createOther(value).subscribe(
        (res: any) => {
          this.loadAllOther();
        },
        () => {
          this.isEditing = false;
        }
      );
    }
    this.isEditing = true;
  }

  saveChidren(idx: any, index: number, item) {
    const listData = <FormArray>this.otherForm.get("data");
    let control = <FormArray>(
      listData.at(index).get("childDtailOther").get("data")
    );
    // control.at(index).get("is_update").setValue(false);
    const value: any = control.at(idx).value;
    if (value.id) {
      this.ObjectRuleService.updateOtherChidren(value, item.value.id).subscribe(
        (res: any) => {
          this.openDetailOther(item, null, index);
        },
        () => {}
      );
    } else {
      this.ObjectRuleService.createOtherChidren(value, item.value.id).subscribe(
        (res: any) => {
          this.openDetailOther(item, null, index);
        },
        () => {}
      );
    }
  }

  saveTagAva(item: any) {
    this.ObjectRuleService.SaveCRS(this.itemDetailAvaiable, item, {
      rule_deatil: item.rule_deatil,
    }).subscribe((res: any) => {
      item.rule_detail = res.body.rule_detail;
      this.closeDeditAvailable(item);
    });
  }

  onDownload(filename) {
    this.urlDownload = this.ObjectRuleService.downloadUrl(
      this.parameters,
      this.filter,
      filename
    );
    this.ObjectRuleService.download(this.urlDownload).subscribe(
      (res: Response) => {
        const data = res.body;
        // @ts-ignore
        const blob = new Blob([data], {
          type: res.headers.get("Content-Type"),
        });
        const downloadURL = window.URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.href = downloadURL;
        link.download = filename ? filename : "export_data.csv";
        link.click();
      }
    );
  }
  onDownloadOther(item) {
    console.log(item);
    this.ObjectRuleService.downloadOther(item).subscribe((res: any) => {
      console.log(res);
      const data = res.body;
      // @ts-ignore
      const blob = new Blob([data], {
        type: res.headers.get("Content-Type"),
      });
      const downloadURL = window.URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = downloadURL;
      link.download = item.value.name ? item.value.name : "export_data.csv";
      link.click();
    });
  }
}
