import { Component, OnInit } from "@angular/core";
import { ObjectWebsiteService } from "@app/views/object/website/object-website.service";
import { IWebsite } from "@app/shared/model/website.model";
import { IPaging } from "@app/shared/model/base-respone.model";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormArray, Validators } from "@angular/forms";
import { ModalDismissReasons, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import {
  getParameterPaging,
  OrderParam,
  PAGING_PER_PAGE,
} from "@app/shared/constants/common.model";
import { ISsl } from "@app/shared/model/ssl.model";
import { ObjectSSLService } from "@app/views/object/ssl/object-ssl.service";

@Component({
  selector: "app-object-website",
  templateUrl: "object-website.component.html",
  styleUrls: ["./object-website.component.scss"],
})
export class ObjectWebsiteComponent implements OnInit {
  PAGING_PER_PAGE = PAGING_PER_PAGE;
  keywordSearch: string;
  websites: IWebsite[] = [];
  paging: IPaging;
  checkDomain: string;
  checkListened: string;
  checkService: string;
  selectedWebsite: IWebsite;
  add: boolean = false;
  hoverSSL: boolean = false;
  ipPortTooltip = false;
  listenPortTooltip = false;
  sslTooltip = false;
  sslTooltipListened = false;
  ssLs: ISsl[];
  ssLActivate = false;
  editing: boolean[] = [];
  editStatus = false;
  order: OrderParam = new OrderParam(null, false);
  mainStreamTypes = [
    { name: "Main", value: "main" },
    { name: "Backup", value: "backup" },
    { name: "Load Balancing", value: "load balancing" },
  ];
  listProtocol = [
    { name: "HTTP", value: "HTTP" },
    { name: "HTTPS", value: "HTTPS" },
  ];
  websiteForm = this.fb.group({
    website_domain: [""],
    upstream: this.fb.array([]),
    cache: [""],
    listened: this.fb.group({
      main: [""],
      redirect: this.fb.array([]),
      protocol: [""],
      ssl_id: [""],
      ssl: this.fb.group({
        id: [null],
        ssl_cert: [""],
        ssl_key: [""],
      }),
    }),
    active: [1],
    ssl_id: [null],
  });

  constructor(
    public objectWebsiteService: ObjectWebsiteService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal,
    protected notificationService: ToastrCustomService,
    public objectSSLService: ObjectSSLService
  ) {
    this.activatedRoute.data.subscribe((data) => {
      this.paging = data.pagingParams;
    });
  }

  ngOnInit(): void {
    this.objectSSLService
      .query({
        offset: 0,
        limit: 100000,
      })
      .subscribe((res) => {
        this.ssLs = res.body.data;
      });

    this.loadAll();
  }

  openService(value, idx) {
    this.getUpstream.controls.map((e, index) => {
      if (value.value.protocol == "HTTPS" && idx == index) {
        this.getUpstream.at(idx).get("hoverSSL").setValue(true);
      } else {
        this.getUpstream.at(index).get("hoverSSL").setValue(false);
      }
    });
  }
  openSSLListened(value, idx) {
    if (value.protocol.value == "HTTPS") {
      this.sslTooltipListened = true;
    } else {
      this.sslTooltipListened = false;
    }
  }

  loadAll() {
    this.paging = getParameterPaging(this.paging);
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit,
    };
    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }
    if (this.keywordSearch && this.keywordSearch.trim().length > 0) {
      parameters["search"] = this.keywordSearch;
    }
    this.objectWebsiteService.query(parameters).subscribe((res) => {
      this.websites = res.body.data;
      this.paging.total = res.body.paging.total;
      this.websites.forEach(() => {
        this.editing.push(false);
      });
    });
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }

  transition() {
    const parameters = {
      offset: this.paging.offset - 1,
      limit: this.paging.limit,
    };

    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }
    this.router.navigate(["/object/website"], {
      queryParams: parameters,
    });
  }

  addWebsite() {
    this.add = true;
    this.resetValidate();
  }

  resetValidate() {
    this.checkDomain = "";
    this.checkService = "";
    this.checkListened = "";
  }

  checkV4V6(value) {
    const regex =
      /((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))/;
    return regex.test(value);
  }

  changeValue(value, check, idx) {
    if (check == "domain") {
      if (this.websiteForm.value.website_domain) {
        this.checkDomain = "";
      } else {
        this.checkDomain = "error.validate.input_required";
      }
    }
    if (check == "ip") {
      if (!value) {
        this.getUpstream
          .at(idx)
          .get("checkIpServie")
          .setValue("error.validate.input_required");
      } else if (!this.checkV4V6(value)) {
        this.getUpstream
          .at(idx)
          .get("checkIpServie")
          .setValue("error.validate.ip.invalid");
      } else {
        this.getUpstream.at(idx).get("checkIpServie").setValue("");
      }
    }
    if (check == "port") {
      if (!value) {
        this.getUpstream
          .at(idx)
          .get("checkPortServie")
          .setValue("error.validate.input_required");
      } else if (0 > value || 65535 < value) {
        this.getUpstream
          .at(idx)
          .get("checkPortServie")
          .setValue("error.validate.port.invalid");
      } else {
        this.getUpstream.at(idx).get("checkPortServie").setValue("");
      }
    }
  }

  save() {
    this.resetValidate();
    let validate = true;
    if (!this.websiteForm.value.website_domain) {
      this.checkDomain = "error.validate.input_required";
      validate = false;
    }
    if ((this.websiteForm.value.upstream || []).length == 0) {
      this.checkService = "error.validate.input_required";
      validate = false;
    }
    if (
      !this.websiteForm.value.listened.main &&
      !this.websiteForm.value.listened.protocol &&
      this.websiteForm.value.listened.redirect.length == 0
    ) {
      this.checkListened = "error.validate.input_required";
      validate = false;
    }
    // checkService
    console.log(this.websiteForm.value);
    this.websiteForm.value.upstream.map((e, idx) => {
      if (!e.ip) {
        this.getUpstream
          .at(idx)
          .get("checkIpServie")
          .setValue("error.validate.input_required");
        validate = false;
      } else if (!this.checkV4V6(e.ip)) {
        this.getUpstream
          .at(idx)
          .get("checkIpServie")
          .setValue("error.validate.ip.invalid");
        validate = false;
      } else {
        this.getUpstream.at(idx).get("checkIpServie").setValue("");
      }
      if (!e.port) {
        this.getUpstream
          .at(idx)
          .get("checkPortServie")
          .setValue("error.validate.input_required");
        validate = false;
      } else if (0 > e.port || 65535 < e.port) {
        this.getUpstream
          .at(idx)
          .get("checkPortServie")
          .setValue("error.validate.port.invalid");
        validate = false;
      } else {
        this.getUpstream.at(idx).get("checkPortServie").setValue("");
      }
    });
    if (validate) {
      this.add = false;
      this.websiteForm.patchValue({
        active: 1,
        cache: this.websiteForm.value.cache ? 1 : 0,
      });
      this.websiteForm.patchValue({
        active: 1,
        cache: this.websiteForm.value.cache ? 1 : 0,
      });
      this.websiteForm.get("upstream").value.map((e, idx) => {
        this.getUpstream.at(idx).get("ssl").setValue({
          id: e.ssl_id,
        });
      });
      this.objectWebsiteService
        .create(this.websiteForm.value)
        .subscribe((res) => {
          this.reset();
          this.loadAll();
        });
    }
  }

  reset() {
    const controlPortListenArray = <FormArray>(
      this.websiteForm.get("listened").get("redirect")
    );
    controlPortListenArray.clear();
    const controlUpstream = <FormArray>this.websiteForm.get("upstream");
    controlUpstream.clear();
    this.websiteForm.reset();
  }

  get getUpstream(): FormArray {
    return <FormArray>this.websiteForm.get("upstream");
  }
  get getListened(): FormArray {
    return <FormArray>this.websiteForm.get("listened");
  }

  get getRedirectPort(): FormArray {
    return <FormArray>this.websiteForm.get("listened").get("redirect");
  }

  addPort() {
    const control = <FormArray>this.websiteForm.get("upstream");
    control.push(
      this.fb.group({
        ip: "",
        port: "",
        protocol: "HTTP",
        ssl: {
          id: 1,
          name: "smart",
        },
        type: "main",
        hoverSSL: false,
        ssl_id: 1,
        checkIpServie: "",
        checkPortServie: "",
      })
    );
  }

  removeUpstream(i) {
    const control = <FormArray>this.websiteForm.get("upstream");
    control.removeAt(i);
  }

  delete(modal, index: number) {
    this.selectedWebsite = this.websites[index];
    this.modalService
      .open(modal, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {
          if (result === "delete") {
            if (index !== undefined) {
              this.objectWebsiteService
                .delete(this.selectedWebsite.id)
                .subscribe((res) => {
                  this.loadAll();
                });
            } else {
              this.notificationService.error("Thao tác không hợp kệ");
            }
          }
        },
        (reason) => {}
      );
  }

  addRedirectPort() {
    const control = <FormArray>this.websiteForm.get("listened").get("redirect");
    control.push(this.fb.control(""));
    console.log(control);
  }

  removeRedirectPort(i) {
    const control = <FormArray>this.websiteForm.get("listened").get("redirect");
    control.removeAt(i);
  }

  onSearch() {
    this.paging.offset = 1;
    if (this.keywordSearch) {
      this.keywordSearch = this.keywordSearch.trim();
    }
    this.loadAll();
  }

  changeEditStatus(index) {
    this.editing.map((e, idx) => {
      e = false;
    });
    this.editing[index] = true;
    this.websiteForm.patchValue({
      active: this.websites[index].active,
      cache: this.websites[index].cache,
      ssl_id: this.websites[index].ssl_id,
      website_domain: this.websites[index].website_domain,
    });
    const controlPortListen = this.websiteForm.get("listened");
    controlPortListen.patchValue({
      main: this.websites[index].listened.main,
      protocol: this.websites[index].listened.protocol,
      ssl_id: this.websites[index].listened.ssl
        ? this.websites[index].listened.ssl.id
        : "",
    });
    const controlPortListenArray = <FormArray>(
      this.websiteForm.get("listened").get("redirect")
    );
    controlPortListenArray.clear();
    this.websites[index].listened.redirect.forEach((value) => {
      controlPortListenArray.push(this.fb.control(value));
    });
    const controlUpstream = <FormArray>this.websiteForm.get("upstream");
    controlUpstream.clear();
    this.websites[index].upstream.forEach((value) => {
      value.hoverSSL = false;
      value.ssl_id = value.ssl.id;
      value.checkIpServie = "";
      value.checkPortServie = "";
      controlUpstream.push(this.fb.group(value));
    });
    this.editStatus = true;
  }

  editSave(index) {
    this.resetValidate();
    let validate = true;
    if (!this.websiteForm.value.website_domain) {
      this.checkDomain = "error.validate.input_required";
      validate = false;
    }
    if ((this.websiteForm.value.upstream || []).length == 0) {
      this.checkService = "error.validate.input_required";
      validate = false;
    }
    if (
      !this.websiteForm.value.listened.main &&
      !this.websiteForm.value.listened.protocol &&
      this.websiteForm.value.listened.redirect.length == 0
    ) {
      this.checkListened = "error.validate.input_required";
      validate = false;
    }
    this.websiteForm.value.upstream.map((e, idx) => {
      if (!e.ip) {
        this.getUpstream
          .at(idx)
          .get("checkIpServie")
          .setValue("error.validate.input_required");
        validate = false;
      } else if (!this.checkV4V6(e.ip)) {
        this.getUpstream
          .at(idx)
          .get("checkIpServie")
          .setValue("error.validate.ip.invalid");
        validate = false;
      } else {
        this.getUpstream.at(idx).get("checkIpServie").setValue("");
      }
      if (!e.port) {
        this.getUpstream
          .at(idx)
          .get("checkPortServie")
          .setValue("error.validate.input_required");
        validate = false;
      } else if (0 > e.port || 65535 < e.port) {
        this.getUpstream
          .at(idx)
          .get("checkPortServie")
          .setValue("error.validate.port.invalid");
        validate = false;
      } else {
        this.getUpstream.at(idx).get("checkPortServie").setValue("");
      }
    });
    if (validate) {
      this.websiteForm.patchValue({
        active: 1,
        cache: this.websiteForm.value.cache ? 1 : 0,
      });
      this.websiteForm.get("upstream").value.map((e, idx) => {
        this.getUpstream.at(idx).get("ssl").setValue({
          id: e.ssl_id,
        });
      });
      const ssl_id = this.websiteForm.get("listened").get("ssl_id").value;
      this.websiteForm.get("listened").get("ssl").get("id").setValue(ssl_id);
      this.objectWebsiteService
        .update(this.websiteForm.value, this.websites[index].id)
        .subscribe((res) => {
          this.editing[index] = false;
          this.reset();
          this.editStatus = false;
          this.loadAll();
          console.log(this.websites[index].id);
        });
    }
  }

  editCancel(index) {
    this.editing[index] = false;
    this.reset();
    this.editStatus = false;
  }

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  cancelSave() {
    this.add = false;
    this.reset();
  }

  onSelectSSL() {
    this.sslTooltip = !this.sslTooltip;
    if (this.websiteForm.get("ssl_id").value) {
      this.ssLActivate = true;
    } else {
      this.ssLActivate = false;
    }
  }

  activeSSL() {
    this.sslTooltip = this.ssLActivate;
  }
}
