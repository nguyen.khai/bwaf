import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';

import {environment} from 'environments/environment';
import {createRequestOption} from 'app/shared';
import {IWebsite} from '@app/shared/model/website.model';
import {IPaging} from '@app/shared/model/base-respone.model';

type EntityResponseType = HttpResponse<IWebsite>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;
interface PagingResponse {
  data: IWebsite[];
  paging: IPaging;
}

@Injectable({ providedIn: 'root' })
export class ObjectWebsiteService {
  public resourceUrl = environment.apiUrl + '/website';

  constructor(protected http: HttpClient) {}

  create(deliveryInfo: IWebsite): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(deliveryInfo);
    return this.http
      .post<IWebsite>(this.resourceUrl, copy, { observe: 'response' });
  }

  update(deliveryInfo: IWebsite, id): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(deliveryInfo);
    return this.http
      .put<IWebsite>(this.resourceUrl + `/${id}`, copy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IWebsite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<PagingResponse>(this.resourceUrl, { params: options, observe: 'response' });
  }
  /*selectWebsite(): Observable<EntityArrayResponseType> {
    return this.http.get<string[]>(this.resourceUrl + );
  }*/

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(website: IWebsite): IWebsite {
    const copy: IWebsite = Object.assign({}, website, {});
    return copy;
  }
}
