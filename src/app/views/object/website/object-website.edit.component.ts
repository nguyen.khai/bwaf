import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, Validators } from "@angular/forms";
import { ObjectWebsiteService } from "@app/views/object/website/object-website.service";
import { ActivatedRoute, Router } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { IWebsite } from "@app/shared/model/website.model";
import { ISsl } from "@app/shared/model/ssl.model";
import { ObjectSSLService } from "@app/views/object/ssl/object-ssl.service";

@Component({
  selector: "app-object-website-edit",
  templateUrl: "object-website.edit.component.html",
})
export class ObjectWebsiteEditComponent implements OnInit {
  website: IWebsite;
  prevUpStream = this.fb.array([]);
  prevPortListen = this.fb.group({
    main: [""],
    redirect: this.fb.array([this.fb.control("")]),
  });
  websiteForm = this.fb.group({
    website_domain: [""],
    upstream: this.fb.array([
      this.fb.group({
        ip: ["", Validators.required],
        port: ["", Validators.required],
        type: ["", Validators.required],
        protocol: ["", Validators.required],
        ssl: this.fb.group({
          id: [null],
          ssl_cert: [""],
          ssl_key: [""],
        }),
      }),
    ]),
    cache: [""],
    listened: this.fb.group({
      main: [""],
      redirect: this.fb.array([this.fb.control("")]),
      protocol: [""],
      ssl: this.fb.group({
        id: [null],
        ssl_cert: [""],
        ssl_key: [""],
      }),
    }),
    active: [""],
    ssl_id: [null],
  });
  sslTooltip = false;
  ssLs: ISsl[];
  ssLActivate = false;

  constructor(
    public objectWebsiteService: ObjectWebsiteService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal,
    public objectSSLService: ObjectSSLService
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ website }) => {
      this.website = website;
    });
    this.objectSSLService.query().subscribe((res) => {
      this.ssLs = res.body.data;
    });

    this.websiteForm.patchValue({
      active: this.website.active,
      cache: this.website.cache,
      ssl_id: this.website.ssl_id ? this.website.ssl_id : null,
      website_domain: this.website.website_domain,
    });
    const controlPortListen = this.websiteForm.get("listened");
    controlPortListen.patchValue({ main: this.website.listened.main });
    const controlPortListenArray = <FormArray>(
      this.websiteForm.get("listened").get("redirect")
    );
    controlPortListenArray.clear();
    this.website.listened.redirect.forEach((value) => {
      controlPortListenArray.push(this.fb.control(value));
    });
    const controlUpstream = <FormArray>this.websiteForm.get("upstream");
    controlUpstream.clear();
    this.website.upstream.forEach((value) => {
      controlUpstream.push(this.fb.group(value));
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    if (!this.ssLActivate) {
      this.websiteForm.patchValue({
        ssl_id: null,
      });
    }
    this.objectWebsiteService
      .update(this.websiteForm.value, this.website.id)
      .subscribe((res) => {
        this.previousState();
      });
  }

  get getUpstream(): FormArray {
    return <FormArray>this.websiteForm.get("upstream");
  }

  get getRedirectPort(): FormArray {
    return <FormArray>this.websiteForm.get("listened").get("redirect");
  }

  addPort() {
    const control = <FormArray>this.websiteForm.get("upstream");
    control.push(
      this.fb.group({
        ip: [""],
        port: [""],
        type: [""],
        protocol: [""],
      })
    );
  }

  removeUpstream(i) {
    const control = <FormArray>this.websiteForm.get("upstream");
    control.removeAt(i);
  }

  addRedirectPort() {
    const control = <FormArray>this.websiteForm.get("listened").get("redirect");
    control.push(this.fb.control(""));
    console.log(control);
  }

  removeRedirectPort(i) {
    const control = <FormArray>this.websiteForm.get("listened").get("redirect");
    control.removeAt(i);
  }
}
