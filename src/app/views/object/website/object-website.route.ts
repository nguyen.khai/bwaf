import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot, Routes} from '@angular/router';
import {ObjectWebsiteComponent} from '@app/views/object/website/object-website.component';
import {ObjectWebsiteEditComponent} from '@app/views/object/website/object-website.edit.component';
import {Paging} from '@app/shared/model/base-respone.model';
import {Injectable} from '@angular/core';
import {IWebsite, Website} from '@app/shared/model/website.model';
import {Observable, of} from 'rxjs';
import {ObjectWebsiteService} from '@app/views/object/website/object-website.service';
import {HttpResponse} from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class WebsiteResolve implements Resolve<IWebsite> {
  constructor(private service: ObjectWebsiteService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IWebsite> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Website>) => response.ok),
        map((website: HttpResponse<Website>) => website.body)
      );
    }
    return of(new Website());
  }
}


export const websiteRoute: Routes = [
  {
    path: '',
    component: ObjectWebsiteComponent,
    data: {
      title: 'Websites',
      pagingParams: new Paging()
    }
  },
  {
    path: ':id',
    component: ObjectWebsiteEditComponent,
    resolve: {
      website: WebsiteResolve
    }
  }
];
