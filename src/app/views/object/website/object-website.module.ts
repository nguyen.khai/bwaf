import {SharedModule} from '@app/shared';
import {RouterModule} from '@angular/router';
import {ProfileComponent} from '@app/views/profile/profile.component';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {AvatarModule} from "ngx-avatar";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {CollapseModule} from "ngx-bootstrap";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {websiteRoute} from "@app/views/object/website/object-website.route";
import {ObjectWebsiteComponent} from "@app/views/object/website/object-website.component";
import {ObjectWebsiteEditComponent} from "@app/views/object/website/object-website.edit.component";
import {ObjectModule} from '@app/views/object/object.module';
import {SortDirective, SortByDirective} from '@app/shared/util/directive/sort.directive';
// import {SortByDirective} from '@app/shared/util/directive/sort-by.directive';

const ENTITY_STATES = [...websiteRoute];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(ENTITY_STATES),
        AvatarModule,
        FormsModule,
        CollapseModule,
        FontAwesomeModule,
        ReactiveFormsModule,
    ],
  declarations: [
    ObjectWebsiteComponent,
    ObjectWebsiteEditComponent,
  ],
  entryComponents: [
    ObjectWebsiteComponent,
    ObjectWebsiteEditComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ObjectWebsiteModule {
}
