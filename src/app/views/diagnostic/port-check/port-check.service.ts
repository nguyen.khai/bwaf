import { Injectable } from '@angular/core';
import {environment} from "@env/environment";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {ICheckPing, IPing} from "@app/shared/model/ping.model";
import {Observable} from "rxjs";
import {IPortCheck} from "@app/shared/model/port-check.model";
type EntityResponseType = HttpResponse<IPortCheck>;

@Injectable({
  providedIn: 'root'
})
export class PortCheckService {
  public resourceUrl = environment.apiUrl + '/port-check';
  constructor(protected http: HttpClient) { }

  create(portCheck: IPortCheck): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(portCheck);
    return this.http
      .post<IPortCheck>(this.resourceUrl, copy, { observe: 'response' });
  }

  protected convertDateFromClient(portCheck: IPortCheck): ICheckPing {
    const copy: IPortCheck = Object.assign({}, portCheck, {});
    return copy;
  }
}
