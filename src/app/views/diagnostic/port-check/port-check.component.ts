import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {PortCheckService} from "@app/views/diagnostic/port-check/port-check.service";
import {IPortCheck, PortCheck} from "@app/shared/model/port-check.model";

@Component({
  selector: 'app-port-check',
  templateUrl: './port-check.component.html',
  styleUrls: ['./port-check.component.scss']
})
export class PortCheckComponent implements OnInit {
  portCheck: IPortCheck;
  editForm = this.fb.group({
    ip_address: [null, [Validators.required]],
    port: [null, [Validators.required, Validators.max(65535), Validators.min(1)]],
    is_open: [''],
  });
  constructor(
    public portCheckService: PortCheckService,
    private fb: FormBuilder,
    protected activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(({portCheck}) => {
      this.updateForm(portCheck);
    });
  }

  save() {
    const portCheck = this.createFromForm();
    this.portCheckService.create(portCheck).subscribe(res => {
      this.portCheck = res.body;
      console.log(res.body);
    });
  }

  updateForm(portCheck: IPortCheck) {
    this.editForm.patchValue({
      ip_address: portCheck.ip_address,
      port: portCheck.port,
    });
  }

  private createFromForm(): IPortCheck {
    return {
      ...new PortCheck(),
      ip_address: this.editForm.get(['ip_address']).value,
      port: this.editForm.get(['port']).value,
    };
  }

}
