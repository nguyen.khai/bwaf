import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from "@angular/common/http";
import {environment} from "@env/environment";
import {Observable} from "rxjs";
import {ICheckPing, IPing} from "@app/shared/model/ping.model";

type EntityResponseType = HttpResponse<IPing>;

@Injectable({
  providedIn: 'root'
})
export class PingService {
  public resourceUrl = environment.apiUrl + '/ping';
  constructor(protected http: HttpClient) { }

  create(checkPing: ICheckPing): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(checkPing);
    return this.http
      .post<IPing>(this.resourceUrl, copy, { observe: 'response' });
  }

  protected convertDateFromClient(checkPing: ICheckPing): ICheckPing {
    const copy: ICheckPing = Object.assign({}, checkPing, {});
    return copy;
  }
}
