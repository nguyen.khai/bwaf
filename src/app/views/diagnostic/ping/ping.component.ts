import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PingService} from "@app/views/diagnostic/ping/ping.service";
import {CheckPing, ICheckPing, IPing} from "@app/shared/model/ping.model";
import {ActivatedRoute} from "@angular/router";
import {delay} from "rxjs/operators";

@Component({
  selector: 'app-ping',
  templateUrl: './ping.component.html',
  styleUrls: ['./ping.component.scss']
})
export class PingComponent implements OnInit {
  checkPing: ICheckPing;
  ping: IPing;
  ip_address: string;
  packet_size: number;
  editForm = this.fb.group({
    ip_address: ['', [Validators.required]],
    count: [null, [Validators.required, Validators.max(50), Validators.min(1)]],
    packet_size: [null, [Validators.required, Validators.max(1472), Validators.min(4)]],
    timeout: [null, [Validators.required, Validators.max(2000), Validators.min(100)]],
  });
  pingForm = this.fb.group({
    ping_packet: this.fb.array([
      this.fb.group({
        seq: [''],
        reply_from: [''],
        packet_size: [''],
        time: [''],
        TTL: [''],
      })
    ]),
    time: this.fb.group({
      min: [''],
      max: [''],
      avg: [''],
    })
  });

  //
  constructor(
    public pingService: PingService,
    private fb: FormBuilder,
    protected activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.packet_size = 64
    this.editForm.patchValue({
      'count': 4,
      'timeout': 800
    })
    this.activatedRoute.data.subscribe(({checkPing}) => {
      this.updateForm(checkPing);
    });

  }

  save() {
    const checkPing = this.createFromForm();
    this.pingService.create(checkPing).subscribe(res => {
      this.ping = res.body;
      if (this.ping) {
        this.mappingData();
      }
      console.log(res.body);
    });
  }

  mappingData() {
    const controlPing = <FormArray>this.pingForm.get('ping_packet');
    controlPing.clear();
    this.ping.ping_packet.forEach(value => {
      controlPing.push(this.fb.group(value));
    });
    const operationTime = this.pingForm.get('time');
    operationTime.patchValue({
      'max': this.ping.time.max,
      'min': this.ping.time.min,
      'avg': this.ping.time.avg,
    });
    console.log(controlPing);
  }

  updateForm(checkPing: ICheckPing) {
    this.editForm.patchValue({
      ip_address: checkPing.ip_address,
      count: checkPing.count,
      packet_size: checkPing.packet_size,
      timeout: checkPing.timeout,
    });
  }

  private createFromForm(): ICheckPing {
    return {
      ...new CheckPing(),
      ip_address: this.editForm.get(['ip_address']).value,
      count: this.editForm.get(['count']).value,
      packet_size: this.editForm.get(['packet_size']).value,
      timeout: this.editForm.get(['timeout']).value,
    };
  }
  get getPing(): FormArray {
    return <FormArray>this.pingForm.get('ping_packet');
  }
}
