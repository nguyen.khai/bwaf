import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Paging} from '@app/shared/model/base-respone.model';
import {PingComponent} from "@app/views/diagnostic/ping/ping.component";
import {TracerouteComponent} from "@app/views/diagnostic/traceroute/traceroute.component";
import {PortCheckComponent} from "@app/views/diagnostic/port-check/port-check.component";

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Diagnostic'
    },
    children: [
      {
        path: 'ping',
        component: PingComponent,
        data: {
          title: 'Ping',
        }
      },
      {
        path: 'traceroute',
        component: TracerouteComponent,
        data: {
          title: 'Traceroute',
        }
      },
      {
        path: 'port-check',
        component: PortCheckComponent,
        data: {
          title: 'Port Check',
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiagnosticRoutingModule {
}
