import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";
import {TracerouteService} from "@app/views/diagnostic/traceroute/traceroute.service";
import {ITraceroute, ITracerouteCheck, Traceroute, TracerouteCheck} from "@app/shared/model/traceroute.model";

@Component({
  selector: 'app-traceroute',
  templateUrl: './traceroute.component.html',
  styleUrls: ['./traceroute.component.scss']
})
export class TracerouteComponent implements OnInit {
  traceroute: ITraceroute;
  ip_address: string;
  max_ttl: number;
  editForm = this.fb.group({
    ip_address: [null, [Validators.required]],
    max_ttl: [null, [Validators.required, Validators.maxLength(30), Validators.minLength(1)]],
  });
  tracerouteForm = this.fb.group({
    traceroute_packet: this.fb.array([
      this.fb.group({
        seq: [''],
        time: this.fb.array([
          this.fb.control('')
        ]),
        reply_from: [''],
      })
    ]),
  });

  constructor(
    public tracerouteService: TracerouteService,
    private fb: FormBuilder,
    protected activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.max_ttl = 20;
    this.activatedRoute.data.subscribe(({tracerouteCheck}) => {
      this.updateForm(tracerouteCheck);
    });
  }

  save() {
    const tracerouteCheck = this.createFromForm();
    this.tracerouteService.create(tracerouteCheck).subscribe(res => {
      this.traceroute = res.body;
    });
  }

  updateForm(tracerouteCheck: ITracerouteCheck) {
    this.editForm.patchValue({
      ip_address: tracerouteCheck.ip_address,
      max_ttl: tracerouteCheck.max_ttl,
    });
  }

  private createFromForm(): ITracerouteCheck {
    return {
      ...new TracerouteCheck(),
      ip_address: this.editForm.get(['ip_address']).value,
      max_ttl: this.editForm.get(['max_ttl']).value,
    };
  }
}
