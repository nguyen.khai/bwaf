import { Injectable } from '@angular/core';
import {environment} from "@env/environment";
import {HttpClient, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs";
import {ITraceroute, ITracerouteCheck, ITraceroutePacket} from "@app/shared/model/traceroute.model";
type EntityResponseType = HttpResponse<ITraceroute>;

@Injectable({
  providedIn: 'root'
})

export class TracerouteService {
  public resourceUrl = environment.apiUrl + '/traceroute';
  constructor(protected http: HttpClient) { }

  create(tracerouteCheck: ITracerouteCheck): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(tracerouteCheck);
    return this.http
      .post<ITraceroute>(this.resourceUrl, copy, { observe: 'response' });
  }

  protected convertDateFromClient(tracerouteCheck: ITracerouteCheck): ITracerouteCheck {
    const copy: ITracerouteCheck = Object.assign({}, tracerouteCheck, {});
    return copy;
  }
}
