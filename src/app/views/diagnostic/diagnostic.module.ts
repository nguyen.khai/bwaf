import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@app/shared';
import {ReactiveFormsModule} from '@angular/forms';
import {CollapseModule, TooltipModule} from 'ngx-bootstrap';
import { PingComponent } from './ping/ping.component';
import {DiagnosticRoutingModule} from "./object-routing.module";
import { TracerouteComponent } from './traceroute/traceroute.component';
import { PortCheckComponent } from './port-check/port-check.component';

@NgModule({
  imports: [DiagnosticRoutingModule, CommonModule, SharedModule, ReactiveFormsModule, CollapseModule, TooltipModule],
  declarations: [
  PingComponent,
  TracerouteComponent,
  PortCheckComponent
  ]
})
export class DiagnosticModule { }
