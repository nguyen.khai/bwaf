import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Paging} from '@app/shared/model/base-respone.model';
import {WafComponent} from './waf.component';

const routes: Routes = [
  {
    path: '',
    component: WafComponent,
    data: {
      title: 'Waf'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WafRoutingModule {
}
