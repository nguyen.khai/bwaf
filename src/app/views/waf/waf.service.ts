import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import * as moment from "moment";
import { map } from "rxjs/operators";
import { IPaging } from "@app/shared/model/base-respone.model";
import {
  IAlertMail,
  IAlertSms,
  IRuleWaf,
  IWaf,
} from "@app/shared/model/waf.model";
import { createRequestOption } from "@app/shared";
import { ObjectGroupWebsiteService } from "@app/views/object/group-website/object-group-website.service";
import { ObjectMailSenderService } from "@app/views/object/mail-sender/object-mail-sender.service";

type EntityResponseType = HttpResponse<IWaf>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;

interface PagingResponse {
  data: IWaf[];
  paging: IPaging;
}

interface WafResponse {
  data: IWaf[];
}

interface AlertResponse {
  alert_mail: IAlertMail;
  alert_sms: IAlertSms;
}

@Injectable({
  providedIn: "root",
})
export class WafService {
  public resourceUrl = environment.apiUrl + "/waf";

  constructor(
    protected http: HttpClient,
    public objectGroupWebsiteService: ObjectGroupWebsiteService,
    public objectMailSenderService: ObjectMailSenderService
  ) {}

  detailGroupWeb(id: number): Observable<HttpResponse<any>> {
    return this.http.get<any>(`${this.resourceUrl}/${id}`, {
      observe: "response",
    });
  }

  websiteException(id: number, idWeb: number): Observable<HttpResponse<any>> {
    return this.http.get<any>(`${this.resourceUrl}/${idWeb}/exception`, {
      observe: "response",
    });
  }

  updateException(itemChidren: any, wafWeb: any): Observable<any> {
    return this.http.put<any>(
      `${this.resourceUrl}/${wafWeb.website_id}/exception/${itemChidren.id}`,
      itemChidren,
      {
        observe: "response",
      }
    );
  }

  createException(itemChidren: any, wafWeb: any): Observable<any> {
    return this.http.post<any>(
      `${this.resourceUrl}/${wafWeb.website_id}/exception`,
      itemChidren,
      {
        observe: "response",
      }
    );
  }

  deleteChidren(idChidren: number, id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.resourceUrl}/${id}/exception/${idChidren}`,
      {
        observe: "response",
      }
    );
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<PagingResponse>(this.resourceUrl, {
      params: options,
      observe: "response",
    });
    // .pipe(map((res: EntityArrayResponseType) => this.getDescription(res)));
  }

  changeStatus(req: IWaf): Observable<HttpResponse<any>> {
    const param = {
      group_rule_status: req.waf_status,
    };
    return this.http.put<any>(`${this.resourceUrl}/${req.id}`, param, {
      observe: "response",
    });
  }

  protected getDescription(
    res: EntityArrayResponseType
  ): EntityArrayResponseType {
    if (res.body.data) {
      res.body.data.forEach((value) => {
        this.objectGroupWebsiteService
          .find(value.id)
          .subscribe((resGruopWebsite) => {
            value.description = resGruopWebsite.body.description;
          });
      });
    }
    return res;
  }

  findAllRules(id: number): Observable<HttpResponse<IRuleWaf[]>> {
    return this.http.get<IRuleWaf[]>(`${this.resourceUrl}/${id}`, {
      observe: "response",
    });
  }

  findRule(idWaf: number, idRule: number): Observable<HttpResponse<IRuleWaf>> {
    return this.http.get<IRuleWaf>(
      `${this.resourceUrl}/${idWaf}/rule/${idRule}`,
      { observe: "response" }
    );
  }

  changeStatusRule(waf: IWaf, rule: IRuleWaf): Observable<HttpResponse<any>> {
    const param = {
      rule_status: rule.status,
    };
    return this.http.put<any>(
      `${this.resourceUrl}/${waf.id}/rule/${rule.id}`,
      param,
      { observe: "response" }
    );
  }

  // update(ruleNetwork: IWaf): Observable<HttpResponse<any>> {
  //   return this.http
  //     .put<any>(`${this.resourceUrl}/${ruleNetwork.id}`, ruleNetwork, {observe: 'response'});
  // }

  findAlert(id: number): Observable<HttpResponse<AlertResponse>> {
    return this.http
      .get<AlertResponse>(`${this.resourceUrl}-alert/${id}`, {
        observe: "response",
      })
      .pipe(
        map((res: HttpResponse<AlertResponse>) => {
          // fake data
          // res.body.alert_mail = {
          //   mail_sender: {},
          //   account: [],
          //   barrier: null,
          //   state: null,
          //   time: null,
          //   alert_type: 'mail'
          // }
          // res.body.alert_sms = {
          //   sms_sender: {},
          //   account: [],
          //   barrier: null,
          //   state: null,
          //   time: null,
          //   alert_type: 'sms'
          // }
          res.body.alert_sms.state =
            res.body.alert_sms.state == null ? 0 : res.body.alert_sms.state;
          res.body.alert_mail.state =
            res.body.alert_mail.state == null ? 0 : res.body.alert_mail.state;

          return res;
        })
      );
    // .get<AlertResponse>(`http://27.72.28.152:3002/api/waf-alert/16`, {observe: 'response'});
  }

  updateAlert(id: number, alertWaf: any): Observable<HttpResponse<any>> {
    return this.http.put<any>(`${this.resourceUrl}-alert/${id}`, alertWaf, {
      observe: "response",
    });
  }

  querySmsSender(req?: any): Observable<any> {
    const options = createRequestOption(req);
    return this.http.get<any>(`${environment.apiUrl}/sms-sender`, {
      params: options,
      observe: "response",
    });
  }
}
