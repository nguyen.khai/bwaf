import { Component, OnInit } from "@angular/core";
import { ObjectMailSenderService } from "@app/views/object/mail-sender/object-mail-sender.service";
import { ActivatedRoute, Router } from "@angular/router";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormArray, FormBuilder, Validators } from "@angular/forms";
import { WafService } from "@app/views/waf/waf.service";
import { IRuleWaf, IWaf } from "@app/shared/model/waf.model";
import { IMailSender } from "@app/shared/model/mail-sender.model";
import { IAccount } from "@app/shared/model/account.model";
import { IDropdownSettings } from "ng-multiselect-dropdown";
import { IPaging, Paging } from "@app/shared/model/base-respone.model";
import {
  getParameterPaging,
  MAX_INT,
  OrderParam,
  PAGING_PER_PAGE,
} from "@app/shared/constants/common.model";
import { ObjectGroupWebsiteService } from "@app/views/object/group-website/object-group-website.service";
import { ObjectAccountService } from "@app/views/object/account/object-account.service";

@Component({
  selector: "app-waf",
  templateUrl: "./waf.component.html",
  styleUrls: ["./waf.component.scss"],
})
export class WafComponent implements OnInit {
  limits = PAGING_PER_PAGE;
  wafs: IWaf[] = [];
  mailSenders: IMailSender[];
  smsSenders: Array<any>;
  accounts: IAccount[];
  searchDetailGroup: any;
  itemException: any;
  ListRule: any;
  demo: any;
  paging: IPaging;
  order: OrderParam = new OrderParam(null, false);
  txtSearch: string;
  formException = this.fb.group({
    data: this.fb.array([]),
  });
  fieldsApplication: Array<any> = [
    { label: "ID Rule", value: "id_rule" },
    { label: "Description", value: "description" },
    { label: "Message", value: "msg" },
    { label: "Tag", value: "tag" },
  ];
  settingMultiSelect: IDropdownSettings = {
    singleSelection: false,
    idField: "id",
    selectAllText: "Select All",
    unSelectAllText: "UnSelect All",
    noDataAvailablePlaceholderText: "Not Found",
    allowSearchFilter: true,
    defaultOpen: false,
  };
  settingMultiSelectAccount: IDropdownSettings = {
    ...this.settingMultiSelect,
    textField: "name",
  };

  constructor(
    public objectMaiSenderService: ObjectMailSenderService,
    public wafService: WafService,
    public objectAccountService: ObjectAccountService,
    public objectGroupWebsiteService: ObjectGroupWebsiteService,
    protected router: Router,
    protected notificationService: ToastrCustomService,
    protected activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fb: FormBuilder
  ) {
    this.paging = new Paging();
  }

  ngOnInit() {
    this.loadMailSenders();
    this.loadAccounts();
    this.loadSmsSender();
    this.loadAll();
  }

  changeLimit() {
    this.paging.page = 1;
    this.loadAll();
  }

  loadAll() {
    this.paging = getParameterPaging(this.paging);
    const parameters = {
      offset: this.paging.offset,
      limit: this.paging.limit,
    };
    if (this.order.orderBy) {
      parameters["orderBy"] = this.order.orderBy;
      parameters["orderType"] = this.order.getOrderType();
    }
    if (this.txtSearch && this.txtSearch.trim().length > 0) {
      parameters["search"] = this.txtSearch;
    }
    this.wafService.query(parameters).subscribe((res) => {
      if (res.status === 200) {
        this.wafs = res.body.data.map((value) => {
          value.show_alert = false;
          value.show_rule = false;
          value.distable_status = false;
          value.distable_show_rule = false;
          value.show_expand = false;
          value.alert_sms_form = this.fb.group({
            alert_type: ["sms"],
            barrier: [null, [Validators.required]],
            time: [null, [Validators.required]],
            sms_sender: [null, [Validators.required]],
            account: [[]],
            state: [null, [Validators.required]],
            is_update: [false],
          });
          value.alert_mail_form = this.fb.group({
            alert_type: ["mail"],
            barrier: [null, [Validators.required]],
            time: [null, [Validators.required]],
            mail_sender: [null, [Validators.required]],
            account: [[]],
            state: [null, [Validators.required]],
            is_update: [false],
          });
          return value;
        });

        // this.wafs = res.body.data;
        // this.wafs.forEach(value => {
        //   value.show_alert = false;
        //   value.show_rule = false;
        //   value.description = this.findDescription(value.id);
        // })

        // this.paging.limit = res.body.paging.limit;
        // this.paging.offset = res.body.paging.offset + 1;
        this.paging.total = res.body.paging.total;
        console.log(this.paging);
        console.log(this.wafs);
        // this.transition();
      } else {
        console.log("can not load waf");
      }
    });
  }

  loadSmsSender() {
    this.wafService.querySmsSender().subscribe((res) => {
      console.log("load sms sender");
      if (res.status === 200) {
        this.smsSenders = res.body.data;
        console.log(this.smsSenders);
      }
    });
  }

  findDescription(id: number): string {
    this.objectGroupWebsiteService.find(id).subscribe((res) => {
      if (res.status === 200) {
        console.log(res.body);
        return res.body.description;
      }
    });
    return null;
  }

  loadMailSenders() {
    this.objectMaiSenderService
      .query({
        offset: 0,
        limit: MAX_INT,
      })
      .subscribe((res) => {
        if (res.status === 200) {
          this.mailSenders = res.body.data;
        } else {
          console.log("can not load waf");
        }
      });
  }

  loadAccounts() {
    this.objectAccountService
      .query({
        offset: 0,
        limit: MAX_INT,
      })
      .subscribe((res) => {
        if (res.status === 200) {
          this.accounts = res.body.data;
          // this.accounts.forEach(value => {
          //   value['isEnableCheck'] = true;
          // })
          console.log("list accouts");
          console.log(this.accounts);
        } else {
          console.log("can not load waf");
        }
      });
  }

  loadRule() {
    this.wafService.findRule(1, 1).subscribe((res) => {
      if (res.status === 200) {
        console.log(res.body);
      } else {
        console.log("can not load rule");
      }
    });
  }

  // transition() {
  //   const parameters = {
  //     offset: this.paging.offset - 1,
  //     limit: this.paging.limit
  //   }
  //   if (this.order.orderBy) {
  //     parameters['orderBy'] = this.order.orderBy;
  //     parameters['orderType'] = this.order.getOrderType();
  //   }
  //   if (this.txtSearch && this.txtSearch.trim().length > 0) {
  //     parameters['search'] = this.txtSearch;
  //   }
  //   this.router.navigate(['/waf'], {
  //     queryParams: parameters
  //   });
  // }

  search() {
    this.paging.page = 1;
    this.loadAll();
  }

  changeOder(order) {
    this.order = order;
    this.loadAll();
  }

  showRuleWebsite(waf) {
    waf.is_show = !waf.is_show;
  }

  openDetailException(waf: any, wafWeb: any) {
    wafWeb.formException = this.fb.group({
      data: this.fb.array([]),
    });
    const control = <FormArray>wafWeb.formException.get("data");
    wafWeb.showException = true;
    this.wafService
      .websiteException(waf.id, wafWeb.website_id)
      .subscribe((res: any) => {
        if (res.status === 200) {
          wafWeb.formException.data = res.body.data.map((value) => {
            const grp = this.fb.group({
              id: [value.id],
              id_rule: [value.id_rule, Validators.required],
              description: [value.description, [Validators.required]],
              rules: [value.rules, [Validators.required]],
              excep_status: [value.excep_status],
              is_update: [false],
              is_show: [false],
              is_distable: true,
              is_site: [false],
              site: [["/acv", "/acv"]],
            });
            control.push(grp);
            return value;
          });
          this.demo = control;
        }
      });
  }

  onEditChidren(itemChidren: any) {
    itemChidren.get("is_update").setValue(true);
    itemChidren.get("is_distable").setValue(false);
  }

  onCance(itemChidren: any, waf: any, wafWeb: any) {
    itemChidren.get("is_update").setValue(true);
    itemChidren.get("is_distable").setValue(true);
    this.openDetailException(waf, wafWeb);
  }

  addException(wafWeb) {
    const control = wafWeb.formException.controls.data;
    control.push(
      this.fb.group({
        id: [null],
        id_rule: ["", Validators.required],
        description: ["", [Validators.required]],
        rules: ["", [Validators.required]],
        excep_status: [1],
        is_update: [true],
        is_show: [false],
        is_distable: false,
        is_site: [false],
      })
    );
  }

  saveChidren(itemChidren: any, wafWeb: any, waf: any) {
    itemChidren.get("is_update").setValue(false);
    if (itemChidren.value.id) {
      this.wafService.updateException(itemChidren.value, wafWeb).subscribe(
        (res: any) => {
          this.openDetailException(waf, wafWeb);
        },
        () => {}
      );
    } else {
      this.wafService.createException(itemChidren.value, wafWeb).subscribe(
        (res: any) => {
          this.openDetailException(waf, wafWeb);
        },
        () => {}
      );
    }
  }

  changeRule(item) {
    item.get("is_show").setValue(true);
  }

  showRule(waf: IWaf) {
    waf.show_rule = !waf.show_rule;
    if (waf.show_rule) {
      this.wafs.forEach((value) => {
        value.distable_show_alert = true;
      });
    } else {
      const tmp = this.wafs.findIndex((value) => {
        return value.show_rule;
      });
      if (tmp < 0) {
        this.wafs.forEach((value) => {
          value.distable_show_alert = false;
        });
      }
    }
    // this.wafService.findAllRules(waf.id).subscribe((res: any) => {
    //   if (res.status === 200) {
    //       waf.rules = res.body.map((value) => {
    //     });
    //   } else {
    //     console.log("can not load all rule");
    //   }
    // });
  }

  deleteChidren(modal, itemChidren, waf, wafWeb) {
    this.itemException = itemChidren.value;
    this.modalService
      .open(modal, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {
          if (result === "delete") {
            this.wafService
              .deleteChidren(itemChidren.value.id, wafWeb.website_id)
              .subscribe((res) => {
                this.openDetailException(waf, wafWeb);
              });
          }
        },
        (reason) => {}
      );
  }

  openWebsite(waf) {
    waf.show_expand = !waf.show_expand;
    if (waf.show_expand === true) {
      this.wafService.detailGroupWeb(waf.id).subscribe((res: any) => {
        if (res.status === 200) {
          waf.group_website = res.body.group_website;
          waf.group_website.map((e: any) => {
            e.is_show = false;
            e.formException = this.fb.group({
              data: this.fb.array([]),
            });
          });
        }
      });
    }
  }

  showAlert(waf: IWaf) {
    this.loadAlert(waf);
    waf.show_alert = !waf.show_alert;
    if (waf.alert_sms && waf.alert_mail) {
      this.onCancelEditAlert(waf, "sms");
      this.onCancelEditAlert(waf, "mail");
    }
    if (waf.show_alert) {
      this.changeDistableAlert(waf);
    } else {
      this.unDistableAlertAll();
    }
  }

  // alert_type: string;
  // barrier: number;
  // time: number;
  // sms_sender: IMailSender;
  // account: IAccount[];
  // state: number;
  changeDistableAlert(waf: IWaf) {
    this.wafs.forEach((value) => {
      if (waf.id != value.id) {
        value.distable_show_alert = true;
      } else {
        value.distable_show_alert = false;
      }
      value.distable_show_rule = true;
    });
  }

  unDistableAlertAll() {
    this.wafs.forEach((value) => {
      value.distable_show_alert = false;
      value.distable_show_rule = false;
    });
  }

  loadAlert(waf: IWaf) {
    this.wafService.findAlert(waf.id).subscribe((res) => {
      if (res.status === 200) {
        waf.alert_sms = res.body.alert_sms;
        waf.alert_mail = res.body.alert_mail;
        // if (waf.alert_sms.sms_sender.id != null) {
        //   waf.alert_sms.sms_sender = this.smsSenders.find(value => {
        //     return value.id === waf.alert_sms.sms_sender.id;
        //   });
        // }
        if (waf.alert_mail.mail_sender.id) {
          const indexMailSender = this.mailSenders.findIndex((value) => {
            return value.id === waf.alert_mail.mail_sender.id;
          });
          if (indexMailSender >= 0) {
            waf.alert_mail.mail_sender = this.mailSenders[indexMailSender];
          } else {
            waf.alert_mail.mail_sender = null;
          }
        }

        if (waf.alert_sms.sms_sender.id) {
          const indexSmsSender = this.smsSenders.findIndex((value) => {
            return value.id === waf.alert_sms.sms_sender.id;
          });
          if (indexSmsSender >= 0) {
            waf.alert_sms.sms_sender = this.smsSenders[indexSmsSender];
          } else {
            waf.alert_sms.sms_sender = null;
          }
        }

        waf.alert_sms_form.controls["barrier"].setValue(waf.alert_sms.barrier);
        waf.alert_sms_form.controls["time"].setValue(waf.alert_sms.time);
        waf.alert_sms_form.controls["sms_sender"].setValue(
          waf.alert_sms.sms_sender
        );
        waf.alert_sms_form.controls["account"].setValue(waf.alert_sms.account);
        waf.alert_sms_form.controls["state"].setValue(waf.alert_sms.state);

        waf.alert_mail_form.controls["barrier"].setValue(
          waf.alert_mail.barrier
        );
        waf.alert_mail_form.controls["time"].setValue(waf.alert_mail.time);
        waf.alert_mail_form.controls["mail_sender"].setValue(
          waf.alert_mail.mail_sender
        );
        waf.alert_mail_form.controls["account"].setValue(
          waf.alert_mail.account
        );
        waf.alert_mail_form.controls["state"].setValue(waf.alert_mail.state);

        // console.log(waf.alert_mail_form.value);
        // console.log(waf.alert_sms_form.value);
      } else {
        console.log("can not load alert");
      }
    });
  }

  openSite(itemChidren: any) {
    itemChidren.get("is_site").setValue(true);
  }

  loadPage(page: number) {
    if (page !== this.paging.previousPage) {
      this.paging.previousPage = page;
      this.loadAll();
    }
  }

  changeStatusException($event, itemChidren: any) {
    itemChidren.get("excep_status").setValue($event.target.checked ? 1 : 0);
  }

  changeStatus($event, waf: IWaf) {
    waf.distable_status = true;
    waf.waf_status = $event.target.checked ? 1 : 0;
    // console.log(waf.waf_status);
    this.wafService.changeStatus(waf).subscribe((res) => {
      if (res.status === 200) {
      } else {
        // reload
        waf.waf_status = $event.target.checked ? 0 : 1;
        console.log("can not change status");
      }
      waf.distable_status = false;
    });
  }

  changeStatusWeb() {}

  changeStatusRule($event, waf: IWaf, rule: IRuleWaf) {
    rule.distable_status = true;
    rule.status = $event.target.checked ? 1 : 0;
    this.wafService.changeStatusRule(waf, rule).subscribe((res) => {
      if (res.status === 200) {
      } else {
        // reload
        rule.status = $event.target.checked ? 0 : 1;
        console.log("can not change status in rule");
      }
      rule.distable_status = false;
    });
  }

  onEditAlert(alert) {
    alert.controls["is_update"].setValue(true);
  }

  saveAlert(saveModal, waf: IWaf, type: string) {
    this.modalService
      .open(saveModal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
      })
      .result.then((result) => {
        if (result === "save") {
          let param = waf.alert_sms_form.value;
          if (type != "sms") {
            param = waf.alert_mail_form.value;
          }
          this.wafService.updateAlert(waf.id, param).subscribe((res) => {
            if (res.status === 200) {
              this.loadAlert(waf);
            } else {
            }
          });
        }
      });
  }

  onCancelEditAlert(waf: IWaf, type: string) {
    if (type === "sms") {
      waf.alert_sms_form.controls["barrier"].setValue(waf.alert_sms.barrier);
      waf.alert_sms_form.controls["time"].setValue(waf.alert_sms.time);
      waf.alert_sms_form.controls["sms_sender"].setValue(
        waf.alert_sms.sms_sender
      );
      waf.alert_sms_form.controls["account"].setValue(waf.alert_sms.account);
      waf.alert_sms_form.controls["state"].setValue(waf.alert_sms.state);
      waf.alert_sms_form.controls["is_update"].setValue(false);
    } else {
      waf.alert_mail_form.controls["barrier"].setValue(waf.alert_mail.barrier);
      waf.alert_mail_form.controls["time"].setValue(waf.alert_mail.time);
      waf.alert_mail_form.controls["mail_sender"].setValue(
        waf.alert_mail.mail_sender
      );
      waf.alert_mail_form.controls["account"].setValue(waf.alert_mail.account);
      waf.alert_mail_form.controls["state"].setValue(waf.alert_mail.state);
      waf.alert_mail_form.controls["is_update"].setValue(false);
    }
  }

  showAccount(accouts: IAccount[]) {
    if (accouts == null || accouts.length <= 0) {
      return "";
    }
    console.log(accouts);
    return accouts
      .map((value) => {
        return value.name;
      })
      .join(", ");
  }

  changeStateAlert(waf: IWaf, alert_form) {
    const value = alert_form.controls["state"].value;
    alert_form.controls["state"].setValue(value === 1 ? 0 : 1);
  }

  // distableShowAlert(waf: IWaf): boolean {
  //   if (waf.id) {
  //     return this.wafs.findIndex(value => {
  //       return value.show_alert;
  //     }) > 0;
  //   } else {
  //     return false;
  //   }
  //
  // }
}
