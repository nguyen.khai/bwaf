import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '@app/shared';
import { WafComponent } from './waf.component';
import {WafRoutingModule} from "@app/views/waf/waf-routing.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {NgMultiSelectModule} from "@app/shared/ng-mutiselect";
import {CommonModule} from "@angular/common";
import {CollapseModule} from "ngx-bootstrap";


@NgModule({
  imports: [WafRoutingModule, TranslateModule, ReactiveFormsModule, SharedModule, CommonModule, CollapseModule,
    NgMultiSelectModule,
      FontAwesomeModule],
  declarations: [
    WafComponent
  ]
})
export class WafModule { }

