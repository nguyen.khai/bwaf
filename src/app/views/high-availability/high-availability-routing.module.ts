import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Paging} from '@app/shared/model/base-respone.model';
import {HighAvailabilityComponent} from './high-availability.component';

const routes: Routes = [
  {
    path: '',
    component: HighAvailabilityComponent,
    data: {
      title: 'High availability',
      pagingParams: new Paging()
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HighAvailabilityRoutingModule {}
