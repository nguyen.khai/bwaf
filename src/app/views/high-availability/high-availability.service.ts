import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';
import {createRequestOption} from 'app/shared';
import {IPaging} from '@app/shared/model/base-respone.model';
import {IHighAvailability, IHighAvailabilityMode} from '@app/shared/model/high-availability.model';


type EntityResponseType = HttpResponse<IHighAvailability>;
type ArrayResponseType = HttpResponse<IHighAvailabilityMode[]>;
type EntityResponse = HttpResponse<IHighAvailability>;
type EntityArrayResponseType = HttpResponse<PagingResponse>;
interface PagingResponse {
  data: IHighAvailability[];
  paging: IPaging;
}

@Injectable({ providedIn: 'root' })
export class HighAvailabilityService {
  public resourceUrl = environment.apiUrl + '/high-availability';
  public resourceUrlMode = environment.apiUrl + '/high-availability-mode';

  constructor(protected http: HttpClient) {}

  create(highAvailability: IHighAvailability): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(highAvailability);
    return this.http
      .post<IHighAvailability>(this.resourceUrl, copy, { observe: 'response' });
  }

  update(highAvailability: IHighAvailability): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(highAvailability);
    return this.http
      .put<IHighAvailability>(this.resourceUrl, copy, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IHighAvailability>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityResponse> {
    const options = createRequestOption(req);
    return this.http
      .get<IHighAvailability>(this.resourceUrl, { params: options, observe: 'response' });
  }

  getHighAvailabilityMode(req?: any): Observable<ArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IHighAvailabilityMode[]>(this.resourceUrlMode, { params: options, observe: 'response' });
  }

  delete(highAvailability: IHighAvailability): Observable<HttpResponse<any>> {
    return this.http.delete<any>(this.resourceUrl, { observe: 'response' });
  }

  protected convertDateFromClient(highAvailability: IHighAvailability): IHighAvailability {
    const copy: IHighAvailability = Object.assign({}, highAvailability, {});
    return copy;
  }
}
