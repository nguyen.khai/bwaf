import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HighAvailabilityService} from '@app/views/high-availability/high-availability.service';
import {HighAvailability, IHighAvailability, IHighAvailabilityMode} from '@app/shared/model/high-availability.model';
import {NetworkService} from '@app/views/network/network.service';
import {IInterface} from '@app/shared/model/interface.model';
import {IConfig} from '@app/shared/model/config.model';
import {VirtualInterface} from '@app/shared/model/virtual-interface.model';
import {map} from "rxjs/operators";
import {object} from "@amcharts/amcharts4/core";

@Component({
  selector: 'app-high-availability',
  templateUrl: 'high-availability.component.html',
})
export class HighAvailabilityComponent implements OnInit {
  highAvailabilities: IHighAvailability;
  config: IConfig;
  highAvailabilityModes: IHighAvailabilityMode[];
  networkInterfaces: IInterface[];
  isChecked: any;
  netWorkInterfaceMapping: any;
  editForm = this.fb.group({
    high_availability_status: [''],
    config: this.fb.group({
      operation_mode_id: [''],
      device_priority: [null, [Validators.required, Validators.max(250), Validators.min(1)]],
      group_name: [null, [Validators.required, Validators.maxLength(20), Validators.minLength(1)]],
      group_password: [null, [Validators.required, Validators.maxLength(20), Validators.minLength(1)]],
      heartbeat_interface_id: [''],
      heartbeat_netmask: [null, [Validators.required]],
      heartbeat_network: [null, [Validators.required]],
      is_show_password: [false],
    }),
    virtual_interface: this.fb.array([
      this.fb.group({
        name: [''],
        id: [''],
        virtual_ip_address: [''],
        is_enable: [''] ? 1 : 0,
        priority: ['']
      })
    ]),
  });

  constructor(
    public networkServiceService: NetworkService,
    protected notificationService: ToastrCustomService,
    public highAvailabilityService: HighAvailabilityService,
    protected router: Router,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
  }

  ngOnInit(): void {
    this.getValue();
  }

  getValue() {
    this.highAvailabilityService.query().subscribe(rs => {
      this.highAvailabilities = rs.body;
      if (this.highAvailabilities) {
        this.mappingData();
      }
      this.networkServiceService.query({}).subscribe(res => {
        this.networkInterfaces = res.body.data;
        this.netWorkInterfaceMapping = new Map(this.networkInterfaces.map(obj => [obj.name, obj.name]));
        if (this.netWorkInterfaceMapping && this.highAvailabilities) {
          this.virtualMapping();
        }
      });
    });
    this.highAvailabilityService.getHighAvailabilityMode().subscribe(res => {
      this.highAvailabilityModes = res.body;
    });
  }

  get getVirtualInterface(): FormArray {
    return <FormArray>this.editForm.get('virtual_interface');
  }

  mappingData() {
    this.editForm.patchValue({
      'high_availability_status': this.highAvailabilities.high_availability_status,
    });
    const operationModeId = this.editForm.get('config');
    operationModeId.patchValue({
      'operation_mode_id': this.highAvailabilities.config.operation_mode_id,
      'device_priority': this.highAvailabilities.config.device_priority,
      'group_name': this.highAvailabilities.config.group_name,
      'group_password': this.highAvailabilities.config.group_password,
      'heartbeat_interface_id': this.highAvailabilities.config.heartbeat_interface_id,
      'heartbeat_network': this.highAvailabilities.config.heartbeat_network,
      'heartbeat_netmask': this.highAvailabilities.config.heartbeat_netmask
    });
  }

  private virtualMapping() {
    const virtualInterface = <FormArray>this.editForm.get('virtual_interface');
    virtualInterface.clear();
    this.highAvailabilities.virtual_interface.forEach(value => {
      value.name = this.netWorkInterfaceMapping.get(value.id);
      virtualInterface.push(this.fb.group(value));
    });
    this.netWorkInterfaceMapping.forEach((value, key) => {
      // some: ham check trung id cua Virtual_interface voi key
      if (!this.highAvailabilities.virtual_interface.some((virtual) => virtual.id === key)) {
        const virtual = new VirtualInterface();
        virtual.id = key;
        virtual.name = value;
        virtual.virtual_ip_address = null;
        virtual.is_enable = 0;
        virtual.priority = null;
        virtualInterface.push(this.fb.group(virtual));
      }
    });
  }

  trackBankBrandById(index: number, item: IHighAvailabilityMode) {
    return item.id;
  }

  checkValue(event: any) {
    // console.log(event);
  }

  save() {
    this.highAvailabilityService.update(this.editForm.value).subscribe(res => {
    });
  }

  delete(modal) {
    if (this.isChecked === false) {
      this.modalService.open(modal, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
        if (result === 'delete') {
          if (this.highAvailabilities) {
            this.highAvailabilityService.delete(this.highAvailabilities).subscribe(res => {
            });
          } else {
            this.notificationService.error('Thao tác không hợp kệ');
          }
        }
        if (result === 'Close') {

        }
      }, (reason) => {});
    }
  }

  onchangeValue() {
    this.isChecked = true;
  }
}
