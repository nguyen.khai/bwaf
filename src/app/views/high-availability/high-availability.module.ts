import { NgModule } from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '@app/shared';
import {HighAvailabilityRoutingModule} from '@app/views/high-availability/high-availability-routing.module';
import {HighAvailabilityComponent} from '@app/views/high-availability/high-availability.component';



@NgModule({
  imports: [HighAvailabilityRoutingModule, TranslateModule, ReactiveFormsModule, SharedModule],
  declarations: [
   HighAvailabilityComponent
  ]
})

export class HighAvailabilityModule { }
