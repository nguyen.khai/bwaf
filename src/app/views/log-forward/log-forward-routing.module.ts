import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SysLogComponent} from "@app/views/log-forward/sys-log/sys-log.component";
import {SnmpComponent} from "@app/views/log-forward/snmp/snmp.component";


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Log Forward'
    },
    children: [
      {
        path: '',
        redirectTo: 'sys-log'
      },
      {
        path: 'sys-log',
        component: SysLogComponent,
        data: {
          title: 'Sys Log',
          pagingParams: {}
        }
      },
      {
        path: 'snmp',
        component: SnmpComponent,
        data: {
          title: 'SNMP',
          pagingParams: {}
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LogForwardRoutingModule { }
