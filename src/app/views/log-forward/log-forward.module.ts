import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogForwardRoutingModule } from './log-forward-routing.module';
import { SysLogComponent } from './sys-log/sys-log.component';
import { SnmpComponent } from './snmp/snmp.component';
import {TranslateModule} from "@ngx-translate/core";
import {ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [SysLogComponent, SnmpComponent],
  imports: [
    CommonModule,
    LogForwardRoutingModule,
    TranslateModule,
    ReactiveFormsModule
  ]
})
export class LogForwardModule { }
