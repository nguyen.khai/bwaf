import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";

export interface SnmpV1v2 {
  name: string;
  queries: {
    v1: number;
    v2: number;
  };
  traps: {
    v1: number;
    v2: number;
  };
  hosts: number;
  events: number;
  status: number;
}

export interface SnmpV3 {
  name: string;
  queries: number;
  security_level: {
    authentication: number;
    private: number;
  };
  traps: number;
  hosts: number;
  events: number;
  status: number;
}

export interface SnmpSetting {
  config_status: number;
  location: string;
  description: string;
  contact_info: string;
  snmp: SnmpV1v2[];
  snmp_v3: SnmpV3[];
}

export interface SnmpEvent {
  [key: string]: {
    name: string;
    description: string;
  };
}

export interface SnmpHost {
  ip_address: string;
  netmask: number;
  accept_queries: number;
  send_traps: number;
}

export interface SnmpCommunity {
  enable: number;
  community_name: string;
  hosts: SnmpHost[];
  queries: {
    v1_enabled: number;
    v1_port: string;
    v2_enabled: number;
    v2_port: string;
  };
  traps: {
    v1_enabled: number;
    v1_local_port: string;
    v1_remote_port: string;
    v2_enabled: number;
    v2_local_port: string;
    v2_remote_port: string;
  };
  snmp_events: string[];
}

export interface SnmpCommunityV3 {
  enable: number;
  community_name: string;
  hosts: SnmpHost[];
  queries: {
    enabled: number;
    port: string;
  };
  traps: {
    enabled: number;
    local_port: string;
    remote_port: string;
  };
  security_level: {
    authentication_algorithm: string;
    authentication_password: string;
    private_protocol: string;
    private_password: string;
  };
  snmp_events: string[];
}

export interface SnmpCommunityConfig {
  config_status: number;
  contact_info: string;
  description: string;
  location: string;
}

@Injectable({
  providedIn: "root",
})
export class SnmpService {
  public resourceUrl = environment.apiUrl;

  constructor(protected http: HttpClient) {}

  getSnmpSetting(): Observable<HttpResponse<SnmpSetting>> {
    return this.http.get<SnmpSetting>(`${this.resourceUrl}/snmp`, {
      observe: "response",
    });
  }

  updateSnmpSetting(body: SnmpCommunityConfig): Observable<HttpResponse<any>> {
    return this.http.post<any>(`${this.resourceUrl}/snmp`, body, {
      observe: "response",
    });
  }

  getSnmpEvents(): Observable<HttpResponse<SnmpEvent>> {
    return this.http.options<SnmpEvent>(`${this.resourceUrl}/snmp-events`, {
      observe: "response",
    });
  }

  addSnmpCommunity(
    body: SnmpCommunity
  ): Observable<HttpResponse<{ data: SnmpCommunity }>> {
    return this.http.post<{ data: SnmpCommunity }>(
      `${this.resourceUrl}/snmp-community`,
      body,
      { observe: "response" }
    );
  }

  addSnmpv3Community(
    body: SnmpCommunityV3
  ): Observable<HttpResponse<{ data: SnmpCommunityV3 }>> {
    return this.http.post<{ data: SnmpCommunityV3 }>(
      `${this.resourceUrl}/snmp-community-v3`,
      body,
      { observe: "response" }
    );
  }

  getSnmpCommunityDetail(
    snmpCommunityName: string
  ): Observable<HttpResponse<SnmpCommunity>> {
    return this.http.get<SnmpCommunity>(
      `${this.resourceUrl}/snmp-community/${snmpCommunityName}`,
      { observe: "response" }
    );
  }

  getSnmpCommunityV3Detail(
    snmpCommunityName: string
  ): Observable<HttpResponse<SnmpCommunityV3>> {
    return this.http.get<SnmpCommunityV3>(
      `${this.resourceUrl}/snmp-community-v3/${snmpCommunityName}`,
      { observe: "response" }
    );
  }

  updateSnmpCommunity(
    body: SnmpCommunity,
    name: string
  ): Observable<HttpResponse<{ data: SnmpCommunity }>> {
    return this.http.put<{ data: SnmpCommunity }>(
      `${this.resourceUrl}/snmp-community/${name}`,
      body,
      { observe: "response" }
    );
  }

  updateSnmpV3Community(
    body: SnmpCommunityV3,
    name: string
  ): Observable<HttpResponse<{ data: SnmpCommunityV3 }>> {
    return this.http.put<{ data: SnmpCommunityV3 }>(
      `${this.resourceUrl}/snmp-community-v3/${name}`,
      body,
      { observe: "response" }
    );
  }

  deleteSnmpCommunity(name: string): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/snmp-community/${name}`, {
      observe: "response",
    });
  }

  deleteSnmpCommunityV3(name: string): Observable<HttpResponse<any>> {
    return this.http.delete<any>(
      `${this.resourceUrl}/snmp-community-v3/${name}`,
      { observe: "response" }
    );
  }
}
