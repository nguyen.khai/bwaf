import { TestBed } from '@angular/core/testing';

import { SnmpService } from './snmp.service';

describe('SnmpService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SnmpService = TestBed.get(SnmpService);
    expect(service).toBeTruthy();
  });
});
