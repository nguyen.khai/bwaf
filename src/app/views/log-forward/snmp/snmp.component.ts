import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from "@angular/forms";
import {
  SnmpCommunity,
  SnmpCommunityV3,
  SnmpCommunityConfig,
  SnmpHost,
  SnmpService,
  SnmpV1v2,
  SnmpV3,
} from "@app/views/log-forward/snmp/snmp.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-snmp",
  templateUrl: "./snmp.component.html",
  styleUrls: ["./snmp.component.scss"],
})
export class SnmpComponent implements OnInit {
  AUTHEN_ALGOS = [
    {
      label: "MD5",
      value: "md5",
    },
    {
      label: "SHA",
      value: "sha",
    },
  ];

  PRIVATE_PROTOCOLS = [
    {
      label: "AES",
      value: "aes",
    },
    {
      label: "DES",
      value: "des",
    },
    {
      label: "AES256",
      value: "aes256",
    },
  ];

  isEditSnmpCommunity = false;
  selectedSnmpCommunity = "";

  snmpSetting = this.setSnmpSetting({
    config_status: false,
    description: "",
    location: "",
    contact_info: "",
    snmp: [],
    snmp_v3: [],
  });

  addSnmpV1v2FormGroup = this.setSnmpFormData({
    community_name: "",
    enable: true,
    hosts: [],
    v1_enabled: false,
    v1_port: "",
    v2_enabled: false,
    v2_port: "",
    v1_local_port: "",
    v1_traps_enabled: false,
    v2_local_port: "",
    v2_traps_enabled: false,
    v1_remote_port: "",
    v2_remote_port: "",
    snmp_events: [],
  });

  addSnmpV3FormGroup = this.setSnmpCommunityV3FormData({
    enable: true,
    community_name: "",
    hosts: [],
    snmp_events: [],
    port: "",
    queries_enabled: false,
    traps_enabled: false,
    local_port: "",
    remote_port: "",
    authentication_algorithm: "md5",
    authentication_enabled: false,
    authentication_password: "",
    private_enabled: false,
    private_protocol: "aes",
    private_password: "",
  });

  addSnmpV1v2HostFormGroup = this.setSnmpHostFormData({
    accept_queries: false,
    ip_address: "",
    netmask: 0,
    send_traps: false,
  });

  snmpEvents: { value: string; label: string }[] = [];

  constructor(
    private fb: FormBuilder,
    public snmpService: SnmpService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.getSnmpSetting();
    this.getSnmpEvents();
  }

  checkForm(event) {
    if (!event.target.checked) {
      this.snmpSetting.get("description").disable();
      // this.snmpSetting.get('port').disable();
      // this.snmpSetting.get('protocol').disable();
      // this.snmpSetting.get('facility').disable();
      // this.snmpSetting.get('severity_level').disable();
      // this.snmpSetting.get('log_type').disable();
    } else {
      this.snmpSetting.get("description").enable();
      // this.snmpSetting.get('port').enable();
      // this.snmpSetting.get('protocol').enable();
      // this.snmpSetting.get('facility').enable();
      // this.snmpSetting.get('severity_level').enable();
      // this.snmpSetting.get('log_type').enable();
    }
  }

  setSnmpSetting({
    config_status,
    description,
    location,
    contact_info,
    snmp,
    snmp_v3,
  }: {
    config_status: boolean;
    description: string;
    location: string;
    contact_info: string;
    snmp: SnmpV1v2[];
    snmp_v3: SnmpV3[];
  }) {
    return this.fb.group({
      config_status: [config_status],
      description: [description],
      location: [location],
      contact_info: [contact_info],
      snmp: [snmp],
      snmp_v3: [snmp_v3],
    });
  }

  setSnmpFormData({
    community_name,
    enable,
    hosts,
    v1_enabled,
    v1_port,
    v2_enabled,
    v2_port,
    v1_traps_enabled,
    v1_local_port,
    v1_remote_port,
    v2_traps_enabled,
    v2_remote_port,
    v2_local_port,
    snmp_events,
  }: {
    community_name: string;
    enable: boolean;
    hosts: SnmpHost[];
    v1_enabled: boolean;
    v1_port: string;
    v2_enabled: boolean;
    v2_port: string;
    v1_traps_enabled: boolean;
    v1_local_port: string;
    v1_remote_port: string;
    v2_traps_enabled: boolean;
    v2_remote_port: string;
    v2_local_port: string;
    snmp_events: string[];
  }) {
    return this.fb.group({
      community_name: [community_name],
      enable: [enable],
      hosts: [hosts],
      v1_enabled: [v1_enabled],
      v1_port: [v1_port],
      v2_enabled: [v2_enabled],
      v2_port: [v2_port],
      v1_traps_enabled: [v1_traps_enabled],
      v1_local_port: [v1_local_port],
      v2_traps_enabled: [v2_traps_enabled],
      v2_local_port: [v2_local_port],
      v1_remote_port: [v1_remote_port],
      v2_remote_port: [v2_remote_port],
      snmp_events: [snmp_events],
    });
  }

  setSnmpCommunityV3FormData({
    community_name,
    enable,
    hosts,
    snmp_events,
    port,
    queries_enabled,
    traps_enabled,
    local_port,
    remote_port,
    authentication_enabled,
    authentication_algorithm,
    authentication_password,
    private_enabled,
    private_protocol,
    private_password,
  }: {
    community_name: string;
    enable: boolean;
    hosts: SnmpHost[];
    snmp_events: string[];
    port: string;
    queries_enabled: boolean;
    traps_enabled: boolean;
    local_port: string;
    remote_port: string;
    authentication_enabled: boolean;
    authentication_algorithm: string;
    authentication_password: string;
    private_enabled: boolean;
    private_protocol: string;
    private_password: string;
  }) {
    return this.fb.group({
      community_name: [community_name],
      enable: [enable],
      hosts: [hosts],
      snmp_events: [snmp_events],
      port: [port],
      queries_enabled: [queries_enabled],
      traps_enabled: [traps_enabled],
      local_port: [local_port],
      remote_port: [remote_port],
      authentication_enabled: [authentication_enabled],
      authentication_algorithm: [authentication_algorithm],
      authentication_password: [authentication_password],
      private_enabled: [private_enabled],
      private_protocol: [private_protocol],
      private_password: [private_password],
    });
  }

  setSnmpHostFormData({
    ip_address,
    netmask,
    accept_queries,
    send_traps,
  }: {
    ip_address: string;
    netmask: number;
    accept_queries: boolean;
    send_traps: boolean;
  }) {
    return this.fb.group({
      ip_address: [ip_address, [this.validateIpv4()]],
      netmask: [netmask, [Validators.min(0), Validators.max(32)]],
      accept_queries: [accept_queries],
      send_traps: [send_traps],
    });
  }

  getSnmpSetting() {
    this.snmpService.getSnmpSetting().subscribe((res) => {
      this.snmpSetting = this.setSnmpSetting({
        config_status: !!res.body.config_status,
        location: res.body.location || "",
        snmp: res.body.snmp || [],
        contact_info: res.body.contact_info || "",
        description: res.body.description || "",
        snmp_v3: res.body.snmp_v3 || [],
      });
    });
  }

  getSnmpEvents() {
    this.snmpService.getSnmpEvents().subscribe((res) => {
      const snmpEvents = [];
      Object.keys(res.body).forEach((key) => {
        if (key !== "status") {
          snmpEvents.push({
            value: key,
            label: res.body[key].name,
          });
        }
      });

      this.snmpEvents = snmpEvents;
    });
  }

  openAddSnmpv1v2Modal(modal) {
    this.isEditSnmpCommunity = false;
    this.addSnmpV1v2FormGroup = this.setSnmpFormData({
      community_name: "",
      enable: true,
      hosts: [],
      v1_enabled: false,
      v1_port: "",
      v2_enabled: false,
      v2_port: "",
      v1_local_port: "",
      v1_traps_enabled: false,
      v2_local_port: "",
      v2_traps_enabled: false,
      v1_remote_port: "",
      v2_remote_port: "",
      snmp_events: [],
    });
    this.addSnmpV1v2FormGroup.get("v1_port").disable();
    this.addSnmpV1v2FormGroup.get("v2_port").disable();
    this.addSnmpV1v2FormGroup.get("v1_local_port").disable();
    this.addSnmpV1v2FormGroup.get("v2_local_port").disable();
    this.addSnmpV1v2FormGroup.get("v1_remote_port").disable();
    this.addSnmpV1v2FormGroup.get("v2_remote_port").disable();
    this.addSnmpV1v2FormGroup.get("snmp_events").disable();
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "xl",
        backdrop: "static",
        centered: true,
      })
      .result.then(
        (result) => {
          // this.addVirtualNetworkFormGroup.get('is_update').setValue(false);
          // if (result === 'Save') {
          //
          // }
        },
        () => {
          // this.addVirtualNetworkFormGroup.get('is_update').setValue(false);
        }
      );
  }

  openAddSnmpv3Modal(modal) {
    this.isEditSnmpCommunity = false;
    this.addSnmpV3FormGroup = this.setSnmpCommunityV3FormData({
      enable: true,
      community_name: "",
      hosts: [],
      snmp_events: [],
      port: "",
      queries_enabled: false,
      traps_enabled: false,
      local_port: "",
      remote_port: "",
      authentication_algorithm: "md5",
      authentication_enabled: false,
      authentication_password: "",
      private_enabled: false,
      private_protocol: "aes",
      private_password: "",
    });
    this.addSnmpV3FormGroup.get("port").disable();
    this.addSnmpV3FormGroup.get("local_port").disable();
    this.addSnmpV3FormGroup.get("remote_port").disable();
    this.addSnmpV3FormGroup.get("authentication_algorithm").disable();
    this.addSnmpV3FormGroup.get("authentication_password").disable();
    this.addSnmpV3FormGroup.get("private_protocol").disable();
    this.addSnmpV3FormGroup.get("private_password").disable();
    this.modalService.open(modal, {
      ariaLabelledBy: "modal-basic-title",
      size: "xl",
      backdrop: "static",
      centered: true,
    });
  }

  openAddSnmpHost(modal) {
    this.addSnmpV1v2HostFormGroup = this.setSnmpHostFormData({
      accept_queries: false,
      ip_address: "",
      netmask: 0,
      send_traps: false,
    });
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
        centered: true,
      })
      .result.then(
        (result) => {
          if (result === "Save") {
            this.addSnmpV1v2FormGroup.get("hosts").value.push({
              accept_queries: this.addSnmpV1v2HostFormGroup.get(
                "accept_queries"
              ).value
                ? 1
                : 0,
              ip_address: this.addSnmpV1v2HostFormGroup.get("ip_address").value,
              netmask: this.addSnmpV1v2HostFormGroup.get("netmask").value
                ? Number(this.addSnmpV1v2HostFormGroup.get("netmask").value)
                : 0,
              send_traps: this.addSnmpV1v2HostFormGroup.get("send_traps").value
                ? 1
                : 0,
            });
            this.addSnmpV3FormGroup.get("hosts").value.push({
              accept_queries: this.addSnmpV1v2HostFormGroup.get(
                "accept_queries"
              ).value
                ? 1
                : 0,
              ip_address: this.addSnmpV1v2HostFormGroup.get("ip_address").value,
              netmask: this.addSnmpV1v2HostFormGroup.get("netmask").value
                ? Number(this.addSnmpV1v2HostFormGroup.get("netmask").value)
                : 0,
              send_traps: this.addSnmpV1v2HostFormGroup.get("send_traps").value
                ? 1
                : 0,
            });
          }
        },
        () => {}
      );
  }

  openEditSnmpHost(modal, data, dataIdx) {
    this.addSnmpV1v2HostFormGroup = this.setSnmpHostFormData({
      accept_queries: !!data.accept_queries,
      ip_address: data.ip_address,
      netmask: data.netmask,
      send_traps: !!data.send_traps,
    });
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
        centered: true,
      })
      .result.then(
        (result) => {
          if (result === "Save") {
            this.addSnmpV1v2FormGroup.get("hosts").value[dataIdx] = {
              accept_queries: this.addSnmpV1v2HostFormGroup.get(
                "accept_queries"
              ).value
                ? 1
                : 0,
              ip_address: this.addSnmpV1v2HostFormGroup.get("ip_address").value,
              netmask: this.addSnmpV1v2HostFormGroup.get("netmask").value,
              send_traps: this.addSnmpV1v2HostFormGroup.get("send_traps").value
                ? 1
                : 0,
            };
          }
        },
        () => {}
      );
  }

  deleteSnmpHost(modal, dataIdx) {
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
        centered: true,
      })
      .result.then(
        (result) => {
          if (result === "Delete") {
            this.addSnmpV1v2FormGroup.get("hosts").value.splice(dataIdx, 1);
            this.addSnmpV3FormGroup.get("hosts").value.splice(dataIdx, 1);
          }
        },
        () => {}
      );
  }

  deleteSnmpCommunity(modal, snmpCommunityName) {
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
        centered: true,
      })
      .result.then(
        (result) => {
          if (result === "Delete") {
            this.snmpService
              .deleteSnmpCommunity(snmpCommunityName)
              .subscribe((res) => {
                console.log(res);
                this.getSnmpSetting();
              });
          }
        },
        () => {}
      );
  }

  deleteSnmpCommunityV3(modal, snmpCommunityName) {
    this.modalService
      .open(modal, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        backdrop: "static",
        centered: true,
      })
      .result.then(
        (result) => {
          if (result === "Delete") {
            this.snmpService
              .deleteSnmpCommunityV3(snmpCommunityName)
              .subscribe((res) => {
                console.log(res);
                this.getSnmpSetting();
              });
          }
        },
        () => {}
      );
  }

  validateIpv4(): ValidatorFn {
    const ipv4RegEx =
      /(^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\.(?!$)|$)){4})/;
    return (control: AbstractControl): ValidationErrors | null => {
      let invalidIp = false;
      const ipv4 = control.value;
      if (ipv4) {
        invalidIp = !ipv4RegEx.test(ipv4);
      }

      return invalidIp ? { invalidIpv4: { value: control.value } } : null;
    };
  }

  changeSnmpEvent(
    event,
    snmp_event: { label: string; value: string },
    field: string
  ) {
    if (event.target.checked) {
      this[field].get("snmp_events").value.push(snmp_event.value);
    } else {
      const idx = this[field]
        .get("snmp_events")
        .value.findIndex((e) => e === snmp_event.value);
      if (idx > -1) {
        this[field].get("snmp_events").value.splice(idx, 1);
      }
    }
  }

  changeCommunityActiveStatus(event) {
    if (event.target.checked) {
      this.addSnmpV1v2FormGroup.get("hosts").enable();
      this.addSnmpV1v2FormGroup.get("v1_enabled").enable();
      if (this.addSnmpV1v2FormGroup.get("v1_enabled").value) {
        this.addSnmpV1v2FormGroup.get("v1_port").enable();
      }

      this.addSnmpV1v2FormGroup.get("v2_enabled").enable();
      if (this.addSnmpV1v2FormGroup.get("v2_enabled").value) {
        this.addSnmpV1v2FormGroup.get("v2_port").enable();
      }

      this.addSnmpV1v2FormGroup.get("v1_traps_enabled").enable();
      if (this.addSnmpV1v2FormGroup.get("v1_traps_enabled").value) {
        this.addSnmpV1v2FormGroup.get("v1_local_port").enable();
        this.addSnmpV1v2FormGroup.get("v1_remote_port").enable();
      }

      this.addSnmpV1v2FormGroup.get("v2_traps_enabled").enable();
      if (this.addSnmpV1v2FormGroup.get("v2_traps_enabled").value) {
        this.addSnmpV1v2FormGroup.get("v2_local_port").enable();
        this.addSnmpV1v2FormGroup.get("v2_remote_port").enable();
      }
      this.addSnmpV1v2FormGroup.get("snmp_events").enable();
    } else {
      this.addSnmpV1v2FormGroup.get("hosts").disable();
      this.addSnmpV1v2FormGroup.get("v1_enabled").disable();
      this.addSnmpV1v2FormGroup.get("v1_port").disable();
      this.addSnmpV1v2FormGroup.get("v2_enabled").disable();
      this.addSnmpV1v2FormGroup.get("v2_port").disable();
      this.addSnmpV1v2FormGroup.get("v1_traps_enabled").disable();
      this.addSnmpV1v2FormGroup.get("v1_local_port").disable();
      this.addSnmpV1v2FormGroup.get("v2_traps_enabled").disable();
      this.addSnmpV1v2FormGroup.get("v2_local_port").disable();
      this.addSnmpV1v2FormGroup.get("v1_remote_port").disable();
      this.addSnmpV1v2FormGroup.get("v2_remote_port").disable();
      this.addSnmpV1v2FormGroup.get("snmp_events").disable();
    }
  }

  changeQueriesv1ActiveStatus(event) {
    if (event.target.checked) {
      this.addSnmpV1v2FormGroup.get("v1_port").enable();
    } else {
      this.addSnmpV1v2FormGroup.get("v1_port").disable();
    }
  }

  changeQueriesActiveStatus(event) {
    if (event.target.checked) {
      this.addSnmpV3FormGroup.get("port").enable();
    } else {
      this.addSnmpV3FormGroup.get("port").disable();
    }
  }

  changeQueriesv2ActiveStatus(event) {
    if (event.target.checked) {
      this.addSnmpV1v2FormGroup.get("v2_port").enable();
    } else {
      this.addSnmpV1v2FormGroup.get("v2_port").disable();
    }
  }

  changeTrapsv1ActiveStatus(event) {
    if (event.target.checked) {
      this.addSnmpV1v2FormGroup.get("v1_local_port").enable();
      this.addSnmpV1v2FormGroup.get("v1_remote_port").enable();
    } else {
      this.addSnmpV1v2FormGroup.get("v1_local_port").disable();
      this.addSnmpV1v2FormGroup.get("v1_remote_port").disable();
    }
  }

  changeTrapsActiveStatus(event) {
    if (event.target.checked) {
      this.addSnmpV3FormGroup.get("local_port").enable();
      this.addSnmpV3FormGroup.get("remote_port").enable();
    } else {
      this.addSnmpV3FormGroup.get("local_port").disable();
      this.addSnmpV3FormGroup.get("remote_port").disable();
    }
  }

  changeTrapsv2ActiveStatus(event) {
    if (event.target.checked) {
      this.addSnmpV1v2FormGroup.get("v2_local_port").enable();
      this.addSnmpV1v2FormGroup.get("v2_remote_port").enable();
    } else {
      this.addSnmpV1v2FormGroup.get("v2_local_port").disable();
      this.addSnmpV1v2FormGroup.get("v2_remote_port").disable();
    }
  }

  saveSnmpSetting(modal) {
    let hasError = false;

    if (!this.addSnmpV1v2FormGroup.get("community_name").value) {
      hasError = true;
    }

    if (this.addSnmpV1v2FormGroup.get("enable").value) {
      if (
        this.addSnmpV1v2FormGroup.get("v1_enabled").value &&
        !this.addSnmpV1v2FormGroup.get("v1_port").value
      ) {
        hasError = true;
      }
      if (
        this.addSnmpV1v2FormGroup.get("v2_enabled").value &&
        !this.addSnmpV1v2FormGroup.get("v2_port").value
      ) {
        hasError = true;
      }
      if (this.addSnmpV1v2FormGroup.get("v1_local_port").value) {
        if (this.addSnmpV1v2FormGroup.get("v1_local_port").value) {
          hasError = true;
        }
        if (this.addSnmpV1v2FormGroup.get("v1_remote_port").value) {
          hasError = true;
        }
      }
      if (this.addSnmpV1v2FormGroup.get("v2_local_port").value) {
        if (this.addSnmpV1v2FormGroup.get("v2_local_port").value) {
          hasError = true;
        }
        if (this.addSnmpV1v2FormGroup.get("v2_remote_port").value) {
          hasError = true;
        }
      }
    }

    if (!hasError) {
      if (this.isEditSnmpCommunity) {
        const data: SnmpCommunity = {
          community_name: this.addSnmpV1v2FormGroup.get("community_name").value
            ? this.addSnmpV1v2FormGroup.get("community_name").value.trim()
            : "",
          enable: this.addSnmpV1v2FormGroup.get("enable").value ? 1 : 0,
          hosts: this.addSnmpV1v2FormGroup.get("hosts").value || [],
          queries: {
            v1_enabled: this.addSnmpV1v2FormGroup.get("v1_enabled").value
              ? 1
              : 0,
            v1_port: this.addSnmpV1v2FormGroup.get("v1_port").value || "",
            v2_enabled: this.addSnmpV1v2FormGroup.get("v2_enabled").value
              ? 1
              : 0,
            v2_port: this.addSnmpV1v2FormGroup.get("v2_port").value || "",
          },
          snmp_events: this.addSnmpV1v2FormGroup.get("snmp_events").value || [],
          traps: {
            v1_enabled: this.addSnmpV1v2FormGroup.get("v1_traps_enabled").value
              ? 1
              : 0,
            v1_local_port:
              this.addSnmpV1v2FormGroup.get("v1_local_port").value || "",
            v1_remote_port:
              this.addSnmpV1v2FormGroup.get("v1_remote_port").value || "",
            v2_enabled: this.addSnmpV1v2FormGroup.get("v2_traps_enabled").value
              ? 1
              : 0,
            v2_local_port:
              this.addSnmpV1v2FormGroup.get("v2_local_port").value || "",
            v2_remote_port:
              this.addSnmpV1v2FormGroup.get("v2_remote_port").value || "",
          },
        };
        this.snmpService
          .updateSnmpCommunity(data, this.selectedSnmpCommunity)
          .subscribe((res) => {
            console.log(res);
            modal.close("Save");
            this.getSnmpSetting();
          });
      }
    }

    if (!this.addSnmpV1v2FormGroup.invalid) {
      const data: SnmpCommunity = {
        community_name: this.addSnmpV1v2FormGroup.get("community_name").value
          ? this.addSnmpV1v2FormGroup.get("community_name").value.trim()
          : "",
        enable: this.addSnmpV1v2FormGroup.get("enable").value ? 1 : 0,
        hosts: this.addSnmpV1v2FormGroup.get("hosts").value || [],
        queries: {
          v1_enabled: this.addSnmpV1v2FormGroup.get("v1_enabled").value ? 1 : 0,
          v1_port: this.addSnmpV1v2FormGroup.get("v1_port").value || "",
          v2_enabled: this.addSnmpV1v2FormGroup.get("v2_enabled").value ? 1 : 0,
          v2_port: this.addSnmpV1v2FormGroup.get("v2_port").value || "",
        },
        snmp_events: this.addSnmpV1v2FormGroup.get("snmp_events").value || [],
        traps: {
          v1_enabled: this.addSnmpV1v2FormGroup.get("v1_traps_enabled").value
            ? 1
            : 0,
          v1_local_port:
            this.addSnmpV1v2FormGroup.get("v1_local_port").value || "",
          v1_remote_port:
            this.addSnmpV1v2FormGroup.get("v1_remote_port").value || "",
          v2_enabled: this.addSnmpV1v2FormGroup.get("v2_traps_enabled").value
            ? 1
            : 0,
          v2_local_port:
            this.addSnmpV1v2FormGroup.get("v2_local_port").value || "",
          v2_remote_port:
            this.addSnmpV1v2FormGroup.get("v2_remote_port").value || "",
        },
      };

      if (!this.isEditSnmpCommunity) {
        this.snmpService.addSnmpCommunity(data).subscribe((res) => {
          console.log(res);
          modal.close("Save");
          this.getSnmpSetting();
        });
      }
    } else {
      this.addSnmpV1v2FormGroup.get("community_name").markAsTouched();
      this.addSnmpV1v2FormGroup.get("hosts").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v1_enabled").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v1_port").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v2_enabled").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v2_port").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v1_traps_enabled").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v1_local_port").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v2_traps_enabled").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v2_local_port").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v1_remote_port").markAsTouched();
      this.addSnmpV1v2FormGroup.get("v2_remote_port").markAsTouched();
      this.addSnmpV1v2FormGroup.get("snmp_events").markAsTouched();
    }
  }

  saveSnmpV3Setting(modal) {
    const data: SnmpCommunityV3 = {
      community_name: this.addSnmpV3FormGroup.get("community_name").value
        ? this.addSnmpV3FormGroup.get("community_name").value.trim()
        : "",
      enable: this.addSnmpV3FormGroup.get("enable").value ? 1 : 0,
      hosts: this.addSnmpV3FormGroup.get("hosts").value || [],
      queries: {
        enabled: this.addSnmpV3FormGroup.get("queries_enabled").value ? 1 : 0,
        port: this.addSnmpV3FormGroup.get("port").value || "",
      },
      snmp_events: this.addSnmpV3FormGroup.get("snmp_events").value || [],
      traps: {
        enabled: this.addSnmpV3FormGroup.get("traps_enabled").value ? 1 : 0,
        local_port: this.addSnmpV3FormGroup.get("local_port").value || "",
        remote_port: this.addSnmpV3FormGroup.get("remote_port").value || "",
      },
      security_level: {
        authentication_algorithm: this.addSnmpV3FormGroup.get(
          "authentication_algorithm"
        ).value,
        authentication_password: this.addSnmpV3FormGroup.get(
          "authentication_password"
        ).value,
        private_password: this.addSnmpV3FormGroup.get("private_password").value,
        private_protocol: this.addSnmpV3FormGroup.get("private_protocol").value,
      },
    };

    let hasError = false;

    if (
      this.addSnmpV3FormGroup.get("authentication_enabled").value &&
      !this.addSnmpV3FormGroup.get("authentication_password").value
    ) {
      hasError = true;
    }
    if (
      this.addSnmpV3FormGroup.get("queries_enabled").value &&
      !this.addSnmpV3FormGroup.get("port").value
    ) {
      hasError = true;
    }
    if (this.addSnmpV3FormGroup.get("traps_enabled").value) {
      if (!this.addSnmpV3FormGroup.get("local_port").value) {
        hasError = true;
      }
      if (!this.addSnmpV3FormGroup.get("remote_port").value) {
        hasError = true;
      }
    }

    if (!hasError && this.isEditSnmpCommunity) {
      this.snmpService
        .updateSnmpV3Community(data, this.selectedSnmpCommunity)
        .subscribe((res) => {
          console.log(res);
          modal.close("Save");
          this.getSnmpSetting();
        });
    }

    if (!this.addSnmpV3FormGroup.invalid) {
      if (!this.isEditSnmpCommunity) {
        this.snmpService.addSnmpv3Community(data).subscribe((res) => {
          console.log(res);
          modal.close("Save");
          this.getSnmpSetting();
        });
      }
    } else {
      this.addSnmpV3FormGroup.get("hosts").markAsTouched();
      this.addSnmpV3FormGroup.get("port").markAsTouched();
      this.addSnmpV3FormGroup.get("local_port").markAsTouched();
      this.addSnmpV3FormGroup.get("remote_port").markAsTouched();
      this.addSnmpV3FormGroup.get("authentication_algorithm").markAsTouched();
      this.addSnmpV3FormGroup.get("authentication_password").markAsTouched();
      this.addSnmpV3FormGroup.get("private_protocol").markAsTouched();
      this.addSnmpV3FormGroup.get("private_password").markAsTouched();
      this.addSnmpV3FormGroup.get("snmp_events").markAsTouched();
    }
  }

  openEditSnmpCommunityEdit(modal, snmpCommunityName: string) {
    this.selectedSnmpCommunity = snmpCommunityName;
    this.snmpService
      .getSnmpCommunityDetail(snmpCommunityName)
      .subscribe((res) => {
        this.modalService.open(modal, {
          ariaLabelledBy: "modal-basic-title",
          size: "xl",
          backdrop: "static",
          centered: true,
        });

        this.isEditSnmpCommunity = true;
        this.addSnmpV1v2FormGroup = this.setSnmpFormData({
          snmp_events: res.body.snmp_events,
          v2_remote_port: res.body.traps.v2_remote_port,
          v1_remote_port: res.body.traps.v1_remote_port,
          v2_traps_enabled: !!res.body.traps.v2_enabled,
          v2_local_port: res.body.traps.v2_local_port,
          v1_traps_enabled: !!res.body.traps.v1_enabled,
          v1_local_port: res.body.traps.v1_local_port,
          v2_port: res.body.queries.v2_port,
          v2_enabled: !!res.body.queries.v2_enabled,
          v1_port: res.body.queries.v1_port,
          v1_enabled: !!res.body.queries.v1_enabled,
          hosts: res.body.hosts,
          community_name: res.body.community_name,
          enable: !!res.body.enable,
        });
        if (!this.addSnmpV1v2FormGroup.get("enable").value) {
          this.addSnmpV1v2FormGroup.get("hosts").disable();
          this.addSnmpV1v2FormGroup.get("v1_enabled").disable();
          this.addSnmpV1v2FormGroup.get("v1_port").disable();
          this.addSnmpV1v2FormGroup.get("v2_enabled").disable();
          this.addSnmpV1v2FormGroup.get("v2_port").disable();
          this.addSnmpV1v2FormGroup.get("v1_traps_enabled").disable();
          this.addSnmpV1v2FormGroup.get("v1_local_port").disable();
          this.addSnmpV1v2FormGroup.get("v2_traps_enabled").disable();
          this.addSnmpV1v2FormGroup.get("v2_local_port").disable();
          this.addSnmpV1v2FormGroup.get("v1_remote_port").disable();
          this.addSnmpV1v2FormGroup.get("v2_remote_port").disable();
          this.addSnmpV1v2FormGroup.get("snmp_events").disable();
        } else if (!this.addSnmpV1v2FormGroup.get("v1_enabled").value) {
          this.addSnmpV1v2FormGroup.get("v1_port").disable();
        } else if (!this.addSnmpV1v2FormGroup.get("v2_enabled").value) {
          this.addSnmpV1v2FormGroup.get("v2_port").disable();
        } else if (!this.addSnmpV1v2FormGroup.get("v1_traps_enabled").value) {
          this.addSnmpV1v2FormGroup.get("v1_local_port").disable();
          this.addSnmpV1v2FormGroup.get("v1_remote_port").disable();
        } else if (!this.addSnmpV1v2FormGroup.get("v2_traps_enabled").value) {
          this.addSnmpV1v2FormGroup.get("v2_local_port").disable();
          this.addSnmpV1v2FormGroup.get("v2_remote_port").disable();
        }
      });
  }

  openEditSnmpCommunityV3Edit(modal, snmpCommunityName: string) {
    this.selectedSnmpCommunity = snmpCommunityName;
    this.snmpService
      .getSnmpCommunityV3Detail(snmpCommunityName)
      .subscribe((res) => {
        this.isEditSnmpCommunity = true;
        this.addSnmpV3FormGroup = this.setSnmpCommunityV3FormData({
          authentication_password:
            res.body.security_level.authentication_password || "",
          authentication_enabled:
            !!res.body.security_level.authentication_password &&
            !!res.body.security_level.authentication_algorithm,
          authentication_algorithm:
            res.body.security_level.authentication_algorithm || "",
          local_port: res.body.traps.local_port || "",
          queries_enabled: !!res.body.queries.enabled,
          snmp_events: res.body.snmp_events || [],
          hosts: res.body.hosts || [],
          community_name: res.body.community_name || "",
          enable: !!res.body.enable,
          port: res.body.queries.port || "",
          private_enabled:
            !!res.body.security_level.private_protocol &&
            !!res.body.security_level.private_password,
          private_password: res.body.security_level.private_password || "",
          private_protocol: res.body.security_level.private_protocol || "",
          remote_port: res.body.traps.remote_port || "",
          traps_enabled: !!res.body.traps.enabled,
        });

        if (!res.body.enable) {
          this.addSnmpV3FormGroup.get("hosts").disable();
          this.addSnmpV3FormGroup.get("port").disable();
          this.addSnmpV3FormGroup.get("local_port").disable();
          this.addSnmpV3FormGroup.get("remote_port").disable();
          this.addSnmpV3FormGroup.get("authentication_algorithm").disable();
          this.addSnmpV3FormGroup.get("authentication_password").disable();
          this.addSnmpV3FormGroup.get("private_protocol").disable();
          this.addSnmpV3FormGroup.get("private_password").disable();
          this.addSnmpV3FormGroup.get("snmp_events").disable();
        } else if (!res.body.queries.enabled) {
          this.addSnmpV3FormGroup.get("port").disable();
        } else if (!res.body.traps.enabled) {
          this.addSnmpV3FormGroup.get("local_port").disable();
          this.addSnmpV3FormGroup.get("remote_port").disable();
        } else if (
          !res.body.security_level.authentication_algorithm &&
          !res.body.security_level.authentication_password
        ) {
          this.addSnmpV3FormGroup.get("authentication_algorithm").disable();
          this.addSnmpV3FormGroup.get("authentication_password").disable();
        } else if (
          !res.body.security_level.private_password &&
          !res.body.security_level.private_protocol &&
          !res.body.security_level.authentication_algorithm &&
          !res.body.security_level.authentication_password
        ) {
          this.addSnmpV3FormGroup.get("private_protocol").disable();
          this.addSnmpV3FormGroup.get("private_password").disable();
        }

        this.modalService.open(modal, {
          ariaLabelledBy: "modal-basic-title",
          size: "xl",
          backdrop: "static",
          centered: true,
        });
      });
  }

  changeCommunityV3ActiveStatus(event) {
    if (event.target.checked) {
      this.addSnmpV3FormGroup.get("hosts").enable();
      if (this.addSnmpV3FormGroup.get("queries_enabled").value) {
        this.addSnmpV3FormGroup.get("port").enable();
      }
      if (this.addSnmpV3FormGroup.get("traps_enabled").value) {
        this.addSnmpV3FormGroup.get("local_port").enable();
        this.addSnmpV3FormGroup.get("remote_port").enable();
      }

      if (this.addSnmpV3FormGroup.get("authentication_enabled").value) {
        this.addSnmpV3FormGroup.get("authentication_algorithm").enable();
        this.addSnmpV3FormGroup.get("authentication_password").enable();
      }

      if (this.addSnmpV3FormGroup.get("private_enabled").value) {
        this.addSnmpV3FormGroup.get("private_protocol").enable();
        this.addSnmpV3FormGroup.get("private_password").enable();
      }
      this.addSnmpV3FormGroup.get("snmp_events").enable();
    } else {
      this.addSnmpV3FormGroup.get("hosts").disable();
      this.addSnmpV3FormGroup.get("port").disable();
      this.addSnmpV3FormGroup.get("local_port").disable();
      this.addSnmpV3FormGroup.get("remote_port").disable();
      this.addSnmpV3FormGroup.get("authentication_algorithm").disable();
      this.addSnmpV3FormGroup.get("authentication_password").disable();
      this.addSnmpV3FormGroup.get("private_protocol").disable();
      this.addSnmpV3FormGroup.get("private_password").disable();
      this.addSnmpV3FormGroup.get("snmp_events").disable();
    }
  }

  changeAuthenStatus(event) {
    if (event.target.checked) {
      this.addSnmpV3FormGroup.get("authentication_algorithm").enable();
      this.addSnmpV3FormGroup.get("authentication_password").enable();
    } else {
      this.addSnmpV3FormGroup.get("authentication_algorithm").disable();
      this.addSnmpV3FormGroup.get("authentication_password").disable();
    }
  }

  changePrivateStatus(event) {
    if (event.target.checked) {
      this.addSnmpV3FormGroup.get("authentication_algorithm").enable();
      this.addSnmpV3FormGroup.get("authentication_password").enable();
    } else {
      this.addSnmpV3FormGroup.get("authentication_algorithm").disable();
      this.addSnmpV3FormGroup.get("authentication_password").disable();
    }
  }

  confirmSave() {
    const data: SnmpCommunityConfig = {
      config_status: this.snmpSetting.get("config_status").value ? 1 : 0,
      contact_info: !this.snmpSetting.get("contact_info").value
        ? ""
        : this.snmpSetting.get("contact_info").value.trim(),
      description: !this.snmpSetting.get("description").value
        ? ""
        : this.snmpSetting.get("description").value.trim(),
      location: !this.snmpSetting.get("location").value
        ? ""
        : this.snmpSetting.get("location").value.trim(),
    };

    this.snmpService.updateSnmpSetting(data).subscribe((res) => {
      console.log("[res]: ", res);
      this.getSnmpSetting();
    });
  }
}
