import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormBuilder,
  ValidationErrors,
  ValidatorFn,
} from "@angular/forms";
import { SysLogService } from "@app/views/log-forward/sys-log/sys-log.service";
import { PROTOCOLS } from "@app/shared/model/interface.model";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: "app-sys-log",
  templateUrl: "./sys-log.component.html",
  styleUrls: ["./sys-log.component.scss"],
})
export class SysLogComponent implements OnInit {
  sysLogSetting = this.setSysLogSetting({
    config_status: false,
    ip_domain: "",
    port: "",
    protocol: "",
    facility: "",
    severity_level: "",
    log_type: [],
  });
  PROTOCOLS = PROTOCOLS;
  facilities: { label: string; value: string }[] = [];
  severityLevels: { label: string; value: string }[] = [];
  logTypes: { label: string; value: string }[] = [];

  constructor(
    private fb: FormBuilder,
    public sysLogService: SysLogService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.getLogTypeOptions();
    this.getSeverityLevelOptions();
    this.getFacilityOptions();
    this.getSysLogInfo();
  }

  getFacilityOptions() {
    this.sysLogService.getFacilityOptions().subscribe((res) => {
      const facilities = [];
      Object.keys(res.body).forEach((key) => {
        if (key !== "status") {
          facilities.push({
            value: key,
            label: res.body[key].name,
          });
        }
      });

      this.facilities = facilities;
    });
  }

  getSeverityLevelOptions() {
    this.sysLogService.getSeverityLevelOptions().subscribe((res) => {
      const severityLevels = [];
      Object.keys(res.body).forEach((key) => {
        if (key !== "status") {
          severityLevels.push({
            value: key,
            label: res.body[key].name,
          });
        }
      });

      this.severityLevels = severityLevels;
    });
  }

  getLogTypeOptions() {
    this.sysLogService.getLogTypeOptions().subscribe((res) => {
      const logTypes = [];
      Object.keys(res.body).forEach((key) => {
        if (key !== "status") {
          logTypes.push({
            value: key,
            label: res.body[key].name,
          });
        }
      });

      this.logTypes = logTypes;
    });
  }

  setSysLogSetting({
    config_status,
    ip_domain,
    port,
    protocol,
    facility,
    severity_level,
    log_type,
  }: {
    config_status: boolean;
    ip_domain: string;
    port: string;
    protocol: string;
    facility: string;
    severity_level: string;
    log_type: string[];
  }) {
    return this.fb.group({
      config_status: [config_status],
      ip_domain: [ip_domain, [this.validateIpDomain()]],
      port: [port],
      protocol: [protocol],
      facility: [facility],
      severity_level: [severity_level],
      log_type: [log_type, [this.validateLogType()]],
    });
  }

  getSysLogInfo() {
    this.sysLogService.getLogForwardSetting().subscribe((res) => {
      this.sysLogSetting = this.setSysLogSetting({
        config_status: !!res.body.config_status,
        ip_domain: res.body.ip_domain || "",
        port: res.body.port || "",
        protocol: res.body.protocol || "",
        facility: res.body.facility || "",
        severity_level: res.body.severity_level || "",
        log_type: res.body.log_type || [],
      });
    });
  }

  validateIpDomain(): ValidatorFn {
    const ipv4RegEx =
      /(^((25[0-5]|(2[0-4]|1[0-9]|[1-9]|)[0-9])(\.(?!$)|$)){4})/;
    const domainRegEx =
      /^[a-zA-Z0-9][a-zA-Z0-9-]{1,61}[a-zA-Z0-9](?:\.[a-zA-Z]{2,})+$/;
    return (control: AbstractControl): ValidationErrors | null => {
      let invalidIp = false;
      const ipDomain = control.value;
      if (ipDomain) {
        invalidIp = !ipv4RegEx.test(ipDomain) && !domainRegEx.test(ipDomain);
      }

      return invalidIp ? { invalidIpDomain: { value: control.value } } : null;
    };
  }

  validateLogType(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      const logType = control.value;
      return !!!control.value.length
        ? { invalidLogType: { value: logType } }
        : null;
    };
  }

  changeLogType(event, log_type: { label: string; value: string }) {
    // console.log(this.sysLogSetting.get('log_type').value, event.target.checked, log_type);
    if (event.target.checked) {
      this.sysLogSetting.get("log_type").value.push(log_type.value);
    } else {
      const idx = this.sysLogSetting
        .get("log_type")
        .value.findIndex((e) => e === log_type.value);
      if (idx > -1) {
        this.sysLogSetting.get("log_type").value.splice(idx, 1);
      }
    }
  }

  checkForm(event, modal) {
    if (!event.target.checked) {
      this.modalService
        .open(modal, {
          ariaLabelledBy: "modal-basic-title",
          size: "lg",
          backdrop: "static",
          centered: true,
        })
        .result.then(
          (result) => {
            if (result === "Delete") {
              this.sysLogService.deleteLogForwardSetting().subscribe((res) => {
                // console.log(res, " :xxx");
                this.getSysLogInfo();
              });
            } else if (result === "Cancel") {
              this.sysLogSetting.get("config_status").setValue(true);
              // console.log(
              //   this.sysLogSetting.get("config_status").value,
              //   " :xxx"
              // );
            }
          },
          () => {}
        );
      // this.sysLogSetting.get("ip_domain").disable();
      // this.sysLogSetting.get("port").disable();
      // this.sysLogSetting.get("protocol").disable();
      // this.sysLogSetting.get("facility").disable();
      // this.sysLogSetting.get("severity_level").disable();
      // this.sysLogSetting.get("log_type").disable();
    } else {
      this.sysLogSetting.get("ip_domain").enable();
      this.sysLogSetting.get("port").enable();
      this.sysLogSetting.get("protocol").enable();
      this.sysLogSetting.get("facility").enable();
      this.sysLogSetting.get("severity_level").enable();
      this.sysLogSetting.get("log_type").enable();
    }
  }

  testConnection() {
    this.sysLogService
      .tesConnection({
        ip_domain: this.sysLogSetting.get("ip_domain").value,
        protocol: this.sysLogSetting.get("protocol").value,
        port: this.sysLogSetting.get("port").value,
      })
      .subscribe((res) => {
        console.log(res);
      });
  }

  saveLogSetting() {
    this.sysLogService
      .saveLogForwardSetting({
        protocol: this.sysLogSetting.get("protocol").value,
        port: this.sysLogSetting.get("port").value,
        ip_domain: this.sysLogSetting.get("ip_domain").value,
        config_status: this.sysLogSetting.get("config_status").value ? 1 : 0,
        facility: this.sysLogSetting.get("facility").value,
        log_type: this.sysLogSetting.get("log_type").value,
        severity_level: this.sysLogSetting.get("severity_level").value,
      })
      .subscribe((res) => {
        console.log(res);
      });
  }
}
