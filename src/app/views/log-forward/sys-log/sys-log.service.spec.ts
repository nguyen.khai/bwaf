import { TestBed } from '@angular/core/testing';

import { SysLogService } from './sys-log.service';

describe('SysLogService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SysLogService = TestBed.get(SysLogService);
    expect(service).toBeTruthy();
  });
});
