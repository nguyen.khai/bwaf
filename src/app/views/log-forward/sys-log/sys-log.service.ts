import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";

export interface LogSysSetting {
  config_status: number;
  ip_domain?: string;
  protocol?: string;
  port?: string;
  facility?: string;
  severity_level?: string;
  log_type?: string[];
}

export interface Facility {
  name: string;
  description: string;
}

export interface FacilityList {
  [key: string]: Facility;
}

@Injectable({
  providedIn: "root",
})
export class SysLogService {
  public resourceUrl = environment.apiUrl;

  constructor(protected http: HttpClient) {}

  getLogForwardSetting(): Observable<HttpResponse<LogSysSetting>> {
    return this.http.get<LogSysSetting>(`${this.resourceUrl}/logforward`, {
      observe: "response",
    });
  }

  deleteLogForwardSetting(): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/logforward`, {
      observe: "response",
    });
  }

  saveLogForwardSetting(
    body: LogSysSetting
  ): Observable<HttpResponse<{ data: LogSysSetting }>> {
    return this.http.post<{ data: LogSysSetting }>(
      `${this.resourceUrl}/logforward`,
      body,
      { observe: "response" }
    );
  }

  getFacilityOptions(): Observable<HttpResponse<FacilityList>> {
    return this.http.options<FacilityList>(`${this.resourceUrl}/facility`, {
      observe: "response",
    });
  }

  getSeverityLevelOptions(): Observable<HttpResponse<FacilityList>> {
    return this.http.options<FacilityList>(`${this.resourceUrl}/severity`, {
      observe: "response",
    });
  }

  getLogTypeOptions(): Observable<HttpResponse<FacilityList>> {
    return this.http.options<FacilityList>(`${this.resourceUrl}/log-type`, {
      observe: "response",
    });
  }

  tesConnection(body?: any): Observable<HttpResponse<any>> {
    return this.http.post<any>(`${this.resourceUrl}/logforward-test`, body, {
      observe: "response",
    });
  }
}
