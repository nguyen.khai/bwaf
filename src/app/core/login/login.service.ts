import { Injectable } from "@angular/core";

import { AccountService } from "@app/core/auth/account.service";
import { AuthServerProvider } from "@app/core/auth/auth-jwt.service";
import { Router } from "@angular/router";
import { Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class LoginService {
  constructor(
    private accountService: AccountService,
    private authServerProvider: AuthServerProvider,
    private router: Router
  ) {}

  login(credentials, callback?) {
    const cb = callback || function () {};

    return new Promise((resolve, reject) => {
      this.authServerProvider.login(credentials).subscribe(
        (data) => {
          this.accountService.identity(true).then((account) => {
            resolve(data.body);
          });
          return data;
        },
        (err) => {
          this.logout();
          reject(err);
          return cb(err);
        }
      );
    });
  }

  loginWithToken(jwt, refreshToken, rememberMe) {
    return this.authServerProvider.loginWithToken(
      jwt,
      refreshToken,
      rememberMe
    );
  }

  logout() {
    this.authServerProvider.logout().subscribe(() => {
      this.router.navigate(["/login"]);
    });
    this.accountService.authenticate(null);
  }

  logoutNotRedirect(): Observable<any> {
    this.accountService.authenticate(null);
    return this.authServerProvider.logout();
  }

  login1() {
    this.router.navigate(["/login"]);
  }
}
