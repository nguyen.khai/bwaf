import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { LocalStorageService, SessionStorageService } from "ngx-webstorage";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { environment } from "@env/environment";

const SERVER_API_URL = environment.apiUrl;

@Injectable({ providedIn: "root" })
export class AuthServerProvider {
  constructor(
    private http: HttpClient,
    private $localStorage: LocalStorageService,
    private $sessionStorage: SessionStorageService
  ) {}

  getToken() {
    return (
      this.$localStorage.retrieve("authenticationToken") ||
      this.$sessionStorage.retrieve("authenticationToken")
    );
  }

  login(credentials): Observable<any> {
    let data = {};
    if (credentials.otp) {
      data = {
        user_name: credentials.username,
        password: credentials.password,
        otp: credentials.otp,
      };
    } else {
      data = {
        user_name: credentials.username,
        password: credentials.password,
      };
    }
    return this.http
      .post(SERVER_API_URL + "/login", data, { observe: "response" })
      .pipe(map(authenticateSuccess.bind(this)));

    function authenticateSuccess(resp) {
      const bearerToken = resp.body.access_token;
      if (bearerToken) {
        const jwt = bearerToken;
        const refreshToken = resp.body.refresh_token;
        credentials.rememberMe = true; // sangnv.fix
        this.storeAuthenticationToken(
          jwt,
          refreshToken,
          credentials.rememberMe
        );
      }
      return resp;
    }
  }

  loginWithToken(jwt, refreshToken, rememberMe) {
    if (jwt) {
      this.storeAuthenticationToken(jwt, refreshToken, rememberMe);
      return Promise.resolve(jwt);
    } else {
      return Promise.reject("auth-jwt-service Promise reject"); // Put appropriate error message here
    }
  }

  storeAuthenticationToken(jwt, refreshToken, rememberMe) {
    if (rememberMe) {
      this.$localStorage.store("authenticationToken", jwt);
      this.$localStorage.store("refreshToken", refreshToken);
    } else {
      this.$sessionStorage.store("authenticationToken", jwt);
      this.$sessionStorage.store("refreshToken", refreshToken);
    }
  }

  storeAuthToken(jwt) {
    this.$localStorage.store("authenticationToken", jwt);
    this.$sessionStorage.store("authenticationToken", jwt);
  }

  logout(): Observable<any> {
    return new Observable((observer) => {
      this.$localStorage.clear("authenticationToken");
      this.$localStorage.clear("refreshToken");
      this.$sessionStorage.clear("refreshToken");
      this.$sessionStorage.clear("authenticationToken");
      observer.complete();
    });
  }

  loginWithRefreshToken(): Observable<any> {
    const refreshToken =
      this.$localStorage.retrieve("refreshToken") ||
      this.$sessionStorage.retrieve("refreshToken");
    const httpOptions = {
      headers: new HttpHeaders({
        Authorization: "Bearer " + refreshToken,
      }),
    };
    return this.http.get(SERVER_API_URL + "/refresh-token", { ...httpOptions });
  }
}
