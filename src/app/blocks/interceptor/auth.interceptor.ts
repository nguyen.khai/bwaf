import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

import {environment} from '@env/environment';
import {Router} from '@angular/router';
import {JhiEventManager} from 'ng-jhipster';

const SERVER_API_URL = environment.apiUrl;

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private localStorage: LocalStorageService,
              private sessionStorage: SessionStorageService,
              private eventManager: JhiEventManager,
              private router: Router,
  ) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request || !request.url || (/^http/.test(request.url) && !(SERVER_API_URL && request.url.startsWith(SERVER_API_URL)))) {
      return next.handle(request);
    }

    const token = this.localStorage.retrieve('authenticationToken') || this.sessionStorage.retrieve('authenticationToken');
    const refreshToken = this.localStorage.retrieve('refreshToken') || this.sessionStorage.retrieve('refreshToken');
    if (!!refreshToken && request.url.indexOf('/refresh-token') > -1) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + refreshToken
        }
      });
    } else if (!!token || request.url.indexOf('/login') > -1) {
      request = request.clone({
        setHeaders: {
          Authorization: 'Bearer ' + token
        }
      });
    } else {
      this.router.navigate(['/login']);
    }


    if (request.method === 'GET' && request.params && request.params.keys()) {
      request.params.keys().forEach(p => {
        if (!request.params.get(p) || request.params.get(p) === 'null') {
          request = request.clone({
            params: request.params.delete(p)
          });
        }
      });
    }
    return next.handle(request);
  }
}
