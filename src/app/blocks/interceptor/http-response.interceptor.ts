import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { IBaseResponse } from "@app/shared/model/base-respone.model";
import { ToastrCustomService } from "@app/shared/toastr-custom-service";

const SUCCESS_CODE = [1, 200, 201];

@Injectable()
export class HttpResponseInterceptor implements HttpInterceptor {
  constructor(public alertService: ToastrCustomService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      map(
        (event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            event = event.clone({ body: this.extractResponseData(event.body) });
          }
          return event;
        },
        (err: any) => {}
      )
    );
  }

  extractResponseData(body: IBaseResponse) {
    const message = body.message;
    if (!body.status) {
      body.status = 200;
    }
    if (body.status && SUCCESS_CODE.indexOf(body.status) > -1) {
      if (message && typeof message === "string") {
        this.alertService.success(message);
      }
      if (body.data instanceof Array && body.paging) {
        return { data: body.data, paging: body.paging };
      } else {
        return body.data;
      }
    } else {
      if (message && typeof message === "string") {
        this.alertService.error(message);
      }
      return body;
    }
  }
}
