import { Injectable } from "@angular/core";
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent,
} from "@angular/common/http";
import {BehaviorSubject, Observable, of} from 'rxjs';
import {
  catchError,
  filter,
  finalize,
  switchMap,
  take,
  tap,
} from "rxjs/operators";
import { LoginService } from "app/core/login/login.service";
import { AuthServerProvider } from "@app/core/auth/auth-jwt.service";
import {Router} from '@angular/router';

@Injectable()
export class AuthExpiredInterceptor implements HttpInterceptor {
  isRefreshing = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(
    null
  );

  constructor(
    private loginService: LoginService,
    private authServerProvider: AuthServerProvider,
    private router: Router
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    // @ts-ignore
    return next.handle(request).pipe(
      catchError((error) => {
        if (error instanceof HttpErrorResponse) {
          switch ((<HttpErrorResponse>error).status) {
            case 401:
              if (request.url.indexOf("/refresh-token") > 0) {
                this.router.navigate(['/login']);
                return this.loginService.logoutNotRedirect();
              }
              return this.handle401Error(request, next);
            case 403:
              this.router.navigate(['/login']);
              return this.loginService.logoutNotRedirect();
            case 422:
              this.router.navigate(['/login']);
              return this.loginService.logoutNotRedirect();
          }
        } else {
          return Observable.throw(error);
        }
      })
    );
  }

  private addToken(request: HttpRequest<any>, token: string) {
    console.log(request);
    request = request.clone({
      setHeaders: {
        Authorization: "Bearer " + token,
      },
    });
    return request;
  }

  private handle401Error(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<any> {
    if (!this.isRefreshing) {
      this.isRefreshing = true;

      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.refreshTokenSubject.next(null);

      return this.authServerProvider.loginWithRefreshToken().pipe(
        switchMap((newToken) => {
          if (!newToken.access_token) {
            this.loginService.logout();
          }
          if (newToken) {
            this.refreshTokenSubject.next(newToken.access_token);
            this.authServerProvider.storeAuthToken(newToken.access_token);
            return next.handle(this.addToken(request, newToken.access_token));
          }
          // If we don't get a new token, we are in trouble so logout.
          return this.authServerProvider.logout();
        }),
        catchError((error) => {
          // If there is an exception calling 'refreshToken', bad news so logout.
          return this.authServerProvider.logout();
        }),
        finalize(() => {
          this.isRefreshing = false;
        })
      );
    } else {
      return this.refreshTokenSubject.pipe(
        filter((token) => token != null),
        take(1),
        switchMap((token) => {
          return next.handle(this.addToken(request, token));
        })
      );
    }
  }
}
