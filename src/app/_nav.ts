import { INavData } from "@coreui/angular";

export const navItems: INavData[] = [
  {
    name: "Dashboard",
    url: "/dashboard",
    icon: "icon-speedometer",
  },
  {
    title: true,
    name: "System",
  },
  {
    name: "Network",
    url: "/network",
    icon: "icon-vector",
  },
  {
    name: "Object",
    url: "/object",
    icon: "icon-organization",
    children: [
      {
        name: "SSL",
        url: "/object/ssl",
      },
      {
        name: "Website",
        url: "/object/website",
      },
      {
        name: "Group Website",
        url: "/object/group-website",
      },
      {
        name: "Mail Sender",
        url: "/object/mail-sender",
      },
      {
        name: "Account",
        url: "/object/account",
      },
      {
        name: "Rule Management",
        url: "/object/rule-management",
      },
    ],
  },
  {
    name: "IP",
    url: "/ip",
    icon: "icon-link",
  },
  {
    name: "Report",
    url: "/report",
    icon: "icon-docs",
  },
  {
    name: "Maintenance",
    url: "/maintaince",
    icon: "icon-fire",
    children: [
      {
        name: "Backup & Restore",
        url: "/maintaince/backup-restore",
      },
      {
        name: "System Time",
        url: "/maintaince/system-time",
      },
    ],
  },
  {
    name: "User",
    url: "/user",
    icon: "icon-people",
  },
  {
    name: "High Availability",
    url: "/high-availability",
    icon: "icon-magic-wand",
  },
  {
    name: "DDoS Protection",
    url: "/ddos",
    icon: "icon-shield",
    children: [
      {
        name: "Network layer",
        url: "/ddos/network",
      },
      {
        name: "Application layer",
        url: "/ddos/application",
      },
    ],
  },
  {
    name: "Web Protection",
    url: "/waf",
    icon: "icon-badge",
  },
  {
    name: "Monitor",
    url: "/monitor",
    icon: "icon-compass",
    children: [
      {
        name: "System",
        url: "/monitor/system",
      },
      {
        name: "Setting",
        url: "/monitor/setting",
      },
      {
        name: "DDoS",
        url: "/monitor/ddos",
      },
      {
        name: "Web Application",
        url: "/monitor/web-application",
      },
      {
        name: "Resource",
        url: "/monitor/resource",
      },
      {
        name: "Connection",
        url: "/monitor/connection",
      },
      {
        name: "File Scanned",
        url: "/monitor/file-scanned",
      },
    ],
  },
  {
    name: "Diagnostic",
    url: "/diagnostic",
    icon: "icon-eyeglass",
    children: [
      {
        name: "Ping",
        url: "/diagnostic/ping",
      },
      {
        name: "Traceroute",
        url: "/diagnostic/traceroute",
      },
      {
        name: "Port Check",
        url: "/diagnostic/port-check",
      },
    ],
  },
  {
    name: "Web Scan",
    url: "/web-vulnerability-scan",
    icon: "icon-magnifier",
    children: [
      {
        name: "Scanner",
        url: "/web-vulnerability-scan",
      },
      {
        name: "Schedule",
        url: "/web-vulnerability-scan/scan-schedule",
      },
      {
        name: "History",
        url: "/web-vulnerability-scan/scan-history",
      },
    ],
  },
  {
    name: "Log Forward",
    url: "/log-forward",
    icon: "icon-exclamation",
    children: [
      {
        name: "Sys Log",
        url: "/log-forward/sys-log",
      },
      {
        name: "SNMP",
        url: "/log-forward/snmp",
      },
    ],
  },
  {
    name: "About",
    url: "/about",
    icon: "icon-exclamation",
  },
];
