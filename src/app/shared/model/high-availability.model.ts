import {Config} from '@app/shared/model/config.model';
import {VirtualInterface} from '@app/shared/model/virtual-interface.model';

export interface IHighAvailability {
  high_availability_status?: number;
  config?: Config;
  virtual_interface?: VirtualInterface[];
}

export class HighAvailability implements IHighAvailability {
  constructor(
    public high_availability_status?: number,
    public config?: Config,
    public virtual_interface?: VirtualInterface[],
  ) {}
}

export interface IHighAvailabilityMode {
  id?: number;
  mode_name?: string;
}

export class HighAvailabilityMode implements IHighAvailabilityMode {
  constructor(
    public id?: number,
    public mode_name?: string,
  ) {}
}




