export interface IWebScan {
  scan_id?: number;
  website_name?: string[];
  ip?: string;
  system_detail?: string;
  datetime?: string;
  status?: number;
  is_show_status?: boolean;
  is_show_scan?: boolean;
  statusDetail?: string;
  progress?: number;
  scan?: number;
  scan_detail?: IScan;
}

export class WebScan implements IWebScan {
  constructor(
    public scan_id?: number,
    public website_name?: string[],
    public ip?: string,
    public system_detail?: string,
    public datetime?: string,
    public status?: number,
    public is_show_status?: boolean,
    public is_show_scan?: boolean,
    public statusDetail?: string,
    public progress?: number,
    public scan?: number,
    public scan_detail?: IScan,
  ) {
  }
}

export interface IVulnerability {
  vulnerability_id?: number;
  vulnerability_name?: string;
}

export class Vulnerability implements IVulnerability {
  constructor(
    public vulnerability_id?: number,
    public vulnerability_name?: string) {
  }
}

export class IScan {
  total?: number;
  detail?: IScanDetail[];
}

export class Scan implements IScan {
  constructor(
    public total?: number,
    public scan_detail?: IScanDetail[]) {
  }
}


export class IScanDetail {
  name?: string;
  count?: number;
  description?: string;
  isExpand?: boolean;
}

export class ScanDetail implements IScanDetail {
  constructor(
    public name?: string,
    public count?: number,
    public description?: string) {
  }
}

export interface ILogFile {
  logfile_id?: number;
  logfile_name?: string;
}

export class LogFile implements ILogFile {
  constructor(
    public logfile_id?: number,
    public logfile_name?: string) {
  }
}

export interface IScanStatus {
  status_code?: number;
  description?: string;
}

export class ScanStatus implements IScanStatus {
  constructor(
    public status_code?: number,
    public description?: string) {
  }
}
