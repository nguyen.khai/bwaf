export interface IAccount {
  id?: number;
  name?: string;
  email?: string;
  phone?: number;
  is_update?: boolean;
}

export class Account implements IAccount {
  constructor(
    public id?: number,
    public name?: string,
    public email?: string,
    public phone?: number,
    public is_update?: boolean
  ) {
    this.is_update = false;
    this.name = name;
  }
}
