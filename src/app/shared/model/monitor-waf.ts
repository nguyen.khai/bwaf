
export interface IMonitorWaf {
  id?: number;
  attacker_country?: string;
  attacker_ip?: string;
  datetime?: string;
  group_rule?: string;
  group_website?: string;
  matched_info?: string;
  request_header?: any;
  violation_code?: string;
  website_domain?: string;
}

export class MonitorWafModel implements IMonitorWaf {
  constructor(
    public id?: number,
    public attacker_country?: string,
    public attacker_ip?: string,
    public datetime?: string,
    public group_rule?: string,
    public group_website?: string,
    public matched_info?: string,
    public request_header?: any,
    public violation_code?: string,
    public website_domain?: string
  ) {
  }
}
