export interface IItem {
    id?: number;
    name?: string;
    storeId?: number;
}

export class Item implements IItem {
    constructor(public id?: number, public name?: string, public storeId?: number) {}
}
