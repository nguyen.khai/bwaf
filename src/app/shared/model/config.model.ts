export interface IConfig {
  device_priority?: any;
  group_name?: string;
  group_password?: string;
  heartbeat_interface_id?: number;
  heartbeat_netmask?: any;
  heartbeat_network?: any;
  operation_mode_id?: number;
}

export class Config implements IConfig {
  constructor(
    public device_priority?: any,
    public group_name?: string,
    public group_password?: string,
    public heartbeat_interface_id?: number,
    public heartbeat_netmask?: any,
    public heartbeat_network?: any,
    public operation_mode_id?: number,
    public is_show_password?: boolean) {
    this.is_show_password = false;
  }
}
