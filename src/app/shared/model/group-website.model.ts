import {IWebsite} from '@app/shared/model/website.model';

export interface IGroupWebsite {
    id?: number;
    name?: string;
    description?: string;
    websites?: IWebsite[];
}

export class GroupWebsite implements IGroupWebsite {
    constructor(public id?: number,
                public name?: string,
                public description?: string,
                public websites?: IWebsite[]) {}
}

