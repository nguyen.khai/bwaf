export interface IVirtualInterface {
  id?: number;
  is_enable?: number;
  virtual_ip_address?: any;
  priority?: number;
  name?: string;
}

export class VirtualInterface implements IVirtualInterface {
  constructor(
    public id?: number,
    public is_enable?: number,
    public virtual_ip_address?: any,
    public priority?: number,
    public  name?: string
  ) {
    this.name = this.name || null;
  }
}
