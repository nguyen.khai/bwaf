import {Moment} from "moment";

export interface IFirmware {
  id?: number;
  version?: string;
  release_date?: Moment;
}

export class Firmware implements IFirmware {
  constructor(
    public id?: number,
    public version?: string,
    public release_date?: Moment
  ) {
  }
}
