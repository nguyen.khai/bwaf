
export interface IScanHistory {
  id?: number;
  datetime?: string;
  website?: string;
  ip_address?: string;
  status?: number;
  result?: IResult;
}

export class ScanHistory implements IScanHistory {
  constructor(public id?: number,
              public datetime?: string,
              public website?: string,
              public ip_address?: string,
              public status?: number,
              public result?: IResult,
  ) {
  }
}

export interface IResult {
  value?: string;
  data?: IData[];
}

export class Result implements IResult {
  constructor(public value?: string,
              public data?: IData[],
  ) {}

}
export interface IData {
  description?: string;
  id?: string;
  vulnerability_name?: string;
}

export class Data implements IData {
  constructor(public description?: string,
              public id?: string,
              public vulnerability_name?: string,
  ) {}

}


