export interface IAbout {
  product_name?: string;
  firmware_version?: string;
  system_cpu?: string;
  system_ram?: string;
  system_storage?: string;
  system_name?: string;
  system_uptime?: string;
}

export class About implements IAbout {
  constructor(
    public product_name?: string,
    public firmware_version?: string,
    public system_cpu?: string,
    public system_ram?: string,
    public system_storage?: string,
    public system_name?: string,
    public system_uptime?: string,
  ) {
  }
}
