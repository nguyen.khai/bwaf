
export interface IMonitorThroughput {
  datetime?: string;
  input?: any;
  output?: any;
}

export class MonitorThroughput implements  IMonitorThroughput {
  constructor(public datetime?: string,
              public input?: any,
              public output?: any,
  ) {
  }
}

export interface IMonitorConnection {
  accepts?: number;
  active?: number;
  datetime?: string;
  handled?: number;
  reading?: number;
  requests?: number;
  waiting?: number;
  writing?: number;
}

export class MonitorConnection implements IMonitorConnection {
  constructor(
    public accepts?: number,
    public active?: number,
    public datetime?: string,
    public handled?: number,
    public reading?: number,
    public requests?: number,
    public waiting?: number,
    public writing?: number,
  ) {
  }
}

export interface IMonitorStatusCode {
    code_1?: number;
    code_2?: number;
    code_3?: number;
    code_4?: number;
    code_5?: number;
    datetime?: string;
}

export class MonitorStatusCode implements IMonitorStatusCode {
  constructor(
    public code_1?: number,
    public code_2?: number,
    public code_3?: number,
    public code_4?: number,
    public code_5?: number,
    public datetime?: string,
  ) {
  }
}






