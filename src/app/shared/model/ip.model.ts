export interface IIp {
  id?: number;
  ip_address?: string;
  netmask?: string;
  description?: string;
  is_update?: boolean;
}

export class Ip implements IIp {
  constructor(
    public id?: number,
    public ip_address?: string,
    public netmask?: string,
    public description?: string,
    public is_update?: boolean
  ) {
    this.is_update = false;
  }
}
