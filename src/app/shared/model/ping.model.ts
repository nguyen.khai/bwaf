export interface IPing {
 ping_packet?: PingPacket[];
 time?: Time;
}

export class Ping implements IPing {
  constructor(
    public ping_packet?: PingPacket[],
    public time?: Time,
  ) {
  }
}


export interface IPingPacket {
  seq?: number;
  reply_from?: any;
  packet_size?: number;
  time?: string;
  TTL?: number;
}

export class PingPacket implements IPingPacket {
  constructor(
    public   seq?: number,
    public reply_from?: any,
    public packet_size?: number,
    public time?: string,
    public TTL?: number,
  ) {
  }
}

export interface ITime {
  min?: any;
  max?: any;
  avg?: any;
}

export class Time implements ITime {
  constructor(
    public min?: any,
    public max?: any,
    public avg?: any,
  ) {
  }
}

export interface ICheckPing {
  ip_address?: any;
  count?: number;
  packet_size?: number;
  timeout?: number;
}

export class CheckPing implements ICheckPing {
  constructor(
    public ip_address?: any,
    public count?: number,
    public packet_size?: number,
    public timeout?: number,
  ) {
  }
}
