import { Moment } from "moment";

export interface IUser {
  id?: number;
  user_name?: string;
  password?: string;
  phone_number?: string;
  email?: string;
  image_link?: string;
  can_edit?: string;
  name?: string;
  sex?: string;
  date_of_birthday?: string;
  date_created?: Moment;
  two_fa_status?: number;
}

export class User implements IUser {
  constructor(
    public id?: number,
    public user_name?: string,
    public password?: string,
    public phone_number?: string,
    public email?: string,
    public can_edit?: string,
    public image_link?: string,
    public name?: string,
    public sex?: string,
    public date_of_birthday?: string,
    public date_created?: Moment,
    public is_update?: boolean,
    public two_fa_status?: number
  ) {
    this.is_update = false;
  }
}
