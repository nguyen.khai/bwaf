import {Moment} from 'moment';


export interface ILicence {
  licence?: string;
  licence_expired?: Moment;
  is_active?: boolean;
}

export class Licence implements ILicence {
  constructor(public licence?: string,
              public licence_expired?: Moment) {

  }
}
