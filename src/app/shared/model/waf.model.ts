import { IMailSender, MailSender } from "@app/shared/model/mail-sender.model";
import { IAccount } from "@app/shared/model/account.model";
import { FormGroup } from "@angular/forms";

export interface IWaf {
  id?: number;
  name?: string;
  waf_status?: number;
  rules?: IRuleWaf[];
  alert_mail?: IAlertMail;
  alert_sms?: IAlertSms;
  show_alert?: boolean;
  distable_show_alert?: boolean;
  distable_show_rule?: boolean;
  show_rule?: boolean;
  description?: string;
  distable_status?: boolean;
  alert_sms_form?: FormGroup;
  alert_mail_form?: FormGroup;
  show_expand?: boolean;
}

export class Waf implements IWaf {
  constructor(
    public id?: number,
    public name?: string,
    public waf_status?: number,
    public rules?: any,
    public alert_mail?: IAlertMail,
    public alert_sms?: IAlertSms,
    public show_alert?: boolean,
    public show_rule?: boolean,
    public description?: string,
    public distable_status?: boolean,
    public alert_sms_form?: FormGroup,
    public alert_mail_form?: FormGroup,
    public distable_show_alert?: boolean,
    public distable_show_rule?: boolean,
    public show_expand?: boolean
  ) {
    this.show_alert = false;
    this.show_rule = false;
    distable_status = false;
    distable_show_alert = false;
    distable_show_rule = false;
  }
}

export interface IRuleWaf {
  description: string;
  id: number;
  name: string;
  status: number;
  distable_status: boolean;
  show_description: boolean;
}

export interface IAlertSms {
  alert_type: string;
  barrier: number;
  time: number;
  sms_sender: IMailSender;
  account: IAccount[];
  state: number;
}

export interface IAlertMail {
  alert_type: string;
  barrier: number;
  time: number;
  mail_sender: IMailSender;
  account: IAccount[];
  state: number;
}
