export interface IDdosApplication {
  id?: number;
  name?: string;
  count?: number;
  time?: number;
  block_time?: number;
  group_websites?: IGroupWebsites;
  websites?: IWebsiteApplication[];
  urls?: IUrlApplication[];
  status?: string;
  active?: number;
  rule_type?: string;
  isShowUrl?: boolean;
  isShowWebsite?: boolean;
}

export const RULE_TYPE = {
  type_1 : "type 1",
  type_2 : "type 2"
}

export class DdosApplication implements IDdosApplication {
  constructor(
    public id?: number,
    public name?: string,
    public count?: number,
    public time?: number,
    public block_time?: number,
    public group_websites?: IGroupWebsites,
    public websites?: IWebsiteApplication[],
    public urls?: IUrlApplication[],
    public status?: string,
    public active?: number,
    public rule_type?: string,
    public isShowUrl?: boolean,
    public isShowWebsite?: boolean
  ) {
    isShowUrl = false;
    isShowWebsite = false;
  }
}

export interface IUrlApplication {
  uri_id;
  uri;
}

export interface IWebsiteApplication {
  website_id;
  website;
}

export interface IGroupWebsites {
  group_website_id;
  group_website;
}
