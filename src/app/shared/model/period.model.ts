export interface IPeriodModel {
  id?: number;
  period_name?: string;
}

export class PeriodModel implements IPeriodModel {
  constructor(
    public id?: number,
    public period_name?: string,
  ) {
  }
}
