import {IAccount} from '@app/shared/model/account.model';

export interface IReportModel {
  id?: number;
  name?: string;
  period?: string;
  account?: string[];
  mail_sender?: string;
  state?: number;
  report_form?: string;
}

export class ReportModel implements IReportModel {
  constructor(
   public id?: number,
   public name?: string,
   public period?: string,
   public account?: string[],
   public mail_sender?: string,
   public state?: number,
   public report_form?: string,
  ) {
  }
}

