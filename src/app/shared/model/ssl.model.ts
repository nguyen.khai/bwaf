export interface ISsl {
    id?: number;
    ssl_name?: string;
    ssl_description?: string;
    ssl_key?: string;
    ssl_cert?: string;
}

export class Ssl implements ISsl {
    constructor(public id?: number,
                public ssl_name?: string,
                public ssl_description?: string,
                public ssl_key?: string,
                public ssl_cert?: string
                ) {
    }
}
