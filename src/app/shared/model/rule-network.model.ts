export interface IRuleNetwork {
  id?: number;
  name?: string;
  threshold?: number;
  duration?: number;
  alert?: number;
  block_duration?: number;
  state?: number;
  created_at?: number;
  updated_at?: number;
}

export class RuleNetwork implements IRuleNetwork {
  constructor(
    public id?: number,
    public name?: string,
    public threshold?: number,
    public duration?: number,
    public alert?: number,
    public block_duration?: number,
    public state?: number,
    public created_at?: number,
    public updated_at?: number,
  ) {
  }
}
