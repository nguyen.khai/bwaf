export interface ISystem {
  id?: number;
  ip_address?: string;
  datetime?: string;
  user_name?: string;
  action?: string;
  description?: string;
}


export class System implements ISystem {
  constructor(public id?: number,
              public ip_address?: string,
              public datetime?: string,
              public user_name?: string,
              public action?: string,
              public description?: string
  ) {
  }
}
