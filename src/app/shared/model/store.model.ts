import {IItem} from 'app/shared/model/item.model';

export interface IStore {
    id?: number;
    name?: string;
    locate?: string;
    items?: IItem[];
}

export class Store implements IStore {
    constructor(public id?: number, public name?: string, public locate?: string, public items?: IItem[]) {}
}
