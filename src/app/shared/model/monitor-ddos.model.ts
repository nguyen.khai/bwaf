export interface IMonitorDdos {
  id?: number;
  ip_addr?: string;
  block_time?: string;
  country?: string;
  rule?: string;
  unlock?: number;
  website?: string;
  detail?: string;
}

export class MonitorDdosModel implements IMonitorDdos {
  constructor(
    public id?: number,
    public ip_addr?: string,
    public block_time?: string,
    public country?: string,
    public rule?: string,
    public unlock?: number,
    public website?: string,
    public detail?: string
  ) {
  }
}

export interface ILookMonitorDdos {
  ip_addr?: string;
  block_time?: string;
}

export class LookMonitorDdos implements ILookMonitorDdos {
  constructor(
    public ip_addr?: string,
    public block_time?: string,
  ) {
  }
}
