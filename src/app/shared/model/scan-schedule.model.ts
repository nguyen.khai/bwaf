export interface IScanSchedule {
  id?: number;
  websites?: any[];
  periods?: number;
  start_in?: string;
  accounts?: any[];
  mail_senders?: string;
}

export class ScanSchedule implements IScanSchedule {
  constructor(public id?: number,
              public websites?: any[],
              public periods?: number,
              public start_in?: string,
              public accounts?: any[],
              public mail_senders?: string
              ) {}
}
