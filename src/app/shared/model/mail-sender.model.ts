export interface IMailServer {
  id?: number;
  mail_server_name?: string
}

export class MailServer implements IMailServer {
  constructor(
    public id?: number,
    public mail_server_name?: string
  ) {
  }
}

export interface IMailSender {
  id?: number;
  email?: string;
  password?: string;
  mail_server_id?: number;
}

export class MailSender implements IMailSender {
  constructor(public id?: number,
              public email?: string,
              public password?: string,
              public mail_server_id?: number
  ) {
  }


}
