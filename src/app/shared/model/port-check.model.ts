export interface IPortCheck {
  ip_address?: any;
  port?: number;
  is_open?: boolean;
}

export class PortCheck implements IPortCheck {
  constructor(
    public ip_address?: any,
    public port?: number,
    public is_open?: boolean,
  ) {
  }
}
