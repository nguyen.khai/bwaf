import { ISsl } from "@app/shared/model/ssl.model";
import { publish } from "rxjs/operators";

export interface IUpStream {
  ip?: string;
  port?: number;
  type?: string;
  priority?: number;
  protocol?: string;
  hoverSSL?: Boolean;
  ssl?: any;
  ssl_id?: number;
  checkIpServie?: String;
  checkPortServie?: String;
}

export class UpStream implements IUpStream {
  constructor(
    public ip?: any,
    public port?: any,
    public type?: any,
    public protocol?: any,
    public ssl?: any,
    public ssl_id?: any,
    public hoverSSL?: any,
    public checkIpServie?: any,
    public checkPortServie?: any
  ) {}
}

export interface IPortListen {
  main?: any;
  redirect?: string[];
  protocol?: string;
  ssl?: any;
}

export class PortListen implements IPortListen {
  constructor(
    public main?: string,
    public redirect?: string[],
    public protocol?: string,
    public ssl?: any
  ) {}
}

export interface IWebsite {
  id?: number;
  website_domain?: string;
  storeId?: number;
  upstream?: IUpStream[];
  cache?: number;
  listened?: IPortListen;
  active?: number;
  ssl?: ISsl;
  ssl_id?: number;
}

export class Website implements IWebsite {
  constructor(
    public id?: number,
    public website_domain?: string,
    public storeId?: number,
    public upstream?: IUpStream[],
    public cache?: number,
    public listened?: IPortListen,
    public active?: number,
    public ssl?: ISsl,
    public ssl_id?: number
  ) {
    this.website_domain = website_domain;
  }
}
