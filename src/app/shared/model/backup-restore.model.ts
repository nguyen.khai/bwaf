export interface IBackupLocal {
  file_name?: string;
  schedule?: number;
  period?: string;
  day_start?: string;
  user_password?: string;
}

export class BackupLocal implements IBackupLocal {
  constructor(
    public file_name?: string,
    public schedule?: number,
    public period?: string,
    public day_start?: string,
    public user_password?: string
  ) {
  }
}

export interface IFile {
  id?: number;
  name?: string;
}

export class File implements IFile {
  constructor(
    public id?: number,
    public name?: string
  ) {
  }
}

export interface IRestoreFile {
  id?: number;
  file_name?: string;
  file_size?: string;
  back_up_time?: string;
  account_backup?: string;
  backup_type?: string;
}

export class RestoreFile implements IRestoreFile {
  constructor(
    public id?: number,
    public file_name?: string,
    public file_size?: string,
    public back_up_time?: string,
    public account_backup?: string,
    public backup_type?: string,
  ) {
  }
}

export interface IBackupSchedule {
  id?: number;
  period_id?: number;
  file_name?: string;
  datetime?: string;
  account?: string;

}
