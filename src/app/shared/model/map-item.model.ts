export interface IDashboardMapItem {
  key?: number;
  value?: string;
}

export class DashboardMapItem implements IDashboardMapItem {
  constructor(
    public key?: number,
    public value?: string,
  ) {
  }
}
