export interface IInterface {
  "id"?: number;
  "dns"?: string[];
  "gateway_ipv4"?: string;
  "gateway_ipv6"?: string;
  "ipv4_address"?: string[];
  "ipv4_addressing_mode"?: string;
  "ipv6_address"?: string[];
  "ipv6_addressing_mode"?: string;
  "name"?: string;
  "type"?: string;
  "active"?: number;
  "status"?: number;
}

export interface IBridgesVirtualNetwork {
  "id"?: number;
  "dns"?: string[];
  interfaces: string[];
  "gateway_ipv4"?: string;
  "gateway_ipv6"?: string;
  "ipv4_address"?: string[];
  "ipv4_addressing_mode"?: string;
  "ipv6_address"?: string[];
  "ipv6_addressing_mode"?: string;
  "name"?: string;
  "type"?: string;
  "active"?: number;
  "status"?: number;
  advanced?: {
    lacp_rate?: string;
    monitor_interval?: number;
    primary_interface?: string;
  };
}

export interface IBondingVirtualNetwork {
  "id"?: number;
  "dns"?: string[];
  interfaces: string[];
  "gateway_ipv4"?: string;
  "gateway_ipv6"?: string;
  "ipv4_address"?: string[];
  "ipv4_addressing_mode"?: string;
  "ipv6_address"?: string[];
  "ipv6_addressing_mode"?: string;
  "name"?: string;
  "type"?: string;
  "active"?: number;
  "status"?: number;
  parameters: string;
  lacp_rate?: string;
  monitor_interval?: number;
  primary_interface?: string;
  advanced?: {
    lacp_rate?: string;
    monitor_interval?: number;
    primary_interface?: string;
  };
}

export class Interface implements IInterface {
  constructor(
    public dns: string[],
    public gateway_ipv4: string,
    public gateway_ipv6: string,
    public ipv4_address: string[],
    public ipv4_addressing_mode: string,
    public ipv6_address: string[],
    public ipv6_addressing_mode: string,
    public name: string,
    public type: string,
    public active: number,
    public status: number
  ) {
  }
}

export interface IServiceAccess {
  id?: number;
  port_from?: number;
  port_to?: number;
  protocol?: string[];
}

export class ServiceAccess implements IServiceAccess {
  constructor(public id?: number,
              public port_from?: number,
              public port_to?: number,
              public protocol?: string[]
  ) {
  }
}

export const ADDRESSING_MODES = [
  {label: 'Off', value: ''},
  {label: 'Static', value: 'static'},
  {label: 'DHCP', value: 'dhcp'},
];

export const PROTOCOLS = [{label: 'TCP', value: 'tcp'},
  {label: 'UDP', value: 'udp'},
  {label: 'SCTP', value: 'sctp'}];

export const NETWORK_TYPES = [
  {label: 'WAN', value: 'WAN'},
  {label: 'LAN', value: 'LAN'}
];

export const IP_TYPES = [
  {label: 'IPv4', value: 4},
  {label: 'IPv6', value: 6}
];

export const VIRTUAL_NETWORK_TYPES = [
  {label: 'Bonding', value: 'Bonding'},
  {label: 'Bridges', value: 'Bridges'}
];

export const PARAMETER_MODES = [
  {label: '802.3ad', value: '802.3ad'},
  {label: 'Active backup', value: 'active-backup'},
  {label: 'Balance Round Robin', value: 'balance-rr'},
  {label: 'Balance XOR', value: 'balance-xor'}
];

export const LACP_RATES = [
  {label: 'Slow', value: 'slow'},
  {label: 'Fast', value: 'fast'}
];
