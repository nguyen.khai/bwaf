export interface ITracerouteCheck {
  ip_address?: any;
  max_ttl?: number;
}

export class TracerouteCheck implements ITracerouteCheck {
  constructor(
    public ip_address?: any,
    public max_ttl?: number,
  ) {
  }
}

export interface ITraceroute {
  traceroute_packet?: ITraceroutePacket[];
}

export class Traceroute implements ITraceroute {
  constructor(
    public traceroute_packet?: ITraceroutePacket[],
  ) {
  }
}

export interface ITraceroutePacket {
  seq?: any;
  time?: string[];
  reply_from?: any;
}

export class TraceroutePacket implements ITraceroutePacket {
  constructor(
    public seq?: any,
    public time?: string[],
    public reply_from?: any,
  ) {
  }
}
