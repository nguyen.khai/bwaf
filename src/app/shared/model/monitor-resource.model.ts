
export interface IMonitorResource {
    cpu?: number;
    disk?: number;
    ram?: number;
    datetime?: string;
}

export class MonitorResource implements IMonitorResource {
  constructor(
    public cpu?: number,
    public disk?: number,
    public ram?: number,
    public datetime?: string
  ) {
  }
}

