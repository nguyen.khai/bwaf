export interface IDdosUri {
  uri_id?: number;
  uri?: string;
}

export class DdosUri implements IDdosUri {
  constructor(
    public uri_id?: number,
    public uri?: string
  ) {
  }
}
