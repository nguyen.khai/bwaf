
export interface ISystemTime {
  timezone?: string;
  timezoneStr?: string;
  server?: string;
  datetime?: string;
}

export class SystemTime implements ISystemTime {
  constructor(
    public timezoneStr: string,
    public timezone: string,
    public server: string,
    public datetime: string
  ) {
  }
}

