export { MultiSelectComponent } from './multiselect.component';
export { NgMultiSelectModule } from './ng-multi-select.module';
export { IDropdownSettings } from './multiselect.model';
