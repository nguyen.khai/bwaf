import {
  AfterContentInit,
  ContentChild, ContentChildren,
  Directive,
  ElementRef,
  EventEmitter, forwardRef, Host,
  HostListener,
  Input, OnChanges,
  Output, QueryList, SimpleChanges
} from '@angular/core';
import {OrderParam} from "@app/shared/constants/common.model";
// import {SortByDirective} from "./sort-by.directive";
import {IconDefinition} from '@fortawesome/fontawesome-common-types';
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {faSort, faSortDown, faSortUp} from '@fortawesome/free-solid-svg-icons';

@Directive({
  selector: '[appSort]'
})
export class SortDirective {

  @Input()
  orderBy: string;

  @Input()
  orderType: boolean;

  @Output() orderChange = new EventEmitter<OrderParam>();

  @ContentChildren(forwardRef(() => SortByDirective))
  sortByDirectives: QueryList<SortByDirective>;

  @Input()
  disabled: boolean = false;

  constructor() {

  }

  load() {
    const orderParam: OrderParam = new OrderParam(this.orderBy, this.orderType);
    this.orderChange.emit(orderParam);
    if (this.sortByDirectives) {
      this.sortByDirectives.forEach(item => {
        item.ngOnChanges();
      });
    }
  }
}

@Directive({
  selector: '[appSortBy]'
})
export class SortByDirective implements AfterContentInit, OnChanges {

  @Input('appSortBy')
  field: string;
  sortIcon: IconDefinition;
  sortAscIcon: IconDefinition;
  sortDescIcon: IconDefinition;
  @ContentChild(FaIconComponent, {static: true}) iconComponent: FaIconComponent;

  constructor(
    Element: ElementRef,
    @Host() private sort: SortDirective
  ) {
    this.sortIcon = faSort;
    this.sortAscIcon = faSortUp;
    this.sortDescIcon = faSortDown;
  }

  ngOnChanges(): void {
    if (this.sort.orderBy !== this.field) {
      this.updateIconDefinition(this.iconComponent, this.sortIcon);
    }
  }

  ngAfterContentInit(): void {
    if (this.sort.orderBy && this.sort.orderType && this.field) {
      this.updateIconDefinition(this.iconComponent, this.sort.orderType ? this.sortAscIcon : this.sortDescIcon);
    } else {
      this.updateIconDefinition(this.iconComponent, this.sortIcon);
    }
  }

  @HostListener('click') onClick() {
    if (!this.sort.disabled) {
      this.sort.orderBy = this.field;
      this.sort.orderType = !this.sort.orderType;
      this.updateIconDefinition(this.iconComponent, this.sort.orderType ? this.sortAscIcon : this.sortDescIcon);
      const orderParam: OrderParam = new OrderParam(this.sort.orderBy, this.sort.orderType);
      this.sort.load();
    }
  }

  private updateIconDefinition(iconComponent: FaIconComponent, icon: IconDefinition) {
    if (iconComponent) {
      iconComponent.iconProp = icon;
      iconComponent.ngOnChanges({});
    }
  }
}

