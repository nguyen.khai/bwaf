import {AfterContentInit, ContentChild, Directive, ElementRef, Host, HostListener, Input, OnChanges} from '@angular/core';

import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {faSort, faSortDown, faSortUp, IconDefinition} from '@fortawesome/free-solid-svg-icons';
import {OrderParam} from '@app/shared/constants/common.model';
import {SortDirective} from '@app/shared/util/directive/sort.directive';

