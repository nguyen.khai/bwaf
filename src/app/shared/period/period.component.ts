import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as moment from 'moment';
import { DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import {MomentDateTimeAdapter} from "ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class";

export const MY_CUSTOM_FORMATS = {
  fullPickerInput: 'YYYY-MM-DD HH:mm:ss',
  parseInput: 'YYYY-MM-DD HH:mm:ss',
  datePickerInput: 'YYYY-MM-DD HH:mm:ss',
  timePickerInput: 'LT',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY'
};

@Component({
  selector: 'app-period',
  templateUrl: './period.component.html',
  styleUrls: ['./period.component.scss'],
  providers: [
    { provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE] },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
  ]
})
export class PeriodComponent implements OnInit {
  @Input() selectTimeOption = [
    {type: 'minute', value: 5, label: '5 Minutes'},
    {type: 'minute', value: 30, label: '30 Minutes'},
    {type: 'hour', value: 1, label: '1 Hour'},
    {type: 'day', value: 1, label: '1 Day'},
    {type: 'day', value: 7, label: '7 Days'},
    {type: 'month', value: 1, label: '1 Month'},
  ];
  @Input() dateRange = null;
  @Output() dateRangeChange: EventEmitter<any> = new EventEmitter<any>();
  @Input() selectTime = this.selectTimeOption[0];
  @Output() selectTimeChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() changeValue: EventEmitter<any> = new EventEmitter<any>();
  dateFrom;
  dateTo;

  isOpen = false;

  selectTimeInterval(selectedTime) {
    this.selectTime = selectedTime;
    this.dateRange = null;
    this.dateFrom = null;
    this.dateTo = null;
    this.selectTimeChange.emit(selectedTime);
    this.changeValue.emit(this.selectTime);
  }

  selectTimeRange() {
    if (this.dateRange != null) {
      this.dateFrom = this.dateRange[0].format('YYYY-MM-DD HH:mm:ss');
      this.dateTo = this.dateRange[1].format('YYYY-MM-DD HH:mm:ss');

      this.selectTime = {
        type: 'between',
        value: this.dateRange,
        label: `${this.dateFrom} ~ ${this.dateTo}`
      }
      this.changeValue.emit(this.selectTime);
    }

  }

  ngOnInit(): void {

  }

}
