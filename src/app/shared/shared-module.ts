import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {NgbDateAdapter} from '@ng-bootstrap/ng-bootstrap';

import {NgbDateMomentAdapter} from '@app/shared/util/datepicker-adapter';
import {SharedLibsModule} from './shared-libs.module';
import {SharedCommonModule} from './shared-common.module';
import {LoginComponent} from './login/login.component';
import {HasAnyAuthorityDirective} from './auth/has-any-authority.directive';
import {TranslateModule} from '@ngx-translate/core';
import {ToastrCustomService} from '@app/shared/toastr-custom-service';
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE,
  OwlDateTimeModule,
  OwlNativeDateTimeModule
} from 'ng-pick-datetime';
import {MomentDateTimeAdapter} from 'ng-pick-datetime-moment';
import {OWL_COMMON_DATE_FORMAT} from '@app/shared/constants/common.model';
import {QuillModule} from 'ngx-quill';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {SortDirective, SortByDirective} from '@app/shared/util/directive/sort.directive';
// import {SortByDirective} from '@app/shared/util/directive/sort-by.directive';
import {PeriodComponent} from '@app/shared/period/period.component';
import {NgMutifilterComponent} from "@app/shared/ng-mutifilter/ng-mutifilter.component";
import {PieChartComponent} from "@app/shared/pie-chart/pie-chart.component";
import {NgWorldMapComponent} from "@app/shared/ng-world-map/ng-world-map.component";

const quillModules = {
  toolbar: [
    [{header: [1, 2, 3, 4, 5, 6, false]}],
    [{font: []}],
    ['bold', 'italic', 'underline', 'strike'], // toggled buttons
    [{list: 'ordered'}, {list: 'bullet'}],
    [{indent: '-1'}, {indent: '+1'}, {align: []}], // outdent/indent
    [{color: []}, {background: []}], // dropdown with defaults from theme

    [{script: 'sub'}, {script: 'super'}], // superscript/subscript
    ['blockquote', 'code-block', {direction: 'rtl'}],
    ['link', 'image', 'video'],
    ['clean'] // link and image, video // remove formatting button
  ]
};

@NgModule({
  imports: [
    SharedLibsModule,
    SharedCommonModule,
    TranslateModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    QuillModule.forRoot({
      modules: quillModules
    }),
    BsDropdownModule
  ],
  declarations: [LoginComponent, HasAnyAuthorityDirective, SortDirective, SortByDirective, PeriodComponent,
    PieChartComponent, NgWorldMapComponent,
    NgMutifilterComponent],
  providers: [
    {provide: NgbDateAdapter, useClass: NgbDateMomentAdapter},
    ToastrCustomService,
    LoginComponent,
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: OWL_COMMON_DATE_FORMAT}
  ],
  entryComponents: [LoginComponent],
  exports: [
    SharedCommonModule,
    LoginComponent,
    HasAnyAuthorityDirective,
    TranslateModule,
    SharedLibsModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    QuillModule,
    BsDropdownModule,
    SortDirective,
    SortByDirective,
    PeriodComponent,
    PieChartComponent,
    NgMutifilterComponent,
    NgWorldMapComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule
    };
  }
}
