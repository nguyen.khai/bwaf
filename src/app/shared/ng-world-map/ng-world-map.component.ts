import {Component, Input, NgZone, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import * as am4maps from "@amcharts/amcharts4/maps";
import {COLOR_RED, COUNTRIES, COUNTRY_CENTER_GEO_POINTS} from "@app/shared/constants/monitor.model";
import * as am4core from "@amcharts/amcharts4/core";
import am4geodata_worldHigh from "@amcharts/amcharts4-geodata/worldHigh";

@Component({
  selector: 'ng-world-map',
  templateUrl: './ng-world-map.component.html',
  styleUrls: ['./ng-world-map.component.scss']
})
export class NgWorldMapComponent implements OnInit, OnChanges {

  private chart: am4maps.MapChart;

  @Input()
  id: string = "chart";
  @Input()
  data: Array<any> = [];
  dataFake: Array<any> = this.generateData();
  includeCountries: Array<any> = [];
  excludedCountries: Array<any> = ["AQ"];
  zoomlv: number;
  dataSeries;
  polygonSeries;

  constructor(
    private zone: NgZone
  ) {
    this.zoomlv = 1;
  }

  ngOnInit() {

  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.dataSeries && this.polygonSeries && this.data) {
      this.resetData();
    }
  }

  ngAfterViewInit() {
    this.dataFake.forEach(value => {
      this.includeCountries.push(value.id);
      this.excludedCountries.push(value.id);
    });
    console.log('ngAfterViewInit');
    this.zone.runOutsideAngular(() => {
      this.chart = am4core.create(this.id, am4maps.MapChart);
      this.createChart(this.chart);
      this.setDataSeries();
      // this.createMiniMap();
      this.resetData();

    });
  }

  converData() {
    const rs = [];
    if (this.data.length <= 0) {
      return rs;
    }
    this.data.forEach(value => {
      if (COUNTRIES.hasOwnProperty(value.key)) {
        // this.polygonSeries.mapPolygons.template.tooltipText = "{name} " + item.;
        const country = COUNTRIES[value.key];
        const tmp = {
          id: value.key,
          // fill: am4core.color(COLOR_RED[index++]), //this.chart.colors.getIndex(COUNTRIES[country.continent_code])
          map: country.maps[0],
          value: `- ${value.value}`
        };
        rs.push(tmp);
      }
    });
    return rs;
  }

  generateData(): Array<any> {
    const keys = Object.keys(COUNTRIES);
    const rs = [];

    for (let i = 0; i < 10; ++i) {
      const random = Math.floor(Math.random() * keys.length);
      const country = COUNTRIES[`${keys[random]}`];
      rs.push({
        id: keys[random],
        fill: am4core.color(COLOR_RED[i]), //this.chart.colors.getIndex(COUNTRIES[country.continent_code])
        map: country.maps[0],
        value: `- ahihi`
      });
    }
    return rs;
  }

  createChart(chart: am4maps.MapChart) {
    chart.geodata = am4geodata_worldHigh;
    chart.projection = new am4maps.projections.Mercator();
    // Set map definition
    chart.geodata = am4geodata_worldHigh;

// Set projection
    chart.projection = new am4maps.projections.Mercator();

// Export
    chart.exporting.menu = new am4core.ExportMenu();

// Zoom control
    chart.zoomControl = new am4maps.ZoomControl();

    const homeButton = new am4core.Button();
    homeButton.events.on("hit", () => {
      chart.goHome();
      this.polygonSeries.exclude.push("VN");
      this.dataSeries.include.push("VN");
    });
    homeButton.icon = new am4core.Sprite();
    homeButton.padding(7, 5, 7, 5);
    homeButton.width = 30;
    homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
    homeButton.marginBottom = 10;
    homeButton.parent = chart.zoomControl;
    homeButton.insertBefore(chart.zoomControl.plusButton);

// Center on the groups by default
    chart.homeZoomLevel = this.zoomlv;
    chart.homeGeoPoint = {longitude: 107.16666666, latitude: 16.83333333};

    // Polygon series
    this.polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());
    // this.polygonSeries.data = this.dataView;
    this.polygonSeries.exclude = ["AQ"];
    this.polygonSeries.useGeodata = true;
    this.polygonSeries.nonScalingStroke = true;
    this.polygonSeries.strokeOpacity = 0.5;
    const polygonTemplate = this.polygonSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name}";
    polygonTemplate.nonScalingStroke = true;
    polygonTemplate.propertyFields.fill = "fill";
    polygonTemplate.fill = am4core.color("#090909");
    const hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color("#808080");

  }

  setDataSeries() {
    this.dataSeries = this.chart.series.push(new am4maps.MapPolygonSeries());
    this.dataSeries.exclude = ["AQ"];
    this.dataSeries.useGeodata = true;
    this.dataSeries.nonScalingStroke = true;
    this.dataSeries.strokeOpacity = 0.5;
    const polygonTemplate = this.dataSeries.mapPolygons.template;
    polygonTemplate.tooltipText = "{name} {value}";
    polygonTemplate.nonScalingStroke = true;
    polygonTemplate.propertyFields.fill = "fill";
    polygonTemplate.fill = am4core.color("#d30000");
    const hs = polygonTemplate.states.create("hover");
    hs.properties.fill = am4core.color("#4f6580");

    // Small map

  }

  createMiniMap() {
    this.chart.smallMap = new am4maps.SmallMap();
    this.chart.smallMap.align = "right";
    this.chart.smallMap.valign = "top";
    this.chart.smallMap.series.push(this.polygonSeries);
    this.chart.smallMap.series.push(this.dataSeries);
  }


  resetData() {
    const dataView: Array<any> = this.converData();
    this.includeCountries = [];
    this.excludedCountries = ["AQ"];
    if (dataView.length > 0) {
      dataView.forEach(value => {
        this.includeCountries.push(value.id);
        this.excludedCountries.push(value.id);
      });
    }
    this.polygonSeries.exclude = this.excludedCountries;
    this.dataSeries.include = this.includeCountries;
    this.dataSeries.data = dataView;
    this.dataSeries.validateData();
    this.polygonSeries.validateData();

  }

  testClick() {
  }


}
