import {Component, Input, NgZone, OnChanges, OnInit} from "@angular/core";
import {DateTimeAdapter, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE} from "ng-pick-datetime";
import {MomentDateTimeAdapter} from "ng-pick-datetime/date-time/adapter/moment-adapter/moment-date-time-adapter.class";
import {MY_CUSTOM_FORMATS} from "@app/shared/period/period.component";
/* Imports */
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import * as am4maps from "@amcharts/amcharts4/maps";

am4core.useTheme(am4themes_animated);

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss'],
  providers: [
    {provide: DateTimeAdapter, useClass: MomentDateTimeAdapter, deps: [OWL_DATE_TIME_LOCALE]},
    {provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS}
  ]
})
export class PieChartComponent implements OnInit, OnChanges {

  private chart;
  private selected;
  @Input()
  private data;
  private pieSeries;
  private types = [];

  constructor(
    private zone: NgZone
  ) {
  }

  ngOnInit(): void {
    // this.chart = am4core.create("chartdiv", am4charts.PieChart);
    // this.loadAll();
  }

  ngOnChanges() {
    console.log(this.data);
    this.chart.data = this.data;
  }

  /* Chart code */
// Themes begin

// Themes end

// Create chart instance

  ngAfterViewInit() {
    console.log(this.data);
    console.log('ngAfterViewInit');
    this.types = this.setData();
    this.zone.runOutsideAngular(() => {
      this.chart = am4core.create("chartdiv", am4charts.PieChart);
      this.chart.data = this.data;
      this.pieSeries = this.chart.series.push(new am4charts.PieSeries());
      this.pieSeries.dataFields.value = "value";
      this.pieSeries.dataFields.category = "label";
    });
  }

  loadAll() {
    // // this.data = this.generateChartData();
    // this.types = this.setData();
    // this.setChart();
  }

  setData() {
    return [{
      type: "Fossil Energy",
      percent: 70,
      color: am4core.color("#ff0000"),
      subs: [{
        type: "Oil",
        percent: 15
      }, {
        type: "Coal",
        percent: 35
      }, {
        type: "Nuclear",
        percent: 20
      }]
    }, {
      type: "Green Energy",
      percent: 30,
      color: am4core.color("#ff0000"),
      subs: [{
        type: "Hydro",
        percent: 15
      }, {
        type: "Wind",
        percent: 10
      }, {
        type: "Other",
        percent: 5
      }]
    }];
  }

// Set data


// Add data

  setChart() {
    // Add and configure Series
    let pieSeries = this.chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "percent";
    pieSeries.dataFields.category = "type";
    pieSeries.slices.template.propertyFields.fill = "color";
    pieSeries.slices.template.propertyFields.isActive = "pulled";
    pieSeries.slices.template.strokeWidth = 0;
    pieSeries.slices.template.events.on("hit", function (event) {
        if (event.target.dataItem.dataContext.id != undefined) {
          this.selected = event.target.dataItem.dataContext.id;
        } else {
          this.selected = undefined;
        }
        this.chart.data = this.generateChartData();
      }
    );
  }


  generateChartData() {
    let chartData = [];
    for (let i = 0; i < this.types.length; i++) {
      if (i == this.selected) {
        for (let x = 0; x < this.types[i].subs.length; x++) {
          chartData.push({
            type: this.types[i].subs[x].type,
            percent: this.types[i].subs[x].percent,
            color: this.types[i].color,
            pulled: true
          });
        }
      } else {
        chartData.push({
          type: this.types[i].type,
          percent: this.types[i].percent,
          color: this.types[i].color,
          id: i
        });
      }
    }
    return chartData;
  }


}
