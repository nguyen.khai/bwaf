import { PeriodMonitor } from "@app/shared/constants/monitor.model";
import { IPaging, Paging } from "@app/shared/model/base-respone.model";

export const PAGING_PER_PAGE = [
  { label: 5, value: 5 },
  { label: 10, value: 10 },
  { label: 20, value: 20 },
  { label: 50, value: 50 },
  { label: 100, value: 100 },
];

export const OWL_COMMON_DATE_FORMAT = {
  fullPickerInput: "DD/MM/YYYY",
  parseInput: "DD/MM/YYYY",
  datePickerInput: "DD/MM/YYYY",
  timePickerInput: "LT",
  monthYearLabel: "MMM YYYY",
  dateA11yLabel: "LL",
  monthYearA11yLabel: "MMMM YYYY",
};

export const OWL_COMMON_DATETIME_FORMAT = {
  fullPickerInput: "DD-MM-YYYY HH:mm:ss",
  parseInput: "DD-MM-YYYY HH:mm:ss",
  datePickerInput: "DD-MM-YYYY HH:mm:ss",
  timePickerInput: "LT",
  monthYearLabel: "MMM YYYY",
  dateA11yLabel: "LL",
  monthYearA11yLabel: "MMMM YYYY",
};

export interface IOrderParam {
  orderBy?: string;
  orderType?: boolean;
}

export class OrderParam implements IOrderParam {
  constructor(public orderBy?: string, public orderType?: boolean) {}

  public getOrderType(): string {
    return this.orderType ? "asc" : "desc";
  }
}

export function getParameterPaging(paging: IPaging): IPaging {
  paging.offset = paging.limit * (paging.page - 1);
  return paging;
}

export class SelectItem {
  label?: string;
  value?: string;
}

export const PERIOD_BACKUP: SelectItem[] = [
  { label: "Daily", value: "1" },
  { label: "Weekly", value: "2" },
  { label: "Monthly", value: "3" },
  { label: "Yearly", value: "4" },
];

export const MODE: SelectItem[] = [
  { label: "compressed", value: "compressed" },
  { label: "uncompressed", value: "uncompressed" },
];

export const MAX_INT: number = Number.MAX_SAFE_INTEGER;

export const REQUEST_INTERVAL: number = 10 * 1000;

export const NETMASK_V4 = [
  { value: 32, label: "255.255.255.255 - /32" },
  { value: 31, label: "255.255.255.254 - /31" },
  { value: 30, label: "255.255.255.252 - /30" },
  { value: 29, label: "255.255.255.248 - /29" },
  { value: 28, label: "255.255.255.240 - /28" },
  { value: 27, label: "255.255.255.224 - /27" },
  { value: 26, label: "255.255.255.192 - /26" },
  { value: 25, label: "255.255.255.128 - /25" },
  { value: 24, label: "255.255.255.0 - /24" },
  { value: 23, label: "255.255.254.0 - /23" },
  { value: 22, label: "255.255.252.0 - /22" },
  { value: 21, label: "255.255.248.0 - /21" },
  { value: 20, label: "255.255.240.0 - /20" },
  { value: 19, label: "255.255.224.0 - /19" },
  { value: 18, label: "255.255.192.0 - /18" },
  { value: 17, label: "255.255.128.0 - /17" },
  { value: 16, label: "255.255.0.0 - /16" },
  { value: 15, label: " 255.254.0.0 - /15" },
  { value: 14, label: " 255.252.0.0 - /14" },
  { value: 13, label: "255.248.0.0 - /13" },
  { value: 12, label: "255.240.0.0 - /12" },
  { value: 11, label: " 255.224.0 0 - /11" },
  { value: 10, label: " 255.192.0.0 - /10" },
  { value: 9, label: " 255.128.0.0 - /9" },
  { value: 8, label: " 255.0.0.0 - /8" },
  { value: 7, label: "254.0.0.0 - /7" },
  { value: 6, label: " 252.0.0.0 - /6" },
  { value: 5, label: "  248.0.0.0 - /5" },
  { value: 4, label: "240.0.0.0 - /4" },
  { value: 3, label: "224.0.0.0 - /3" },
  { value: 2, label: "192.0.0.0 - /2" },
  { value: 1, label: "128.0.0.0 - /1" },
  { value: 0, label: " 0.0.0.0 - /0" },
];
