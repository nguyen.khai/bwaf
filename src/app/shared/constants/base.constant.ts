export const PAGING_LIMITS: number[] = [5, 10, 20, 50, 100];

export enum canEdit_STATUS {
  YES = 1,
  NO = 0
}

