export interface IItem {
  value: string;
  field: IField;
  operator: IOperator;
}

export interface IOperator {
  value: string;
  label: string;
}

export interface IField {
  value: string;
  label: string;
}

export const OPERATORS: Array<IOperator> = [
  {value: '=', label : 'exists'},
  {value: '=!', label : 'not exists'}
]
