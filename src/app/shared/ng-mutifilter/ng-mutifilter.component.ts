import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IField, IItem, IOperator, OPERATORS} from "@app/shared/ng-mutifilter/ng-mutifilter.model";

@Component({
  selector: 'app-ng-mutifilter',
  templateUrl: './ng-mutifilter.component.html',
  styleUrls: ['./ng-mutifilter.component.scss']
})
export class NgMutifilterComponent implements OnInit {

  constructor() {
  }

  @Input() fields: Array<IField> = [];

  @Output()  filter: EventEmitter<any> = new EventEmitter<any>();
  items: Array<IItem> = [];
  operators: Array<IOperator> = OPERATORS;

  ngOnInit() {
  }


  selectField(field) {
    this.items.push({
      field: field,
      operator: {label: 'exit', value: '!='},
      value: null
    });
  }

  onItemClick($event, item) {
    this.items = this.items.filter(value => {
      return item !== value;
    });
  }

  addItem() {

    this.items.push({
      field: this.fields[0],
      operator: this.operators[0],
      value: ''
    });
  }

  removeItem(index) {
    this.items.splice(index, 1);
  }

  // trackByFn(index, item) {
  //   return item.id;
  // }
  onFilter() {
    let rs: string = '';
    this.items.forEach(value => {
      rs += `${value.field.value}${value.operator.value}${value.value}&`;
    })
    if (rs != '') {
      rs = rs.substring(0, rs.length - 1);
    }
    console.log(rs);
    this.filter.emit(rs);
  }
}
